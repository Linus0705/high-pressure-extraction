# Holdup_korreliert

Holdup Fitting with Correlation of Kumar & Hartland
***
## Instruction Manual:

1. Create ```Holdup_Ergebnisse_Korrelation_KumarHartland.csv``` from your Excelfile and watch out
that you use the exact same scheme

2. Save ```Holdup_Ergebnisse_Korrelation_KumarHartland.csv``` in ```01 - Input```

3. Start ```main.py```

4. In ```02 - Output``` two new files were created:
  * ```Holdup_Fitting_Korrelation_KumarHartland_Parameter.csv```
  * ```Holdup_Fitting_Korrelation_KumarHartland_Evaluation.csv```
    * The first file consists of the fitted parameter.
    * The second file gives the prediction errors for each measurement.