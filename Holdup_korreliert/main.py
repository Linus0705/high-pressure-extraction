# Import basic packages
import logging
import os
import sys

# Import modules
import config
import paramfit

# Set up -- Logging
logging.basicConfig(format='%(asctime)s :%(name)s:%(levelname)s:  %(message)s',
                    datefmt='%m/%d/%Y %I:%M:%S %p',
                    level=logging.DEBUG,
                    handlers=[
                        logging.FileHandler(filename=f'{os.path.basename(__file__)}.log', mode='w'),
                        logging.StreamHandler(sys.stdout)
                    ]
                    )
LOGGER = logging.getLogger(__name__)

LOGGER.debug("Starting main.py ...")

paramfit.param_fit()

LOGGER.debug("... Finished main.py")
