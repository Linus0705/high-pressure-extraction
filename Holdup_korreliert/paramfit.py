import logging
import csv

import config
import pandas
import numpy as np
import scipy
import scipy.optimize
import warnings




LOGGER = logging.getLogger(__name__)
LOGGER.debug(f"__name__: {__name__}")


def load_csv():
    LOGGER.debug("Starting load_csv ...")

    # create filepath
    csv_file = f"{config.PATH}/01 - Input/Holdup_Ergebnisse_Korrelation_KumarHartland.csv"
    LOGGER.debug(csv_file)

    # read csv and log
    df_input = pandas.read_csv(csv_file, delimiter=';')
    LOGGER.debug(f"df_input: \n{df_input.to_string()}")

    LOGGER.debug("... Finished load_csv")
    return df_input

def param_fit():
    LOGGER.debug("Starting param_fit ...")

    # load input csv for the holdup correlation
    df = load_csv()

    # set predictors/independent variables and holdup as data to be predicted
    ydata = df['Holdup_exp [%]'].to_numpy()
    xdata = df[['Temperatur [K]',
                'Druck [bar]',
                'm_c [kg/h]',
                'm_d [kg/h]',
                'Volumenspezifische Oberflaeche a_p [m^-1]',
                'epsilon [-]',
                'Dichte Wasser [kg/m3]',
                'Viskositaet Wasser [Pas]',
                'Dichte CO2 [kg/m3]',
                'Viskositaet CO2 [Pas]',
                'Dichtedifferenz [kg/m3]',
                'Kontibelastung [kg/m2s]',
                'Dispersbelastung [kg/m2s]',
                'Stroemungsgeschwindigkeit_konti_berechnet [m/s]',
                'Stroemungsgeschwindigkeit dispers_berechnet [m/s]']]
    xdata = xdata.assign(d_roh=xdata['Dichtedifferenz [kg/m3]'])
    xdata = xdata.assign(roh_c=xdata['Dichte CO2 [kg/m3]'])
    xdata = xdata.assign(mu_c=xdata['Viskositaet CO2 [Pas]'])
    xdata = xdata.assign(mu_d=xdata['Viskositaet Wasser [Pas]'])
    xdata = xdata.assign(gamma=np.repeat(70*pow(10,-3), len(xdata)))
    xdata = xdata.assign(a_p=xdata['Volumenspezifische Oberflaeche a_p [m^-1]'])
    xdata = xdata.assign(eps=xdata['epsilon [-]'])
    xdata = xdata.assign(V_c=xdata['Stroemungsgeschwindigkeit_konti_berechnet [m/s]'])
    xdata = xdata.assign(V_d=xdata['Stroemungsgeschwindigkeit dispers_berechnet [m/s]'])
    xdata = xdata[['d_roh',
                'roh_c',
                'mu_c',
                'mu_d',
                'gamma',
                'a_p',
                'eps',
                'V_c',
                'V_d'
                ]]
    LOGGER.debug(f"xdata as pandas data frame: \n{xdata.to_string()}")
    xdata = xdata.to_numpy()
    xdata = xdata.T
    LOGGER.debug(f"xdata as numpy matrix: \n{xdata}")

    # function for genetic algorithm to minimize (sum of squared error)
    def sumOfSquaredError(parameterTuple):
        LOGGER.debug("Starting sumOfSquaredError ...")
        warnings.filterwarnings("ignore")  # do not print warnings by genetic algorithm
        val = func(xdata, *parameterTuple)
        LOGGER.debug("... Finished sumOfSquaredError")
        return np.sum((ydata - val) ** 2.0)

    def generate_initial_param():
        LOGGER.debug("Starting generate_initial_param ...")

        # set bounds for parameters
        parameterBounds = []
        parameterBounds.append([-1.0, 1.0])  # search bounds for c1
        parameterBounds.append([-1.0, 1.0])  # search bounds for c2
        parameterBounds.append([-1.0, 1.0])  # search bounds for c3
        parameterBounds.append([-1.0, 1.0])  # search bounds for c4
        parameterBounds.append([-1.0, 1.0])  # search bounds for c5
        parameterBounds.append([-1.0, 1.0])  # search bounds for c6
        parameterBounds.append([-1.0, 1.0])  # search bounds for c7
        parameterBounds.append([-1.0, 1.0])  # search bounds for c8

        # find global minimum for the initial parameters
        result = scipy.optimize.differential_evolution(sumOfSquaredError, parameterBounds, seed=3)
        LOGGER.debug(f"result: \n{result}")
        LOGGER.debug("... Finished generate_initial_param")
        return result.x

    # set the initial values p0 for parameters c1 to c8
    #init_cX = np.array([3.76, -1.11, -0.5, -0.72, 0.1, 1.03, 0.95, 0])
    init_cX = generate_initial_param()

    # parameter fitting
    #popt, pcov = scipy.optimize.curve_fit(func, xdata, ydata, p0=init_cX, method='dogbox')
    popt, pcov = scipy.optimize.curve_fit(f=func, xdata=xdata, ydata=ydata, p0=init_cX, bounds=(-1, 1))
    LOGGER.debug(f"popt: \n{popt}")
    LOGGER.debug(f"pcov: \n{pcov}")
    df_popt = pandas.DataFrame(data={'parameter': popt}, index=['C1', 'C2', 'C3', 'C4', 'C5', 'C6', 'C7', 'C8'])
    df_popt.to_csv(path_or_buf=f"{config.PATH}/02 - Output/Holdup_Fitting_Korrelation_KumarHartland_Parameter.csv",
                       sep=';')

    # evaluate the fit
    predictions = func(xdata, *popt)
    abs_error = predictions - ydata
    opt_results = pandas.DataFrame(data={'predictions': predictions, 'ydata': ydata, 'abs_error': abs_error})
    LOGGER.debug(f"opt_results: \n{opt_results}")
    opt_results.to_csv(path_or_buf=f"{config.PATH}/02 - Output/Holdup_Fitting_Korrelation_KumarHartland_Evaluation.csv",
                       sep=';')



    LOGGER.debug("... Finished param_fit")

# correlation for the hold up with xdata as array of predictors and the parameters to fit as the remaining arguments
def func(xdata, c1, c2, c3, c4, c5, c6, c7, c8):
    LOGGER.debug("Starting func ...")

    # get the predictors
    d_roh, roh_c, mu_c, mu_d, gamma, a_p, eps, V_c, V_d = xdata

    # correlation for the holdup by Kumar & Hartland
    holdup = (c1
              * pow(eps, c2)
              * pow((d_roh/roh_c), c3)
              * pow(((1/a_p)*pow(((np.square(roh_c)*9.81)/np.square(mu_c)), (1/3))), c4)
              * pow((mu_d/mu_c), c5)
              * pow((V_d*pow((roh_c/(9.81*mu_c)),(1/3))), c6)
              * np.exp(c7*V_c*pow((roh_c/(9.81*mu_c)), (1/3)))
              * pow((mu_c/(np.sqrt(roh_c*(gamma/a_p)))), c8)
              )
    LOGGER.debug(f"holdup: \n{holdup}")

    LOGGER.debug("... Finished func")
    return holdup
