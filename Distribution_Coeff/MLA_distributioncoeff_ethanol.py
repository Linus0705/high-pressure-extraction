import pandas as pd
import numpy as np
import logging

LOGGER = logging.getLogger(__name__)
LOGGER.debug(f"__name__: {__name__}")

def train_models():
    """
    Methode um Modelle neu zu trainieren und zu speichern.
    :return:
    """
    LOGGER.debug("Starting train_models ...")
    from sklearn.svm import SVR
    from sklearn.neural_network import MLPRegressor
    from sklearn.gaussian_process import GaussianProcessRegressor
    from sklearn.ensemble import RandomForestRegressor
    from sklearn.gaussian_process.kernels import RBF, Matern, DotProduct, RationalQuadratic
    from sklearn.preprocessing import MinMaxScaler, OneHotEncoder
    from sklearn.pipeline import Pipeline
    from sklearn.model_selection import train_test_split
    from sklearn.compose import ColumnTransformer
    import joblib

    def loadalldata():
        """
        Methode um alle Excelsheets im Inputordner einzuladen welche in der Methode benannt werden
        :return: ein Dataframe mit allen Daten
        """
        LOGGER.debug("Starting loadalldata ...")

        import pandas as pd

        def loaddatafromexcel(filename, columns_used, column_names, categorical_or_boolean_columns_names,
                              rows_header):
            """
            Läd Daten aus einzelner Excel Datei
            :param filename: String Name der Datei
            :param columns_used: array zB. [1,2,4] Spalten in Excel mit einzulesenden Daten, beginne mit 0 zu zählen
            :param column_names: array zB. ['v_g','massentransport'] Spalten-Namen der einzulesenden Daten
            :param categorical_or_boolean_columns_names: array zB ['massentransport']
            :param rows_header: int zB 3 oberste Zeilen in Excel mit Überschriften, Variablenname und Einheiten, beginne mit 1 zu zählenrows_header = 3  # oberste Zeilen in Excel mit Überschriften, Variablenname und Einheiten, beginne mit 1 zu zählen
            :return:
            """
            LOGGER.debug(f"Starting loaddatafromexcel {filename} ...")

            columns_used = np.array(columns_used)
            # Pfadname der Exeldatei realtiv vom Speicherort des Skriptes Backslash macht evtl unter Linux Probleme und muss durch Slash ersetzt werden
            pathname = 'Input/' + filename + '.xlsx'
            # einlesen der Daten in ein Pandas Dataframe
            df = pd.read_excel(pathname, usecols=columns_used, names=column_names, header=None, skiprows=rows_header)
            # Filtern aller Zeilen mit fehlenden Daten, bei vielen fehlenden Daten kann man über Stragetien zum Füllen der Daten nachdenken
            df = df.dropna()
            df[categorical_or_boolean_columns_names] = df[categorical_or_boolean_columns_names].astype(
                np.int64)  # Kategorische Daten bei uns sind ganzzahlig und werden als integer gespeichert, eine Unterscheidung im Skript findet auch genau darüber statt
            LOGGER.debug(f"... Finished loaddatafromexcel {filename}")
            return df

        # Läd drei verschiedene Dataframes ein
        # Hochdruck
        df_kuek = pd.concat([
        loaddatafromexcel(filename="K-datadriven", columns_used=[1, 2, 3, 4, 17],
                          column_names=["T", "p", "$x_{\\textrm{co2}}$",
                                        "$x_{\\textrm{w}}$", "$K_{\\textrm{uek}}$"
                                        ],
                          categorical_or_boolean_columns_names=[], rows_header=[0,1,2])

        ])
        df_kco2 = pd.concat([
        loaddatafromexcel(filename="K-datadriven", columns_used=[1, 2, 3, 4, 19],
                          column_names=["T", "p", "$x_{\\textrm{co2}}$",
                                        "$x_{\\textrm{w}}$", "$K_{\\textrm{co2}}$"
                                        ],
                          categorical_or_boolean_columns_names=[], rows_header=[0,1,2])

        ])
        df_kw = pd.concat([
        loaddatafromexcel(filename="K-datadriven", columns_used=[1, 2, 3, 4,5, 18],
                          column_names=["T", "p", "$x_{\\textrm{co2}}$",
                                        "$x_{\\textrm{w}}$", "$x_{\\textrm{e}}$", "$K_{\\textrm{w}}$"
                                        ],
                          categorical_or_boolean_columns_names=[], rows_header=[0,1,2])

        ])

        # resettet Index, um doppelt vorkommende Indices zu vermeiden
        df_kuek = df_kuek.reset_index(drop=True)
        df_kco2 = df_kco2.reset_index(drop=True)
        df_kw = df_kw.reset_index(drop=True)
        print('Alle Daten eingelesen')
        LOGGER.debug("... Finished loadalldata")
        return df_kuek, df_kco2, df_kw

    def train(mla, df, changeoutact):
        LOGGER.debug("Starting train ...")
        def splitfeaturetarget(df, targetname):
            """
            Methode um Feature und Target-Dataframe zu erstellen
            :param df: Dataframe mit Features und Target
            :return: x, y Dataframes
            """
            import numpy as np
            y = np.array(df[targetname]).reshape(-1, 1).astype(np.float32)
            x = df.drop(columns=[targetname])
            return x, y

        x, y = splitfeaturetarget(df, targetname=targetname)
        categorical_features = x.columns[x.dtypes.apply(lambda c: np.issubdtype(c, np.int64))].tolist()
        numeric_features = x.columns[x.dtypes.apply(lambda c: np.issubdtype(c, np.float64))].tolist()

        categorical_transformer = OneHotEncoder(drop='first', categories='auto')
        numeric_transformer = MinMaxScaler()

        preprocessor = ColumnTransformer(transformers=[
            ('num', numeric_transformer, numeric_features),
            ('cat', categorical_transformer, categorical_features)])

        pipeline = Pipeline([
            ('preprocessor', preprocessor),
            ('MLA', mla)
        ])
        pipeline.fit(x, y.ravel())
        if changeoutact:
            out_act = pipeline._final_estimator.__getattribute__('activation')
            out_act = 'relu'
            pipeline._final_estimator.__setattr__('out_activation_', out_act)

        LOGGER.debug("... Finished train")
        return pipeline

    # Zielgröße
    targetname = "$K_{\\textrm{uek}}$"

    # Lädt Daten ein
    df_kuek, df_kco2, df_kw = loadalldata()
    df_kuek_train, df_kuek_test = train_test_split(df_kuek, test_size=0.1, shuffle=True)
    df_co2_train, df_co2_test = train_test_split(df_kco2, test_size=0.1, shuffle=True)
    df_w_train, df_w_test = train_test_split(df_kw, test_size=0.1, shuffle=True)

    # Definiert Modelle
    mla_K_uek = MLPRegressor(activation='tanh', alpha=1e-05, hidden_layer_sizes=(25,25), max_iter=15000, solver='lbfgs', tol=1e-08)
    #mla_K_uek = GaussianProcessRegressor(alpha=0.001, kernel=1**2*Matern(length_scale=1, nu=1.5), n_restarts_optimizer=5, normalize_y=False)
    # ohne binär mla_K_co2 = MLPRegressor(activation='relu', alpha=0.001, hidden_layer_sizes=(30,30), max_iter=15000, solver='lbfgs', tol=1e-08)
    mla_K_co2 = RandomForestRegressor(bootstrap=True, criterion='mse', max_depth=None, max_features=None, min_samples_split=2, n_estimators=1000)
    mla_K_w = MLPRegressor(activation='relu', alpha=1e-03, hidden_layer_sizes=(30,15), max_iter=15000, solver='lbfgs', tol=1e-08)
    #mla_K_w = MLPRegressor(activation='tanh', alpha=1e-06, hidden_layer_sizes=(40,40), max_iter=15000, solver='lbfgs', tol=1e-08)

    # Trainiert Modelle
    trained_model_K_uek = train(mla_K_uek, df_kuek, False)
    trained_model_K_uekohneTest = train(mla_K_uek, df_kuek_train, False)

    # Zielgröße ändern
    targetname = "$K_{\\textrm{co2}}$"
    trained_model_K_co2 = train(mla_K_co2, df_kco2, False)
    trained_model_K_co2ohneTest = train(mla_K_co2, df_co2_train, False)

    # Zielgröße ändern
    targetname = "$K_{\\textrm{w}}$"
    trained_model_K_w = train(mla_K_w, df_kw, False)
    trained_model_K_wohnetest = train(mla_K_w, df_w_train, False)


    # Speichert Modelle
    LOGGER.debug("Dump Models ...")
    joblib.dump(trained_model_K_uek, 'pkl-Dateien/MLA_distributioncoeff_Kuek.pkl')
    joblib.dump(trained_model_K_co2, 'pkl-Dateien/MLA_distributioncoeff_Kco2.pkl')
    joblib.dump(trained_model_K_w, 'pkl-Dateien/MLA_distributioncoeff_Kw.pkl')
    joblib.dump(trained_model_K_uekohneTest, 'pkl-Dateien/MLA_distributioncoeff_Kuek_ohneTest.pkl')
    joblib.dump(trained_model_K_co2ohneTest, 'pkl-Dateien/MLA_distributioncoeff_Kco2ohneTest.pkl')
    joblib.dump(trained_model_K_wohnetest, 'pkl-Dateien/MLA_distributioncoeff_KwohneTest.pkl')
    LOGGER.debug("... Dumped model pkl-Dateien/MLA_distributioncoeff_Kuek.pkl")
    LOGGER.debug("... Dumped model pkl-Dateien/MLA_distributioncoeff_Kco2.pkl")
    LOGGER.debug("... Dumped model pkl-Dateien/MLA_distributioncoeff_Kw.pkl")
    LOGGER.debug("... Dumped model pkl-Dateien/MLA_distributioncoeff_Kuek_ohneTest.pkl")
    LOGGER.debug("... Dumped model pkl-Dateien/MLA_distributioncoeff_Kco2ohneTest.pkl")
    LOGGER.debug("... Dumped model pkl-Dateien/MLA_distributioncoeff_KwohneTest.pkl")

    LOGGER.debug("... Finished train_models")

def make_distributioncoeff_predictions_hp(T, p, x_co2, x_w, x_ethanol):
    """
    Methode um einzelnen Punkt abzufragen
    :param rho_c: Dichte der kontinuierlichen Phase [kg/m³]
    :param rho_d: Dichte der dispersen Phase [kg/m³]
    :param eta_c: dynamische Viskosität der kontinuierlichen Phase [Pa*s]
    :param eta_d: dynamische Viskosität der dispersen Phase [Pa*s]
    :param x_co2: Massenanteil CO2 [g/g]
    :param x_w: Massenanteil Wasser [g/g]

    :return: K_uek, K_co2, K_w: massenbezogene Verteilungeskoeff
    """
    LOGGER.debug("Starting make_distributioncoeff_predictions_hp ...")
    import joblib

    # läd modell
    pipeline_uek = joblib.load('Distribution_Coeff/pkl-Dateien/MLA_distributioncoeff_Kuek.pkl')
    pipeline_co2 = joblib.load('Distribution_Coeff/pkl-Dateien/MLA_distributioncoeff_Kco2.pkl')
    pipeline_w = joblib.load('Distribution_Coeff/pkl-Dateien/MLA_distributioncoeff_Kw.pkl')
    LOGGER.debug(".. Loaded model Distribution_Coeff/pkl-Dateien/MLA_distributioncoeff_Kuek.pkl")
    LOGGER.debug(".. Loaded model Distribution_Coeff/pkl-Dateien/MLA_distributioncoeff_Kco2.pkl")
    LOGGER.debug(".. Loaded model Distribution_Coeff/pkl-Dateien/MLA_distributioncoeff_Kw.pkl")
    # erstellt abfragepunkt
    X = pd.DataFrame(columns=['T', 'p', '$x_{\\textrm{co2}}$', '$x_{\\textrm{w}}$',
                                            ])

    X.loc[0, 'T'] = float(T)
    X.loc[0, 'p'] = float(p)
    X.loc[0, '$x_{\\textrm{co2}}$'] = float(x_co2)
    X.loc[0, '$x_{\\textrm{w}}$'] = float(x_w)
    yp_uek = pipeline_uek.predict(X)
    yp_co2 = pipeline_co2.predict(X)
    #X.loc[0, '$x_{\\textrm{e}}$'] = float(x_ethanol)



    yp_w = pipeline_w.predict(X)
    #if x_w+x_co2>=1:
     #   yp_uek=0

    if yp_w < 0:
        print("ERROR K")
        print('Kw')
        print(yp_w)
        print(x_w)
        print(x_co2)
    if yp_co2 < 0:
        print("ERROR K")
        #print(T)
        print('Kco2')
        print(yp_co2)
        print(x_w)
        print(x_co2)
    if yp_uek < 0:
        print("ERROR K")
        print('Kuek')
        print(yp_uek)
        print(x_w)
        print(x_co2)

    LOGGER.debug("... Finished make_distributioncoeff_predictions_hp")
    return float(yp_uek), float(yp_co2), float(yp_w)


def parityplotdata(writer):
    LOGGER.debug("Starting parityplotdata ...")
    import joblib

    # läd modell
    pipeline_uek = joblib.load('Distribution_Coeff/pkl-Dateien/MLA_distributioncoeff_Kuek_ohneTest.pkl')
    pipeline_co2 = joblib.load('Distribution_Coeff/pkl-Dateien/MLA_distributioncoeff_Kco2ohneTest.pkl')
    pipeline_w = joblib.load('Distribution_Coeff/pkl-Dateien/MLA_distributioncoeff_KwohneTest.pkl')

    def loadalldata():
        """
        Methode um alle Excelsheets im Inputordner einzuladen welche in der Methode benannt werden
        :return: ein Dataframe mit allen Daten
        """
        LOGGER.debug("Starting loadalldata ...")

        import pandas as pd

        def loaddatafromexcel(filename, columns_used, column_names, categorical_or_boolean_columns_names,
                              rows_header):
            """
            Läd Daten aus einzelner Excel Datei
            :param filename: String Name der Datei
            :param columns_used: array zB. [1,2,4] Spalten in Excel mit einzulesenden Daten, beginne mit 0 zu zählen
            :param column_names: array zB. ['v_g','massentransport'] Spalten-Namen der einzulesenden Daten
            :param categorical_or_boolean_columns_names: array zB ['massentransport']
            :param rows_header: int zB 3 oberste Zeilen in Excel mit Überschriften, Variablenname und Einheiten, beginne mit 1 zu zählenrows_header = 3  # oberste Zeilen in Excel mit Überschriften, Variablenname und Einheiten, beginne mit 1 zu zählen
            :return:
            """
            columns_used = np.array(columns_used)
            # Pfadname der Exeldatei realtiv vom Speicherort des Skriptes Backslash macht evtl unter Linux Probleme und muss durch Slash ersetzt werden
            pathname = 'Distribution_Coeff/Input/' + filename + '.xlsx'
            # einlesen der Daten in ein Pandas Dataframe
            df = pd.read_excel(pathname, usecols=columns_used, names=column_names, header=None, skiprows=rows_header)
            # Filtern aller Zeilen mit fehlenden Daten, bei vielen fehlenden Daten kann man über Stragetien zum Füllen der Daten nachdenken
            df = df.dropna()
            df[categorical_or_boolean_columns_names] = df[categorical_or_boolean_columns_names].astype(
                np.int64)  # Kategorische Daten bei uns sind ganzzahlig und werden als integer gespeichert, eine Unterscheidung im Skript findet auch genau darüber statt
            return df

        # Läd drei verschiedene Dataframes ein
        # Hochdruck
        df_kuek = pd.concat([
            loaddatafromexcel(filename="K-datadriven", columns_used=[1, 2, 3, 4, 17],
                              column_names=["T", "p", "$x_{\\textrm{co2}}$",
                                            "$x_{\\textrm{w}}$", "$K_{\\textrm{uek}}$"
                                            ],
                              categorical_or_boolean_columns_names=[], rows_header=[0, 1, 2])

        ])
        df_kco2 = pd.concat([
            loaddatafromexcel(filename="K-datadriven", columns_used=[1, 2, 3, 4, 19],
                              column_names=["T", "p", "$x_{\\textrm{co2}}$",
                                            "$x_{\\textrm{w}}$", "$K_{\\textrm{co2}}$"
                                            ],
                              categorical_or_boolean_columns_names=[], rows_header=[0, 1, 2])

        ])
        df_kw = pd.concat([
            loaddatafromexcel(filename="K-datadriven", columns_used=[1, 2, 3, 4,5, 18],
                              column_names=["T", "p", "$x_{\\textrm{co2}}$",
                                            "$x_{\\textrm{w}}$","$x_{\\textrm{e}}$", "$K_{\\textrm{w}}$"
                                            ],
                              categorical_or_boolean_columns_names=[], rows_header=[0, 1, 2])

        ])

        # resettet Index, um doppelt vorkommende Indices zu vermeiden
        df_kuek = df_kuek.reset_index(drop=True)
        df_kco2 = df_kco2.reset_index(drop=True)
        df_kw = df_kw.reset_index(drop=True)
        print('Alle Daten eingelesen')
        LOGGER.debug("... Finished loadalldata")
        return df_kuek, df_kco2, df_kw

    # Lädt Daten ein
    df_kuek, df_kco2, df_kw = loadalldata()

    def splitfeaturetarget(df, targetname):
        """
        Methode um Feature und Target-Dataframe zu erstellen
        :param df: Dataframe mit Features und Target
        :return: x, y Dataframes
        """
        import numpy as np
        y = np.array(df[targetname]).reshape(-1, 1).astype(np.float32)
        x = df.drop(columns=[targetname])
        return x, y

    x_kuek, y_kuek = splitfeaturetarget(df_kuek, targetname="$K_{\\textrm{uek}}$")
    x_w, y_w = splitfeaturetarget(df_kw, targetname="$K_{\\textrm{w}}$")
    x_co2, y_co2 = splitfeaturetarget(df_kco2, targetname="$K_{\\textrm{co2}}$")
    y_kuek_pred = pipeline_uek.predict(x_kuek).reshape(-1, 1)
    y_w_pred = pipeline_w.predict(x_w).reshape(-1, 1)
    y_co2_pred = pipeline_co2.predict(x_co2).reshape(-1, 1)
    df_pred_kuek = pd.DataFrame()
    df_pred_kuek['Actual Value'] = y_kuek.ravel()
    df_pred_kuek['Prediction'] = y_kuek_pred
    df_pred_kuek.to_excel(writer, sheet_name='Prädiktionen Kuek')
    df_pred_kw = pd.DataFrame()
    df_pred_kw['Actual Value'] = y_w.ravel()
    df_pred_kw['Prediction'] = y_w_pred
    df_pred_kw.to_excel(writer, sheet_name='Prädiktionen Kw')
    df_pred_kco2 = pd.DataFrame()
    df_pred_kco2['Actual Value'] = y_co2.ravel()
    df_pred_kco2['Prediction'] = y_co2_pred
    df_pred_kco2.to_excel(writer, sheet_name='Prädiktionen Kco2')

    LOGGER.debug("... Finished parityplotdata")
    return y_kuek, y_kuek_pred, y_w, y_w_pred, y_co2, y_co2_pred, df_kuek, df_kw, df_kco2

# Befehl um Modelle zu trainieren
#train_models()

# Befehle zum Abfragen eines Punktes
"""for i in range(9,14,1):
    K_uek, K_co2, K_w = make_distributioncoeff_predictions_hp(p=i, T=313, x_w=0.011, x_co2=0.97, x_ethanol=0.019)
    print(K_uek/K_w)"""