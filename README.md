# High Pressure Extraction

Hybrid Model for High Pressure Extraction of Ethanol from Water with Supercritical CO2

Requires ```Python 3.7.9```
***
## On cluster (Linux)
Use env
```
cd <yourRepoFolder>
$ module load python/3.7.9
$ pip3.7 install virtualenv
$ python3.7 -m virtualenv env
$ source env/bin/activate
$ pip3 install -r requirements.txt
$ deactivate
```
#### Start the Job with
```
$ sbatch jobscript.sh
```

## On your local Windows Machine
Use venv

***
***
***
Some Notes (you can ignore those)
### before 15.1.0
```
virtualenv --no-site-packages --distribute .env &&\
    source .env/bin/activate &&\
    pip install -r requirements.txt
```

### after deprecation of some arguments in 15.1.0
```
virtualenv .env && source .env/bin/activate && pip install -r requirements.txt # creates a new virtual environment and installs all required packages
```

