#!/usr/local_rwth/bin/zsh

####SBATCH --account=thes0955

### Settings for performancemap with 10 PVs
#SBATCH --ntasks=32
#SBATCH --mem-per-cpu=10000M
#SBATCH --time=05:00:00


#SBATCH --job-name=extraction_column
#SBATCH --output=output.%J.txt

#SBATCH --mail-user=linus.hammacher@rwth-aachen.de
#SBATCH --mail-type=ALL

###SBATCH --mem=10GB
### Use one node for parallel jobs on shared-memory systems
###SBATCH --nodes=1
### Number of threads to use, e. g. 24
###SBATCH --cpus-per-task=2
### Number of hyperthreads per core
###SBATCH --ntasks-per-core=1
### Tasks per node (for shared-memory parallelisation, use 1)
###SBATCH --ntasks-per-node=1

cd /home/gu177080/FVT_model

module load python/3.7.9
###virtualenv --no-download $SLURM_SUBMIT_DIR/env # creates new env
source $SLURM_SUBMIT_DIR/env/bin/activate
### pip install --no-index --upgrade pip
### pip install -r requirements.txt

###python performancemap.py
python fit_mt.py

deactivate
