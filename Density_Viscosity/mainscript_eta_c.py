import dataloader_eta_c

import pandas as pd
import numpy as np

# from matplotlib import pyplot as plt
# plt.ioff()

import warnings
import time
import os

from sklearn.compose import ColumnTransformer
from sklearn.model_selection import KFold
from sklearn.preprocessing import StandardScaler,MinMaxScaler
from sklearn.linear_model import LinearRegression
from sklearn.svm import SVR
from sklearn.neighbors import KNeighborsRegressor
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.neural_network import MLPRegressor
from sklearn.gaussian_process.kernels import (RBF, Matern, RationalQuadratic,
                                              ExpSineSquared, DotProduct,
                                              ConstantKernel)



from sklearn.metrics import r2_score, mean_squared_error
from sklearn.metrics import make_scorer
#import matplotlib
#matplotlib.use('Agg')
print('Module eingeladen')

# %matplotlib inline

# Warnungen ignorieren
warnings.filterwarnings(action='ignore', category=FutureWarning)
warnings.filterwarnings(action='ignore', category=UserWarning)
warnings.filterwarnings(action='ignore', category=SyntaxWarning)
warnings.filterwarnings(action='ignore', category=RuntimeWarning)

# Parameter
targetname="eta_c"
n_jobs = 7
seed = None  # Nummer des Random Seed falls Reproduzierbarkeit erwünscht ist, falls nicht gleich None setzen
testseed = None #Nummer des Random Seed in Testsplit der Wrapper methode falls Reproduzierbarkeit erwünscht ist, falls nicht gleich None setzen
n_folds_hyperparameter = 5  # Anzahl der Folds für die Kreuzvalidierung zur Hyperparametersuche; die Vaidierungs-Menge beträgt 1/n_folds der Trainings+Validierungsmenge
n_folds_feature_selection = 5 # Anzahl der Folds für die Kreuzvalidierung zur Wrapper Feature Selection
forewardfeatureselection = True
mixedforeandbackwardfeatureselection = True
print('Parameter erstellt')

# Beginnt Programm

# startet Zeitmessung
start = time.time()

# erstellt Zeitstempel
t = time.localtime()
current_time = time.strftime("%y%m%d__%H_%M_%S", t)

print(current_time)

# erstellt Output-Unterordner
output_path = 'GridSearches//' + str(current_time)
os.mkdir(output_path)

# initialisiert Excel-Schreiber
options = {}
options['strings_to_formulas'] = False
options['strings_to_urls'] = False
excel_path = output_path + '/Output_eta_c'+ str(current_time) + '.xlsx'

writer = pd.ExcelWriter(excel_path, options=options)

# Lädt Daten ein
df_eta_c = dataloader_eta_c.loadalldata()



# Modellübertrag aus Siebbodenkolonne

#Z-Score-Filtern
#df_des = dataloader.z_score_filter_per_category(df_des,threshold=3,columns=df_des.columns)
#Gruppern Filtern
#df_des = dataloader.filtercategoryunderthreshold(df_des, threshold=5)

#Z-Score-Filtern
#df_ex = dataloader.z_score_filter_per_category(df_ex,threshold=3,columns=df_ex.columns)
#Gruppern Filtern
#df_ex = dataloader.filtercategoryunderthreshold(df_ex, threshold=5)

#Z-Score-Filtern
#df_hp = dataloader.z_score_filter_per_category(df_hp,threshold=3,columns=df_hp.columns)
#Gruppern Filtern
#df_hp = dataloader.filtercategoryunderthreshold(df_hp, threshold=5)


# Analysiert Art und Verteilung der Daten und speichert diese in Excel. Erstellt Histogramme
#dataviewer.analysedata(df=df_K_uek, name='Hochdruck', writer=writer, output_path=output_path)
'''
# 1. Feature Importance Analyse mit Filter Methoden: Mutual Information
dataviewer.analysemutualinformation(df=df_des,name='Destillation', targetname=targetname,writer=writer,output_path=output_path,random_state=seed)
dataviewer.analysemutualinformation(df=df_ex,name='Extraktion', targetname=targetname,writer=writer,output_path=output_path,random_state=seed)
dataviewer.analysemutualinformation(df=df_hp,name='Hochdruck', targetname=targetname,writer=writer,output_path=output_path,random_state=seed)

# 2. Feature Importance Analyse mit Embedded Methoden: Feature Importance mit Random Forest
dataviewer.analysefeatureimportance(df=df_des,name='Destillation', targetname=targetname,writer=writer,output_path=output_path,random_state=seed)
dataviewer.analysefeatureimportance(df=df_ex,name='Extraktion', targetname=targetname,writer=writer,output_path=output_path,random_state=seed)
dataviewer.analysefeatureimportance(df=df_hp,name='Hochdruck', targetname=targetname,writer=writer,output_path=output_path,random_state=seed)
'''
# 3. Feature Importance Analyse mit Wrapper Methoden: Sequential Foreward Selection

#Definition der Parametergrids
gausskernels = [1.0 * RBF(length_scale=1.0, length_scale_bounds=(1e-1, 10.0)),
                1.0 * RationalQuadratic(length_scale=1.0, alpha=0.1),
                ConstantKernel(0.1, (0.01, 10.0))
                * (DotProduct(sigma_0=1.0, sigma_0_bounds=(0.1, 10.0)) ** 2),
                1.0 * Matern(length_scale=1.0, length_scale_bounds=(1e-1, 10.0),
                             nu=1.5)]
kernel = ['linear', 'poly', 'rbf', 'sigmoid']
degree = [2, 3, 4]
gamma = ['scale', 'auto']
coef0 = [0.0, 0.01, 0.1],
tol = 10.0 ** -np.arange(4, 9)
C = [25, 50, 75, 100]
epsilon = 10.0 ** -np.arange(1, 5)
alpha = 10.0 ** -np.arange(2, 8)
ssearch_space = [{'MLA': [LinearRegression()]},
                {'MLA': [KNeighborsRegressor()],
                 'MLA__n_neighbors': np.arange(2, 7, 1),
                 'MLA__weights': ['distance', 'uniform'],
                 'MLA__algorithm': ['auto'],
                 'MLA__p': [1, 2]}]

search_space = [{'MLA': [LinearRegression()]},

                {'MLA': [KNeighborsRegressor()],
                 'MLA__n_neighbors': np.arange(2, 7, 1),
                 'MLA__weights': ['uniform'],
                 'MLA__algorithm': ['auto'],
                 'MLA__p': [1, 2]},

                {'MLA': [DecisionTreeRegressor(random_state=seed)],
                 'MLA__criterion': ['mse'],
                 'MLA__splitter': ['best'],
                 'MLA__min_samples_split': [5, 4, 3, 2],  # 5 empfohlen dann niedriger
                 'MLA__max_depth': np.arange(3, 21, 1)  # 3 und höher
                 },

                {'MLA': [SVR()],
                 'MLA__kernel': ['rbf'],
                 'MLA__gamma': gamma,
                 'MLA__tol': tol,
                 'MLA__epsilon': epsilon,
                 'MLA__C': C,
                 'MLA__cache_size': [1000]},

                {'MLA': [RandomForestRegressor(random_state=seed)],
                 'MLA__n_estimators': [1000],
                 'MLA__max_features': [None],
                 'MLA__criterion': ['mse'],
                 'MLA__bootstrap': [True],
                 'MLA__max_depth': [None],
                 'MLA__min_samples_split': [2]
                 },
                {'MLA': [GaussianProcessRegressor(random_state=seed)],
                 'MLA__kernel': gausskernels,
                 'MLA__alpha': alpha,
                 'MLA__n_restarts_optimizer': [5],
                 'MLA__normalize_y': [False]},

                {'MLA': [MLPRegressor(random_state=seed)],
                 'MLA__hidden_layer_sizes': [(5,), (10,), (15,), (20,), (25,), (30,), (35,), (40,), (45,), (50,),
                                             (5, 5), (10, 10), (15, 15), (20, 20), (25, 25), (30, 30), (35, 35),
                                             (40, 40), (45, 45), (50, 50), (5, 2), (10, 5), (15, 7), (20, 10), (25, 12),
                                             (30, 15), (35, 17), (40, 20), (45, 22), (50, 25), (5, 5, 5), (10, 10, 10),
                                             (15, 10, 5)],
                 'MLA__activation': ['relu', 'tanh'],
                 'MLA__solver': ['lbfgs'],
                 'MLA__alpha': alpha,
                 'MLA__tol': [1e-4, 1e-8],
                 'MLA__max_iter': [15000],
                 }
                ]

mla_hp = GaussianProcessRegressor(alpha=1e-06, copy_X_train=True,
                                  kernel=1 ** 2 * RBF(length_scale=1),
                                  n_restarts_optimizer=5, normalize_y=False,
                                  optimizer='fmin_l_bfgs_b', random_state=None)

final_search_space =  [{'MLA': [mla_hp]}]

# Initialisierung der Kreuzvalidierungen
cv_optimize = KFold(n_splits=n_folds_hyperparameter, shuffle=True, random_state=seed)  # Für Hyperparametersuche

# Definiert Scoring Funktionen für Grid Search
scoring = {'neg_mean_squared_error': 'neg_mean_squared_error',
           'r2': 'r2',
           'rmse': make_scorer(score_func=dataloader_eta_c.root_mean_squared_error, greater_is_better=False),
           'aare': make_scorer(score_func=dataloader_eta_c.average_absolute_relative_error, greater_is_better=False)}

# Initialisierung des Scalers
scaler= MinMaxScaler()

#Hochdruck
best_mla_hp, best_scaling_hp, X_train_val_reduced_hp, y_train_val_hp, best_mla_unreduced_hp, X_train_val_scaled_hp  = dataloader_eta_c.makewrapperfeatureselection(name='Verteilungskoeffizient_Uek', df=df_eta_c, targetname=targetname, test_random_seed=testseed, scaler=scaler, search_space=search_space, cv=cv_optimize, scoring=scoring, n_jobs=n_jobs, writer=writer)

# Untersuchung des Modellverhaltens der nicht reduzierten Modelle
#dataviewer.analysemodelbehaviour(X_train_val_scaled_des,X_train_val_scaled_ex,X_train_val_scaled_hp,points_to_analyse=100,trained_mla_des=best_mla_unreduced_des,trained_mla_ex=best_mla_unreduced_ex,trained_mla_hp=best_mla_unreduced_hp,output_path=output_path)

#Quantitativer Vergleich
# Scaler

#One-Hot-Encoding auf gesamter Hochdruckdatenbank
# One-Hot-Encoding der kategorischen Eingangsgrößen
df_hp_encoded = df_eta_c

X_hp,y_hp = dataloader_eta_c.splitfeaturetarget(df=df_hp_encoded, targetname=targetname)

# Speichert Prädiktionen in Exceldatei
df_predictions = pd.DataFrame()
df_predictions['Actual Value'] = y_hp.ravel()

df_predictions.to_excel(writer, sheet_name='Prädiktionen Hochdruck auf ' )

# Tabelle mit Scores

# Parametergrid
pd.DataFrame(search_space).to_excel(writer, sheet_name='Parametergrid')
import numpy as np
# macht Flutkurven


# Schreibt Ergebnisse in Excel-Datei
writer.save()
writer.close()

# Stoppen und Ausgeben der Zeit
end = time.time()
runtime = (end - start)/60
print("Gesamtzeit: \n%.1f Minuten " %runtime )
