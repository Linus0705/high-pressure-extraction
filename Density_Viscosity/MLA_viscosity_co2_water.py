import pandas as pd
import numpy as np
import logging

LOGGER = logging.getLogger(__name__)
LOGGER.debug(f"__name__: {__name__}")

def loadalldata():
    """
    Methode um alle Excelsheets im Inputordner einzuladen welche in der Methode benannt werden
    :return: ein Dataframe mit allen Daten
    """
    LOGGER.debug("Starting loadalldata ...")

    import pandas as pd

    def loaddatafromexcel(filename, columns_used, column_names, categorical_or_boolean_columns_names,
                          rows_header):
        """
        Läd Daten aus einzelner Excel Datei
        :param filename: String Name der Datei
        :param columns_used: array zB. [1,2,4] Spalten in Excel mit einzulesenden Daten, beginne mit 0 zu zählen
        :param column_names: array zB. ['v_g','massentransport'] Spalten-Namen der einzulesenden Daten
        :param categorical_or_boolean_columns_names: array zB ['massentransport']
        :param rows_header: int zB 3 oberste Zeilen in Excel mit Überschriften, Variablenname und Einheiten, beginne mit 1 zu zählenrows_header = 3  # oberste Zeilen in Excel mit Überschriften, Variablenname und Einheiten, beginne mit 1 zu zählen
        :return:
        """
        LOGGER.debug(f"Starting loaddatafromexcel {filename}...")
        columns_used = np.array(columns_used)
        # Pfadname der Exeldatei realtiv vom Speicherort des Skriptes Backslash macht evtl unter Linux Probleme und muss durch Slash ersetzt werden
        pathname = 'Density_Viscosity/Input/' + filename + '.xlsx'
        # einlesen der Daten in ein Pandas Dataframe
        df = pd.read_excel(pathname, usecols=columns_used, names=column_names, header=None, skiprows=rows_header)
        # Filtern aller Zeilen mit fehlenden Daten, bei vielen fehlenden Daten kann man über Stragetien zum Füllen der Daten nachdenken
        df = df.dropna()
        df[categorical_or_boolean_columns_names] = df[categorical_or_boolean_columns_names].astype(
            np.int64)  # Kategorische Daten bei uns sind ganzzahlig und werden als integer gespeichert, eine Unterscheidung im Skript findet auch genau darüber statt
        LOGGER.debug(f"... Finished loaddatafromexcel {filename}")
        return df

    # Läd drei verschiedene Dataframes ein
    # Hochdruck

    df_eta_c = pd.concat([
        loaddatafromexcel(filename="NIST-Density_Viscosity", columns_used=[3, 5, 6],
                          column_names=["eta_c", "T", "p"
                                        ],
                          categorical_or_boolean_columns_names=[], rows_header=[0, 1, 2])

    ])
    df_eta_d = pd.concat([
        loaddatafromexcel(filename="NIST-Density_Viscosity", columns_used=[4, 5, 6],
                          column_names=["eta_d", "T", "p"
                                        ],
                          categorical_or_boolean_columns_names=[], rows_header=[0, 1, 2])

    ])

    # resettet Index, um doppelt vorkommende Indices zu vermeiden
    df_eta_c = df_eta_c.reset_index(drop=True)
    df_eta_d = df_eta_d.reset_index(drop=True)

    LOGGER.debug("... Finished loadalldata")
    return df_eta_c, df_eta_d


def train_models():
    """
    Methode um Modelle neu zu trainieren und zu speichern.
    :return:
    """
    LOGGER.debug("Starting train_models ...")
    from sklearn.gaussian_process import GaussianProcessRegressor
    from sklearn.gaussian_process.kernels import Matern, DotProduct, RBF
    from sklearn.neural_network import MLPRegressor
    from sklearn.neighbors import KNeighborsRegressor
    from sklearn.preprocessing import MinMaxScaler, OneHotEncoder, StandardScaler
    from sklearn.pipeline import Pipeline
    from sklearn.compose import ColumnTransformer
    import joblib



    def train(mla, df):
        def splitfeaturetarget(df, targetname):
            """
            Methode um Feature und Target-Dataframe zu erstellen
            :param df: Dataframe mit Features und Target
            :return: x, y Dataframes
            """
            import numpy as np
            y = np.array(df[targetname]).reshape(-1, 1).astype(np.float32)
            x = df.drop(columns=[targetname])
            return x, y

        x, y = splitfeaturetarget(df, targetname=targetname)

        #categorical_features = x.columns[x.dtypes.apply(lambda c: np.issubdtype(c, np.int64))].tolist()
        numeric_features = x.columns.tolist()

        #categorical_transformer = OneHotEncoder(drop='first', categories='auto')
        numeric_transformer = MinMaxScaler()

        preprocessor = ColumnTransformer(transformers=[
            ('num', numeric_transformer, numeric_features)]
            )

        pipeline = Pipeline([
            ('preprocessor', preprocessor),
            ('MLA', mla)
        ])
        pipeline.fit(x, y.ravel())

        return pipeline

    # Zielgröße
    targetname = "eta_c"

    # Lädt Daten ein
    df_eta_c, df_eta_d = loadalldata()

    # Definiert Modelle
    mla_eta_c = KNeighborsRegressor(algorithm='auto', n_neighbors=4, p=2, weights='distance')
    #mla_eta_c = GaussianProcessRegressor(alpha=1e-07, kernel=0.316**2 * DotProduct(sigma_0=1) ** 2, n_restarts_optimizer=5, normalize_y=False)

    mla_eta_d = GaussianProcessRegressor(alpha=1e-07, kernel=0.316**2 * DotProduct(sigma_0=1) ** 2, n_restarts_optimizer=5, normalize_y=False)

    # Trainiert Modelle
    trained_model_eta_c = train(mla_eta_c, df_eta_c)

    # Zielgröße ändern
    targetname = "eta_d"
    trained_model_eta_d = train(mla_eta_d, df_eta_d)


    # Speichert Modelle
    LOGGER.debug("Save models ...")
    joblib.dump(trained_model_eta_c, 'pkl-Dateien/MLA_eta_c.pkl')
    joblib.dump(trained_model_eta_d, 'pkl-Dateien/MLA_eta_d.pkl')
    LOGGER.debug("... Saved model at pkl-Dateien/MLA_eta_c.pkl")
    LOGGER.debug("... Saved model at pkl-Dateien/MLA_eta_d.pkl")

    LOGGER.debug("... Finished train_models")


def make_viscosity_predictions_hp(T, p):
    """
    Methode um einzelnen Punkt abzufragen
    :param T: Temperatur [K]
    :param p: Druck [MPa]
    :return: eta_c, eta_d: Viskositäten [Pas]
    """
    LOGGER.debug("Starting make_viscosity_predictions_hp ...")
    import joblib

    # läd modell
    pipeline_eta_c = joblib.load('Density_Viscosity/pkl-Dateien/MLA_eta_c.pkl')
    pipeline_eta_d = joblib.load('Density_Viscosity/pkl-Dateien/MLA_eta_d.pkl')

    # erstellt abfragepunkt
    X = pd.DataFrame(columns=['T', 'p'])

    X.loc[0, 'T'] = float(T)
    X.loc[0, 'p'] = float(p)

    yp_eta_c = pipeline_eta_c.predict(X)
    yp_eta_d = pipeline_eta_d.predict(X)

    LOGGER.debug("... Finished make_viscosity_predictions_hp")
    return float(yp_eta_c), float(yp_eta_d)


def NIST_get_viscosity(df_c, df_d, T, p):
    LOGGER.debug("Starting NIST_get_viscosity ...")
    iT = df_c[df_c['T']==T].index.values
    ip = df_c[df_c['p']==p].index.values
    i = np.intersect1d(iT,ip)
    LOGGER.debug("... Finished NIST_get_viscosity")
    return df_c.at[i[0],'eta_c'], df_d.at[i[0],'eta_d']


# Befehl um Modelle zu trainieren
#train_models()
# Befehle zum Abfragen eines Punktes

# Hochdruck
# train_models()
# Temperatur in Kelvin, Druck in MPa
#print(make_viscosity_predictions_hp(T=313, p=13))

