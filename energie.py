import numpy as np
import pandas as pd


def get_q_co2(df, q_c, T1, p1, T2, p2):
    h1 = NIST_get_h_co2(df,T1,p1)
    h2 = NIST_get_h_co2(df, T2, p2)
    return q_c*(h2-h1)


def get_q_w(df, q_d, T1, p1, T2, p2):
    h1 = NIST_get_h_w(df,T1,p1)
    h2 = NIST_get_h_w(df, T2, p2)
    return q_d*(h2-h1)


def get_p_co2(df, q_c, T1, p1, p2, kappa):
    h1 = NIST_get_h_co2(df,T1,p1)
    T2 = T1*(p2/p1)**((kappa-1)/kappa)
    h2 = NIST_get_h_co2(df, T2, p2)
    np = 0.9
    nm = 0.85
    return q_c*(h2-h1)/(np*nm), T2


def get_p_w(df, q_d, p1, p2, T1, T2, n):
    h1 = NIST_get_h_w(df,T1,p1)
    h2 = NIST_get_h_w(df, T2, p2)
    return q_d*(h2-h1)/(n)


def NIST_get_h_w(df, T, p):
    iT = df[df['T']==T].index.values
    ip = df[df['p']==p].index.values
    i = np.intersect1d(iT,ip)
    return df.at[i[0],'h_w']


def NIST_get_h_co2(df, T, p):
    iT = df[df['T']==T].index.values
    ip = df[df['p']==p].index.values
    i = np.intersect1d(iT,ip)
    return df.at[i[0],'h_co2']


def load_data_h():
    df = pd.read_excel('Density_Viscosity/Input/NIST_h.xlsx', usecols=[0,1,2,3], names=['T','p', 'h_w', 'h_co2'], header=None, skiprows=1)
    return df


def co2_kreislauf(q_c, Tex, pex):
    df = load_data_h()
    # kj/h
    q_elektrisch, T_pumpe = get_p_co2(df=df, q_c=q_c, T1=289.5, p1=6, p2=pex, kappa=1.0287)
    q_heiz1 = get_q_co2(df=df, q_c=q_c, T1=T_pumpe, T2=Tex, p1=pex, p2=pex)
    q_heiz2 = get_q_co2(df=df, q_c=q_c, T1=293.5, T2=298, p1=6, p2=6)
    q_kühl = -get_q_co2(df=df, q_c=q_c, T1=298, T2=289.5, p1=6, p2=6)
    return q_elektrisch, q_heiz1+q_heiz2,q_kühl

def w_kreislauf(q_d, Tex, pex):
    df = load_data_h()
    q_elektrisch = get_p_w(df=df, q_d=q_d, p1=0.01, p2=pex, T1=298, T2=298, n=0.85)
    q_heiz = get_q_w(df=df, q_d=q_d, p1=pex, p2=pex, T1=298, T2=Tex)
    return q_elektrisch, q_heiz

