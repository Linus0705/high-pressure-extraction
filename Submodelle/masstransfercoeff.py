import math
import logging

LOGGER = logging.getLogger(__name__)
LOGGER.debug(f"__name__: {__name__}")

def mtcoeff_c_seibert(diff, d32, vs, rho, visc, holdup, fit_c):
    """
    Mtkoeff konti bestimmen nach Seibert 1990
    :param diff: Diffusionskoeffizient kontinuierliche Phase [m^2/s]
    :param d32: Tropfendurchmesser [m]
    :param vs: Slip velocity [m/s]
    :param rho: Dichte der kontinuierlichen Phase [kg/m^3]
    :param visc: Viskosität der kontinuierlichen Phase [Pas]
    :param holdup: Volumenanteil der dispersen Phase
    :return: k_c: Massentransferkoeffizient der kontinuierlichen Phase [kg/s*m^2]
    """
    LOGGER.debug("Starting mtcoeff_c_seibert ...")
    re = rho*vs*d32/visc
    sc = visc/(rho*diff)
    k_c = fit_c[0] * (diff/d32) * sc**fit_c[1] * re**fit_c[2] * (1-holdup)
    # k_c = 0.725 * (diff/d32) * sc**0.42 * re**0.57 * (1-holdup)  # Treybal (1980)
    LOGGER.debug("... Finished mtcoeff_c_seibert")
    return k_c


def mtcoeff_d_seibert(diff, vs, rho, visc_c, visc_d, fit_d):
    """
    Mtkoeff dispers bestimmen nach Seibert 1990
    :param diff: Diffusionskoeffizient der dispersen Phase [m^2/s]
    :param vs: Slip velocity [m/s]
    :param rho: Dichte der dispersen Phase [kg/m^3]
    :param visc_c: Viskosität der konti Phase [Pas]
    :param visc_d: Viskosität der disp Phase [Pas]
    :return: k_d Massentransferkoeff der disp Phase [kg/s*m^2]
    """
    LOGGER.debug("Starting mtcoeff_d_seibert ...")
    phi = (visc_d/(rho*diff))**0.5 / (1+visc_d/visc_c)
    LOGGER.debug(f"phi = {phi} [-]")
    if phi < 6:
        k_d = fit_d[0]*vs/(1+visc_d/visc_c)
    else:
        sc = visc_d/(rho*diff)
        k_d = fit_d[1]*vs*(sc**(-0.5))
    LOGGER.debug("... Finished mtcoeff_d_seibert")
    return k_d


def mtcoeff_c_onda(re_c, sc_c, a_s, d_p, diff):
    """

    :param re_c:
    :param sc_c:
    :param a_s:
    :param d_p:
    :param diff:
    :return:
    """
    LOGGER.debug("Starting mtcoeff_c_onda ...")
    alpha = 5.23
    ki_c = alpha* (a_s*d_p)**(-2) * re_c**0.7 * sc_c**0.33 * (diff/d_p)
    LOGGER.debug("... Finished mtcoeff_c_onda")
    return ki_c

def mtcoeff_d_onda(re_d, sc_d, a_s, d_p, rho_d, eta_d):
    """

    :param re_d:
    :param sc_d:
    :param a_s:
    :param d_p:
    :param rho_d:
    :param eta_d:
    :return:
    """
    LOGGER.debug("Starting mtcoeff_d_onda ...")
    g = 9.81
    beta = 0.0051
    ki_d = beta * (a_s*d_p)**0.4 * re_d**0.66 * sc_d**(-0.5) * (eta_d*g/rho_d)**0.33
    LOGGER.debug("... Finished mtcoeff_d_onda")
    return ki_d

def mtcoeff_c_ruivo(re_c, sc_c, diff, d_p):
    """
    Massentransferkoeff konti nach Ruivo (2004)
    :param re_c: Reynoldszahl konti[-]
    :param sc_c: Schmidtzahl konti [-]
    :param diff: Diffusionskoeff in konti [m^2/s]
    :param d_p: sphärischer Partiekldurchmesser [m]
    :return:
    """
    LOGGER.debug("Starting mtcoeff_c_ruivo ...")
    ki_c = 0.0066 * (diff/d_p) * re_c**0.8 * sc_c**0.33
    LOGGER.debug("... Finished mtcoeff_c_ruivo")
    return ki_c

def mtcoeff_d_ruivo(diff, v_d, d_p):
    """
    Massentransferkoeff dispers nach Ruivo (2004)
    :param diff: Diffusionskoeff in konti [m^2/s]
    :param v_d: Geschwindigkeit dispers [m/s]
    :param d_p: spherischer Partiekldurchmesser [m]
    :return:
    """
    LOGGER.debug("Starting mtcoeff_d_ruivo ...")
    ki_d = 2* (diff*0.9*v_d/(math.pi*d_p))**0.5
    LOGGER.debug("... Finished mtcoeff_d_ruivo")
    return ki_d


def mtcoeff_c_catchpole(re_c, sc_c, diff, d32):
    LOGGER.debug("Starting mtcoeff_c_catchpole ...")
    ki_c = 0.198*diff/d32* (re_c*sc_c)**0.5
    LOGGER.debug("... Finished mtcoeff_c_catchpole")
    return ki_c


def mtcoeff_d_catchpole(diff, d32):
    LOGGER.debug("Starting mtcoeff_c_catchpole ...")
    ki_d = 6*diff/d32
    LOGGER.debug("... Finished mtcoeff_d_catchpole")
    return ki_d