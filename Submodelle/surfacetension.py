import math


def chunwilkinson(rho_c, x_ethanol):
    """
    Berechnung der Grenzflächenspannung nach Chun&Wilkinsion 1995
    :param rho_c: Dichte überkritisches CO2 [kg/m^3]
    :param x_ethanol: Stoffmengenanteil ethanol
    :return sigma: Grenzflächenspannung [N/m]
    """
    # lineare Interpolation
    if x_ethanol <= 0.016:
        a = -0.2333 + x_ethanol * ((-0.249+0.2333)/0.016)
    else:
        a = -0.2490 + (x_ethanol-0.016) * ((-0.3016+0.2490) / (0.0332-0.016))

    b = 1.66
    sigma = math.exp(b+a*math.log(rho_c))/1000  # Umrechung in N/m
    return sigma


def train_models():
    """
    Methode um Modelle neu zu trainieren und zu speichern.
    :return:
    """
    from sklearn.svm import SVR
    from sklearn.gaussian_process import GaussianProcessRegressor
    from sklearn.gaussian_process.kernels import RBF, RationalQuadratic
    from sklearn.preprocessing import MinMaxScaler, OneHotEncoder
    from sklearn.pipeline import Pipeline
    from sklearn.compose import ColumnTransformer
    import joblib

    def loadalldata():
        """
        Methode um alle Excelsheets im Inputordner einzuladen welche in der Methode benannt werden
        :return: ein Dataframe mit allen Daten
        """

        import pandas as pd

        def loaddatafromexcel(filename, columns_used, column_names, categorical_or_boolean_columns_names,
                              rows_header):
            """
            Läd Daten aus einzelner Excel Datei
            :param filename: String Name der Datei
            :param columns_used: array zB. [1,2,4] Spalten in Excel mit einzulesenden Daten, beginne mit 0 zu zählen
            :param column_names: array zB. ['v_g','massentransport'] Spalten-Namen der einzulesenden Daten
            :param categorical_or_boolean_columns_names: array zB ['massentransport']
            :param rows_header: int zB 3 oberste Zeilen in Excel mit Überschriften, Variablenname und Einheiten, beginne mit 1 zu zählenrows_header = 3  # oberste Zeilen in Excel mit Überschriften, Variablenname und Einheiten, beginne mit 1 zu zählen
            :return:
            """
            columns_used = np.array(columns_used)
            # Pfadname der Exeldatei realtiv vom Speicherort des Skriptes Backslash macht evtl unter Linux Probleme und muss durch Slash ersetzt werden
            pathname = 'Input/' + filename + '.xlsx'
            # einlesen der Daten in ein Pandas Dataframe
            df = pd.read_excel(pathname, usecols=columns_used, names=column_names, header=None, skiprows=rows_header)
            # Filtern aller Zeilen mit fehlenden Daten, bei vielen fehlenden Daten kann man über Stragetien zum Füllen der Daten nachdenken
            df = df.dropna()
            df[categorical_or_boolean_columns_names] = df[categorical_or_boolean_columns_names].astype(
                np.int64)  # Kategorische Daten bei uns sind ganzzahlig und werden als integer gespeichert, eine Unterscheidung im Skript findet auch genau darüber statt
            return df

        # Läd drei verschiedene Dataframes ein
        # Hochdruck
        df_ift = pd.concat([
            loaddatafromexcel(filename="IFT", columns_used=[1, 2, 3, 4],
                              column_names=["sigma", "T", "p", "x_ethanol"],
                              categorical_or_boolean_columns_names=[], rows_header=[0, 1, 2])

        ])

        # resettet Index, um doppelt vorkommende Indices zu vermeiden
        df_ift = df_ift.reset_index(drop=True)
        print('Alle Daten eingelesen')
        return df_ift

    def train(mla, df):
        def splitfeaturetarget(df, targetname):
            """
            Methode um Feature und Target-Dataframe zu erstellen
            :param df: Dataframe mit Features und Target
            :return: x, y Dataframes
            """
            import numpy as np
            y = np.array(df[targetname]).reshape(-1, 1).astype(np.float32)
            x = df.drop(columns=[targetname])
            return x, y

        x, y = splitfeaturetarget(df, targetname=targetname)
        categorical_features = x.columns[x.dtypes.apply(lambda c: np.issubdtype(c, np.int64))].tolist()
        numeric_features = x.columns[x.dtypes.apply(lambda c: np.issubdtype(c, np.float64))].tolist()

        categorical_transformer = OneHotEncoder(drop='first', categories='auto')
        numeric_transformer = MinMaxScaler()

        preprocessor = ColumnTransformer(transformers=[
            ('num', numeric_transformer, numeric_features),
            ('cat', categorical_transformer, categorical_features)])

        pipeline = Pipeline([
            ('preprocessor', preprocessor),
            ('MLA', mla)
        ])
        pipeline.fit(x, y.ravel())

        return pipeline

    # Zielgröße
    targetname = "sigma"

    # Lädt Daten ein
    df_ift = loadalldata()

    # Definiert Modelle
    mla_ift = GaussianProcessRegressor(alpha=0.01, kernel= 1**2 * RationalQuadratic(alpha=0.1, length_scale=1), n_restarts_optimizer=5, normalize_y=False)
    # Trainiert Modelle
    trained_model_ift = train(mla_ift, df_ift)

    # Speichert Modelle
    joblib.dump(trained_model_ift, 'pkl-Dateien/MLA_ift.pkl')


def make_holdup_predictions(T, p, x_ethanol):
    """
    Methode um einzelnen Punkt abzufragen
    :param T: Temperatur [K]
    :param p: Druck [MPa]
    :param x_ethanol: Molanteil Ethanol [mol/mol]

    :return: yp_ift: Volumenanteil der dispersen Phase
    """
    import joblib

    # läd modell
    pipeline_ift = joblib.load('Holdup/pkl-Dateien/MLA_ift.pkl')
    # erstellt abfragepunkt
    X = pd.DataFrame(columns=["T", "p", "x_ethanol"])

    X.loc[0, 'T'] = float(T)
    X.loc[0, 'p'] = float(p)
    X.loc[0, 'x_ethanol'] = float(x_ethanol)

    yp_ift = pipeline_ift.predict(X)

    return float(yp_ift)


# Befehl um Modelle zu trainieren
#train_models()
# Befehle zum Abfragen eines Punktes
# holdup = make_holdup_predictions(T=19.2, p=4.7, rho_d=1015, x_ethanol=628.61, eta_c=0.0000332, eta_d=0.001, Kwinkel=130, D=38, d_h=0.000343, a_s=700, cut_off=0.1, sigma=30, epsilon=0.86)
# print(holdup)
