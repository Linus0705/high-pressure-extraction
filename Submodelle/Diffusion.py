import logging

LOGGER = logging.getLogger(__name__)
LOGGER.debug(f"__name__: {__name__}")

def WilkeChang(T, Mb, Va, eta_b, phi):
    """
    Methode nach Wilke und Chang um Diffusionskoeffizienten bei unendlicher Verdünnung zu bestimmen
    :param T: Temperatur [K]
    :param Mb: Molare Masse des Solvents [g/mol]
    :param Va: Molares Volumen des gelösten Stoffes bei Siedetemperatur [cm^3/mol]
    :param eta_b: Viskosität des Solvents [Pas]
    :param phi: Assozationsfaktor des Solvents (2.6-1)
    :return: D_ab Diffussionskoeffizient Stoff A gelöst in Stoff B bei unendlicher Verdünnung [m^2/s]
    """
    LOGGER.debug("Starting WilkeChang ...")
    D_ab = 7.4 * 1e-15 * (phi*Mb)**0.5 * T / (eta_b * Va ** 0.6)
    LOGGER.debug("... Finished WilkeChang")
    return D_ab

def diff_ethanolwasserco2(eta_c, eta_d, T):
    """

    :param eta_c: Viskosität kontinuierliche Phase [Pas]
    :param eta_d: Viskosität disperse Phase Phase [Pas]
    :param T: Temperatur [K]
    :return: Diffusionskoeffizienten [m^2/s] für Ethanol/CO2 in Wasser und Ethanol/Wasser in CO2 D_e_w, D_co2_w, D_e_co2, D_w_co2
    """
    LOGGER.debug("Starting diff_ethanolwasserco2 ...")
    # Molare Masse
    M_co2 = 44.01  # [g/mol]
    M_w = 18.01528 # [g/mol]

    #Molares Volumen
    # LeBas
    # V_e = 59.2 # [cm^3/mol]
    # Schröder
    # V_e = 63
    # Tyn & Calus
    V_ec = 168
    V_e = 0.285*V_ec**1.048
    V_w = 18.99
    V_co2 = 34

    # Assoziationsfaktor
    phi_w = 2.6
    phi_co2 = 1
    # Diffusionskoeff berechnen
    D_e_w = WilkeChang(T=T, Mb=M_w, Va=V_e, eta_b=eta_d*1000, phi=phi_w)/100
    D_co2_w = WilkeChang(T=T, Mb=M_w, Va=V_co2, eta_b=eta_d*1000, phi=phi_w)/100
    D_e_co2 = WilkeChang(T=T, Mb=M_co2, Va=V_e, eta_b=eta_c*1000, phi=phi_co2)/100
    D_w_co2 = WilkeChang(T=T, Mb=M_co2, Va=V_w, eta_b=eta_c*1000, phi=phi_co2)/100

    LOGGER.debug("... Finished diff_ethanolwasserco2")
    return D_e_w, D_co2_w, D_e_co2, D_w_co2


def getVeb():
    """
    Gibt Molare Volumen (der flüssigen Phase) bei Siedepunkt [cm^3/mol] von p=1atm zurück
    :return: V_e, V_w, V_co2
    """
    LOGGER.debug("Starting getVeb ...")
    # Molares Volumen
    # LeBas
    # V_e = 59.2 # [cm^3/mol]
    # Schröder
    # V_e = 63
    # Tyn & Calus Relation
    V_ec = 168  # critical molar volume [cm^3/mol] at critical point for EtOH (T=304.1 K, p=73.8 bar)
    V_e = 0.285 * V_ec ** 1.048 # Tyn & Calus Relation for V_EtOH_critical to V_EtOH_boiling at normal boiling temperature (thus at p=1atm)
    V_w = 18.79 # [cm^3/mol] molar volume at normal boiling point (p=1atm, T=373.15K)
    # V_co2 = 34
    # V_co2 = 22.4
    V_co2 = 28 # [cm^3/mol] molar volume at boiling point for the liquid co2 (p=1atm, T=194,7K)
    LOGGER.debug("... Finished getVeb")
    return V_e, V_w, V_co2



def SassiatMourier(T, Mi, Vi, eta_c):
    """
    Diffusionskoeffizient in scCO2
    :param T: Temperatur [K]
    :param Mi: Molare Masse des gelösten Stoffes [g/mol]
    :param Vi: Molares Volumen des gelösten Stoffes bei Siedetemperatur [cm^3/mol]
    :param eta_c: Viskosität CO2 [N/m]
    :return: d_i_co2: Diffkoeff [m^2/s]
    """
    LOGGER.debug("Starting SassiatMourier ...")
    d_i_co2 = 8.6 * 1e-15 * (T * Mi**0.5)/(eta_c * Vi**0.6)
    LOGGER.debug("... Finished SassiatMourier")
    return d_i_co2


#print(WilkeChang(308.2, 18, 18.99, 0.0007492, 2.6))
#print(SassiatMourier(313.2,18,18.99,0.0006804))

