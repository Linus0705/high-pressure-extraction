def loadalldata():
    """
    Methode um alle Excelsheets im Inputordner einzuladen welche in der Methode benannt werden
    :return: ein Dataframe mit allen Daten
    """
    import numpy as np
    import pandas as pd
    def loaddatafromexcel(filename, columns_used, column_names, categorical_or_boolean_columns_names,
                          rows_header):
        """
        Läd Daten aus einzelner Excel Datei
        :param filename: String Name der Datei
        :param columns_used: array zB. [1,2,4] Spalten in Excel mit einzulesenden Daten, beginne mit 0 zu zählen
        :param column_names: array zB. ['v_g','massentransport'] Spalten-Namen der einzulesenden Daten
        :param categorical_or_boolean_columns_names: array zB ['massentransport']
        :param rows_header: int zB 3 oberste Zeilen in Excel mit Überschriften, Variablenname und Einheiten, beginne mit 1 zu zählenrows_header = 3  # oberste Zeilen in Excel mit Überschriften, Variablenname und Einheiten, beginne mit 1 zu zählen
        :return:
        """
        columns_used = np.array(columns_used)
        # Pfadname der Exeldatei realtiv vom Speicherort des Skriptes Backslash macht evtl unter Linux Probleme und muss durch Slash ersetzt werden
        pathname = 'Input/' + filename + '.xlsx'
        # einlesen der Daten in ein Pandas Dataframe
        df = pd.read_excel(pathname, usecols=columns_used, names=column_names, header=None, skiprows=rows_header)
        # Filtern aller Zeilen mit fehlenden Daten, bei vielen fehlenden Daten kann man über Stragetien zum Füllen der Daten nachdenken
        df = df.dropna()
        df[categorical_or_boolean_columns_names] = df[categorical_or_boolean_columns_names].astype(
            np.int64)  # Kategorische Daten bei uns sind ganzzahlig und werden als integer gespeichert, eine Unterscheidung im Skript findet auch genau darüber statt
        return df

    # Läd drei verschiedene Dataframes ein
    # Hochdruck
    #todo: Durchmesser in mm und sigma in mN/m
    df_holdup = pd.concat([
        loaddatafromexcel(filename="Holdup", columns_used=[3, 7, 10, 11, 12, 13, 14, 15, 17, 18, 19, 20, 21],
                          column_names=["Holdup", "q_d","$\\rho_{\\textrm{c}}$", "$\\eta_{\\textrm{c}}$", "$\\rho_{\\textrm{d}}$",
                                        "$\\eta_{\\textrm{d}}$", "$\\sigma$", "Kontaktwinkel", "D", "d_h", "a_s", "$\\epsilon$",
                                        "Cut-off"
                                        ],
                          categorical_or_boolean_columns_names=[], rows_header=[0,1,2,3])

    ])

    # resettet Index, um doppelt vorkommende Indices zu vermeiden
    df_holdup = df_holdup.reset_index(drop=True)
    print('Alle Daten eingelesen')

    return df_holdup


def splitfeaturetarget(df, targetname):
    """
    Methode um Feature und Target-Dataframe zu erstellen
    :param df: Dataframe mit Features und Target
    :param targetname: Name der Zielgröße/Target
    :return: x, y Dataframes
    """
    import numpy as np
    y = np.array(df[targetname]).reshape(-1, 1).astype(np.float32)
    x = df.drop(columns=[targetname])
    return x, y


def average_absolute_relative_error(y_true, y_pred):
    # Berechnet durschschnittlichen absoluten relativen Fehler in Prozent
    import numpy as np
    y_true, y_pred = np.array(y_true), np.array(y_pred)
    return np.mean(np.abs((y_true - y_pred) / y_true)) * 100


def root_mean_squared_error(y_true, y_pred):  # Berechnet Wurzel aus MSE
    from sklearn.metrics import mean_squared_error
    import numpy as np
    return np.sqrt(mean_squared_error(y_true, y_pred))


def makewrapperfeatureselection(df, targetname, test_random_seed, scaler, search_space, cv, scoring, n_jobs, name,
                                writer):
    from copy import deepcopy
    # import dataviewer
    import numpy as np
    import pandas as pd
    from sklearn.model_selection import train_test_split
    from sklearn.base import BaseEstimator
    from sklearn.pipeline import Pipeline
    from sklearn.model_selection import GridSearchCV

    from sklearn.metrics import r2_score, mean_squared_error

    from sklearn.compose import \
        ColumnTransformer  # Erlaubt unterschiedliche Transformationen auf unterschiedliche Columns

    # Erstelle Platzhalter Estimator zum Ersetzen in der GridSeach
    class DummyEstimator(BaseEstimator):
        def fit(self): pass

        def score(self): pass


    # One-Hot-Encoding der kategorischen Eingangsgrößen
    df_encoded = pd.get_dummies(df, columns=df.columns[
        df.dtypes.apply(lambda c: np.issubdtype(c, np.int64))], drop_first=False, dtype=np.int64, prefix_sep='-')

    # Test Split zum Testen der Vorhersagegenauigkeit auf ungesehene Daten
    df_encoded_train_val, df_encoded_test = train_test_split(df_encoded, test_size=0.1, shuffle=True,
                                                             random_state=test_random_seed)

    # Aufteilen der df in Features und Target
    X_train_val, y_train_val = splitfeaturetarget(df=df_encoded_train_val, targetname=targetname)
    X_test, y_test = splitfeaturetarget(df=df_encoded_test, targetname=targetname)

    # Initialisiert Scaler
    scalertrans = ColumnTransformer([('continuous', scaler, X_train_val.columns[
        X_train_val.dtypes.apply(lambda c: np.issubdtype(c, np.float64))].tolist())], remainder='passthrough',
                                    sparse_threshold=0)

    # Initialisiert Dummy Estimator
    mla = DummyEstimator()

    pipeline = Pipeline([
        ('scaler', scalertrans),
        ('MLA', mla)
    ])

    # Initialisiert und started Grid Search
    print("Starte GridSearch")
    GridSCV = GridSearchCV(estimator=pipeline, param_grid=search_space, cv=cv, scoring=scoring,
                           refit='neg_mean_squared_error', n_jobs=n_jobs, return_train_score=True, verbose=1).fit(
        X_train_val, y_train_val.ravel())
    # schreibt die Ergebnisse der Grid Search in ein Dataframe und sortiert nach Score
    GridSCV_results = pd.DataFrame(GridSCV.cv_results_)
    GridSCV_results.sort_values(by='rank_test_neg_mean_squared_error', ascending=True, axis=0, inplace=True)

    # speichert Dataframe in Exceldatei
    GridSCV_results.to_excel(writer, sheet_name='Gridsearch' + name)

    # leere Dataframes zum Speichern der Ergebnisse pro Algorithmus
    df_predictions = pd.DataFrame(index=X_test.index)
    df_scores = pd.DataFrame(index=['mse', 'r2', 'AARE', 'RMSE'])
    df_train_predictions = pd.DataFrame(index=X_train_val.index)
    df_train_scores = pd.DataFrame(index=['mse', 'r2', 'AARE', 'RMSE'])
    df_hyperparameter = pd.DataFrame(index=['Hyperparameter'])

    # Dummy Score für schleife
    score_to_beat = 1

    # Für jeden benutzten Algorithmus
    for alg in GridSCV_results.loc[:, 'param_MLA'].unique():
        # hol den Namen des Algorithmus
        alg_name = str(alg).split('(')[0]
        # alle plätze auf denen Algorithmus ist
        alg_rankings = GridSCV_results.loc[GridSCV_results['param_MLA'] == alg, 'rank_test_neg_mean_squared_error']
        # bester platz auf dem algorithmus ist
        alg_best_ranking = alg_rankings.min()
        # indes des besten platzes
        zw = GridSCV_results[GridSCV_results['rank_test_neg_mean_squared_error'] == alg_best_ranking].index.values
        alg_best_ranking_index = int(zw[0])
        # beste Hyperparamter
        params = GridSCV_results['params'][alg_best_ranking_index]
        # übergibt hyperparameter
        alg_best_mla = GridSCV.estimator.set_params(**params)
        # training auf gesamten Trainings und Validierungsdatensatz
        alg_best_mla.fit(X_train_val, y_train_val)
        # Vorhersage auf Testdatensatz
        alg_yp = alg_best_mla.predict(X_test).reshape(-1, 1)
        alg_train_yp = alg_best_mla.predict(X_train_val).reshape(-1, 1)
        # speichert prädiktion
        df_predictions.loc[:, alg_name] = alg_yp
        df_train_predictions.loc[:, alg_name] = alg_train_yp

        # Berechnet Scores
        df_scores.loc['mse', alg_name] = mean_squared_error(y_test, alg_yp)
        df_scores.loc['r2', alg_name] = r2_score(y_test, alg_yp)
        df_scores.loc['AARE', alg_name] = average_absolute_relative_error(y_test, alg_yp)
        df_scores.loc['RMSE', alg_name] = root_mean_squared_error(y_test, alg_yp)

        df_train_scores.loc['mse', alg_name] = mean_squared_error(y_train_val, alg_train_yp)
        df_train_scores.loc['r2', alg_name] = r2_score(y_train_val, alg_train_yp)
        df_train_scores.loc['AARE', alg_name] = average_absolute_relative_error(y_train_val, alg_train_yp)
        df_train_scores.loc['RMSE', alg_name] = root_mean_squared_error(y_train_val, alg_train_yp)

        # speichert Hyperparameter
        df_hyperparameter.loc['Hyperparameter', alg_name] = str(params)

        # merkt sich modell wenn dieses besser als die bisherigen ist
        if df_scores.loc['mse', alg_name] < score_to_beat:
            best_pipe_name = alg_name
            best_pipe = deepcopy(alg_best_mla)
            score_to_beat = df_scores.loc['mse', alg_name]

    # Bester Algorithmus
    best_mla = best_pipe.steps[1][1]
    best_mla_unreduced = deepcopy(best_mla)



    writer.save()

    # führt Scaling durch
    # ursprüngliche diskrete Eingangsgrößen
    columns_cat = X_train_val.columns[X_train_val.dtypes.apply(lambda c: np.issubdtype(c, np.int64))]

    # Skalierung
    X_train_val_scaled = pd.DataFrame(data=best_pipe.steps[0][1].fit_transform(X=X_train_val),
                                      columns=X_train_val.columns, index=X_train_val.index)

    # Ändert Datentypen
    X_train_val_scaled[columns_cat] = X_train_val_scaled[columns_cat].astype(np.int64)


    # reduziert Mengen
    # Erstellt DF mit Datentypen ursprünglicher Features
    X_train_val_reduced = X_train_val_scaled


    # Erstellt DF mit Datentypen ursprünglicher Features
    X_test_reduced = X_test
    # speichert cat columns
    columns_cat = X_train_val_reduced.columns[X_train_val_reduced.dtypes.apply(lambda c: np.issubdtype(c, np.int64))]
    # führt Scaling durch
    scaler_after_wfw = ColumnTransformer([('continuous', scaler, X_train_val_reduced.columns[
        X_train_val_reduced.dtypes.apply(lambda c: np.issubdtype(c, np.float64))].tolist())], remainder='passthrough',
                                         sparse_threshold=0)
    # fit auf Train-Val-Menge
    scaler_after_wfw.fit(X=X_train_val_reduced)
    # transform Train-Val
    X_train_val_reduced_scaled = pd.DataFrame(data=scaler_after_wfw.transform(X_train_val_reduced),
                                              columns=X_train_val_reduced.columns, index=X_train_val_reduced.index)
    # Transform Test
    X_test_reduced_scaled = pd.DataFrame(data=scaler_after_wfw.transform(X=X_test_reduced),
                                         columns=X_test_reduced.columns, index=X_test_reduced.index)
    # Ändert Datentypen wieder zurück
    X_train_val_reduced_scaled[columns_cat] = X_train_val_reduced_scaled[columns_cat].astype(np.int64)
    X_test_reduced_scaled[columns_cat] = X_test_reduced_scaled[columns_cat].astype(np.int64)

    # fittet bestes Modell aus Gridsearch auf reduziertem datensatz
    best_mla.fit(X_train_val_reduced_scaled, y_train_val)
    pred_MLA_test = best_mla.predict(X_test_reduced_scaled).reshape(-1, 1)
    pred_MLA_train = best_mla.predict(X_train_val_reduced_scaled).reshape(-1, 1)

    # Berechnet Scores
    df_scores.loc['mse', best_pipe_name + '_on_reduced'] = mean_squared_error(y_test, pred_MLA_test)
    df_scores.loc['r2', best_pipe_name + '_on_reduced'] = r2_score(y_test, pred_MLA_test)
    df_scores.loc['AARE', best_pipe_name + '_on_reduced'] = average_absolute_relative_error(y_test, pred_MLA_test)
    df_scores.loc['RMSE', best_pipe_name + '_on_reduced'] = root_mean_squared_error(y_test, pred_MLA_test)

    # Hyperparamter
    df_hyperparameter.to_excel(writer, sheet_name=name + ' Beste Hyperparameter')

    # Speichert Prädiktionen in Exceldatei
    df_predictions['Actual Value'] = y_test.ravel()
    df_predictions[best_pipe_name + '_on_reduced'] = pred_MLA_test
    df_predictions.to_excel(writer, sheet_name='Prädiktion auf Test-Set ' + name)
    df_train_predictions['Actual Value'] = y_train_val.ravel()
    df_train_predictions[best_pipe_name + '_on_reduced'] = pred_MLA_train
    df_train_predictions.to_excel(writer, sheet_name='Prädiktion auf Train-Set ' + name)

    # Tabelle mit Scores
    df_scores.to_excel(writer, sheet_name='Testscores ' + name)
    df_train_scores.to_excel(writer, sheet_name='Trainscores ' + name)

    # speichert Mengen in Exceldatei
    df_encoded_train_val.to_excel(writer, sheet_name='Train-Validation-Set')
    df_encoded_test.to_excel(writer, sheet_name='Test-Set')

    return deepcopy(
        best_mla), scaler_after_wfw, X_train_val_reduced, y_train_val, best_mla_unreduced, X_train_val_scaled


def z_score_filter(inputdf, threshhold, columns):
    """
    Methode um Dataframe nach Werten zu Filtern, welche das Threshholdfache der Standartabweichung vom Mittelwert abweichen. 3 $\\sigma$ Rule=99,7% Der Daten befinden sich innerhalb der 3 fachen Standartabweichung
    :param inputdf:
    :param threshhold: Faktor um wieviele Standartabweichungen Wert noch abweichen darf
    :return:
    """
    import pandas as pd
    import numpy as np
    from sklearn.preprocessing import StandardScaler
    scaler = StandardScaler()
    ZScores = pd.DataFrame(scaler.fit_transform(X=inputdf), columns=inputdf.columns, index=inputdf.index)
    isin = abs(ZScores) <= threshhold
    outputdf = inputdf.copy()
    columns = columns[columns.isin(inputdf.columns)]  # nur Spalten die auch noch in inputdf sind
    for col in columns:
        outputdf[isin[col] == True] = inputdf[isin[col] == True]
        outputdf[isin[col] == False] = np.NaN
    outputdf = outputdf.dropna()  # Filtern aller NaN Zeilen
    outputdf[outputdf.columns[inputdf.dtypes.apply(lambda c: np.issubdtype(c, np.int64))]] = outputdf[
        (outputdf.columns[inputdf.dtypes.apply(lambda c: np.issubdtype(c, np.int64))])].astype(np.int64)

    return outputdf


def z_score_filter_per_category(df, threshold, columns):
    """
    Methode um Z-Score Filter auf df Kategorieweise anzuwenden.
    :param df:
    :param threshold:
    :return:
    """
    import pandas as pd
    import numpy as np
    df_filtered = pd.DataFrame()
    df_grouped = df.groupby(by=df.columns[df.dtypes.apply(lambda c: np.issubdtype(c, np.int64))].tolist(),
                            observed=True)

    for key, group in df_grouped:
        df_filtered = df_filtered.append(z_score_filter(group, threshhold=threshold, columns=columns),
                                         ignore_index=False)  # Füllt Dataframe mit gefilterten Daten

    df_filtered = df_filtered.sort_index(ascending=True)
    return df_filtered


def filtercategoryunderthreshold(df, threshold):
    """
    Methode um Kategorien zu filtern dessen Anteil an den Daten kleiner als der threshold ist. z.B. threshold = 0.05 5% der Daten müssen min in Kategorie sein
    :param df:
    :param threshold: mindestanteil der Kategorien an Datensatz um nicht gefiltert zu werden in Prozent
    :return:
    """
    import numpy as np
    df_grouped = df.groupby(by=df.columns[df.dtypes.apply(lambda c: np.issubdtype(c, np.int64))].tolist(),
                            observed=True)

    for key, group in df_grouped:
        if group.shape[0] / df.shape[0] < threshold / 100:
            df = df.drop(axis=1, index=group.index)
    return df

