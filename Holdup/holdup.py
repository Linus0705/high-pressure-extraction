import pandas as pd
import numpy as np


def loadalldata():
    """
    Methode um alle Excelsheets im Inputordner einzuladen welche in der Methode benannt werden
    :return: ein Dataframe mit allen Daten
    """

    import pandas as pd

    def loaddatafromexcel(filename, columns_used, column_names, categorical_or_boolean_columns_names,
                          rows_header):
        """
        Läd Daten aus einzelner Excel Datei
        :param filename: String Name der Datei
        :param columns_used: array zB. [1,2,4] Spalten in Excel mit einzulesenden Daten, beginne mit 0 zu zählen
        :param column_names: array zB. ['v_g','massentransport'] Spalten-Namen der einzulesenden Daten
        :param categorical_or_boolean_columns_names: array zB ['massentransport']
        :param rows_header: int zB 3 oberste Zeilen in Excel mit Überschriften, Variablenname und Einheiten, beginne mit 1 zu zählenrows_header = 3  # oberste Zeilen in Excel mit Überschriften, Variablenname und Einheiten, beginne mit 1 zu zählen
        :return:
        """
        columns_used = np.array(columns_used)
        # Pfadname der Exeldatei realtiv vom Speicherort des Skriptes Backslash macht evtl unter Linux Probleme und muss durch Slash ersetzt werden
        pathname = 'Holdup/Input/' + filename + '.xlsx' # Training: Dateipfad "Holdup/" für Ordner entfernen
        # einlesen der Daten in ein Pandas Dataframe
        df = pd.read_excel(pathname, usecols=columns_used, names=column_names, header=None, skiprows=rows_header)
        # Filtern aller Zeilen mit fehlenden Daten, bei vielen fehlenden Daten kann man über Stragetien zum Füllen der Daten nachdenken
        df = df.dropna()
        df[categorical_or_boolean_columns_names] = df[categorical_or_boolean_columns_names].astype(
            np.int64)  # Kategorische Daten bei uns sind ganzzahlig und werden als integer gespeichert, eine Unterscheidung im Skript findet auch genau darüber statt
        return df

    # Läd drei verschiedene Dataframes ein
    # Hochdruck
    # todo: einheiten D und sigma
    df_holdup = pd.concat([
        loaddatafromexcel(filename="Holdup",
                          columns_used=[3, 7, 10, 11, 12, 13, 14, 15, 17, 18, 19, 20, 21],
                          column_names=["Holdup", "q_d", "$\\rho_{\\textrm{c}}$", "$\\eta_{\\textrm{c}}$",
                                        "$\\rho_{\\textrm{d}}$",
                                        "$\\eta_{\\textrm{d}}$", "$\\sigma$", "Kontaktwinkel", "D",
                                        "d_h", "a_s", "$\\epsilon$",
                                        "Cut-off"
                                        ],
                          categorical_or_boolean_columns_names=[], rows_header=[0, 1, 2, 3])

    ])

    # resettet Index, um doppelt vorkommende Indices zu vermeiden
    df_holdup = df_holdup.reset_index(drop=True)
    print('Alle Daten eingelesen')
    return df_holdup


def train_models():
    """
    Methode um Modelle neu zu trainieren und zu speichern.
    :return:
    """
    from sklearn.svm import SVR
    from sklearn.gaussian_process import GaussianProcessRegressor
    from sklearn.gaussian_process.kernels import RBF, Matern, DotProduct
    from sklearn.model_selection import train_test_split
    from sklearn.ensemble import RandomForestRegressor
    from sklearn.preprocessing import MinMaxScaler, OneHotEncoder
    from sklearn.pipeline import Pipeline
    from sklearn.compose import ColumnTransformer
    import joblib



    def train(mla, df):
        def splitfeaturetarget(df, targetname):
            """
            Methode um Feature und Target-Dataframe zu erstellen
            :param df: Dataframe mit Features und Target
            :return: x, y Dataframes
            """
            import numpy as np
            y = np.array(df[targetname]).reshape(-1, 1).astype(np.float32)
            x = df.drop(columns=[targetname])
            return x, y

        x, y = splitfeaturetarget(df, targetname=targetname)
        categorical_features = x.columns[x.dtypes.apply(lambda c: np.issubdtype(c, np.int64))].tolist()
        numeric_features = x.columns[x.dtypes.apply(lambda c: np.issubdtype(c, np.float64))].tolist()

        categorical_transformer = OneHotEncoder(drop='first', categories='auto')
        numeric_transformer = MinMaxScaler()

        preprocessor = ColumnTransformer(transformers=[
            ('num', numeric_transformer, numeric_features),
            ('cat', categorical_transformer, categorical_features)])

        pipeline = Pipeline([
            ('preprocessor', preprocessor),
            ('MLA', mla)
        ])
        pipeline.fit(x, y.ravel())

        return pipeline

    # Zielgröße
    targetname = "Holdup"

    # Lädt Daten ein
    df_holdup = loadalldata()
    df_train, df_test = train_test_split(df_holdup, test_size=0.1, shuffle=True)
    # Definiert Modelle
    #mla_holdup = GaussianProcessRegressor(alpha=0.00001, kernel=1**2 * Matern(length_scale=1, nu=1.5), n_restarts_optimizer=5, normalize_y=False)
    mla_holdup = SVR(C=25, cache_size=1000, epsilon=0.0001, gamma='scale', kernel='rbf', tol=0.0001)
    # Trainiert Modelle
    trained_model_holdup = train(mla_holdup, df_holdup)
    trained_model_ohnetest = train(mla_holdup, df_train)
    # Speichert Modelle
    joblib.dump(trained_model_holdup, 'pkl-Dateien/MLA_holdup.pkl')
    joblib.dump(trained_model_ohnetest, 'pkl-Dateien/MLA_holdup_ohneTest.pkl')

def make_holdup_predictions(q_d, rho_c, rho_d, eta_c, eta_d, sigma, Kwinkel, D, d_h, a_s, epsilon, cut_off):
    """
    Methode um einzelnen Punkt abzufragen
    :param q_c: Massenstrom CO2 [kg/h]
    :param q_d: Massenstrom Wasser [kg/h]
    :param rho_c: Dichte der kontinuierlichen Phase [kg/m³]
    :param rho_d: Dichte der dispersen Phase [kg/m³]
    :param eta_c: dynamische Viskosität der kontinuierlichen Phase [Pa*s]
    :param eta_d: dynamische Viskosität der dispersen Phase [Pa*s]
    :param sigma: Grenzflächenspannung [mN/m]
    :param Kwinkel: Kontaktwinkel [°]
    :param D: Kolonnendurchmesser [mm]
    :param d_h: hydraulischer Durchmesser [m]
    :param a_s: spezifische Oberfläche pro volumen Packung [m^2/m^3]

    :return: yp_holdup: Volumenanteil der dispersen Phase
    """
    import joblib

    # läd modell
    pipeline_holdup = joblib.load('Holdup/pkl-Dateien/MLA_holdup.pkl')
    # erstellt abfragepunkt
    X = pd.DataFrame(columns=[ "q_d", "$\\rho_{\\textrm{c}}$", "$\\eta_{\\textrm{c}}$",
                                            "$\\rho_{\\textrm{d}}$",
                                            "$\\eta_{\\textrm{d}}$", "$\\sigma$", "Kontaktwinkel", "D",
                                            "d_h", "a_s", "$\\epsilon$",
                                            "Cut-off"])

    X.loc[0, 'q_d'] = float(q_d)
    X.loc[0, '$\\rho_{\\textrm{c}}$'] = float(rho_c)
    X.loc[0, '$\\rho_{\\textrm{d}}$'] = float(rho_d)
    X.loc[0, '$\\eta_{\\textrm{c}}$'] = float(eta_c)
    X.loc[0, '$\\eta_{\\textrm{d}}$'] = float(eta_d)
    X.loc[0, '$\\sigma$'] = float(sigma)
    X.loc[0, 'Kontaktwinkel'] = float(Kwinkel)
    X.loc[0, 'D'] = float(D)
    X.loc[0, 'd_h'] = float(d_h)
    X.loc[0, 'a_s'] = float(a_s)
    X.loc[0, '$\\epsilon$'] = float(epsilon)
    X.loc[0, 'Cut-off'] = float(cut_off)

    yp_holdup = pipeline_holdup.predict(X)

    return float(yp_holdup)


def parityplotdata(writer, df_holdup):

    import joblib

    # läd modell
    pipeline_holdup = joblib.load('Holdup/pkl-Dateien/MLA_holdup_ohneTest.pkl')



    def splitfeaturetarget(df, targetname):
        """
        Methode um Feature und Target-Dataframe zu erstellen
        :param df: Dataframe mit Features und Target
        :return: x, y Dataframes
        """
        import numpy as np
        y = np.array(df[targetname]).reshape(-1, 1).astype(np.float32)
        x = df.drop(columns=[targetname])
        return x, y

    x, y = splitfeaturetarget(df_holdup, targetname="Holdup")
    pred_y = pipeline_holdup.predict(x).reshape(-1, 1)
    df_pred_holdup = pd.DataFrame()
    df_pred_holdup['Actual Value'] = y.ravel()
    df_pred_holdup['Prediction'] = pred_y
    df_pred_holdup.to_excel(writer, sheet_name='Prädiktionen Hold-up')
    x.to_excel(writer, sheet_name='Input Hold-up')
    yp = np.array(df_pred_holdup['Prediction']).reshape(-1, 1).astype(np.float32)
    return y, yp



def korr_stockfleth(df, k1, k2):
    fr2re = fr2_re(df)
    yk = k1 * np.power(fr2re,k2)
    return yk.reshape(-1,1), fr2re.reshape(-1,1)


def fr2_re(df):
    g = 9.81
    rho_c = np.array(df['$\\rho_{\\textrm{c}}$']).reshape(-1, 1).astype(np.float32)
    rho_d = np.array(df['$\\rho_{\\textrm{d}}$']).reshape(-1, 1).astype(np.float32)
    drho = np.subtract(rho_d, rho_c)
    q_d = np.array(df['q_d']).reshape(-1, 1).astype(np.float32)
    d = np.array(df['D']).reshape(-1, 1).astype(np.float32)/1000
    a_s = np.array(df['a_s']).reshape(-1, 1).astype(np.float32)
    eta_d = np.array(df['$\\eta_{\\textrm{d}}$']).reshape(-1, 1).astype(np.float32)
    u_l = q_d/rho_d / np.power(d, 2)/np.pi*4/3600
    # u_l = df['q_d'] / rho_d * np.power(df['D'], 2) * np.pi / 4
    fr2_re = np.power(u_l, 3) * np.power(a_s, 3) * eta_d * rho_d * np.power(drho, -2) / g ** 2
    return fr2_re



# Befehl um Modelle zu trainieren vorher # vor train_models() löschen
#train_models()
# Befehle zum Abfragen eines Punktes
#holdup = make_holdup_predictions(q_d=10, rho_d=996, rho_c=717, eta_c=0.000058, eta_d=0.00065, Kwinkel=130, D=38, d_h=0.000419, a_s=700, cut_off=0.1, sigma=30, epsilon=0.86)
#print(holdup)
