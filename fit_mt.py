from Density_Viscosity import MLA_viscosity_co2_water, MLA_density_co2_water
from Distribution_Coeff import MLA_distributioncoeff_ethanol
from Flooding import MLA_flooding_point_packed_columns
from Sauterdurchmesser import sauterdiameter
from Velocity import velocity
from Submodelle import masstransfercoeff, Diffusion, Kontaktwinkel
from interfacialtension import ift
from Holdup import holdup
from Axiale_Dispersion import use_MLA_axial_dispersion
from Distribution_Coeff import MLA_distributioncoeff_ethanol
import numpy as np
import pandas as pd
import math
import time
import os
import sys
from scipy.integrate import solve_ivp, ode, odeint
from scipy import optimize
import logging
import matplotlib.pyplot as plt
from numba import jit
from multiprocessing import Pool

# Set up -- Logging
logging.basicConfig(format='%(asctime)s :%(name)s:%(levelname)s:  %(message)s',
                    datefmt='%m/%d/%Y %I:%M:%S %p',
                    level=logging.DEBUG,
                    handlers=[
                        logging.FileHandler(filename=f'{os.path.basename(__file__)}.log', mode='w'),
                        logging.StreamHandler(sys.stdout)
                    ]

                    )
LOGGER = logging.getLogger(__name__)
LOGGER.debug("Starting fit_mt.py ...")

LOGGER.debug("... modules were loaded")


def main(n, dt, h, T, p, D, a_s, epsilon, d_h, cutoff, q_c, q_d, y_e_0, y_w_0, x_e_0, x_co2_0, param):
    """
    Hauptmethode
    :param n: Anzahl Stufen
    :param dt: Größe des Zeitschrittes [s]
    :param h: Kolonnenhöhe [m]
    :param T: Temperatur [K]
    :param p: Druck [MPa]
    :param D: Kolonnendurchmesser [m]
    :param a_s: spezifische Oberfläche pro Packung [1/m]
    :param epsilon: Leervolumenanteil Kolonne [-]
    :param d_h: hydraulischer Durchmesser
    :param cutoff: Cut-off [-]
    :param q_c: Massenstrom konti Phase [kg/h]
    :param q_d: Massenstrom disperse Phase [kg/h]
    :param x_e_0: Massenanteil Ethanol im Feedstrom [g/g]
    :param x_co2_0: Massenanteil CO2 im Feedstrom [g/g]
    :param y_e_0: Massenanteil Ethanol im Lösemittel [g/g]
    :param y_w_0: Massenanteil Wasser im Lösemittel [g/g]
    :return:
    """
    LOGGER.debug("Starting main ...")

    LOGGER.debug("Inputs:")
    LOGGER.debug(f"Number of Stages n = {n} [-]")
    LOGGER.debug(f"Timestep dt = {dt}s")
    LOGGER.debug(f"Column Height h = {h}m")
    LOGGER.debug(f"Temperature T = {T}K")
    LOGGER.debug(f"Pressure p = {p}MPa")
    LOGGER.debug(f"Column Diameter D = {D}m")
    LOGGER.debug(f"Specific Surface per Packung a_s = {a_s} 1/m")
    LOGGER.debug(f"Leervolumenanteil epsilon = {epsilon} [-]")
    LOGGER.debug(f"Hydraulic Diameter d_h = {d_h}")
    LOGGER.debug(f"Cut-Off = {cutoff} [-]")
    LOGGER.debug(f"Mass Flow Conti q_c = {q_c} kg/h")
    LOGGER.debug(f"Mass Flow Disperse q_d = {q_d} kg/h")
    LOGGER.debug(f"w_Solvent_Ethanol y_e_0 = {y_e_0} g/g")
    LOGGER.debug(f"w_Solvent_Water y_w_0 = {y_w_0} g/g")
    LOGGER.debug(f"w_Feed_Ethanol x_e_0 = {x_e_0} g/g")
    LOGGER.debug(f"w_Feed_CO2 x_co2_0 = {x_co2_0} g/g")
    LOGGER.debug(f"param: \n{param}")

    # Zeitmessung starten
    start = time.time()
    # erstellt Zeitstempel
    tlocal = time.localtime()
    current_time = time.strftime("%y%m%d__%H_%M_%S", tlocal)
    #fig_path = 'fig//' + str(current_time) + 'Heydrich'
    #os.mkdir(fig_path)

    # initialisiert Excel-Schreiber
    options = {}
    options['strings_to_formulas'] = False
    options['strings_to_urls'] = False
    #excel_path = fig_path + '//Data' + str(current_time) + '.xlsx'
    #writer = pd.ExcelWriter(excel_path, options=options)

    pbar = p*10
    # Geometrische Größen
    LOGGER.debug("Geometric Quantities:")
    z = h/n
    LOGGER.debug(f"z = {z}")
    d_p = 6*(1-epsilon)/a_s
    LOGGER.debug(f"Packung als monodisperse Schüttung d_p = {d_p}")
    S = math.pi * D**2 / 4
    LOGGER.debug(f"S = {S}")
    V_K = S*h*epsilon # Kolonnenvolumen, durch welches Fluid fließen kann, daher epsilon
    LOGGER.debug(f"Kolonnenvolumen V_K = {V_K} m^3")

    # Stoffdaten
    LOGGER.debug("Component Data:")
    M_co2 = 44.01  # [g/mol]
    M_w = 18.01528  # [g/mol]
    M_e = 46.07 # [g/mol]
    rho_c, rho_d = MLA_density_co2_water.make_density_predictions_hp(T=T, p=p)
    LOGGER.debug(f"rho_c = {rho_c}")
    LOGGER.debug(f"rho_d = {rho_d}")
    eta_c, eta_d = MLA_viscosity_co2_water.make_viscosity_predictions_hp(T=T, p=p)
    LOGGER.debug(f"eta_c = {eta_c}")
    LOGGER.debug(f"eta_d = {eta_d}")

    #Diffusion
    LOGGER.debug("Diffusion:")
    phi_w = 2.6 # Assozationsfaktor des Solvents (2.6-1)
    V_e, V_w, V_co2 = Diffusion.getVeb()
    LOGGER.debug(f"V_e = {V_e}")
    LOGGER.debug(f"V_w = {V_w}")
    LOGGER.debug(f"V_co2 = {V_co2}")
    D_co2 = Diffusion.SassiatMourier(T, M_co2, V_co2, eta_c)
    D_w_co2 = Diffusion.SassiatMourier(T, M_w, V_w, eta_c)
    D_e_co2 = Diffusion.SassiatMourier(T, M_e, V_e, eta_c)
    D_e_w = Diffusion.WilkeChang(T, M_w, V_e, eta_d, phi_w)
    D_co2_w = Diffusion.WilkeChang(T, M_w, V_co2, eta_d, phi_w)
    D_w = Diffusion.WilkeChang(T, M_w, V_w, eta_d, phi_w)
    LOGGER.debug(f"SassiatMourier D_co2 = {D_co2} cm^2/s")
    LOGGER.debug(f"SassiatMourier D_w_co2 = {D_w_co2}")
    LOGGER.debug(f"SassiatMourier D_e_co2 = {D_e_co2}")
    LOGGER.debug(f"WilkeChang D_e_w = {D_e_w}")
    LOGGER.debug(f"WilkeChang D_co2_w = {D_co2_w}")
    LOGGER.debug(f"WilkeChang D_w = {D_w}")

    #----------------------------------------
    # Massenanteil Ethanol im Feed in molaren Anteil der Gesamtkompostion berechnen
    # Annahme: kein Ethanol oder Wasser in CO2
    # wird nicht gebraucht
    LOGGER.debug("Massenanteil Ethanol in Stoffmengenanteil als Teil der Gesamtmasse:")
    z_ethanol_0 = q_d/(q_c+q_d) * x_e_0
    z_wasser_0 = q_d/(q_c+q_d) * (1 - x_e_0 - x_co2_0)
    z_co2_0 = 1-z_ethanol_0-z_wasser_0
    z_ethanol_mol = (z_ethanol_0/M_e)/(z_ethanol_0/M_e+z_co2_0/M_co2+z_wasser_0/M_w)
    LOGGER.debug(f"z_ethanol_0 = {z_ethanol_0}")
    LOGGER.debug(f"z_wasser_0 = {z_wasser_0}")
    LOGGER.debug(f"z_co2_0 = {z_co2_0}")
    LOGGER.debug(f"z_ethanol_mol = {z_ethanol_mol}")

    kwinkel = 130
    # sigma = ift.chunwilkinson(rho_c=rho_c, x_ethanol=z_ethanol_mol)
    #sigma = ift.make_ift_prediction(T, p, z_ethanol_mol)
    sigma = 30
    #--------------------------------------

    # Holdup, Sauterdurchmesser, Stoffaustauschfläche
    #hl = holdup.make_holdup_predictions(q_d=q_d, rho_d=rho_d, rho_c=rho_c, eta_c=eta_c, eta_d=eta_d, Kwinkel=kwinkel, D=D*1000, d_h=d_h, a_s=a_s, cut_off=cutoff, sigma=sigma, epsilon=epsilon)
    #d32 = sauterdiameter.make_sauterdiameter_predictions_hp(q_c=q_c, q_d=q_d, rho_d=rho_d, rho_c=rho_c, eta_c=eta_c, eta_d=eta_d, Kwinkel=kwinkel, D=D*1000, d_h=d_h, a_s=a_s, cut_off=cutoff, sigma=sigma, epsilon=epsilon)/1000
    #nach Heydrich
    LOGGER.debug("Sauterdurchmesser:")
    d32=4.02*((q_c+q_d)/3600/S)**(-0.34)*x_e_0**(-0.51)/1000 # /1000 da die Korrelation d32 in mm ausgibt eigentlich
    LOGGER.debug(f"d32 = {d32} m")

    # Geschwindigkeiten
    LOGGER.debug("Geschwindigkeiten:")
    pv = q_c/q_d
    v_d_leer = q_d / 3600 / rho_d / S
    v_c_leer = q_c / 3600 / rho_c / S
    LOGGER.debug(f"Phase-Ratio Conti(CO2)/Disperse(H20) pv = {pv} [-]")
    LOGGER.debug(f"v_d_leer = {v_d_leer} m/s")
    LOGGER.debug(f"v_c_leer = {v_c_leer} m/s")
    #v_c = velocity.make_v_c_prediction(pbar, T, q_c, q_d, rho_c, eta_c*1e6, D, a_s, epsilon)
    #v_d = velocity.make_v_d_prediction(pbar, T, q_c, q_d, pv, rho_c, eta_c, rho_d, eta_d, D, a_s, epsilon)

    # Holdup, Sauterdurchmesser, Stoffaustauschfläche
    LOGGER.debug("Holdup:")
    #frl = (v_d_leer ** 2) * (a_s + 160) * rho_d / (rho_d * 9.81)
    #re_d = reynolds(rho_d, eta_d, v_d_leer, 1 / (a_s + 160))
    # Holdup Stockfleth (gefittete Parameter)
    #hl = 1.12110162527996 * (frl ** 2 / re_d) ** 0.222984619

    # angepasster Kumar
    c1 = 0.9912
    c2 = 0.86 ** -0.837
    c3 = ((rho_d - rho_c) / rho_c) ** -0.9999
    c4 = ((1 / 700) * ((((rho_c ** 2) * 9.81) / (eta_c ** 2)) ** (1 / 3))) ** -0.99999
    c5 = (eta_d / eta_c) ** 0.6254
    c6 = ((v_d_leer * ((rho_c / (9.81 * eta_c)) ** (1 / 3)))) ** 0.40441
    c7 = math.exp(0.6696 * v_c_leer * ((rho_c / (9.81 * eta_c)) ** (1 / 3)))
    c8 = ((eta_c / (((rho_c * 70 * 10 ** -3) / (700)) ** 0.5)) ** -0.4091)
    hl = c1 * c2 * c3 * c4 * c5 * c6 * c7 * c8 / 100
    LOGGER.debug(f"c1 = {c1}")
    LOGGER.debug(f"c2 = {c2}")
    LOGGER.debug(f"c3 = {c3}")
    LOGGER.debug(f"c4 = {c4}")
    LOGGER.debug(f"c5 = {c5}")
    LOGGER.debug(f"c6 = {c6}")
    LOGGER.debug(f"c7 = {c7}")
    LOGGER.debug(f"c8 = {c8}")
    LOGGER.debug(f"Holdup hl = {hl} [-]")
    #Brockkötter Korrelation
    #hl = 1.3 * (frl ** 2 / re_d) ** 0.23
    #eigene Korrelation
    #hl = 1.03 * (a_s / epsilon) * (rho_c / (rho_d - rho_c)) * (eta_d / eta_c) * ((v_d_leer * v_c_leer) / 9.81) ** 0.819


    LOGGER.debug("Surface for Mass Transfer:")
    masse_d = V_K * hl * rho_d # V_K = Leervolumen der Kolonne, wo FLuid reinfließen kann
    masse_c = V_K * (1 - hl) * rho_c
    # nTropfen = hl * (S * epsilon) / (d32 ** 3 / 6 * math.pi) # V_dispersePhase / V_Tropfen # !!!!!!!!!!!!!!!!!!!!! der Zähler wird nicht m^3
    V_einzeltropfen = d32 ** 3 / 6 * math.pi
    V_dispers = V_K * hl
    nTropfen = V_dispers / V_einzeltropfen
    A = nTropfen * d32 ** 2 * math.pi
    LOGGER.debug(f"masse_d = {masse_d} kg")
    LOGGER.debug(f"masse_c = {masse_c} kg")
    LOGGER.debug(f"V_einzeltropfen = {V_einzeltropfen} m^3")
    LOGGER.debug(f"V_dispers = {V_dispers} m^3")
    LOGGER.debug(f"nTropfen = {nTropfen} [-]")
    LOGGER.debug(f"Gesamtoberfläche Tropfen A = {A} m^2")

    # mechanistisch korreliert/normal
    LOGGER.debug("Mechanistic Correlation:")
    # stat ist ein Anpassungsparameter 'gesamte parameter Saeger Excel'
    # Strömungsrohr Verjüngung --> Effektive Geschwindigkeit wird schneller als Leerrohrgeschw
    stat = 4.773359091  # CY ##Fabian
    # stat = 8.592192302 # Mellapak ##Fabian
    LOGGER.debug(f"stat = {stat}")
    v_d = q_d / (rho_d * S * 3600 * epsilon * hl*stat) #Fabian # theoretisch *cos(45°) aus Korrelation nehmen, wegen Neigungswinkel der Kreuzkanäle (Heydrich, 1999)
    v_c = q_c / (rho_c * S * 3600 * epsilon * (1 - hl*stat)) #Fabian
    v_s = (v_c + v_d)
    LOGGER.debug(f"Effektive Geschw. v_d = {v_d} m/s")
    LOGGER.debug(f"Effektive Geschw. v_c = {v_c} m/s")
    LOGGER.debug(f"Relativgeschw. v_s = {v_s} m/s")

    # Reynolds und Schmidtzahl
    LOGGER.debug("Dimensionless Numbers:")
    re_sc = reynolds(rho_c, eta_c, v_c, d_p) # hier mit effektiver Geschwindigkeit
    re_d = reynolds(rho_d, eta_d, v_d, 1/a_s) # hier mit effektiver Geschwindigkeit
    sc_c = schmidt(rho_c, eta_c, D_co2)
    sc_d = schmidt(rho_d, eta_d, D_w)
    LOGGER.debug(f"Reynolds Conti(CO2) re_sc = {re_sc} [-]")
    LOGGER.debug(f"Reynolds Disperse(H20) re_d = {re_d} [-]")
    LOGGER.debug(f"Schmidt Conti(CO2) sc_c = {sc_c} [-]")
    LOGGER.debug(f"Schmidt Disperse(H20) sc_d = {sc_d} [-]")


    # axiale Dispersion in [m^2/s]
    LOGGER.debug("Axiale Dispersion:")
    pe = use_MLA_axial_dispersion.make_axial_dispersion_prediction(re_sc, sc_c)
    dax = v_c * h / pe # mit effektiver Geschwindigkeit
    LOGGER.debug(f"Pecletzahl Pe = {pe} [-]")
    LOGGER.debug(f"Axialer Dispersionskoeffizient D_Ax dax = {dax} [m^2/s]")

    # Massentransferkoeffizienten bestimmen
    LOGGER.debug("Mass Transfer Coefficients:")
    fit_c = param[0:3] # P3, P4, P5 in den Korrelationen von Seibert
    fit_d = param[3:5] # P1, P2 in den Korrelationen von Seibert
    k_c_co2 = masstransfercoeff.mtcoeff_c_seibert(D_co2, d32, v_s, rho_c, eta_c, hl, [2, 0.983, 0.997])
    k_d_co2 = masstransfercoeff.mtcoeff_d_seibert(D_co2_w, v_s, rho_d, eta_c, eta_d, [0.02, 0.023])
    k_c_w = masstransfercoeff.mtcoeff_c_seibert(D_w_co2, d32, v_s, rho_c, eta_c, hl, [0.3, 0.1, 0.39274044])
    k_d_w = masstransfercoeff.mtcoeff_d_seibert(D_w, v_s, rho_d, eta_c, eta_d, [0.001, 0.023])
    k_c_e = masstransfercoeff.mtcoeff_c_seibert(D_e_co2, d32, v_s, rho_c, eta_c, hl, fit_c)
    k_d_e = masstransfercoeff.mtcoeff_d_seibert(D_e_w, v_s, rho_d, eta_c, eta_d, fit_d)
    LOGGER.debug(f"k_c_co2 = {k_c_co2} [kg/s*m^2]")
    LOGGER.debug(f"k_d_co2 = {k_d_co2} [kg/s*m^2]")
    LOGGER.debug(f"k_c_w  = {k_c_w } [kg/s*m^2]")
    LOGGER.debug(f"k_d_w = {k_d_w} [kg/s*m^2]")
    LOGGER.debug(f"k_c_e = {k_c_e} [kg/s*m^2]")
    LOGGER.debug(f"k_d_e = {k_d_e} [kg/s*m^2]")



    def rhs_fast(t, u, k):
        #LOGGER.debug("Starting rhs_fast ...")
        """
        DGL-System (K- und J-Berechnung ausgelagert für schnellere Berechnung)
        """
        # Vektor anlegen
        rhs_fast = np.zeros(4 * (n + 1))
        je = k[0]
        jco2 = k[1]
        jw = k[2]
        # y_ethanol 0 - n
        # y_wasser n+1 - 2n+1
        # x_ethanol 2n+2 - 3n+2
        # x_co2 3n+3 - 4n+3
        # rhs = dyi/dt
        # u[i] = y an Ort i

        # Kompartments zwischen Boden und Kopf
        for i in range(1, n):  # 1....n-1
            i1 = i + n + 1
            i2 = i + 2 * (n + 1)
            i3 = i + 3 * (n + 1)
            # Massentransfer bestimmen

            rhs_fast[i] = (v_c / z) * (u[i - 1] - u[i]) + (dax / (2*(z ** 2))) * (u[i + 1] - 2 * u[i] + u[i - 1]) \
                          + je[i] / (h * S * epsilon * (1 - hl) * rho_c)

            rhs_fast[i1] = (v_c / z) * (u[i1 - 1] - u[i1]) + (dax / (2*(z ** 2))) * (u[i1 + 1] - 2 * u[i1] + u[i1 - 1]) \
                           + jw[i] / (h * S * epsilon * (1 - hl) * rho_c)
            rhs_fast[i2] = (v_d / z) * (u[i2 + 1] - u[i2]) \
                           - je[i] / (h * S * epsilon * hl * rho_d)

            rhs_fast[i3] = (v_d / z) * (u[i3 + 1] - u[i3]) \
                           - jco2[i] / (h * S * epsilon * hl * rho_d)

        # RANDBEDINGUNGEN
        # Eingang (konti)
        rhs_fast[0] = (dax / z * rhs_fast[1]) / (v_c + dax / z)
        rhs_fast[n + 1] = (dax / z * rhs_fast[n + 2]) / (v_c + dax / z)
        # Ausgang (dispers)
        rhs_fast[2 * (n + 1)] = rhs_fast[2 * (n + 1) + 1]  # dx0dt = dx1dt
        rhs_fast[3 * (n + 1)] = rhs_fast[3 * (n + 1) + 1]
        # Ausgang (konti)
        rhs_fast[n] = rhs_fast[n - 1]  # dyN/dt=dyN-1/dt
        rhs_fast[2 * n + 1] = rhs_fast[2 * n]
        # Eingang (dispers)
        rhs_fast[3 * n + 2] = 0  # dxN/dt=0 da xN=xein (konst)
        rhs_fast[4 * n + 3] = 0

        #LOGGER.debug("... Finished rhs_fast")
        return rhs_fast

    def rhs_fast_fit(t, u, k):
        """
        DGL-System (K- und J-Berechnung ausgelagert für schnellere Berechnung)
        """
        #LOGGER.debug("Starting rhs_fast_fit ...")
        # Vektor anlegen
        rhs_fast = np.zeros(4 * (n + 1))
        je = np.zeros(n)
        jw = np.zeros(n)
        jco2 = np.zeros(n)
        Ke = k[0]
        Kco2 = k[1]
        Kw = k[2]
        # y_ethanol 0 - n
        # y_wasser n+1 - 2n+1
        # x_ethanol 2n+2 - 3n+2
        # x_co2 3n+3 - 4n+3
        # rhs = dyi/dt
        # u[i] = y an Ort i

        # Kompartments zwischen Boden und Kopf
        for i in range(1, n):  # 1....n-1, da i=0 und i=n durch RB festgelegt sind
            # Für jede Komponente einen eigenen Arrayabschnitt von 0 bis n --> Matrix ausgeschrieben als langer Vektor
            # i geht von 0 bis n, i1 geht von n+1 bis (n+1)+(n)=2(n+1)-1, i2 daher von 2(n+1) bis 2(n+1)+n = 3(n+1)-1, i3 von 3(n+1) bis 3(n+1)+n
            # i für y_1, i1 für y_2, i2 für x_1, i3 für x_3
            i1 = i + 1 * (n + 1)
            i2 = i + 2 * (n + 1)
            i3 = i + 3 * (n + 1)
            # Massentransfer bestimmen
            je[i] = diffstrom_i(A, rho_c, rho_d, k_c_e, k_d_e, u[i2], u[i], Ke)
            jw[i] = diffstrom_i(A, rho_c, rho_d, k_c_w, k_d_w, 1 - u[i2] - u[i3], u[i1], Kw)
            jco2[i] = diffstrom_i(A, rho_c, rho_d, k_c_co2, k_d_co2, u[i3], 1 - u[i] - u[i1], Kco2)

            # Without /2 in second term
            # rhs_fast[i] = (v_c / z) * (u[i - 1] - u[i]) + (dax / z ** 2) * (u[i + 1] - 2 * u[i] + u[i - 1]) + je[i] / (
            #         h * S * epsilon * (1 - hl) * rho_c)
            #
            # rhs_fast[i1] = (v_c / z) * (u[i1 - 1] - u[i1]) + (dax / z ** 2) * (u[i1 + 1] - 2 * u[i1] + u[i1 - 1]) + jw[i] / (
            #         h * S * epsilon * (1 - hl) * rho_c)
            # rhs_fast[i2] = (v_d / z) * (u[i2 + 1] - u[i2]) - je[i] / (h * S * epsilon * hl * rho_d)
            #
            # rhs_fast[i3] = (v_d / z) * (u[i3 + 1] - u[i3]) - jco2[i] / (h * S * epsilon * hl * rho_d)

            # With /2 in second term
            rhs_fast[i] = (v_c / z) * (u[i - 1] - u[i]) + (dax / (2*(z ** 2))) * (u[i + 1] - 2 * u[i] + u[i - 1]) + je[i] / (
                    h * S * epsilon * (1 - hl) * rho_c)

            rhs_fast[i1] = (v_c / z) * (u[i1 - 1] - u[i1]) + (dax / (2*(z ** 2))) * (u[i1 + 1] - 2 * u[i1] + u[i1 - 1]) + jw[i] / (
                    h * S * epsilon * (1 - hl) * rho_c)
            rhs_fast[i2] = (v_d / z) * (u[i2 + 1] - u[i2]) - je[i] / (h * S * epsilon * hl * rho_d)

            rhs_fast[i3] = (v_d / z) * (u[i3 + 1] - u[i3]) - jco2[i] / (h * S * epsilon * hl * rho_d)

        # RANDBEDINGUNGEN (also für i=0 und i=n im Kompartment)
        # Eingang (konti)
        rhs_fast[0] = (dax / z * rhs_fast[1]) / (v_c + dax / z) # i(i=0)=0 und i(i=1)=1
        rhs_fast[n + 1] = (dax / z * rhs_fast[n + 2]) / (v_c + dax / z) # i1(i=0)=i+1(n+1)=n+1 und i1(i=1)=i+1(n+1)=1+1(n+1)=n+2
        # Ausgang (dispers)
        # dx0dt = dx1dt
        rhs_fast[2 * (n + 1)] = rhs_fast[2 * (n + 1) + 1]  # i2(i=0)=i+2(n+1)=2(n+1) und i2(i=1)=i+2(n+1)=1+2(n+1)
        rhs_fast[3 * (n + 1)] = rhs_fast[3 * (n + 1) + 1]  # i3(i=0)=i+3(n+1)=3(n+1) und i3(i=1)=i+3(n+1)=1+3(n+1)
        # Ausgang (konti)
        # dyN/dt=dyN-1/dt
        rhs_fast[n] = rhs_fast[n - 1]  # i(i=n) = n und i(i=n-1)=n-1
        rhs_fast[2 * n + 1] = rhs_fast[2 * n] # i1(i=n)=i+1(n+1)=2n+1 und i1(i=n-1)=i+1(n+1)=n-1+1(n+1)=2n
        # Eingang (dispers)
        # dxN/dt=0 da xN=xein (konst)
        rhs_fast[3 * n + 2] = 0 # i2(i=n) = i+2(n+1) = n+2(n+1) = 3n+2
        rhs_fast[4 * n + 3] = 0 # i3(i=n) = i+3(n+1) = n+3(n+1) = 4n+3

        #LOGGER.debug("... Finished rhs_fast_fit")
        return rhs_fast

    def j_zwischen(u):
        """
        ausgelagerte Berechnung von Ji
        :param u: Vektor mit Massenanteilen für alle Kompartments
        :return: j_ethanol, j_co2, j_wasser
        """
        LOGGER.debug("Starting j_zwischen ...")
        je = np.zeros(n+1)
        jw = np.zeros(n+1)
        jco2 = np.zeros(n+1)
        z_ethanol = np.zeros(n+1)
        z_wasser = np.zeros(n+1)
        z_co2 = np.ones(n+1)
        z_ethanol[1:n-1+1] = (masse_d * u[2*n+3:3*n+1+1] + masse_c * u[1:n-1+1]) / (masse_c + masse_d)
        z_wasser[1:n-1+1] = (masse_d * (1 - u[2*n+3:3*n+1+1] - u[3*n+4:4*n+2+1]) + masse_c * u[n+2:2*n+1]) / (masse_c + masse_d)
        z_co2 = z_co2 - z_ethanol - z_wasser
        Ke, dummy2, dummy3 = MLA_distributioncoeff_ethanol.make_distributioncoeff_predictions_hp(T, p, z_co2[1], z_wasser[1], z_ethanol[1])
        Ke = Ke
        Kco2 = dummy2
        Kw = dummy3
        for i in range(1, n+1):  # 1....n-1
            i1 = i + n + 1
            i2 = i + 2 * (n + 1)
            i3 = i + 3 * (n + 1)
            # Verteilungskoeff bestimmen
            # globale Zusammensetzung :todo Methode schreiben

            dummy, Kco2, Kw = MLA_distributioncoeff_ethanol.make_distributioncoeff_predictions_hp(T,p, z_co2[i], z_wasser[i], z_ethanol[i])

            je[i] = diffstrom_i(A, rho_c, rho_d, k_c_e, k_d_e, u[i2], u[i], Ke)
            jw[i] = diffstrom_i(A, rho_c, rho_d, k_c_w, k_d_w, 1 - u[i2] - u[i3], u[i1], Kw)
            jco2[i] = diffstrom_i(A, rho_c, rho_d, k_c_co2, k_d_co2, u[i3], 1 - u[i] - u[i1], Kco2)

        LOGGER.debug("... Finished j_zwischen")
        return je, jco2, jw

    def solve():
        LOGGER.debug("Starting solve ...")
        daten = pd.DataFrame()
        # u = xi(z),yi(z)
        u = np.zeros(4*(n+1))
        # u0 Startwerte für Solver
        u0 = np.zeros(4*(n+1))
        u0[0] = y_e_0
        u0[n+1] = y_w_0
        u0[3*n+2] = x_e_0
        u0[4*n+3] = x_co2_0
        t=0
        ende = 41
        y_e_exit = np.zeros((1000,2))
        success = True
        # Änderung < tol = stationärer Zustand
        tol = 5e-3/dt/n
        deltay = 1  # dummy
        i = 0
        #for i in range(1, ende, 1):   # (a, b, c) a bis b-1 mit Schrittweite c
        while success and deltay > tol:  # Solver erfolgreich und instationär
            i = i+1
            LOGGER.debug(f"Begin while i: {i} ...")
            sol = solve_ivp(rhs_fast, [t, t+dt], u0, method='BDF', t_eval=[t+dt], args=[j_zwischen(u0)])
            success = sol.success
            deltay = max(sol.y[:,0]-u0)  # größte Änderung zum vorherigen Zeitschritt
            LOGGER.debug(f"deltay: {deltay}")
            t = i * dt
            # Lösungen werden Startwerte für nächsten Zeitschritt

            u0 = sol.y[:, 0]
            y_e_exit[i, 0] = u0[n]
            y_e_exit[i, 1] = t
            y_ethanol = u0[0:n+1]
            y_wasser = u0[n + 1:2 * n + 1+1]
            x_ethanol = u0[2 * (n + 1):3 * n + 2+1]
            x_co2 = u0[3 * (n + 1):4 * n + 3+1]
            x_w = np.ones(n+1) - x_ethanol - x_co2
            y_co2 = np.ones(n+1) - y_ethanol - y_wasser

            # plotten

            daten['y_Ethanol'] = y_ethanol
            daten['y_wasser'] = y_wasser
            daten['y_CO2'] = y_co2

            daten['x_Ethanol'] = x_ethanol
            daten['x_CO2'] = x_co2
            daten['x_Wasser'] = x_w
            #daten.to_excel(excel_writer=writer, sheet_name='Zeit = ' + str(t))

            data = [y_ethanol[n-1], y_co2[n-1], x_ethanol[0], x_w[0], y_ethanol[0], y_co2[0], x_ethanol[n-1], x_w[n-1], pv, x_ethanol[9], x_ethanol[20], x_ethanol[28]]
            LOGGER.debug(f"... End while i: {i}")

        LOGGER.debug("... Finished solve")
        return data

    def solve_fast():
        LOGGER.debug("Starting solve_fast ...")
        daten = pd.DataFrame()
        # u = xi(z),yi(z)
        u = np.zeros(4*(n+1))
        # u0 Startwerte für Solver
        u0 = np.zeros(4*(n+1))
        u0[0] = y_e_0
        u0[n+1] = y_w_0
        u0[3*n+2] = x_e_0
        u0[4*n+3] = x_co2_0
        t=0
        ende = 41
        y_e_exit = np.zeros((20000,2))
        success = True
        # Änderung < tol = stationärer Zustand
        tol = 5e-3/dt/n
        LOGGER.debug(f"tol: {tol}")
        deltay = 1  # dummy
        i = 0
        z_ethanol = np.zeros(n)
        z_wasser = np.zeros(n)
        z_co2 = np.ones(n)
        z_ethanol[1:n - 1] = (masse_d * u[2 * n + 3:3 * n + 1] + masse_c * u[1:n - 1]) / (masse_c + masse_d)
        z_wasser[1:n - 1] = (masse_d * (1 - u[2 * n + 3:3 * n + 1] - u[3 * n + 4:4 * n + 2]) + masse_c * u[n + 2:2 * n]) / (
                    masse_c + masse_d)
        z_co2 = z_co2 - z_ethanol - z_wasser
        df_z = pd.DataFrame(data={'z_co2': z_co2, 'z_ethanol': z_ethanol, 'z_wasser': z_wasser})
        LOGGER.debug(f"mass weights z: \n{df_z}")
        Ke, Kco2, Kw = MLA_distributioncoeff_ethanol.make_distributioncoeff_predictions_hp(T,
                                                                                           p,
                                                                                           z_co2[1],
                                                                                           z_wasser[1],
                                                                                           z_ethanol[1])
        LOGGER.debug(f"Ke: {Ke}")
        LOGGER.debug(f"Kco2: {Kco2}")
        LOGGER.debug(f"Kw: {Kw}")

        #for i in range(1, ende, 1):   # (a, b, c) a bis b-1 mit Schrittweite c
        while success and deltay > tol and i<2160:  # Solver erfolgreich und instationär
            i = i+1
            LOGGER.debug(f"Begin while i {i} ...")
            sol = solve_ivp(rhs_fast_fit, [t, t+dt], u0, method='BDF', t_eval=[t+dt], args=[[Ke, Kco2, Kw]])
            success = sol.success
            LOGGER.debug(f"solve_ivp.success: {success}")
            deltay = max(sol.y[:,0]-u0)  # größte Änderung zum vorherigen Zeitschritt
            LOGGER.debug(f"deltay: {deltay}")
            t = i * dt
            # Lösungen werden Startwerte für nächsten Zeitschritt

            u0 = sol.y[:, 0]
            y_e_exit[i, 0] = u0[n]
            y_e_exit[i, 1] = t
            y_ethanol = u0[0:n+1]
            y_wasser = u0[n + 1:2 * n + 1+1]
            x_ethanol = u0[2 * (n + 1):3 * n + 2+1]
            x_co2 = u0[3 * (n + 1):4 * n + 3+1]
            x_w = np.ones(n+1) - x_ethanol - x_co2
            y_co2 = np.ones(n+1) - y_ethanol - y_wasser

            # plotten

            daten['y_Ethanol'] = y_ethanol
            daten['y_wasser'] = y_wasser
            daten['y_CO2'] = y_co2

            daten['x_Ethanol'] = x_ethanol
            daten['x_CO2'] = x_co2
            daten['x_Wasser'] = x_w
            #daten.to_excel(excel_writer=writer, sheet_name='Zeit = ' + str(t))
            # Stützstellen bei 116cm, 250cm, 347cm
            # entspricht Kompartment 9, 20, 28
            data = [y_ethanol[n-1+1], y_co2[n-1+1], x_ethanol[0], x_w[0], y_ethanol[0], y_co2[0], x_ethanol[n-1+1], x_w[n-1+1], pv]
            LOGGER.debug(f"... End while i {i}")

        LOGGER.debug("... Finished solve_fast")
        return data

    #_fast für konstanten Verteilungskoeff, für variablen Verteilungskoeff wegmachen
    model = solve_fast()


    # Excel speichern
    #writer.save()
    #writer.close()
    # Stoppen und Ausgeben der Zeit
    end = time.time()
    runtime = (end - start) / 60
    LOGGER.debug(f"Gesamtzeit: {runtime} min")
    print("Gesamtzeit: \n%.1f Minuten " % runtime)
    LOGGER.debug(f"main.model: \n y_EtOH_n, y_CO2_n, x_EtOH_0, x_W_0, y_EtOH_0, y_CO2_0, x_EtOH_n, x_W_n, PV \n{model}")
    LOGGER.debug("... Finished main")
    return model
# Massentransfer nach dem Zweischichtmodell


def diffstrom_i(A, rho_c, rho_d, k_ci, k_di, xi, yi, Ki):
    """
    Massentransferstrom für Komponente i
    :param A: Stoffaustauschfläche [m^2]
    :param rho_c: Dichte der kontinuierlichen Phase [kg/m^3]
    :param rho_d: Dichte der dispersen Phase [kg/m^3]
    :param k_ci: Massentransferkoeff konti Phase Stoff i [m/s]
    :param k_di: Massentransferkoeff disperse Phase Stoff i [m/s]
    :param xi: Massenanteil Stoff i in konti Phase
    :param yi: Massenanteil Stoff i in disp Phase
    :param Ki: massenbezogener Verteilungskoeff Stoff i
    :return: ji: Massentransferstrom für Stoff i [kg/s]
    """
    #beta = 1/(1/(rho_d*k_di) + Ki/(rho_c*k_ci))
    #LOGGER.debug("Starting diffstrom_i ...")
    beta = 1 / (1 / (rho_c * k_ci) + Ki / (rho_d * k_di))
    ji = A * beta * (Ki * xi - yi)
    #LOGGER.debug(f"diffstrom ji = {ji}")
    #LOGGER.debug("... Finished diffstrom_i")
    return ji


def reynolds(rho, eta, v, d):
    """
    Bestimmt Reynoldszahl
    :param rho: Dichte [kg/m^3]
    :param eta: Viskosität [Pas]
    :param v: Geschwindigkeit [m/s]
    :param d: charakteristische Länge [m]
    :return: Reynoldszahl [-]
    """
    LOGGER.debug("Starting reynolds ...")
    LOGGER.debug(f"reynolds number Re = {rho*v*d/eta}")
    LOGGER.debug("... Finished reynolds")
    return rho*v*d/eta


def schmidt(rho, eta, diff):

    """
    Bestimme Schmidtzahl
    :param rho: Dichte [kg/m^3]
    :param eta: Viskosität [Pas]
    :param diff: Diffusionskoeff [m^2/s]
    :return: Schmidtzahl [-]
    """
    LOGGER.debug("Starting schmidt ...")
    LOGGER.debug(f"schmidt number Sc = {eta/(rho*diff)}")
    LOGGER.debug("... Finished schmidt")
    return eta/(rho*diff)


def hets_kbs(data, h, m):
    """
    Nth nach Kremser-Brown-Souders-Gleichung
    :param data: array mit [y_ethanolout,y_co2out, x_ethanolout, x_wasserout, y_ethanolin,y_co2in, x_ethanolin, x_wasserin, q_c, q_d, sf, holdup]
    :param h: Kolonnenhöhe [m]
    :param m: durschnittlicher Ke-Wert
    :return: HETS-Wert
    """
    LOGGER.debug("Starting hets_kbs ...")
    x_f = data[6]
    x_r = data[2]
    y_a = data[4]
    y_w = data[1]
    lam = data[8] * m

    zähler = (x_f-y_a/m)/(x_r-y_a/m) * (1-1/lam) + 1/lam
    nth = np.log(zähler) / np.log(lam)
    if nth < 1:
        nth = 1
    hets = h/nth
    LOGGER.debug(f"HETS = {hets}")
    LOGGER.debug("... Finished hets_kbs")
    return hets


def scorefit(param):
    LOGGER.debug("Starting scorefit ...")
    messwerte = []
    # for every measuring point in Fit_XXX.xlsx, main is executed with those specific data points
    for g in range(0, length, 1):
        # n = 35 was the default
        c = (100, 10, fit_H[g], fit_T[g], fit_p[g], fit_D[g], fit_as[g], fit_eps[g], fit_dh[g], 0.1, fit_mc[g], fit_mw[g],
        fit_ye0[g], fit_yw0[g], fit_xe0[g], fit_xco20[g], param)
        # n, dt, h, T, p, D, a_s, epsilon, d_h, cutoff, q_c, q_d, y_e_0, y_w_0, x_e_0, x_co2_0, param
        messwerte.append(c)
    # [y_ethanol_exit, y_co2_exit, x_ethanol_exit, x_w_exit, y_ethanol_in, y_co2_in, x_ethanol_in, x_w_in, pv, x_ethanol_s1, x_ethanol_s2, x_ethanols_3]
    # one single 'main'-output which is 'model': [y_EtOH_n, y_CO2_n, x_EtOH_0, x_W_0, y_EtOH_0, y_CO2_0, x_EtOH_n, x_W_n, PV]
    model = np.array(p.starmap(main, messwerte)) # 'length'-times the 'main'-output which is 'model' with this 9 entries
    LOGGER.debug(f"scorefit.model: \n{model}")

    beladung_model = np.array((model[:, 0]/(1-model[:, 1]), model[:, 2]/(model[:, 2] + model[:, 3])))
    LOGGER.debug(f"beladung_model before transpose: \n{beladung_model}")
    beladung_model = np.transpose(beladung_model)
    LOGGER.debug(f"beladung_model after transpose: \n{beladung_model}")
    score = (abs((beladung_data-beladung_model))/beladung_data).ravel()
    LOGGER.debug(f"score: \n{score}")

    LOGGER.debug("... Finished scorefit")
    return score

if __name__ == '__main__':
    LOGGER.debug("Starting __name__ == __main__ ...")
    s = time.time()
    # Anzahl der parallelen Threads
    p = Pool(8)

    # Choose your packing:
    #filepath = f"{os.path.dirname(__file__)}/Fit_CY.xlsx"
    # filepath = f"{os.path.dirname(__file__)}/Fit_Mella.xlsx"
    filepath = 'Fit_Mella.xlsx'

    LOGGER.debug(f"filepath: {filepath}")
    # fit_daten = pd.read_excel(filepath,
    #                           usecols=[1,2,3,4,5,6,7,8,9,10,11,12,13,17,18],
    #                           names=['H','T','p','D','a_s','epsilon','d_h','mco2','mw','ye0','yw0','xe0','xco20','yeend','xeend'],
    #                           skiprows=[0,1])
    fit_daten = pd.read_excel(filepath,
                              header= None,
                              names=['H', 'T', 'p', 'D', 'a_s', 'epsilon', 'd_h', 'mco2', 'mw', 'ye0', 'yw0', 'xe0',
                                      'xco20', 'yeend', 'xeend'],
                              usecols=[1,2,3,4,5,6,7,8,9,10,11,12,13,17,18],
                              skiprows=[0,1,2]
                              )
    fit_daten = fit_daten.dropna()
    LOGGER.debug(f"fit_daten: \n{fit_daten}")
    length = fit_daten.index.size
    LOGGER.debug(f":fit_daten.index.size: {length}")
    fit_H = fit_daten.loc[:, 'H']
    fit_T = fit_daten.loc[:, 'T']
    fit_p = fit_daten.loc[:, 'p']
    fit_D = fit_daten.loc[:, 'D']
    fit_as = fit_daten.loc[:, 'a_s']
    fit_eps = fit_daten.loc[:, 'epsilon']
    fit_dh = fit_daten.loc[:, 'd_h']
    fit_mc = fit_daten.loc[:, 'mco2']
    fit_mw = fit_daten.loc[:, 'mw']
    fit_ye0 = fit_daten.loc[:, 'ye0']
    fit_yw0 = fit_daten.loc[:, 'yw0']
    fit_xe0 = fit_daten.loc[:, 'xe0']
    fit_xco20 = fit_daten.loc[:, 'xco20']

    fit_yeend = np.array(fit_daten.loc[:, 'yeend']).reshape(-1,1)
    fit_xeend = np.array(fit_daten.loc[:, 'xeend']).reshape(-1,1)
    beladung_data = np.hstack((fit_yeend,fit_xeend))
    LOGGER.debug(f"beladung_data: \n{beladung_data}")

    LOGGER.debug("Begin optimize.least_squares ...")
    result = optimize.least_squares(scorefit, [0.698, 0.4, 0.5, 0.0035, 0.023], bounds=([0, 0, 0, 0, 0],[2, 2, 2, 2, 2]))
    LOGGER.debug("... Ended optimize.least_squares")
    if result.success:
        print("Fit erfolgreich")
        LOGGER.debug("Fit Successful!")
    print("gefundener Fit:")
    print(result.x)
    LOGGER.debug(f"Fit: \n{result.x}")
    p.terminate()
    e = time.time()
    print((e-s)/60)
    LOGGER.debug(f"Time Needed: {(e-s)/60} min")
    LOGGER.debug("... Finished __name__ == __main__")

LOGGER.debug("... Finished fit_mt.py")