import pandas as pd
import numpy as np


def loadalldata():
    """
    Methode um alle Excelsheets im Inputordner einzuladen welche in der Methode benannt werden
    :return: ein Dataframe mit allen Daten
    """

    import pandas as pd

    def loaddatafromexcel(filename, columns_used, column_names, categorical_or_boolean_columns_names,
                          rows_header):
        """
        Läd Daten aus einzelner Excel Datei
        :param filename: String Name der Datei
        :param columns_used: array zB. [1,2,4] Spalten in Excel mit einzulesenden Daten, beginne mit 0 zu zählen
        :param column_names: array zB. ['v_g','massentransport'] Spalten-Namen der einzulesenden Daten
        :param categorical_or_boolean_columns_names: array zB ['massentransport']
        :param rows_header: int zB 3 oberste Zeilen in Excel mit Überschriften, Variablenname und Einheiten, beginne mit 1 zu zählenrows_header = 3  # oberste Zeilen in Excel mit Überschriften, Variablenname und Einheiten, beginne mit 1 zu zählen
        :return:
        """
        columns_used = np.array(columns_used)
        # Pfadname der Exeldatei realtiv vom Speicherort des Skriptes Backslash macht evtl unter Linux Probleme und muss durch Slash ersetzt werden
        pathname = 'Sauterdurchmesser/Input/' + filename + '.xlsx'
        # einlesen der Daten in ein Pandas Dataframe
        df = pd.read_excel(pathname, usecols=columns_used, names=column_names, header=None, skiprows=rows_header)
        # Filtern aller Zeilen mit fehlenden Daten, bei vielen fehlenden Daten kann man über Stragetien zum Füllen der Daten nachdenken
        df = df.dropna()
        df[categorical_or_boolean_columns_names] = df[categorical_or_boolean_columns_names].astype(
            np.int64)  # Kategorische Daten bei uns sind ganzzahlig und werden als integer gespeichert, eine Unterscheidung im Skript findet auch genau darüber statt
        return df

    # Läd drei verschiedene Dataframes ein
    # Hochdruck
    # todo: einheiten D und sigma
    df_d32 = pd.concat([
        loaddatafromexcel(filename="Sauterdurchmesser",
                          columns_used=[5, 6, 7, 10, 11, 12, 13, 14, 15, 17, 18, 19, 20, 21],
                          column_names=["Sauterdurchmesser", "q_c", "q_d", "$\\rho_{\\textrm{c}}$",
                                        "$\\eta_{\\textrm{c}}$",
                                        "$\\rho_{\\textrm{d}}$",
                                        "$\\eta_{\\textrm{d}}$", "$\\sigma$", "Kontaktwinkel", "D",
                                        "d_h", "a_s", "$\\epsilon$",
                                        "Cut-off"
                                        ],
                          categorical_or_boolean_columns_names=[], rows_header=[0, 1, 2, 3])

    ])

    # resettet Index, um doppelt vorkommende Indices zu vermeiden
    df_d32 = df_d32.reset_index(drop=True)
    print('Alle Daten eingelesen')
    return df_d32


def train_models():
    """
    Methode um Modelle neu zu trainieren und zu speichern.
    :return:
    """
    from sklearn.svm import SVR
    from sklearn.ensemble import RandomForestRegressor
    from sklearn.gaussian_process import GaussianProcessRegressor
    from sklearn.gaussian_process.kernels import RBF, DotProduct
    from sklearn.preprocessing import MinMaxScaler, OneHotEncoder
    from sklearn.model_selection import train_test_split
    from sklearn.pipeline import Pipeline
    from sklearn.compose import ColumnTransformer
    import joblib


    def train(mla, df):
        def splitfeaturetarget(df, targetname):
            """
            Methode um Feature und Target-Dataframe zu erstellen
            :param df: Dataframe mit Features und Target
            :return: x, y Dataframes
            """
            import numpy as np
            y = np.array(df[targetname]).reshape(-1, 1).astype(np.float32)
            x = df.drop(columns=[targetname])
            return x, y

        x, y = splitfeaturetarget(df, targetname=targetname)
        categorical_features = x.columns[x.dtypes.apply(lambda c: np.issubdtype(c, np.int64))].tolist()
        numeric_features = x.columns[x.dtypes.apply(lambda c: np.issubdtype(c, np.float64))].tolist()

        categorical_transformer = OneHotEncoder(drop='first', categories='auto')
        numeric_transformer = MinMaxScaler()

        preprocessor = ColumnTransformer(transformers=[
            ('num', numeric_transformer, numeric_features),
            ('cat', categorical_transformer, categorical_features)])

        pipeline = Pipeline([
            ('preprocessor', preprocessor),
            ('MLA', mla)
        ])
        pipeline.fit(x, y.ravel())

        return pipeline

    # Zielgröße
    targetname = "Sauterdurchmesser"

    # Lädt Daten ein
    df_d32 = loadalldata()
    df_train, df_test = train_test_split(df_d32, test_size=0.1, shuffle=True)
    # Definiert Modelle
    #mla_d32 = SVR(C=75, cache_size=1000, gamma='auto', kernel='rbf', tol=0.0001, epsilon=0.1)
    mla_d32 = SVR(C=25, cache_size=1000, gamma='scale', kernel='rbf', tol=1e-06, epsilon=0.1)
    # Trainiert Modelle
    trained_model_d32 = train(mla_d32, df_d32)
    trained_model_ohnetest = train(mla_d32, df_train)
    # Speichert Modelle
    joblib.dump(trained_model_d32, 'pkl-Dateien/MLA_d32.pkl')
    joblib.dump(trained_model_ohnetest, 'pkl-Dateien/MLA_d32_ohneTest.pkl')


def make_sauterdiameter_predictions_hp(q_c, q_d, rho_c, eta_c, rho_d, eta_d, sigma, Kwinkel, D, d_h, a_s, epsilon, cut_off):
    """
    Methode um einzelnen Punkt abzufragen
    :param q_c: Massenstrom CO2 [kg/h]
    :param q_d: Massenstrom Wasser [kg/h]
    :param rho_c: Dichte der kontinuierlichen Phase [kg/m³]
    :param rho_d: Dichte der dispersen Phase [kg/m³]
    :param eta_c: dynamische Viskosität der kontinuierlichen Phase [Pa*s]
    :param eta_d: dynamische Viskosität der dispersen Phase [Pa*s]
    :param sigma: Grenzflächenspannung [mN/m]
    :param Kwinkel: Kontaktwinkel [°]
    :param D: Kolonnendurchmesser [mm]
    :param d_h: hydraulischer Durchmesser [m]
    :param a_s: spezifische Oberfläche pro volumen Packung [m^2/m^3]

    :return: yp_d32: Sauterdurchmesser [mm]
    """
    import joblib

    # läd modell
    pipeline_d32 = joblib.load('Sauterdurchmesser/pkl-Dateien/MLA_d32.pkl')
    # erstellt abfragepunkt
    X = pd.DataFrame(columns=["q_c", "q_d", "$\\rho_{\\textrm{c}}$", "$\\eta_{\\textrm{c}}$",
                                            "$\\rho_{\\textrm{d}}$",
                                            "$\\eta_{\\textrm{d}}$", "$\\sigma$", "Kontaktwinkel", "D",
                                            "d_h", "a_s", "$\\epsilon$",
                                            "Cut-off"])

    X.loc[0, 'q_c'] = float(q_c)
    X.loc[0, 'q_d'] = float(q_d)
    X.loc[0, '$\\rho_{\\textrm{c}}$'] = float(rho_c)
    X.loc[0, '$\\rho_{\\textrm{d}}$'] = float(rho_d)
    X.loc[0, '$\\eta_{\\textrm{c}}$'] = float(eta_c)
    X.loc[0, '$\\eta_{\\textrm{d}}$'] = float(eta_d)
    X.loc[0, '$\\sigma$'] = float(sigma)
    X.loc[0, 'Kontaktwinkel'] = float(Kwinkel)
    X.loc[0, 'D'] = float(D)
    X.loc[0, 'd_h'] = float(d_h)
    X.loc[0, 'a_s'] = float(a_s)
    X.loc[0, '$\\epsilon$'] = float(epsilon)
    X.loc[0, 'Cut-off'] = float(cut_off)

    yp_d32 = pipeline_d32.predict(X)

    return float(yp_d32)


def parityplotdata(writer, df_d32):

    import joblib

    # läd modell
    pipeline_d32 = joblib.load('Sauterdurchmesser/pkl-Dateien/MLA_d32_ohneTest.pkl')


    def splitfeaturetarget(df, targetname):
        """
        Methode um Feature und Target-Dataframe zu erstellen
        :param df: Dataframe mit Features und Target
        :return: x, y Dataframes
        """
        import numpy as np
        y = np.array(df[targetname]).reshape(-1, 1).astype(np.float32)
        x = df.drop(columns=[targetname])
        return x, y

    x, y = splitfeaturetarget(df_d32, targetname="Sauterdurchmesser")
    pred_y = pipeline_d32.predict(x).reshape(-1, 1)
    df_pred_d32 = pd.DataFrame()
    df_pred_d32['Actual Value'] = y.ravel()
    df_pred_d32['Prediction'] = pred_y
    df_pred_d32.to_excel(writer, sheet_name='Prädiktionen Tropfendurchmesser')
    x.to_excel(writer, sheet_name='Input Sauterdurchmesser')
    yp = np.array(df_pred_d32['Prediction']).reshape(-1, 1).astype(np.float32)
    yk = korrelation_216Heydrich(x)

    return y, yp, yk


def korrelation_216Heydrich(df):
    """
    Korrelation für Sauterdurchmesser nach Seibert 1988
    :param df: Dataframe mit Input-Daten
    :return:
    """
    rho_c = np.array(df['$\\rho_{\\textrm{c}}$']).reshape(-1, 1).astype(np.float32)
    rho_d = np.array(df['$\\rho_{\\textrm{d}}$']).reshape(-1, 1).astype(np.float32)
    sigma = np.array(df['$\\sigma$']).reshape(-1, 1).astype(np.float32)
    g = 9.81
    drho = np.subtract(rho_d, rho_c)
    d32 = 1000 * (1.15/(g**0.5)) * np.power(np.divide(sigma/1000, drho), 0.5)
    return d32


# Befehl um Modelle zu trainieren
#train_models()
# Befehle zum Abfragen eines Punktes
#print(make_sauterdiameter_predictions_hp(q_c=36.013709860470314, q_d=1.1418236394343915, rho_d=996.515, rho_c=628.612, eta_c=0.0000478248, eta_d=0.0006538690000000001, Kwinkel=130, D=38, d_h=0.000343, a_s=700, cut_off=0.1, sigma=30, epsilon=0.86))
