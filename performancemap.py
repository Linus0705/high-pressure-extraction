from Density_Viscosity import MLA_viscosity_co2_water, MLA_density_co2_water
from Distribution_Coeff import MLA_distributioncoeff_ethanol
from Flooding import MLA_flooding_point_packed_columns
from Sauterdurchmesser import sauterdiameter
from Velocity import velocity
from Submodelle import masstransfercoeff, Diffusion, Kontaktwinkel, surfacetension
from Holdup import holdup
from Axiale_Dispersion import use_MLA_axial_dispersion
from multiprocessing import Pool
import numpy as np
import pandas as pd
import math
import time
import os
import sys
import logging
from scipy.integrate import solve_ivp, ode, odeint
import matplotlib.pyplot as plt

# Set up -- Logging
logging.basicConfig(format='%(asctime)s :%(name)s:%(levelname)s:  %(message)s',
                    datefmt='%m/%d/%Y %I:%M:%S %p',
                    level=logging.DEBUG,
                    handlers=[
                        logging.FileHandler(filename=f'{os.path.basename(__file__)}.log', mode='w'),
                        logging.StreamHandler(sys.stdout)
                    ]

                    )
LOGGER = logging.getLogger(__name__)
LOGGER.debug("Starting performancemap.py ...")

def main(n, dt, h, T, p, D, a_s, epsilon, d_h, cutoff, pv, y_e_0, y_w_0, x_e_0, x_co2_0, fit, belfaktor, expath, fig_path):
    """
    Hauptmethode
    :param n: Anzahl Stufen
    :param dt: Größe des Zeitschrittes [s]
    :param h: Kolonnenhöhe [m]
    :param T: Temperatur [K]
    :param p: Druck [MPa]
    :param D: Kolonnendurchmesser [m]
    :param a_s: spezifische Oberfläche pro Packung [1/m]
    :param epsilon: Leervolumenanteil Kolonne [-]
    :param d_h: hydraulischer Durchmesser
    :param cutoff: Cut-off [-]
    :param pv: Phasenverhältnis c/d [m^3/m^3]
    :param x_e_0: Massenanteil Ethanol im Feedstrom [g/g]
    :param x_co2_0: Massenanteil CO2 im Feedstrom [g/g]
    :param y_e_0: Massenanteil Ethanol im Lösemittel [g/g]
    :param y_w_0: Massenanteil Wasser im Lösemittel [g/g]
    :param fit: array mit linearen Fits für Massentransferkoeffizienten
    :param belfaktor: Anteil der Flutbelastung, um von PV auf dispers oder konti Belastung absolut schließen zu können
    :param expath: Speicherpfad
    :param fig_path: Speicherpfad des Überordners
    :return:
    """
    LOGGER.debug("Starting main ...")

    LOGGER.debug("Inputs:")
    LOGGER.debug(f"Number of Stages n = {n} [-]")
    LOGGER.debug(f"Timestep dt = {dt}s")
    LOGGER.debug(f"Column Height h = {h}m")
    LOGGER.debug(f"Temperature T = {T}K")
    LOGGER.debug(f"Pressure p = {p}MPa")
    LOGGER.debug(f"Column Diameter D = {D}m")
    LOGGER.debug(f"Specific Surface per Packung a_s = {a_s} 1/m")
    LOGGER.debug(f"Leervolumenanteil epsilon = {epsilon} [-]")
    LOGGER.debug(f"Hydraulic Diameter d_h = {d_h}")
    LOGGER.debug(f"Cut-Off = {cutoff} [-]")
    LOGGER.debug(f"PV = {pv} [-]")
    LOGGER.debug(f"w_Solvent_Ethanol y_e_0 = {y_e_0} g/g")
    LOGGER.debug(f"w_Solvent_Water y_w_0 = {y_w_0} g/g")
    LOGGER.debug(f"w_Feed_Ethanol x_e_0 = {x_e_0} g/g")
    LOGGER.debug(f"w_Feed_CO2 x_co2_0 = {x_co2_0} g/g")
    LOGGER.debug(f"fit: \n{fit}")
    LOGGER.debug(f"belfaktor: \n{belfaktor}")
    LOGGER.debug(f"expath: \n{expath}")



    # initialisiert Excel-Schreiber
    options = {}
    options['strings_to_formulas'] = False
    options['strings_to_urls'] = False
    writer = pd.ExcelWriter(expath, options=options)

    pbar = p*10
    # Geometrische Größen
    LOGGER.debug("Geometric Quantities:")
    z = h/n
    d_p = 6*(1-epsilon)/a_s
    S = math.pi * D**2 / 4
    V_K = S*h*epsilon
    LOGGER.debug(f"z = {z}")
    LOGGER.debug(f"Packung als monodisperse Schüttung d_p = {d_p}")
    LOGGER.debug(f"S = {S}")
    LOGGER.debug(f"Kolonnenvolumen V_K = {V_K} m^3")


    # Stoffdaten
    LOGGER.debug("Component Data:")
    df_rho_c, df_rho_d = MLA_density_co2_water.loadalldata()
    df_eta_c, df_eta_d = MLA_viscosity_co2_water.loadalldata()
    M_co2 = 44.01  # [g/mol]
    M_w = 18.01528  # [g/mol]
    M_e = 46.07 # [g/mol]
    rho_c, rho_d = MLA_density_co2_water.NIST_get_density(df_rho_c,df_rho_d,T,p)
    eta_c, eta_d = MLA_viscosity_co2_water.NIST_get_viscosity(df_eta_c,df_eta_d,T,p)
    d_rho = rho_d-rho_c
    LOGGER.debug(f"rho_c = {rho_c}")
    LOGGER.debug(f"rho_d = {rho_d}")
    LOGGER.debug(f"eta_c = {eta_c}")
    LOGGER.debug(f"eta_d = {eta_d}")

    # Flutbelastung und Massenströme bestimmen
    LOGGER.debug("Flutbelastung:")
    sf = pv * (rho_c / rho_d)
    LOGGER.debug(f"sf = {sf}")
    # Data-driven
    bel_flood = MLA_flooding_point_packed_columns.make_floodingpoint_predictions_hp(PV=1/pv, rho_d=rho_d, rho_c=rho_c, eta_c=eta_c, eta_d=eta_d, D=D, a_s=a_s, epsilon=epsilon, Mat=101, Tr=4, Pt=2)
    belastung = bel_flood * belfaktor # gesamtbelastung beide phasen zusammen
    LOGGER.debug(f"belastung = {belastung}")
    # Correlation Stockfleth (wird nicht verwendet)
    bel_stockfleth = MLA_flooding_point_packed_columns.stockfleth_korr(rho_c, rho_d, pv, epsilon, 4*epsilon/(a_s+160)) * belfaktor
    LOGGER.debug(f"bel_stockfleth = {bel_stockfleth}")

    bel_d_s = bel_stockfleth/(1+pv)
    bel_c_s = bel_stockfleth * pv/(1+pv)
    bel_d = belastung/(1+pv)
    bel_c = belastung * pv/(1+pv)
    LOGGER.debug(f"bel_d_s = {bel_d_s}")
    LOGGER.debug(f"bel_c_s = {bel_c_s}")
    LOGGER.debug(f"bel_d = {bel_d} [m^3/(m^2 s)]")
    LOGGER.debug(f"bel_c = {bel_c} [m^3/(m^2 s)]")

    q_c = bel_c*S*rho_c*3600
    q_d = bel_d*S*rho_d*3600
    LOGGER.debug(f"q_c = {q_c} [kg/h]")
    LOGGER.debug(f"q_d = {q_d} [kg/h]")


    #Diffusion
    LOGGER.debug("Diffusion:")
    phi_w = 2.6 # Assozationsfaktor des Solvents (2.6-1)
    V_e, V_w, V_co2 = Diffusion.getVeb()
    LOGGER.debug(f"V_e = {V_e}")
    LOGGER.debug(f"V_w = {V_w}")
    LOGGER.debug(f"V_co2 = {V_co2}")
    D_co2 = Diffusion.SassiatMourier(T, M_co2, V_co2, eta_c)
    D_w_co2 = Diffusion.SassiatMourier(T, M_w, V_w, eta_c)
    D_e_co2 = Diffusion.SassiatMourier(T, M_e, V_e, eta_c)
    D_e_w = Diffusion.WilkeChang(T, M_w, V_e, eta_d, phi_w)
    D_co2_w = Diffusion.WilkeChang(T, M_w, V_co2, eta_d, phi_w)
    D_w = Diffusion.WilkeChang(T, M_w, V_w, eta_d, phi_w)
    LOGGER.debug(f"SassiatMourier D_co2 = {D_co2} m^2/s")
    LOGGER.debug(f"SassiatMourier D_w_co2 = {D_w_co2} m^2/s")
    LOGGER.debug(f"SassiatMourier D_e_co2 = {D_e_co2} m^2/s")
    LOGGER.debug(f"WilkeChang D_e_w = {D_e_w} m^2/s")
    LOGGER.debug(f"WilkeChang D_co2_w = {D_co2_w} m^2/s")
    LOGGER.debug(f"WilkeChang D_w = {D_w} m^2/s")

    # -----------------------------------
    # wird nicht verwendet
    # Massenanteil Ethanol im Feed in molaren Anteil der Gesamtkompostion berechnen
    # Annahme: kein Ethanol oder Wasser in CO2
    z_ethanol_0 = q_d/(q_c+q_d) * x_e_0
    z_wasser_0 = q_d/(q_c+q_d) * (1 - x_e_0 - x_co2_0)
    z_co2_0 = 1-z_ethanol_0-z_wasser_0
    z_ethanol_mol = (z_ethanol_0/M_e)/(z_ethanol_0/M_e+z_co2_0/M_co2+z_wasser_0/M_w)

    kwinkel = 130
    #sigma = surfacetension.chunwilkinson(rho_c=rho_c, x_ethanol=z_ethanol_mol)
    # Beachte sigma in mN/m und D in mm umwandeln
    #sigma = surfacetension.make_ift_prediction(T, p, z_ethanol_mol)
    sigma = 30
    #-------------------------------------

    # Geschwindigkeiten Leerrohr
    LOGGER.debug("Geschwindigkeiten Leerrohr:")
    v_d_leer = q_d / 3600 / rho_d / S
    v_c_leer = q_c / 3600 / rho_c / S
    LOGGER.debug(f"v_d_leer = {v_d_leer} m/s")
    LOGGER.debug(f"v_c_leer = {v_c_leer} m/s")

    # angepasster Kumar Holdup
    LOGGER.debug("Holdup:")
    c1 = 0.9912
    c2 = 0.86 ** -0.837
    c3 = ((rho_d - rho_c) / rho_c) ** -0.9999
    c4 = ((1 / 700) * ((((rho_c ** 2) * 9.81) / (eta_c ** 2)) ** (1 / 3))) ** -0.99999
    c5 = (eta_d / eta_c) ** 0.6254
    c6 = ((v_d_leer * ((rho_c / (9.81 * eta_c)) ** (1 / 3)))) ** 0.40441
    c7 = math.exp(0.6696 * v_c_leer * ((rho_c / (9.81 * eta_c)) ** (1 / 3)))
    c8 = ((eta_c / (((rho_c * 70 * 10 ** -3) / (700)) ** 0.5)) ** -0.4091)
    hl = c1 * c2 * c3 * c4 * c5 * c6 * c7 * c8 /100
    LOGGER.debug(f"c1 = {c1}")
    LOGGER.debug(f"c2 = {c2}")
    LOGGER.debug(f"c3 = {c3}")
    LOGGER.debug(f"c4 = {c4}")
    LOGGER.debug(f"c5 = {c5}")
    LOGGER.debug(f"c6 = {c6}")
    LOGGER.debug(f"c7 = {c7}")
    LOGGER.debug(f"c8 = {c8}")
    LOGGER.debug(f"Holdup hl = {hl} [-]")

    # stat ist ein Anpassungsparameter 'gesamte parameter Saeger Excel'
    # Strömungsrohr Verjüngung --> Effektive Geschwindigkeit wird schneller als Leerrohrgeschw
    LOGGER.debug("Mechanistic Correlation:")
    stat = 4.773359091   # CY
    #stat = 8.592192302   # Mellapak
    #stat =6.031801026 #gesamt
    v_d = q_d / (rho_d * S * 3600 * epsilon * hl*stat) # theoretisch *cos(45°) aus Korrelation nehmen, wegen Neigungswinkel der Kreuzkanäle (Heydrich, 1999)
    v_c = q_c / (rho_c * S * 3600 * epsilon * (1 - hl*stat))
    v_s = (v_c + v_d)
    LOGGER.debug(f"Effektive Geschw. v_d = {v_d} m/s")
    LOGGER.debug(f"Effektive Geschw. v_c = {v_c} m/s")
    LOGGER.debug(f"Relativgeschw. v_s = {v_s} m/s")

    # Sauterdurchmesser nach Heydrich
    LOGGER.debug("Sauterdurchmesser:")
     # d32 nach Heydrich
    d32=4.02*((q_c+q_d)/3600/S)**(-0.34)*x_e_0**(-0.51)/1000 # /1000 da die Korrelation d32 in mm ausgibt eigentlich
    LOGGER.debug(f"d32 = {d32} m")


    # Stoffaustauschfläche bestimmen
    LOGGER.debug("Surface for Mass Transfer:")
    # nTropfen = hl * (S*epsilon) / (d32**3/6*math.pi) # !!!!!!!!!!!!!!!!!!!!! der Zähler wird nicht m^3
    V_einzeltropfen = d32 ** 3 / 6 * math.pi
    V_dispers = V_K * hl
    nTropfen = V_dispers / V_einzeltropfen
    A = nTropfen * d32**2 * math.pi
    LOGGER.debug(f"V_einzeltropfen = {V_einzeltropfen} m^3")
    LOGGER.debug(f"V_dispers = {V_dispers} m^3")
    LOGGER.debug(f"nTropfen = {nTropfen} [-]")
    LOGGER.debug(f"Gesamtoberfläche Tropfen A = {A} m^2")

    # Masse disperse und konti Phase
    masse_d = V_K * hl * rho_d
    masse_c = V_K * (1 - hl) * rho_c
    LOGGER.debug(f"masse_d = {masse_d} kg")
    LOGGER.debug(f"masse_c = {masse_c} kg")



    if hl < 0:
        print('Holdup negativ für %s' % expath)
        LOGGER.debug(f"Holdup negativ für {expath}")
        dummy = np.zeros(23)
        dummy[8:11] = 3600*bel_c, 3600*bel_d, pv
        return dummy
    if d32 < 0:
        print('Sauterdurchmesser negativ für %s' % expath)
        LOGGER.debug(f"Sauterdurchmesser negativ für {expath}")
        dummy = np.zeros(23)
        dummy[8:11] = 3600 * bel_c, 3600 * bel_d, pv
        return dummy
    if v_c < 0 or v_d < 0:
        print('Geschwindigkeit ist negativ für %s' % expath)
        LOGGER.debug(f"Geschwindigkeit negativ für {expath}")
        return np.zeros(23)

    # Reynolds und Schmidtzahl
    LOGGER.debug("Dimensionless Numbers:")
    re_sc = reynolds(rho_c, eta_c, v_c, d_p) # mit effektiver Geschwindigkeit statt Leerrohr
    sc_c = schmidt(rho_c, eta_c, D_co2)
    sc_d = schmidt(rho_d, eta_d, D_w)
    LOGGER.debug(f"Reynolds Conti(CO2) re_sc = {re_sc} [-]")
    LOGGER.debug(f"Schmidt Conti(CO2) sc_c = {sc_c} [-]")
    LOGGER.debug(f"Schmidt Disperse(H20) sc_d = {sc_d} [-]")

    # axiale Dispersion in [m^2/s]
    LOGGER.debug("Axiale Dispersion:")
    pe = use_MLA_axial_dispersion.make_axial_dispersion_prediction(re_sc, sc_c)
    dax = v_c*h/pe # mit effektiver Geschwindigkeit statt Leerrohr
    LOGGER.debug(f"Pecletzahl Pe = {pe} [-]")
    LOGGER.debug(f"Axialer Dispersionskoeffizient D_Ax dax = {dax} [m^2/s]")


    #Massentransferkoeffizienten bestimmen
    LOGGER.debug("Mass Transfer Coefficients:")
    fit_c = fit[0:3]
    fit_d = fit[3:5]
    LOGGER.debug(f"fit_c = {fit_c}")
    LOGGER.debug(f"fit_d = {fit_d}")
    k_c_co2 = masstransfercoeff.mtcoeff_c_seibert(D_co2, d32, v_s, rho_c, eta_c, hl, fit_c)
    k_d_co2 = masstransfercoeff.mtcoeff_d_seibert(D_co2_w, v_s, rho_d, eta_c, eta_d, fit_d)
    k_c_w = masstransfercoeff.mtcoeff_c_seibert(D_w_co2, d32, v_s, rho_c, eta_c, hl, fit_c)
    k_d_w = masstransfercoeff.mtcoeff_d_seibert(D_w, v_s, rho_d, eta_c, eta_d, fit_d)
    k_c_e = masstransfercoeff.mtcoeff_c_seibert(D_e_co2, d32, v_s, rho_c, eta_c, hl, fit_c)
    k_d_e = masstransfercoeff.mtcoeff_d_seibert(D_e_w, v_s, rho_d, eta_c, eta_d, fit_d)
    LOGGER.debug(f"k_c_co2 = {k_c_co2} [kg/s*m^2]")
    LOGGER.debug(f"k_d_co2 = {k_d_co2} [kg/s*m^2]")
    LOGGER.debug(f"k_c_w  = {k_c_w } [kg/s*m^2]")
    LOGGER.debug(f"k_d_w = {k_d_w} [kg/s*m^2]")
    LOGGER.debug(f"k_c_e = {k_c_e} [kg/s*m^2]")
    LOGGER.debug(f"k_d_e = {k_d_e} [kg/s*m^2]")


    def rhs_fast(t, u, k):
        """
        DGL-System (K- und J-Berechnung ausgelagert für schnellere Berechnung)
        """
        #LOGGER.debug("Starting rhs_fast ...")
        #LOGGER.debug(f"t: {t}")
        #LOGGER.debug(f"u: {u}")
        #LOGGER.debug(f"k: {k}")

        # Vektor anlegen
        rhs_fast = np.zeros(4 * (n + 1))
        je = k[0]
        jco2 = k[1]
        jw = k[2]
        # y_ethanol 0 - n
        # y_wasser n+1 - 2n+1
        # x_ethanol 2n+2 - 3n+2
        # x_co2 3n+3 - 4n+3
        # rhs = dyi/dt
        # u[i] = y an Ort i

        # Kompartments zwischen Boden und Kopf
        for i in range(1, n):  # 1....n-1
            i1 = i + n + 1
            i2 = i + 2 * (n + 1)
            i3 = i + 3 * (n + 1)
            # Massentransfer bestimmen

            rhs_fast[i] = (v_c / z) * (u[i - 1] - u[i]) + (dax /(2* (z ** 2))) * (u[i + 1] - 2 * u[i] + u[i - 1]) \
                          + je[i] / (h * S * epsilon * (1 - hl) * rho_c)

            rhs_fast[i1] = (v_c / z) * (u[i1 - 1] - u[i1]) + (dax /(2* (z ** 2))) * (u[i1 + 1] - 2 * u[i1] + u[i1 - 1]) \
                           + jw[i] / (h * S * epsilon * (1 - hl) * rho_c)
            rhs_fast[i2] = (v_d / z) * (u[i2 + 1] - u[i2]) \
                           - je[i] / (h * S * epsilon * hl * rho_d)

            rhs_fast[i3] = (v_d / z) * (u[i3 + 1] - u[i3]) \
                           - jco2[i] / (h * S * epsilon * hl * rho_d)

        # RANDBEDINGUNGEN
        # Eingang (konti)
        rhs_fast[0] = (dax / z * rhs_fast[1]) / (v_c + dax / z)
        rhs_fast[n + 1] = (dax / z * rhs_fast[n + 2]) / (v_c + dax / z)
        # Ausgang (dispers)
        rhs_fast[2 * (n + 1)] = rhs_fast[2 * (n + 1) + 1]  # dx0dt = dx1dt
        rhs_fast[3 * (n + 1)] = rhs_fast[3 * (n + 1) + 1]
        # Ausgang (konti)
        rhs_fast[n] = rhs_fast[n - 1]  # dyN/dt=dyN-1/dt
        rhs_fast[2 * n + 1] = rhs_fast[2 * n]
        # Eingang (dispers)
        rhs_fast[3 * n + 2] = 0  # dxN/dt=0 da xN=xein (konst)
        rhs_fast[4 * n + 3] = 0
        #LOGGER.debug("... Finished rhs_fast")
        return rhs_fast

    def j_zwischen(u):
        #LOGGER.debug("Starting j_zwischen ...")
        """
        ausgelagerte Berechnung von Ji
        :param u: Vektor mit Massenanteilen für alle Kompartments
        :return: j_ethanol, j_co2, j_wasser
        """
        je = np.zeros(n+1)
        jw = np.zeros(n+1)
        jco2 = np.zeros(n+1)

        z_ethanol, z_wasser, z_co2 = globalcomp(n=n, q_c=masse_c, q_d=masse_d, x_e=u[2*n+3:3*n+1+1], y_e=u[1:n-1+1], x_w=(1 - u[2*n+3:3*n+1+1] - u[3*n+4:4*n+2+1]), y_w=u[n+2:2*n+1])

        for i in range(1, n+1):  # 1....n-1
            i1 = i + n + 1
            i2 = i + 2 * (n + 1)
            i3 = i + 3 * (n + 1)

            # Verteilungskoeff bestimmen
            Ke, Kco2, Kw = MLA_distributioncoeff_ethanol.make_distributioncoeff_predictions_hp(T=T, p=p, x_co2=z_co2[i], x_w=z_wasser[i], x_ethanol=z_ethanol[i])
            LOGGER.debug(f"j_zwischen.Ke: {Ke}")
            LOGGER.debug(f"j_zwischen.Kco2: {Kco2}")
            LOGGER.debug(f"j_zwischen.Kw: {Kw}")



            if Ke < 0 or Kco2 < 0 or Kw < 0:
                print(belfaktor)
                print(pv)
                LOGGER.debug(f"belfaktor: {belfaktor}")
                LOGGER.debug(f"pv: {pv}")

            je[i] = diffstrom_i(A, rho_c, rho_d, k_c_e, k_d_e, u[i2], u[i], Ke)
            jw[i] = diffstrom_i(A, rho_c, rho_d, k_c_w, k_d_w, 1 - u[i2] - u[i3], u[i1], Kw)
            jco2[i] = diffstrom_i(A, rho_c, rho_d, k_c_co2, k_d_co2, u[i3], 1 - u[i] - u[i1], Kco2)

        #LOGGER.debug("... Finished j_zwischen")
        LOGGER.debug(f"j_zwischen.je: {je}")
        LOGGER.debug(f"j_zwischen.jco2: {jco2}")
        LOGGER.debug(f"j_zwischen.jw: {jw}")



        return je, jco2, jw

    def solve():
        """
        Löst das DGL-System für Kompartments n und Zeitschritte dt und plottet Konzentrationsverläufe
        :return: array mit Endkonzentrationen [y_ethanol_out, y_co2_out, x_ethanol_out, x_wasser_out, y_ethanol_in,  y_co2_in, x_ethanol_in, x_w_in, Belastung c, Belastung d, SF, Holdup, PV]
        """
        LOGGER.debug("Starting solve ...")

        daten = pd.DataFrame()
        # u = xi(z),yi(z)
        u = np.zeros(4*(n+1))
        # u0 Startwerte für Solver
        u0 = np.zeros(4*(n+1))
        u0[0] = y_e_0
        u0[n+1] = y_w_0
        u0[3*n+2] = x_e_0
        u0[4*n+3] = x_co2_0
        t=0
        success = True
        # Änderung < tol = stationärer Zustand
        tol = 5e-3/dt/n
        LOGGER.debug(f"tol: {tol}")
        deltay = 1  # dummy
        i = 0
        LOGGER.debug("Start of while-Loop ...")
        while success and deltay > tol and i < 2160:  # Solver erfolgreich und instationär
            i = i + 1
            LOGGER.debug(f"Begin while i: {i} ...")
            sol = solve_ivp(rhs_fast, [t, t+dt], u0, method='BDF', t_eval=[t+dt], args=[j_zwischen(u0)])
            success = sol.success
            deltay = max(sol.y[:,0]-u0)  # größte Änderung zum vorherigen Zeitschritt
            LOGGER.debug(f"deltay: {deltay}")
            LOGGER.debug(f"deltay - tol: {deltay - tol}")
            t = i * dt
            LOGGER.debug(f"timestep t = {t} sec \n")

            # Lösungen werden Startwerte für nächsten Zeitschritt
            u0 = sol.y[:, 0] # Calues for the solution at t
            LOGGER.debug(f"u0: {u0}")
            # continuous phase (CO2): y [kg/kg]
            # dispersed phase (H20): x [kg/kg]
            # j=0 is the column entry of the continous phase (CO2)
            y_ethanol = u0[0:n+1] # y_EtOH from j=0 to j=N for timestep t
            y_wasser = u0[n + 1:2 * n + 1+1]
            x_ethanol = u0[2 * (n + 1):3 * n + 2+1]
            x_co2 = u0[3 * (n + 1):4 * n + 3+1]
            x_w = np.ones(n+1) - x_ethanol - x_co2
            y_co2 = np.ones(n+1) - y_ethanol - y_wasser
            LOGGER.debug(f"y_ethanol(t={t}): {y_ethanol}")
            LOGGER.debug(f"y_wasser(t={t}): {y_wasser}")
            LOGGER.debug(f"x_ethanol(t={t}): {x_ethanol}")
            LOGGER.debug(f"x_co2(t={t}): {x_co2}")
            LOGGER.debug(f"x_w(t={t}): {x_w}")
            LOGGER.debug(f"y_co2(t={t}): {x_w}")


            daten['y_Ethanol'] = y_ethanol
            daten['y_wasser'] = y_wasser
            daten['y_CO2'] = y_co2
            daten['x_Ethanol'] = x_ethanol
            daten['x_CO2'] = x_co2
            daten['x_Wasser'] = x_w
            #daten.to_excel(excel_writer=writer, sheet_name='Massenanteil')

            data = [y_ethanol[n-1+1], y_co2[n-1+1], x_ethanol[0], x_w[0], y_ethanol[0], y_co2[0], x_ethanol[n-1+1], x_w[n-1+1], bel_c, bel_d, sf, hl, pv]
            LOGGER.debug(f"data: {data}")
            LOGGER.debug(f"... End while i: {i}")
        LOGGER.debug("... End of while-Loop") # ab einer bestimmten Zeit ändern sich die Stoffmengenanteile nicht mehr --> stationär Bereich erreicht
        LOGGER.debug(f"Steady-state was achieved after {i} steps with time {t} sec!")

        LOGGER.debug(f"y_ethanol_steadystate(t={t}): {y_ethanol}")
        LOGGER.debug(f"y_wasser_steadystate(t={t}): {y_wasser}")
        LOGGER.debug(f"x_ethanol_steadystate(t={t}): {x_ethanol}")
        LOGGER.debug(f"x_co2_steadystate(t={t}): {x_co2}")
        LOGGER.debug(f"x_w_steadystate(t={t}): {x_w}")
        LOGGER.debug(f"y_co2_steadystate(t={t}): {y_co2}")

        # save steady state concentrations per compartment for this PV and q_c to q_d ratio
        df_steadystate_mass_weights = pd.DataFrame(data={'y_EtOH': y_ethanol,
                                                         'y_H20': y_wasser,
                                                         'y_CO2': y_co2,
                                                         'x_EtOH': x_ethanol,
                                                         'x_H20': x_w,
                                                         'x_CO2': x_co2
                                                         })
        df_steadystate_mass_weights.to_csv(path_or_buf=f"{fig_path}/PV{pv}_qc{q_c}_qd{q_d}_BelD_{bel_d}_Massweights_over_Height.csv",
                                           sep=';')

        # save steady state concentrations per compartment for this PV and q_c to q_d ratio - Concentration without CO2
        df_steadystate_mass_weights_wo_CO2 = pd.DataFrame(data={'y_EtOH*': (y_ethanol/(1 - y_co2)),
                                                                'y_H20*': (y_wasser/(1 - y_co2)),
                                                                'x_EtOH*': (x_ethanol/(x_ethanol + x_w)),
                                                                'x_H20*': (x_w/(x_ethanol + x_w)),
                                                                })
        df_steadystate_mass_weights_wo_CO2.to_csv(path_or_buf=f"{fig_path}/PV{pv}_qc{q_c}_qd{q_d}_BelD_{bel_d}_Massweights_wo_CO2_over_Height.csv",
                                           sep=';')

        # Check Mass Balance
        LOGGER.debug(f"Mass Balance Check:")
        LOGGER.debug(f"Massflow Conti: {q_c} kg/h")
        LOGGER.debug(f"Massflow Disperse: {q_d} kg/h")
        LOGGER.debug(f"y_ethanol(n=0): {y_ethanol[0]} kg/kg")
        LOGGER.debug(f"y_ethanol(n=N): {y_ethanol[-1]} kg/kg")
        LOGGER.debug(f"y_wasser(n=0): {y_wasser[0]} kg/kg")
        LOGGER.debug(f"y_wasser(n=N): {y_wasser[-1]} kg/kg")
        LOGGER.debug(f"y_co2(n=0): {y_co2[0]} kg/kg")
        LOGGER.debug(f"y_co2(n=N): {y_co2[-1]} kg/kg")
        LOGGER.debug(f"x_ethanol(n=0): {x_ethanol[0]} kg/kg")
        LOGGER.debug(f"x_ethanol(n=N): {x_ethanol[-1]} kg/kg")
        LOGGER.debug(f"x_w(n=0): {x_w[0]} kg/kg")
        LOGGER.debug(f"x_w(n=N): {x_w[-1]} kg/kg")
        LOGGER.debug(f"x_co2(n=0): {x_co2[0]} kg/kg")
        LOGGER.debug(f"x_co2(n=N): {x_co2[-1]} kg/kg")
        LOGGER.debug(f"mdot_H20_in: {x_w[-1]*q_d + y_wasser[0]*q_c} kg/h")
        LOGGER.debug(f"mdot_H20_out: {x_w[0] * q_d + y_wasser[-1] * q_c} kg/h")
        LOGGER.debug(f"mdot_EtOH_in: {x_ethanol[-1] * q_d + y_ethanol[0] * q_c} kg/h")
        LOGGER.debug(f"mdot_EtOH_out: {x_ethanol[0] * q_d + y_ethanol[-1] * q_c} kg/h")
        LOGGER.debug(f"mdot_CO2_in (aus Kompartmentmodell): {x_co2[-1] * q_d + y_co2[0] * q_c} kg/h")
        LOGGER.debug(f"mdot_CO2_out (aus Kompartmentmodell): {x_co2[0] * q_d + y_co2[-1] * q_c} kg/h")
        LOGGER.debug(f"mdot_CO2_in (aus Schließbedingung): {(1 - x_w[-1] - x_ethanol[-1]) * q_d + (1 - y_wasser[0] - y_ethanol[0]) * q_c} kg/h")
        LOGGER.debug(f"mdot_CO2_out (aus Schließbedingung): {(1 - x_w[0] - x_ethanol[0]) * q_d + (1 - y_wasser[-1] - y_ethanol[-1]) * q_c} kg/h")

        # HETS
        LOGGER.debug("Gesamtmassenanteile statt nur Massenanteil an Phase x bzw y:")
        z_co2_final = np.ones(n+1)
        z_e_final = (masse_c * y_ethanol + masse_d * x_ethanol)/(masse_c+masse_d)
        z_w_final = (masse_c * y_wasser + masse_d * x_w)/(masse_c+masse_d)
        z_co2_final = z_co2_final-z_e_final-z_w_final
        LOGGER.debug(f"Stationaeres Konzentrationsprofil von j=1 bis j=N z_EtOH: {z_e_final}")
        LOGGER.debug(f"Stationaeres Konzentrationsprofil von j=1 bis j=N z_CO2: {z_co2_final}")
        LOGGER.debug(f"Stationaeres Konzentrationsprofil von j=1 bis j=N z_H20: {z_w_final}")

        # save total massweight per compartment for this PV and q_c to q_d ratio
        df_totalmassweight = pd.DataFrame(data={'z_EtOH': z_e_final,
                                                'z_H20': z_w_final,
                                                'z_CO2': z_co2_final
                                                })
        df_totalmassweight.to_csv(path_or_buf=f"{fig_path}/PV{pv}_qc{q_c}_qd{q_d}_BelD_{bel_d}_totalmassweight_over_Height.csv",
                                           sep=';')

        Ke = np.ones(n)
        Kw = np.ones(n)
        Kco2 = np.ones(n)
        for l in range(1, n):  # 1....n-1 --> j=0 and j=35 are missed out because these phases are not in contact since they are entering or leaving streams
            # Verteilungskoeff bestimmen
            Ke[l], Kco2[l], Kw[l] = MLA_distributioncoeff_ethanol.make_distributioncoeff_predictions_hp(T=T, p=p, x_co2=z_co2_final[l], x_w=z_w_final[l], x_ethanol=z_e_final[l])
        LOGGER.debug(f"Verteilungskoeffizient von j=1 bis j=N-1 K_EtOH: {Ke}")
        LOGGER.debug(f"Verteilungskoeffizient von j=1 bis j=N-1 K_CO2: {Kco2}")
        LOGGER.debug(f"Verteilungskoeffizient von j=1 bis j=N-1 K_H20: {Kw}")

        # save partition coefficient per compartment for this PV and q_c to q_d ratio
        df_partitioncoeff = pd.DataFrame(data={'K_EtOH': Ke,
                                               'K_CO2': Kco2,
                                               'K_H20': Kw
                                               })
        df_partitioncoeff.to_csv(path_or_buf=f"{fig_path}/PV{pv}_qc{q_c}_qd{q_d}_BelD_{bel_d}_K_over_Height.csv",
                                           sep=';')

        # average K for HETS calculation
        average_Ke = Ke[1:n-1+1].mean() # entry Ke[0] is still 1 from np.ones() and need to be missed out in order not to confuse the .mean()
        average_Kco2 = Kco2[1:n-1+1].mean()
        average_Kw = Kw[1:n-1+1].mean()
        hets = hets_kbs(data, h, average_Ke)
        LOGGER.debug(f"hets: {hets}")
        hets_bel = hets_kbs_bel(data, h, average_Ke, average_Kco2, x_e_0)


        data.append(hets)
        data.append(d32)
        data.append(v_c)
        data.append(v_d)
        data.append(average_Ke)
        data.append(average_Kw)
        data.append(average_Kco2)
        data.append(bel_c_s)
        data.append(bel_d_s)
        daten.loc[:,'load_c'] = bel_c
        daten.loc[:,'load_d'] =bel_d
        daten.loc[:, 'PV'] = pv
        daten.loc[:, 'Holdup'] = hl
        daten.loc[:, 'HETS'] = hets
        data.append(hets_bel)
        #daten.to_excel(excel_writer=writer)

        # append mass weights without CO2
        data.append(y_ethanol[n]/(1 - y_co2[n]))
        data.append(y_wasser[n]/(1 - y_co2[n]))
        data.append(x_ethanol[0]/(x_ethanol[0] + x_w[0]))
        data.append(x_w[0]/(x_ethanol[0] + x_w[0]))

        # add mass flow [kg/h]
        data.append(q_c)
        data.append(q_d)

        LOGGER.debug("... Finished solve")
        return data


    model = solve()
    # Excel speichern
    #writer.save()
    #writer.close()

    LOGGER.debug("... Finished main")

    return model


def globalcomp(n, q_c, q_d, x_e, y_e, x_w, y_w):
    """
    Methode um Gesamtzusammensetzung zu bestimmen
    :param n: Anazhl Kompartments
    :param q_c: Massenstrom konti [kg/h]
    :param q_d: Massenstrom dispers [kg/h]
    :param x_e: Array mit x_Ethanol
    :param y_e: Array mit y_Ethanol
    :param x_w: Array mit x_Wasser
    :param y_w: Array mit y_Wasser
    :return: Arrays mit Gesamtzusammensetzung Ethanol, Wasser, CO2
    """
    LOGGER.debug("Starting globalcomp ...")
    z_ethanol = np.zeros(n+1)
    z_wasser = np.zeros(n+1)
    z_co2 = np.ones(n+1)
    z_ethanol[1:n - 1+1] = (q_d * x_e + q_c * y_e) / (q_c + q_d)
    z_wasser[1:n - 1+1] = (q_d * x_w + q_c * y_w) / (
                q_c + q_d)
    z_co2 = z_co2 - z_ethanol - z_wasser
    LOGGER.debug("... Finished globalcomp")
    return z_ethanol, z_wasser, z_co2


def diffstrom_i(A, rho_c, rho_d, k_ci, k_di, xi, yi, Ki):
    """
    Massentransferstrom für Komponente i
    :param A: Stoffaustauschfläche [m^2]
    :param rho_c: Dichte der kontinuierlichen Phase [kg/m^3]
    :param rho_d: Dichte der dispersen Phase [kg/m^3]
    :param k_ci: Massentransferkoeff konti Phase Stoff i [m/s]
    :param k_di: Massentransferkoeff disperse Phase Stoff i [m/s]
    :param xi: Massenanteil Stoff i in konti Phase
    :param yi: Massenanteil Stoff i in disp Phase
    :param Ki: massenbezogener Verteilungskoeff Stoff i
    :return: ji: Massentransferstrom für Stoff i [kg/s]
    """
    #beta = 1/(1/(rho_d*k_di) + Ki/(rho_c*k_ci))
    beta = 1 / (1 / (rho_c * k_ci) + Ki / (rho_d * k_di))
    ji = A * beta * (Ki * xi - yi)
    return ji


def reynolds(rho, eta, v, d):
    """
    Bestimmt Reynoldszahl
    :param rho: Dichte [kg/m^3]
    :param eta: Viskosität [Pas]
    :param v: Geschwindigkeit [m/s]
    :param d: charakteristische Länge [m]
    :return: Reynoldszahl [-]
    """
    return rho*v*d/eta


def schmidt(rho, eta, diff):
    """
    Bestimme Schmidtzahl
    :param rho: Dichte [kg/m^3]
    :param eta: Viskosität [Pas]
    :param diff: Diffusionskoeff [m^2/s]
    :return: Schmidtzahl [-]
    """
    return eta/(rho*diff)


def hets_kbs(data, h, m):
    """
    Nth nach Kremser-Brown-Souders-Gleichung
    :param data: array mit [y_ethanolout,y_co2out, x_ethanolout, x_wasserout, y_ethanolin,y_co2in, x_ethanolin, x_wasserin, q_c, q_d, sf, holdup]
    :param h: Kolonnenhöhe [m]
    :param m: durschnittlicher Ke-Wert
    :return: HETS-Wert
    """
    LOGGER.debug("Starting hets_kbs ...")
    x_f = data[6]
    x_r = data[2]
    y_a = data[4]
    lam = data[10] * m

    zähler = (x_f-y_a/m)/(x_r-y_a/m) * (1-1/lam) + 1/lam
    nth = np.log(zähler) / np.log(lam)
    if nth < 1:
        nth = 1
    hets = h/nth
    LOGGER.debug("... Finished hets_kbs")
    return hets


def hets_kbs_bel(data, h, m1, m2, x_e0):
    """
    Nth nach Kremser-Brown-Souders-Gleichung mit Beladungen (bezogen auf Wasser oder CO2 möglich)
    :param data: array mit [y_ethanolout,y_co2out, x_ethanolout, x_wasserout, y_ethanolin,y_co2in, x_ethanolin, x_wasserin, q_c, q_d, sf, holdup]
    :param h: Kolonnenhöhe [m]
    :param m: durschnittlicher Ke-Wert
    :return: HETS-Wert
    """
    LOGGER.debug("Starting hets_kbs_bel ...")

    x_f = data[6]/(data[7])
    x_r = data[2]/(data[3])
    y_a = data[4]/data[5]
    m = m1
    lam = data[10] * m / (1-x_e0)
    zähler = (x_f-y_a/m)/(x_r-y_a/m) * (1-1/lam) + 1/lam
    nth = np.log(zähler) / np.log(lam)
    if nth < 1:
        nth = 1
    hets = h/nth
    LOGGER.debug("... Finished hets_kbs_bel")
    return hets




def make_pmap(T,p,pv):
    """
    Vorhersage der Punkte zur Erstellung einer Perfomancemap
    :param T: Temperatur in K
    :param p: Druck in MPa
    :param pv: Array mit volumenbezogenen Phasenverhältnissen, Bsp.: [5,10,20]
    :return: Speichert Excel-Datei unter fig/Pmap/
    """
    if __name__ == '__main__':
        LOGGER.debug("Starting __name__ == __main__ ...")
        # Zeitmessung starten
        start = time.time()
        # erstellt Zeitstempel
        tlocal = time.localtime()
        current_time = time.strftime("%y%m%d_%H_%M_%S", tlocal)

        datei = "PMap_T%dK_p%dMPa" % (T,p)
        fig_path = 'fig//Pmap//'+datei+'_'+current_time
        os.mkdir(fig_path)
        #excel_path = fig_path + '//Data.xlsx'

        # P3,P4,P5, P1,P2
        # param = [0.00052967241, 1.999, 1.28335579, 0.00375, 0.023]#gesamte Datenbank
        if (MELLAPAK):
            #param = [0.01476669, 1.90732289, 0.78412048, 0.0035, 0.023]  # Mella
            param = [1.99987328, 1.96493915, 1.99690561, 0.00251654, 0.023]  # Mella
        elif (SULZERCY):
            param = [0.13854462, 0.96936199, 0.62412655, 0.00375, 0.023]  # CY
        else:
            raise ValueError('No choice for extraction packing given, please select one!')
        #[5.29672410e-04, 1.99999990e+00, 1.28335579e+00, 3.75000000e-03, 2.30000000e-02]#gesamte Datenbank
        liste_in = []
        pol = Pool(8)
        for i in pv:
            for j in range(1,21,1):
                ipath = '//pv%0d_%02s.xlsx' % (i, j/20)
                excelpath = fig_path+ipath
                c = (35, 10, 4.19, T, p, 0.038, 700, 0.86, 0.004914286, 0.1, i, 0.0, 0.0, 0.1, 0.0, param, j/20, excelpath, fig_path)
                # n, dt, h, T, p, D, a_s, epsilon, d_h, cutoff, pv, y_ethanol_0, y_wasser_0, x_ethanol_0, x_co2_0, Massentransferparameter, Anteil der Belastung, Speicherpfad, Speicherpfad
                liste_in.append(c)

        # one single 'main'-output which is 'model':
        # [y_ethanol_n, y_co2_n, x_ethanol_0, x_w_0, y_ethanol_0, y_co2_0, x_ethanol_n, x_w_n, bel_c, bel_d, sf, hl, pv,
        # hets, d32, v_c, v_d, average_Ke, average_Kw, average_KCO2, bel_c_s, bel_d_s, hets_bel,
        # y_EtOH*, y_H20*, x_EtOH*, x_H20*, q_c, q_d]
        model = np.array(pol.starmap(main, liste_in))
        pol.terminate()
        #Abspeichern
        pmap_rohdaten = pd.DataFrame()
        pmap_rohdaten.loc[:, 'Belastung c'] = 3600*model[:,8]
        pmap_rohdaten.loc[:, 'Belastung d'] = 3600*model[:,9]
        pmap_rohdaten.loc[:, 'PV'] = model[:,12]
        pmap_rohdaten.loc[:, 'Holdup'] = model[:,11]
        pmap_rohdaten.loc[:, 'HETS'] = model[:,13]
        pmap_rohdaten.loc[:, 'HETS-Bel'] = model[:,22]
        pmap_rohdaten.loc[:, 'Sauterdurchmesser'] = model[:, 14]
        pmap_rohdaten.loc[:, 'v_c'] = model[:, 15]
        pmap_rohdaten.loc[:, 'v_d'] = model[:, 16]
        pmap_rohdaten.loc[:, 'avg_Ke'] = model[:, 17]
        pmap_rohdaten.loc[:, 'avg_Kw'] = model[:, 18]
        pmap_rohdaten.loc[:, 'avg_KCo2'] = model[:, 19]
        pmap_rohdaten.loc[:, 'y_ethanol'] = model[:, 0]
        pmap_rohdaten.loc[:, 'y_co2'] = model[:, 1]
        pmap_rohdaten.loc[:, 'x_ethanol'] = model[:, 2]

        # mass weights without y_CO2 in denominator (fits to unit of measurement data)
        pmap_rohdaten.loc[:, 'y_EtOH_N* [kg_EtOH/(kg_EtOH+kg_H20)]'] = model[:, 23]
        pmap_rohdaten.loc[:, 'y_H20_N* [kg_H20/(kg_EtOH+kg_H20)]'] = model[:, 24]
        pmap_rohdaten.loc[:, 'x_EtOH_0* [kg_EtOH/(kg_EtOH+kg_H20)]'] = model[:, 25]
        pmap_rohdaten.loc[:, 'x_H20_0* [kg_H20/(kg_EtOH+kg_H20)]'] = model[:, 26]

        # mass flows [kg/h]
        pmap_rohdaten.loc[:, 'q_c [kg/h]'] = model[:, 27]
        pmap_rohdaten.loc[:, 'q_d [kg/h]'] = model[:, 28]

        flutgrenze = pmap_rohdaten.iloc[19::20, :]
        pmap_writer = pd.ExcelWriter(fig_path+'//übersicht.xlsx')
        pmap_rohdaten.to_excel(pmap_writer, sheet_name='alleDaten')
        flutgrenze.to_excel(pmap_writer, sheet_name='Flutgrenze')
        pmap_writer.save()
        pmap_writer.close()
        e = time.time()
        print("Zeit")
        print((e-start)/60)
        LOGGER.debug(f"Time needed: {(e-start)/60} min")
        LOGGER.debug("... Finished __name__ == __main__ ")


"""
Perfomance Map berechen:
Beispiel: für 313K, 10MPa, Phasenverhältnisse 10, 20, 50

make_pmap(313, 10, [10,20,50])

Falls Kolonnengeometrie geändert werden soll, müssen die Werte in Zeile 461 entsprechend angepasst werden.
"""

LOGGER.debug("Read Inputs ...")
# Mark your choice with True or False
MELLAPAK = True
SULZERCY = False
LOGGER.debug(f"MELLAPK: {MELLAPAK}")
LOGGER.debug(f"SULZERCY: {SULZERCY}")
make_pmap(313,10, [5,10,15,20,25,30,35,40,45,50])
LOGGER.debug("... Read Inputs")


LOGGER.debug("... Finished performancemap.py")