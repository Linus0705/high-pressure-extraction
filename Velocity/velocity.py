import math
import numpy as np
import pandas as pd

def leerrohr(rho_c, rho_d, D, m_d, holdup):
    """Methode zur Bestimmung der Geschwindigkeit nach Fit für Leerrohrgeschw.
    :param rho_c: Dichte kontinuierlich [kg/m^3]
    :param rho_d: Dichte disper [kg/m^3]
    :param D: Kolonnendurchmesser [m]
    :param m_d: Massenstrom dispers [kg/h]
    :param holdup: Volumenanteil der dispersen Phase [-]
    :return v_cal: Geschwindigkeit der dispersen Phase [m/s]"""
    pi = math.pi
    A = pi*D**2/4
    pi1 = rho_c/(rho_d-rho_c)
    pi2 = math.exp(-6 * holdup / pi)
    a = 0.418988864
    b = 80.4818148
    n = 382.271862
    v_leer = m_d/(rho_d*A*3600)
    v_cal = n*v_leer*pi1**a*pi2**b
    return v_cal


def seibert(rho_c, rho_d, d32, holdup):
    """Methode zur Bestimmung der Geschwindigkeit nach Modell von Seibert
    :param rho_c: Dichte kontinuierlich [kg/m^3]
    :param rho_d: Dichte disper [kg/m^3]
    :param d32: Sauterdurchmesser [m]
    :param holdup: Volumenanteil der dispersen Phase [-]
    :return v_cal: Geschwindigkeit der dispersen Phase [m/s]"""
    pi = math.pi
    pi1 = rho_c / (rho_d - rho_c)
    pi2 = math.exp(-6 * holdup / pi)
    g = 9.81
    n = 0.005182868
    a = 0.016830648
    b = -67.90858628
    v_seibert = (4*g*d32/(3*pi1))**0.5
    v_cal = n * v_seibert * pi1**a * pi2**b
    return v_cal


def velocity_c(rho_c, D, m_c, epsilon, holdup):
    """
    Berechnet Geschwindigkeit der konti Phase
    :param rho_c: Dichte der konti Phase [kg/m^3]
    :param D: Durchmesser der Kolonne [m]
    :param m_c: Massenstrom der konti Phase [kg/h]
    :param epsilon: Leervolumenanteil
    :param holdup: Volumenanteil disperse Phase [-]
    :return: Geschwindigkeit der konti Phase [m/s]
    """
    Vpunkt = m_c/(rho_c*3600)  # Volumenstrom [m^3/s]
    S = math.pi/4 * D**2  # Kolonnenfläche [m^2]
    v_c = Vpunkt/(S*epsilon*(1-holdup))
    return v_c


def velocity_c_np(df):
    """
    Berechnet Geschwindigkeit der konti Phase für numpy array
    :param df: Dataframe das Dichte der konti Phase [kg/m^3]
     Durchmesser der Kolonne [m]
     Massenstrom der konti Phase [kg/h]
     epsilon: Leervolumenanteil
     holdup: Volumenanteil disperse Phase [-] enthalten müsen
    :return: Geschwindigkeit der konti Phase [m/s]
    """
    Vpunkt = np.array(df['Re']).reshape(-1, 1).astype(np.float32)
    sc = np.array(df['Sc']).reshape(-1, 1).astype(np.float32)

    Vpunkt = m_c/(rho_c*3600)  # Volumenstrom [m^3/s]
    S = math.pi/4 * D**2  # Kolonnenfläche [m^2]
    v_c = Vpunkt/(S*epsilon*(1-holdup))
    return v_c


def slip(rho_c, rho_d, D, m_c, m_d, epsilon, holdup):
    """
    Gibt slip Velocity aus [m/s] (Zweischichtmodell)
    :param rho_c: Dichte konti Phase [kg/m^3]
    :param rho_d: Dichte disperse Phase [kg/m^3]
    :param D: Kolonnendurchmesser [m^]
    :param m_c: Massenstrom konti [kg/h]
    :param m_d: Massenstrom disper [kg/h]
    :param epsilon: Leeraumanteil Kolonne
    :param holdup: Volumenanteil disperse Phase
    :return:
    """
    v_c = velocity_c(rho_c=rho_c, D=D, m_c=m_c, epsilon=epsilon, holdup=holdup)
    v_d = leerrohr(rho_c=rho_c, rho_d=rho_d, m_d=m_d, holdup=holdup, D=D)
    # todo: überprüfe ob v_d noch durch epsilon*hold-up geteilt werden muss
    vs = v_d + v_c
    return vs


def chunwilkinson(rho_c, x_ethanol):
    """
    Berechnung der Grenzflächenspannung nach Chun&Wilkinsion 1995
    :param rho_c: Dichte überkritisches CO2 [kg/m^3]
    :param x_ethanol: Stoffmengenanteil ethanol
    :return sigma: Grenzflächenspannung [N/m]
    """
    # lineare Interpolation
    if x_ethanol <= 0.016:
        a = -0.2333 + x_ethanol * ((-0.249+0.2333)/0.016)
    else:
        a = -0.2490 + (x_ethanol-0.016) * ((-0.3016+0.2490) / (0.0332-0.016))

    b = 1.66
    sigma = math.exp(b+a*math.log(rho_c))/1000  # Umrechung in N/m
    return sigma


def loadalldata():
    """
    Methode um alle Excelsheets im Inputordner einzuladen welche in der Methode benannt werden
    :return: ein Dataframe mit allen Daten
    """

    import pandas as pd

    def loaddatafromexcel(filename, columns_used, column_names, categorical_or_boolean_columns_names,
                          rows_header):
        """
        Läd Daten aus einzelner Excel Datei
        :param filename: String Name der Datei
        :param columns_used: array zB. [1,2,4] Spalten in Excel mit einzulesenden Daten, beginne mit 0 zu zählen
        :param column_names: array zB. ['v_g','massentransport'] Spalten-Namen der einzulesenden Daten
        :param categorical_or_boolean_columns_names: array zB ['massentransport']
        :param rows_header: int zB 3 oberste Zeilen in Excel mit Überschriften, Variablenname und Einheiten, beginne mit 1 zu zählenrows_header = 3  # oberste Zeilen in Excel mit Überschriften, Variablenname und Einheiten, beginne mit 1 zu zählen
        :return:
        """
        columns_used = np.array(columns_used)
        # Pfadname der Exeldatei realtiv vom Speicherort des Skriptes Backslash macht evtl unter Linux Probleme und muss durch Slash ersetzt werden
        pathname = 'Velocity/Input/' + filename + '.xlsx'
        # einlesen der Daten in ein Pandas Dataframe
        df = pd.read_excel(pathname, usecols=columns_used, names=column_names, header=None, skiprows=rows_header)
        # Filtern aller Zeilen mit fehlenden Daten, bei vielen fehlenden Daten kann man über Stragetien zum Füllen der Daten nachdenken
        df = df.dropna()
        df[categorical_or_boolean_columns_names] = df[categorical_or_boolean_columns_names].astype(
            np.int64)  # Kategorische Daten bei uns sind ganzzahlig und werden als integer gespeichert, eine Unterscheidung im Skript findet auch genau darüber statt
        return df

    # Läd drei verschiedene Dataframes ein
    # Hochdruck
    df_velocity_c = pd.concat([
        loaddatafromexcel(filename="Velocity_konti", columns_used=[1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
                          column_names=["v_c", "p", "T", "m_co2", "m_w", "rho_c", "eta_c", "D", "a_s", "epsilon"],
                          categorical_or_boolean_columns_names=[], rows_header=[0, 1, 2])

    ])
    df_velocity_d = pd.concat([
        loaddatafromexcel(filename="Velocity_dispers",
                          columns_used=[2, 3, 4, 5, 6, 7, 8, 9, 10, 14, 12, 15, 16],
                          column_names=["v_d", "p", "T", "m_co2", "m_w", "PV", "rho_c", "eta_c", "rho_d", "eta_d",
                                         "D", "a_s", "epsilon"],
                          categorical_or_boolean_columns_names=[], rows_header=[0, 1, 2])

    ])

    # resettet Index, um doppelt vorkommende Indices zu vermeiden
    df_velocity_c = df_velocity_c.reset_index(drop=True)
    df_velocity_d = df_velocity_d.reset_index(drop=True)
    print('Alle Daten eingelesen')
    return df_velocity_c, df_velocity_d


# Datengetrieben

def train_models():
    """
    Methode um Modelle neu zu trainieren und zu speichern.
    :return:
    """
    from sklearn.svm import SVR
    from sklearn.linear_model import LinearRegression
    from sklearn.neural_network import MLPRegressor
    from sklearn.gaussian_process import GaussianProcessRegressor
    from sklearn.gaussian_process.kernels import RBF, RationalQuadratic, Matern
    from sklearn.preprocessing import MinMaxScaler, OneHotEncoder
    from sklearn.model_selection import train_test_split
    from sklearn.pipeline import Pipeline
    from sklearn.compose import ColumnTransformer
    from sklearn.ensemble import RandomForestRegressor
    import joblib

    def train(mla, df):
        def splitfeaturetarget(df, targetname):
            """
            Methode um Feature und Target-Dataframe zu erstellen
            :param df: Dataframe mit Features und Target
            :return: x, y Dataframes
            """
            import numpy as np
            y = np.array(df[targetname]).reshape(-1, 1).astype(np.float32)
            x = df.drop(columns=[targetname])
            return x, y

        x, y = splitfeaturetarget(df, targetname=targetname)
        categorical_features = x.columns[x.dtypes.apply(lambda c: np.issubdtype(c, np.int64))].tolist()
        numeric_features = x.columns[x.dtypes.apply(lambda c: np.issubdtype(c, np.float64))].tolist()

        categorical_transformer = OneHotEncoder(drop='first', categories='auto')
        numeric_transformer = MinMaxScaler()

        preprocessor = ColumnTransformer(transformers=[
            ('num', numeric_transformer, numeric_features),
            ('cat', categorical_transformer, categorical_features)])

        pipeline = Pipeline([
            ('preprocessor', preprocessor),
            ('MLA', mla)
        ])
        pipeline.fit(x, y.ravel())

        return pipeline

    # Zielgröße
    targetname = "v_c"

    # Lädt Daten ein
    df_v_c, df_v_d = loadalldata()
    df_train_c, df_test_c = train_test_split(df_v_c, test_size=0.25, shuffle=True)
    df_train_d, df_test_d = train_test_split(df_v_d, test_size=0.25, shuffle=True)
    # Definiert Modelle
    #mla_v_c = GaussianProcessRegressor(alpha=1e-07, kernel=1**2 * Matern(length_scale=1, nu=1.5), n_restarts_optimizer=5, normalize_y=False)
    mla_v_c = SVR(C=25, cache_size=1000, epsilon=0.0001, gamma='auto', kernel='rbf', tol=0.0001)
    # Trainiert Modelle
    trained_model_v_c = train(mla_v_c, df_v_c)
    trained_model_c_ohneTest = train(mla_v_c, df_train_c)
    #mla_v_d = GaussianProcessRegressor(alpha=1e-06, kernel=1**2 * RBF(length_scale=1), n_restarts_optimizer=5, normalize_y=False)
    mla_v_d = SVR(C=25, cache_size=1000, epsilon=0.0001, gamma='auto', kernel='rbf', tol=1e-07)

    targetname="v_d"
    # Trainiert Modelle
    trained_model_v_d = train(mla_v_d, df_v_d)
    trained_model_d_ohneTest = train(mla_v_d, df_train_d)

    # Speichert Modelle
    joblib.dump(trained_model_v_c, 'pkl-Dateien/MLA_v_c.pkl')
    joblib.dump(trained_model_v_d, 'pkl-Dateien/MLA_v_d.pkl')
    joblib.dump(trained_model_c_ohneTest, 'pkl-Dateien/MLA_v_c_ohneTest.pkl')
    joblib.dump(trained_model_d_ohneTest, 'pkl-Dateien/MLA_v_d_ohneTest.pkl')

def make_v_c_prediction(p, T, m_co2, m_w, rho_c, eta_c, D, a_s, epsilon):
    """

    :param p: Druck [bar]
    :param T: Temperatur [K]
    :param m_co2: Massenstrom co2 [kg/h]
    :param m_w: Massensrom wasser [kg/h]
    :param rho_c: Dichte CO2 [kg/m^3]
    :param eta_c: Viskosität CO2 [mikroPas]
    :param D: Kolonnendurchmesser [m]
    :param a_s: spez. Oberfläche pro Vol Packung [m^-1]
    :param epsilon: Leervolumenanteil
    :return: yp_v_c: Geschwindigkeit CO2 Phase [m/s]
    """
    import joblib

    # läd modell
    pipeline_v_c = joblib.load('Velocity/pkl-Dateien/MLA_v_c.pkl')
    # erstellt abfragepunkt
    X = pd.DataFrame(columns=["p", "T", "m_co2", "m_w", "rho_c", "eta_c", "D", "a_s", "epsilon"])

    X.loc[0, 'p'] = float(p)
    X.loc[0, 'T'] = float(T)
    X.loc[0, 'm_co2'] = float(m_co2)
    X.loc[0, 'm_w'] = float(m_w)
    X.loc[0, 'rho_c'] = float(rho_c)
    X.loc[0, 'eta_c'] = float(eta_c)
    X.loc[0, 'D'] = float(D)
    X.loc[0, 'a_s'] = float(a_s)
    X.loc[0, 'epsilon'] = float(epsilon)

    yp_v_c = pipeline_v_c.predict(X)

    return float(yp_v_c)


def make_v_d_prediction(p, T, m_co2, m_w, PV, rho_c, eta_c, rho_d, eta_d,
                                         D, a_s, epsilon):
    """

    :param p: Druck [bar]
    :param T: Temperatur [K]
    :param m_co2: Massenstrom co2 [kg/h]
    :param m_w: Massensrom wasser [kg/h]
    :param PV: Phasenverhältnis
    :param rho_c: Dichte CO2 [kg/m^3]
    :param eta_c: Viskosität CO2 [Pas]
    :param rho_d: Dichte Wasser [kg/m^3]
    :param eta_d: Viskosität Wasser [Pas]
    :param D: Kolonnendurchmesser [m]
    :param a_s: spez. Oberfläche pro Vol Packung [m^-1]
    :param epsilon: Leervolumenanteil
    :return: yp_v_d: Geschwindigkeit disperse Phase[m/s]
    """
    import joblib

    # läd modell
    pipeline_v_d = joblib.load('Velocity/pkl-Dateien/MLA_v_d.pkl')
    # erstellt abfragepunkt
    X = pd.DataFrame(columns=["p", "T", "m_co2", "m_w", "PV", "rho_c", "eta_c", "rho_d", "eta_d",
                                        "D", "a_s", "epsilon"])

    X.loc[0, 'p'] = float(p)
    X.loc[0, 'T'] = float(T)
    X.loc[0, 'm_co2'] = float(m_co2)
    X.loc[0, 'm_w'] = float(m_w)
    X.loc[0, 'PV'] = float(PV)
    X.loc[0, 'rho_c'] = float(rho_c)
    X.loc[0, 'eta_c'] = float(eta_c)
    X.loc[0, 'rho_d'] = float(rho_d)
    X.loc[0, 'eta_d'] = float(eta_d)
    X.loc[0, 'D'] = float(D)
    X.loc[0, 'a_s'] = float(a_s)
    X.loc[0, 'epsilon'] = float(epsilon)

    yp_v_d = pipeline_v_d.predict(X)

    return float(yp_v_d)

def parityplotdata(writer, df_v_c, df_v_d):

    import joblib

    # läd modell
    pipeline_v_c = joblib.load('Velocity/pkl-Dateien/MLA_v_c_ohneTest.pkl')
    pipeline_v_d = joblib.load('Velocity/pkl-Dateien/MLA_v_d_ohneTest.pkl')


    def splitfeaturetarget(df, targetname):
        """
        Methode um Feature und Target-Dataframe zu erstellen
        :param df: Dataframe mit Features und Target
        :return: x, y Dataframes
        """
        import numpy as np
        y = np.array(df[targetname]).reshape(-1, 1).astype(np.float32)
        x = df.drop(columns=[targetname])
        return x, y

    x_v_c, y_v_c = splitfeaturetarget(df_v_c, targetname="v_c")
    x_v_d, y_v_d = splitfeaturetarget(df_v_d, targetname="v_d")
    y_v_c_pred = pipeline_v_c.predict(x_v_c).reshape(-1, 1)
    y_v_d_pred = pipeline_v_d.predict(x_v_d).reshape(-1, 1)
    df_pred_v_c = pd.DataFrame()
    df_pred_v_c['Actual Value'] = y_v_c.ravel()
    df_pred_v_c['Prediction'] = y_v_c_pred
    df_pred_v_c.to_excel(writer, sheet_name='Prädiktionen v_c')
    x_v_c.to_excel(writer, sheet_name='Input v_c')

    df_pred_v_d = pd.DataFrame()
    df_pred_v_d['Actual Value'] = y_v_d.ravel()
    df_pred_v_d['Prediction'] = y_v_d_pred
    df_pred_v_d.to_excel(writer, sheet_name='Prädiktionen v_d')
    x_v_d.to_excel(writer, sheet_name='Input v_d')
    yp_v_c = np.array(df_pred_v_c['Prediction']).reshape(-1, 1).astype(np.float32)
    yp_v_d = np.array(df_pred_v_d['Prediction']).reshape(-1, 1).astype(np.float32)



    return y_v_c, y_v_d, yp_v_c, yp_v_d

# Befehl um Modelle zu trainieren
#train_models()

