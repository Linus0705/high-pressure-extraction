import pandas as pd
import numpy as np


def loadalldata():
    """
    Methode um alle Excelsheets im Inputordner einzuladen welche in der Methode benannt werden
    :return: ein Dataframe mit allen Daten
    """
    import numpy as np
    import pandas as pd
    def loaddatafromexcel(process, source, columns_used, column_names, categorical_or_boolean_columns_names,
                          rows_header):
        """
        Läd Daten aus einzelner Excel Datei
        :param process: string zB 'Extraktion','Destillation' oder 'Hochdruck
        :param source: string zB 'Thornton'
        :param columns_used: array zB. [1,2,4] Spalten in Excel mit einzulesenden Daten, beginne mit 0 zu zählen
        :param column_names: array zB. ['v_g','massentransport'] Spalten-Namen der einzulesenden Daten
        :param categorical_or_boolean_columns_names: array zB ['massentransport']
        :param rows_header: int zB 3 oberste Zeilen in Excel mit Überschriften, Variablenname und Einheiten, beginne mit 1 zu zählenrows_header = 3  # oberste Zeilen in Excel mit Überschriften, Variablenname und Einheiten, beginne mit 1 zu zählen
        :return:
        """
        columns_used = np.array(columns_used)
        # Pfadname der Exeldatei realtiv vom Speicherort des Skriptes Bachslash macht evtl unter Linux Probleme und muss durch Slash ersetzt werden
        pathname = 'Flooding/Input/Flutpunkte_gepackt_' + process + '_' + source
        # einlesen der Daten in ein Pandas Dataframe
        df = pd.read_excel(pathname, usecols=columns_used, names=column_names, header=None, skiprows=rows_header)
        # Filtern aller Zeilen mit fehlenden Daten, bei vielen fehlenden Daten kann man über Stragetien zum Füllen der Daten nachdenken
        df = df.dropna()
        df[categorical_or_boolean_columns_names] = df[categorical_or_boolean_columns_names].astype(
            np.int64)  # Kategorische Daten bei uns sind ganzzahlig und werden als integer gespeichert, eine Unterscheidung im Skript findet auch genau darüber statt
        return df

    # Läd drei verschiedene Dataframes ein

    # Destillation
    """df_des = pd.concat([
        loaddatafromexcel(source='Liu', process='Destillation',
                          columns_used=[3, 4, 5, 6, 7, 8, 10, 12, 13, 14, 15, 16],
                          column_names=["$(V_{\\textrm{c}}+V_{\\textrm{d}})_{\\textrm{f}}$", '$PV$',
                                        "$\\rho_{\\textrm{c}}$", "$\\rho_{\\textrm{d}}$", "$\\eta_{\\textrm{c}}$",
                                        "$\\eta_{\\textrm{d}}$", '$D$',
                                        "$a_{\\textrm{s}}$", "$\\epsilon_{\\textrm{v}}$", "Tr", "Mat", "Pt"],
                          categorical_or_boolean_columns_names=["Tr", "Mat", "Pt"], rows_header=3),
        loaddatafromexcel(source='Brunazzi', process='Destillation',
                          columns_used=[3, 4, 5, 6, 7, 8, 10, 12, 13, 14, 15, 16],
                          column_names=["$(V_{\\textrm{c}}+V_{\\textrm{d}})_{\\textrm{f}}$", '$PV$',
                                        "$\\rho_{\\textrm{c}}$", "$\\rho_{\\textrm{d}}$", "$\\eta_{\\textrm{c}}$",
                                        "$\\eta_{\\textrm{d}}$", '$D$',
                                        "$a_{\\textrm{s}}$", "$\\epsilon_{\\textrm{v}}$", "Tr", "Mat", "Pt"],
                          categorical_or_boolean_columns_names=["Tr", "Mat", "Pt"], rows_header=3),
        loaddatafromexcel(source='Mackowiak', process='Destillation',
                          columns_used=[3, 4, 5, 6, 7, 8, 10, 12, 13, 14, 15, 16],
                          column_names=["$(V_{\\textrm{c}}+V_{\\textrm{d}})_{\\textrm{f}}$", '$PV$',
                                        "$\\rho_{\\textrm{c}}$", "$\\rho_{\\textrm{d}}$", "$\\eta_{\\textrm{c}}$",
                                        "$\\eta_{\\textrm{d}}$", '$D$',
                                        "$a_{\\textrm{s}}$", "$\\epsilon_{\\textrm{v}}$", "Tr", "Mat", "Pt"],
                          categorical_or_boolean_columns_names=["Tr", "Mat", "Pt"], rows_header=3),
        loaddatafromexcel(source='Sherwood_und_Drinkwater', process='Destillation',
                          columns_used=[3, 4, 5, 6, 7, 8, 10, 12, 13, 14, 15, 16],
                          column_names=["$(V_{\\textrm{c}}+V_{\\textrm{d}})_{\\textrm{f}}$", '$PV$',
                                        "$\\rho_{\\textrm{c}}$", "$\\rho_{\\textrm{d}}$", "$\\eta_{\\textrm{c}}$",
                                        "$\\eta_{\\textrm{d}}$", '$D$',
                                        "$a_{\\textrm{s}}$", "$\\epsilon_{\\textrm{v}}$", "Tr", "Mat", "Pt"],
                          categorical_or_boolean_columns_names=["Tr", "Mat", "Pt"], rows_header=3)
    ])

    # Extraktion
    df_ex = pd.concat([
        loaddatafromexcel(source='Seibert', process='Extraktion',
                          columns_used=[3, 4, 5, 6, 7, 8, 10, 12, 13, 14, 15, 16],
                          column_names=["$(V_{\\textrm{c}}+V_{\\textrm{d}})_{\\textrm{f}}$", '$PV$',
                                        "$\\rho_{\\textrm{c}}$", "$\\rho_{\\textrm{d}}$", "$\\eta_{\\textrm{c}}$",
                                        "$\\eta_{\\textrm{d}}$", '$D$',
                                        "$a_{\\textrm{s}}$", "$\\epsilon_{\\textrm{v}}$", "Tr", "Mat", "Pt"],
                          categorical_or_boolean_columns_names=["Tr", "Mat", "Pt"], rows_header=3),
        loaddatafromexcel(source='Thornton', process='Extraktion',
                          columns_used=[3, 4, 5, 6, 7, 8, 10, 12, 13, 14, 15, 16],
                          column_names=["$(V_{\\textrm{c}}+V_{\\textrm{d}})_{\\textrm{f}}$", '$PV$',
                                        "$\\rho_{\\textrm{c}}$", "$\\rho_{\\textrm{d}}$", "$\\eta_{\\textrm{c}}$",
                                        "$\\eta_{\\textrm{d}}$", '$D$',
                                        "$a_{\\textrm{s}}$", "$\\epsilon_{\\textrm{v}}$", "Tr", "Mat", "Pt"],
                          categorical_or_boolean_columns_names=["Tr", "Mat", "Pt"], rows_header=3),
        loaddatafromexcel(source='Mackowiak', process='Extraktion',
                          columns_used=[3, 4, 5, 6, 7, 8, 10, 12, 13, 14, 15, 16],
                          column_names=["$(V_{\\textrm{c}}+V_{\\textrm{d}})_{\\textrm{f}}$", '$PV$',
                                        "$\\rho_{\\textrm{c}}$", "$\\rho_{\\textrm{d}}$", "$\\eta_{\\textrm{c}}$",
                                        "$\\eta_{\\textrm{d}}$", '$D$',
                                        "$a_{\\textrm{s}}$", "$\\epsilon_{\\textrm{v}}$", "Tr", "Mat", "Pt"],
                          categorical_or_boolean_columns_names=["Tr", "Mat", "Pt"], rows_header=3)
    ])"""

    # Hochdruck
    df_hp = pd.concat([
        loaddatafromexcel(source='Meyer.xlsx', process='Hochdruck',
                          columns_used=[3, 4, 7, 8, 9, 10, 12, 14, 15, 16, 17, 18],
                          column_names=["$(V_{\\textrm{c}}+V_{\\textrm{d}})_{\\textrm{f}}$", '$PV$',
                                        "$\\rho_{\\textrm{c}}$", "$\\rho_{\\textrm{d}}$", "$\\eta_{\\textrm{c}}$",
                                        "$\\eta_{\\textrm{d}}$", '$D$',
                                        "$a_{\\textrm{s}}$", "$\\epsilon_{\\textrm{v}}$", "Tr", "Mat", "Pt"],
                          categorical_or_boolean_columns_names=["Tr", "Mat", "Pt"], rows_header=3),
        loaddatafromexcel(source='Stockfleth.xls', process='Hochdruck',
                          columns_used=[3, 4, 7, 8, 9, 10, 12, 14, 15, 16, 17, 18],
                          column_names=["$(V_{\\textrm{c}}+V_{\\textrm{d}})_{\\textrm{f}}$", '$PV$',
                                        "$\\rho_{\\textrm{c}}$", "$\\rho_{\\textrm{d}}$", "$\\eta_{\\textrm{c}}$",
                                        "$\\eta_{\\textrm{d}}$", '$D$',
                                        "$a_{\\textrm{s}}$", "$\\epsilon_{\\textrm{v}}$", "Tr", "Mat", "Pt"],
                          categorical_or_boolean_columns_names=["Tr", "Mat", "Pt"], rows_header=3)
    ])

    # resettet Index, um doppelt vorkommende Indices zu vermeiden
    # df_des = df_des.reset_index(drop=True)
    # df_ex = df_ex.reset_index(drop=True)
    df_hp = df_hp.reset_index(drop=True)
    print('Alle Daten eingelesen')
    return df_hp


def train_models():
    """
    Methode um Modelle neu zu trainieren und zu speichern.
    :return:
    """
    from sklearn.gaussian_process import GaussianProcessRegressor
    from sklearn.neural_network import MLPRegressor
    from sklearn.ensemble import RandomForestRegressor
    from sklearn.gaussian_process.kernels import (RBF)
    from sklearn.preprocessing import MinMaxScaler, OneHotEncoder
    from sklearn.pipeline import Pipeline
    from sklearn.compose import ColumnTransformer
    import joblib



    def train(mla, df):
        def splitfeaturetarget(df, targetname):
            """
            Methode um Feature und Target-Dataframe zu erstellen
            :param df: Dataframe mit Features und Target
            :return: x, y Dataframes
            """
            import numpy as np
            y = np.array(df[targetname]).reshape(-1, 1).astype(np.float32)
            x = df.drop(columns=[targetname])
            return x, y

        x, y = splitfeaturetarget(df, targetname=targetname)
        categorical_features = x.columns[x.dtypes.apply(lambda c: np.issubdtype(c, np.int64))].tolist()
        numeric_features = x.columns[x.dtypes.apply(lambda c: np.issubdtype(c, np.float64))].tolist()

        categorical_transformer = OneHotEncoder(drop='first', categories='auto')
        numeric_transformer = MinMaxScaler()

        preprocessor = ColumnTransformer(transformers=[
            ('num', numeric_transformer, numeric_features),
            ('cat', categorical_transformer, categorical_features)])

        pipeline = Pipeline([
            ('preprocessor', preprocessor),
            ('MLA', mla)
        ])
        pipeline.fit(x, y.ravel())

        return pipeline

    # Zielgröße
    targetname = "$(V_{\\textrm{c}}+V_{\\textrm{d}})_{\\textrm{f}}$"

    # Lädt Daten ein
    df_hp = loadalldata()

    # Fallenlassen der Dichte bei Destillation verbessert Score
    """df_des = df_des.drop(axis=1, columns='$\\rho_{\\textrm{c}}$')

    # Definiert Modelle
    mla_ex = RandomForestRegressor(bootstrap=True, criterion='mse', max_depth=None,
                                   max_features=None, max_leaf_nodes=None,
                                   min_impurity_decrease=0.0, min_impurity_split=None,
                                   min_samples_leaf=1, min_samples_split=2,
                                   min_weight_fraction_leaf=0.0, n_estimators=1000,
                                   n_jobs=None, oob_score=False, random_state=None,
                                   verbose=0, warm_start=False)

    mla_des = MLPRegressor(activation='tanh', alpha=0.001, batch_size='auto', beta_1=0.9,
                           beta_2=0.999, early_stopping=False, epsilon=1e-08,
                           hidden_layer_sizes=(50, 50), learning_rate='constant',
                           learning_rate_init=0.001, max_iter=50000, momentum=0.9,
                           n_iter_no_change=10, nesterovs_momentum=True, power_t=0.5,
                           random_state=None, shuffle=True, solver='lbfgs', tol=1e-08,
                           validation_fraction=0.1, verbose=False,
                           warm_start=False)"""

    mla_hp = GaussianProcessRegressor(alpha=1e-06, copy_X_train=True,
                                      kernel=1 ** 2 * RBF(length_scale=1),
                                      n_restarts_optimizer=5, normalize_y=False,
                                      optimizer='fmin_l_bfgs_b', random_state=None)

    # Trainiert Modelle
    #trained_model_des = train(mla_des, df_des)
    #trained_model_ex = train(mla_ex, df_ex)
    trained_model_hp = train(mla_hp, df_hp)

    # Speichert Modelle
    #joblib.dump(trained_model_des, 'MLA_flooding_point_packed_des.pkl')
    #joblib.dump(trained_model_ex, 'MLA_flooding_point_packed_ex.pkl')
    joblib.dump(trained_model_hp, 'MLA_flooding_point_packed_hp.pkl')


def make_floodingpoint_predictions_ex(PV, rho_c, rho_d, eta_c, eta_d, D, a_s, epsilon, Mat, Pt, Tr):
    """
    Methode um einzelnen Punkt abzufragen
    :param PV: Phasenverhältnis dispers zu kontinuierliche Phase [-]
    :param rho_c: Dichte der kontinuierlichen Phase [kg/m³]
    :param rho_d: Dichte der dispersen Phase [kg/m³]
    :param eta_c: dynamische Viskosität der kontinuierlichen Phase [Pa*s]
    :param eta_d: dynamische Viskosität der dispersen Phase [Pa*s]
    :param D: Kolonneninnendurchmesser [m]
    :param a_s: spezifische Packungsoberfläche [m²/m³]
    :param epsilon: Lückengrad der Packung [-]
    :param Mat: Packungsmaterial: 101 metallisch, 102 Kunststoff, 103 keramisch
    :param Pt: Packungstyp: 1 Füllkörper, 2 strukturierte Packung
    :param Tr: Stofftransportrichtung: 1 aus der dispersen Phase in die kontinuierliche, 2 andersherum, 4 kein Stofftransport

    :return: (V_c+V_d)_f: Flutbelastung [m/s]
    """
    import joblib

    # läd modell
    pipeline = joblib.load('MLA_flooding_point_packed_ex.pkl')

    # erstellt abfragepunkt
    X = pd.DataFrame(columns=['$PV$', '$\\rho_{\\textrm{c}}$', '$\\rho_{\\textrm{d}}$', '$\\eta_{\\textrm{c}}$',
                              '$\\eta_{\\textrm{d}}$', '$D$', '$a_{\\textrm{s}}$',
                              '$\\epsilon_{\\textrm{v}}$', 'Tr', 'Mat', 'Pt'])

    X.loc[0, '$PV$'] = float(PV)
    X.loc[0, '$\\rho_{\\textrm{c}}$'] = float(rho_c)
    X.loc[0, '$\\rho_{\\textrm{d}}$'] = float(rho_d)
    X.loc[0, '$\\eta_{\\textrm{c}}$'] = float(eta_c)
    X.loc[0, '$\\eta_{\\textrm{d}}$'] = float(eta_d)
    X.loc[0, '$D$'] = float(D)
    X.loc[0, '$a_{\\textrm{s}}$'] = float(a_s)
    X.loc[0, '$\\epsilon_{\\textrm{v}}$'] = float(epsilon)
    X.loc[0, 'Tr'] = int(Tr)
    X.loc[0, 'Mat'] = int(Mat)
    X.loc[0, 'Pt'] = int(Pt)

    yp = pipeline.predict(X)

    return float(yp)


def make_floodingpoint_predictions_des(PV, rho_d, eta_c, eta_d, D, a_s, epsilon, Mat, Pt, Tr):
    """
    Methode um einzelnen Punkt abzufragen
    :param PV: Phasenverhältnis dispers zu kontinuierliche Phase [-]
    :param rho_c: Dichte der kontinuierlichen Phase [kg/m³]
    :param rho_d: Dichte der dispersen Phase [kg/m³]
    :param eta_c: dynamische Viskosität der kontinuierlichen Phase [Pa*s]
    :param eta_d: dynamische Viskosität der dispersen Phase [Pa*s]
    :param D: Kolonneninnendurchmesser [m]
    :param a_s: spezifische Packungsoberfläche [m²/m³]
    :param epsilon: Lückengrad der Packung [-]
    :param Mat: Packungsmaterial: 101 metallisch, 102 Kunststoff, 103 keramisch
    :param Pt: Packungstyp: 1 Füllkörper, 2 strukturierte Packung
    :param Tr: Stofftransportrichtung: 1 aus der dispersen Phase in die kontinuierliche, 2 andersherum, 4 kein Stofftransport

    :return: (V_c+V_d)_f: Flutbelastung [m/s]
    """
    import joblib

    # läd modell
    pipeline = joblib.load('MLA_flooding_point_packed_des.pkl')

    # erstellt abfragepunkt
    X = pd.DataFrame(columns=['$PV$', '$\\rho_{\\textrm{d}}$', '$\\eta_{\\textrm{c}}$',
                              '$\\eta_{\\textrm{d}}$', '$D$', '$a_{\\textrm{s}}$',
                              '$\\epsilon_{\\textrm{v}}$', 'Tr', 'Mat', 'Pt'])

    X.loc[0, '$PV$'] = float(PV)
    X.loc[0, '$\\rho_{\\textrm{d}}$'] = float(rho_d)
    X.loc[0, '$\\eta_{\\textrm{c}}$'] = float(eta_c)
    X.loc[0, '$\\eta_{\\textrm{d}}$'] = float(eta_d)
    X.loc[0, '$D$'] = float(D)
    X.loc[0, '$a_{\\textrm{s}}$'] = float(a_s)
    X.loc[0, '$\\epsilon_{\\textrm{v}}$'] = float(epsilon)
    X.loc[0, 'Tr'] = int(Tr)
    X.loc[0, 'Mat'] = int(Mat)
    X.loc[0, 'Pt'] = int(Pt)

    yp = pipeline.predict(X)

    return float(yp)


def make_floodingpoint_predictions_hp(PV, rho_c, rho_d, eta_c, eta_d, D, a_s, epsilon, Mat, Pt, Tr):
    """
    Methode um einzelnen Punkt abzufragen
    :param PV: Phasenverhältnis dispers zu kontinuierliche Phase [-]
    :param rho_c: Dichte der kontinuierlichen Phase [kg/m³]
    :param rho_d: Dichte der dispersen Phase [kg/m³]
    :param eta_c: dynamische Viskosität der kontinuierlichen Phase [Pa*s]
    :param eta_d: dynamische Viskosität der dispersen Phase [Pa*s]
    :param D: Kolonneninnendurchmesser [m]
    :param a_s: spezifische Packungsoberfläche [m²/m³]
    :param epsilon: Lückengrad der Packung [-]
    :param Mat: Packungsmaterial: 101 metallisch, 102 Kunststoff, 103 keramisch
    :param Pt: Packungstyp: 1 Füllkörper, 2 strukturierte Packung
    :param Tr: Stofftransportrichtung: 1 aus der dispersen Phase in die kontinuierliche, 2 andersherum, 4 kein Stofftransport

    :return: (V_c+V_d)_f: Flutbelastung [m/s]
    """
    import joblib

    # läd modell
    pipeline = joblib.load('Flooding/MLA_flooding_point_packed_hp.pkl')

    # erstellt abfragepunkt
    X = pd.DataFrame(columns=['$PV$', '$\\rho_{\\textrm{c}}$', '$\\rho_{\\textrm{d}}$', '$\\eta_{\\textrm{c}}$',
                              '$\\eta_{\\textrm{d}}$', '$D$', '$a_{\\textrm{s}}$',
                              '$\\epsilon_{\\textrm{v}}$', 'Tr', 'Mat', 'Pt'])

    X.loc[0, '$PV$'] = float(PV)
    X.loc[0, '$\\rho_{\\textrm{c}}$'] = float(rho_c)
    X.loc[0, '$\\rho_{\\textrm{d}}$'] = float(rho_d)
    X.loc[0, '$\\eta_{\\textrm{c}}$'] = float(eta_c)
    X.loc[0, '$\\eta_{\\textrm{d}}$'] = float(eta_d)
    X.loc[0, '$D$'] = float(D)
    X.loc[0, '$a_{\\textrm{s}}$'] = float(a_s)
    X.loc[0, '$\\epsilon_{\\textrm{v}}$'] = float(epsilon)
    X.loc[0, 'Tr'] = int(Tr)
    X.loc[0, 'Mat'] = int(Mat)
    X.loc[0, 'Pt'] = int(Pt)

    yp = pipeline.predict(X)

    return float(yp)


def stockfleth_korr(rho_c, rho_d, pv, epsilon, d_h):
    """

    :param rho_c:
    :param rho_d:
    :param D:
    :param a_s:
    :param epsilon:
    :param d_h:
    :return:
    """

    g = 9.81
    zaehler = 1 + 1/pv
    nenner = np.sqrt(rho_c / (g * d_h * (rho_d - rho_c))) * (1 + 1.1457 * np.sqrt(1/pv * (rho_d / rho_c) ** 0.5)) ** 2
    v_g = 0.4222 * epsilon * zaehler / nenner
    return v_g

# Befehl um Modelle zu trainieren
#train_models()
# Befehle zum Abfragen eines Punktes
# Destillation
#yp_des = make_floodingpoint_predictions_des(PV=0.00071, rho_d=1000, eta_c=0.00002, eta_d=0.00100, D=0.31, a_s=312, epsilon=0.751, Mat=103, Tr=4, Pt=1)
# Extraktion
#yp_ex = make_floodingpoint_predictions_ex(PV=2.52381, rho_c=994, rho_d=860, eta_c=0.00092, eta_d=0.00054, D=0.102, a_s=340, epsilon=0.64, Mat=103, Tr=1, Pt=1)
# Hochdruck
#yp_hp = make_floodingpoint_predictions_hp(PV=0.1, rho_c=683.4275, rho_d=1017.25, eta_c=0.0000550312, eta_d=0.000403832, D=0.05, a_s=700, epsilon=0.86, Mat=101, Tr=4, Pt=2)

#print(yp_hp)


'''
# Beispiel Abfrage mehrerer Punkte
import dataviewer
import dataloader
import joblib
import time
import os
targetname="$(V_{\\textrm{c}}+V_{\\textrm{d}})_{\\textrm{f}}$"
# startet Zeitmessung
start = time.time()

# erstellt Zeitstempel
t = time.localtime()
current_time = time.strftime("%y%m%d__%H_%M_%S", t)

print(current_time)

# erstellt Output-Unterordner
output_path = 'Output//' + str(current_time)
os.mkdir(output_path)

# Lädt Daten ein
df_des, df_ex, df_hp = dataloader.loadalldata()

# Feature Target Split (nicht notwendig)
X, y = dataloader.splitfeaturetarget(df=df_hp,targetname=targetname)

# läd Modell
pipeline = joblib.load('MLA_flooding_point_packed_hp.pkl')

# macht Vorhersage
yp = pipeline.predict(X)

# Plottet Regressionsplot
dataviewer.regressionsplot(df_hp[targetname],yp,name='Test',output_path=output_path, marker='D', facecolors='None')
'''
