from Density_Viscosity import MLA_viscosity_co2_water, MLA_density_co2_water
from Distribution_Coeff import MLA_distributioncoeff_ethanol
from Flooding import MLA_flooding_point_packed_columns
from Sauterdurchmesser import sauterdiameter
from Velocity import velocity
from Submodelle import masstransfercoeff, Diffusion, Kontaktwinkel
from Holdup import holdup
from Axiale_Dispersion import use_MLA_axial_dispersion
from multiprocessing import Pool
import numpy as np
import pandas as pd
import math
import time
import os
from scipy.integrate import solve_ivp, ode, odeint
import matplotlib.pyplot as plt
import energie
#print('Module eingeladen')


def main(n, dt, h, T, p, D, a_s, epsilon, d_h, cutoff, pv, y_e_0, y_w_0, x_e_0, x_co2_0, fit, belfaktor, expath):
    """
    Hauptmethode
    :param n: Anzahl Stufen
    :param dt: Größe des Zeitschrittes [s]
    :param h: Kolonnenhöhe [m]
    :param T: Temperatur [K]
    :param p: Druck [bar]
    :param D: Kolonnendurchmesser [m]
    :param a_s: spezifische Oberfläche pro Packung [1/m]
    :param epsilon: Leervolumenanteil Kolonne [-]
    :param d_h: hydraulischer Durchmesser
    :param cutoff: Cut-off [-]
    :param sf: Solvent to Feed ratio
    :param x_e_0: Massenanteil Ethanol im Feedstrom [g/g]
    :param x_co2_0: Massenanteil CO2 im Feedstrom [g/g]
    :param y_e_0: Massenanteil Ethanol im Lösemittel [g/g]
    :param y_w_0: Massenanteil Wasser im Lösemittel [g/g]
    :param fit: array mit linearen Fits für Massentransferkoeffizienten
    :param belfaktor: Anteil der Flutbelastung
    :return:
    """

    # initialisiert Excel-Schreiber
    options = {}
    options['strings_to_formulas'] = False
    options['strings_to_urls'] = False
    writer = pd.ExcelWriter(expath, options=options)

    pbar = p*10
    # Geometrische Größen
    z = h/n
    d_p = 6*(1-epsilon)/a_s
    S = math.pi * D**2 / 4
    V_K = S*h*epsilon

    # Stoffdaten
    df_rho_c, df_rho_d = MLA_density_co2_water.loadalldata()
    df_eta_c, df_eta_d = MLA_viscosity_co2_water.loadalldata()

    M_co2 = 44.01  # [g/mol]
    M_w = 18.01528  # [g/mol]
    M_e = 46.07 # [g/mol]
    rho_c, rho_d = MLA_density_co2_water.NIST_get_density(df_rho_c,df_rho_d,T,p)
    eta_c, eta_d = MLA_viscosity_co2_water.NIST_get_viscosity(df_eta_c,df_eta_d,T,p)
    d_rho = rho_d-rho_c
    # Flutbelastung und Massenströme bestimmen
    sf = pv * (rho_c / rho_d)
    #print(sf)
    #pv = 1 / sf * (rho_c / rho_d)
    bel_flood = MLA_flooding_point_packed_columns.make_floodingpoint_predictions_hp(PV=1/pv, rho_d=rho_d, rho_c=rho_c, eta_c=eta_c, eta_d=eta_d, D=D, a_s=a_s, epsilon=epsilon, Mat=101, Tr=4, Pt=2)
    belastung = bel_flood * belfaktor
    bel_d = belastung/(1+pv)
    bel_c = belastung * pv/(1+pv)
    q_c = bel_c*S*rho_c*3600
    q_d = bel_d*S*rho_d*3600

    bel_stockfleth = MLA_flooding_point_packed_columns.stockfleth_korr(rho_c,rho_d,pv,epsilon,d_h)
    bel_d_s = bel_stockfleth/(1+pv)
    bel_c_s = bel_stockfleth * pv/(1+pv)
    #Diffusion
    phi_w = 2.6
    V_e, V_w, V_co2 = Diffusion.getVeb()
    D_co2 = Diffusion.SassiatMourier(T, M_co2, V_co2, eta_c)
    D_w_co2 = Diffusion.SassiatMourier(T, M_w, V_w, eta_c)
    D_e_co2 = Diffusion.SassiatMourier(T, M_e, V_e, eta_c)
    D_e_w = Diffusion.WilkeChang(T, M_w, V_e, eta_d, phi_w)
    D_co2_w = Diffusion.WilkeChang(T, M_w, V_co2, eta_d, phi_w)
    D_w = Diffusion.WilkeChang(T, M_w, V_w, eta_d, phi_w)


    kwinkel = 130
    # sigma = ift.chunwilkinson(rho_c=rho_c, x_ethanol=z_ethanol_mol)
    # Beachte sigma in mN/m und D in mm umwandeln
    # sigma = ift.make_ift_prediction(T, p, z_ethanol_mol)
    sigma = 30
    # Holdup, Sauterdurchmesser, Stoffaustauschfläche

    # Geschwindigkeiten
    v_c = velocity.make_v_c_prediction(pbar, T, q_c, q_d, rho_c, eta_c*1e6, D, a_s, epsilon)
    v_d = velocity.make_v_d_prediction(pbar, T, q_c, q_d, sf, rho_c, eta_c, rho_d, eta_d, D, a_s, epsilon)
    v_d_leer = q_d/3600/rho_d/S
    # v_d = velocity.leerrohr(rho_c, rho_d, D, q_d, hl)
    v_s = v_c + v_d

    frl = (v_d_leer ** 2) * (a_s + 160) * rho_d / (d_rho * 9.81)
    re_d = reynolds(rho_d, eta_d, v_d_leer, 1 / (a_s + 160))


    hl = holdup.make_holdup_predictions(q_d=q_d, rho_d=rho_d, rho_c=rho_c, eta_c=eta_c, eta_d=eta_d, Kwinkel=kwinkel, D=D*1000, d_h=d_h, a_s=a_s, cut_off=cutoff, sigma=sigma, epsilon=epsilon)
    #d32 = sauterdiameter.make_sauterdiameter_predictions_hp(q_c=q_c, q_d=q_d, rho_d=rho_d, rho_c=rho_c, eta_c=eta_c, eta_d=eta_d, Kwinkel=kwinkel, D=D*1000, d_h=d_h, a_s=a_s, cut_off=cutoff, sigma=sigma, epsilon=epsilon)/1000
    #nach Heydrich
    d32=4.02*((q_c+q_d)/3600/S)**(-0.34)*x_e_0**(-0.51)/1000
    if hl<=0:
        hl = 1.1*(frl**2/re_d)**0.25

    # Stoffaustauschfläche bestimmen
    nTropfen = hl * (S*epsilon) / (d32**3/6*math.pi)
    A = nTropfen * d32**2 * math.pi

    # Masse disperse und konti Phase
    masse_d = V_K * hl * rho_d
    masse_c = V_K * (1 - hl) * rho_c



    if hl < 0:
        print('Holdup negativ für %s' % expath)
        dummy = np.zeros(23)
        dummy[8:11] = 3600*bel_c, 3600*bel_d, pv
        return dummy
    if d32 < 0:
        print('Sauterdurchmesser negativ für %s' % expath)
        dummy = np.zeros(23)
        dummy[8:11] = 3600 * bel_c, 3600 * bel_d, pv
        return dummy
    if v_c < 0 or v_d < 0:
        print('Geschwindigkeit ist negativ für %s' % expath)
        return np.zeros(23)

    # Reynolds und Schmidtzahl
    re_sc = reynolds(rho_c, eta_c, v_c, d_p)
    sc_c = schmidt(rho_c, eta_c, D_co2)
    sc_d = schmidt(rho_d, eta_d, D_w)

    # axiale Dispersion in [m^2/s]
    pe = use_MLA_axial_dispersion.make_axial_dispersion_prediction(re_sc, sc_c)
    dax = v_c*h/pe

    #Massentransferkoeffizienten bestimmen

    fit_c = fit[0:4]
    fit_d = fit[4:7]
    k_c_co2 = masstransfercoeff.mtcoeff_c_seibert(D_co2, d32, v_s, rho_c, eta_c, hl, fit_c)
    k_d_co2 = masstransfercoeff.mtcoeff_d_seibert(D_co2_w, v_s, rho_d, eta_c, eta_d, fit_d)
    k_c_w = masstransfercoeff.mtcoeff_c_seibert(D_w_co2, d32, v_s, rho_c, eta_c, hl, fit_c)
    k_d_w = masstransfercoeff.mtcoeff_d_seibert(D_w, v_s, rho_d, eta_c, eta_d, fit_d)
    k_c_e = masstransfercoeff.mtcoeff_c_seibert(D_e_co2, d32, v_s, rho_c, eta_c, hl, fit_c)
    k_d_e = masstransfercoeff.mtcoeff_d_seibert(D_e_w, v_s, rho_d, eta_c, eta_d, fit_d)

    def rhs_fast(t, u, k):
        """
        DGL-System (K- und J-Berechnung ausgelagert für schnellere Berechnung)
        """
        # Vektor anlegen
        rhs_fast = np.zeros(4 * (n + 1))
        je = k[0]
        jco2 = k[1]
        jw = k[2]
        # y_ethanol 0 - n
        # y_wasser n+1 - 2n+1
        # x_ethanol 2n+2 - 3n+2
        # x_co2 3n+3 - 4n+3
        # rhs = dyi/dt
        # u[i] = y an Ort i

        # Kompartments zwischen Boden und Kopf
        for i in range(1, n):  # 1....n-1
            i1 = i + n + 1
            i2 = i + 2 * (n + 1)
            i3 = i + 3 * (n + 1)
            # Massentransfer bestimmen

            rhs_fast[i] = (v_c / z) * (u[i - 1] - u[i]) + (dax / z ** 2) * (u[i + 1] - 2 * u[i] + u[i - 1]) \
                          + je[i] / (h * S * epsilon * (1 - hl) * rho_c)

            rhs_fast[i1] = (v_c / z) * (u[i1 - 1] - u[i1]) + (dax / z ** 2) * (u[i1 + 1] - 2 * u[i1] + u[i1 - 1]) \
                           + jw[i] / (h * S * epsilon * (1 - hl) * rho_c)
            rhs_fast[i2] = (v_d / z) * (u[i2 + 1] - u[i2]) \
                           - je[i] / (h * S * epsilon * hl * rho_d)

            rhs_fast[i3] = (v_d / z) * (u[i3 + 1] - u[i3]) \
                           - jco2[i] / (h * S * epsilon * hl * rho_d)

        # RANDBEDINGUNGEN
        # Eingang (konti)
        rhs_fast[0] = (dax / z * rhs_fast[1]) / (v_c + dax / z)
        rhs_fast[n + 1] = (dax / z * rhs_fast[n + 2]) / (v_c + dax / z)
        # Ausgang (dispers)
        rhs_fast[2 * (n + 1)] = rhs_fast[2 * (n + 1) + 1]  # dx0dt = dx1dt
        rhs_fast[3 * (n + 1)] = rhs_fast[3 * (n + 1) + 1]
        # Ausgang (konti)
        rhs_fast[n] = rhs_fast[n - 1]  # dyN/dt=dyN-1/dt
        rhs_fast[2 * n + 1] = rhs_fast[2 * n]
        # Eingang (dispers)
        rhs_fast[3 * n + 2] = 0  # dxN/dt=0 da xN=xein (konst)
        rhs_fast[4 * n + 3] = 0
        return rhs_fast

    def j_zwischen(u):
        """
        ausgelagerte Berechnung von Ji
        :param u: Vektor mit Massenanteilen für alle Kompartments
        :return: j_ethanol, j_co2, j_wasser
        """
        je = np.zeros(n+1)
        jw = np.zeros(n+1)
        jco2 = np.zeros(n+1)

        z_ethanol, z_wasser, z_co2 = globalcomp(n=n, q_c=masse_c, q_d=masse_d, x_e=u[2*n+3:3*n+1+1], y_e=u[1:n-1+1], x_w=(1 - u[2*n+3:3*n+1+1] - u[3*n+4:4*n+2+1]), y_w=u[n+2:2*n+1])

        for i in range(1, n + 1):  # 1....n-1
            i1 = i + n + 1
            i2 = i + 2 * (n + 1)
            i3 = i + 3 * (n + 1)
            # Verteilungskoeff bestimmen

            Ke, Kco2, Kw = MLA_distributioncoeff_ethanol.make_distributioncoeff_predictions_hp(T=T, p=p, x_co2=z_co2[i], x_w=z_wasser[i], x_ethanol=z_ethanol[i])
            if Ke < 0 or Kco2 < 0 or Kw < 0:
                print(belfaktor)
                print(pv)
            je[i] = diffstrom_i(A, rho_c, rho_d, k_c_e, k_d_e, u[i2], u[i], Ke)
            jw[i] = diffstrom_i(A, rho_c, rho_d, k_c_w, k_d_w, 1 - u[i2] - u[i3], u[i1], Kw)
            jco2[i] = diffstrom_i(A, rho_c, rho_d, k_c_co2, k_d_co2, u[i3], 1 - u[i] - u[i1], Kco2)

        return je, jco2, jw

    def solve():
        """
        Löst das DGL-System für Kompartments n und Zeitschritte dt und plottet Konzentrationsverläufe
        :return: array mit Endkonzentrationen [y_ethanol, y_co2, x_ethanol, x_wasser]
        """
        #print("Starte Solver")
        daten = pd.DataFrame()

        # u = xi(z),yi(z)
        u = np.zeros(4*(n+1))
        # u0 Startwerte für Solver
        u0 = np.zeros(4*(n+1))
        u0[0] = y_e_0
        u0[n+1] = y_w_0
        u0[3*n+2] = x_e_0
        u0[4*n+3] = x_co2_0
        t=0
        success = True
        # Änderung < tol = stationärer Zustand
        tol = 5e-3/dt/n
        deltay = 1  # dummy
        i = 0
        #for i in range(1, ende, 1):   # (a, b, c) a bis b-1 mit Schrittweite c
        while success and deltay > tol:  # Solver erfolgreich und instationär
            i = i+1
            sol = solve_ivp(rhs_fast, [t, t+dt], u0, method='BDF', t_eval=[t+dt], args=[j_zwischen(u0)])
            success = sol.success
            deltay = max(sol.y[:,0]-u0)  # größte Änderung zum vorherigen Zeitschritt
            t = i * dt
            # Lösungen werden Startwerte für nächsten Zeitschritt

            u0 = sol.y[:, 0]

            y_ethanol = u0[0:n + 1]
            y_wasser = u0[n + 1:2 * n + 1 + 1]
            x_ethanol = u0[2 * (n + 1):3 * n + 2 + 1]
            x_co2 = u0[3 * (n + 1):4 * n + 3 + 1]
            x_w = np.ones(n + 1) - x_ethanol - x_co2
            y_co2 = np.ones(n + 1) - y_ethanol - y_wasser

            daten['y_Ethanol'] = y_ethanol
            daten['y_wasser'] = y_wasser
            daten['y_CO2'] = y_co2

            daten['x_Ethanol'] = x_ethanol
            daten['x_CO2'] = x_co2
            daten['x_Wasser'] = x_w
            #daten.to_excel(excel_writer=writer, sheet_name='Massenanteil')

            data = [y_ethanol[n-1+1], y_co2[n-1+1], x_ethanol[0], x_w[0], y_ethanol[0], y_co2[0], x_ethanol[n-1+1], x_w[n-1+1], bel_c, bel_d, sf, hl, pv]

        z_co2_final = np.ones(n + 1)
        z_e_final = (masse_c * y_ethanol + masse_d * x_ethanol) / (masse_c + masse_d)
        z_w_final = (masse_c * y_wasser + masse_d * x_w) / (masse_c + masse_d)
        z_co2_final = z_co2_final - z_e_final - z_w_final
        Ke = np.ones(n)
        Kw = np.ones(n)
        Kco2 = np.ones(n)
        for i in range(1, n):  # 1....n-1
            # Verteilungskoeff bestimmen

            Ke[i], Kco2[i], Kw[i] = MLA_distributioncoeff_ethanol.make_distributioncoeff_predictions_hp(T=T, p=p, x_co2=z_co2_final[i], x_w=z_w_final[i], x_ethanol=z_e_final[i])
        average_Ke = Ke[1:n - 1 + 1].mean()
        average_Kco2 = Kco2[1:n - 1 + 1].mean()
        average_Kw = Kw[1:n - 1 + 1].mean()
        hets = hets_kbs(data, h, average_Ke)
        hets_bel = hets_kbs_bel(data, h, average_Ke, average_Kw)
        #print(hets)
        data.append(hets)
        data.append(d32)
        data.append(v_c)
        data.append(v_d)
        data.append(average_Ke)
        data.append(average_Kw)
        data.append(average_Kco2)
        data.append(p)
        data.append(hets_bel)
        data.append(q_c)
        data.append(q_d)
        q_e_co2, q_h_co2, q_k_co2 = energie.co2_kreislauf(q_c,T,p)
        q_e_w, q_h_w = energie.w_kreislauf(q_d, T,p)
        data.append(q_e_co2)
        data.append(q_h_co2)
        data.append(q_k_co2)
        data.append(q_e_w)
        data.append(q_h_w)

        daten.loc[:,'load_c'] = bel_c
        daten.loc[:,'load_d'] =bel_d
        daten.loc[:, 'PV'] = pv
        daten.loc[:, 'Holdup'] = hl
        daten.loc[:, 'HETS'] = hets
        #daten.to_excel(excel_writer=writer)
        return data


    model = solve()

    # Excel speichern
    #writer.save()
    #writer.close()

    return model


def globalcomp(n, q_c, q_d, x_e, y_e, x_w, y_w):
    z_ethanol = np.zeros(n+1)
    z_wasser = np.zeros(n+1)
    z_co2 = np.ones(n+1)
    z_ethanol[1:n - 1+1] = (q_d * x_e + q_c * y_e) / (q_c + q_d)
    z_wasser[1:n - 1+1] = (q_d * x_w + q_c * y_w) / (
                q_c + q_d)
    z_co2 = z_co2 - z_ethanol - z_wasser
    return z_ethanol, z_wasser, z_co2


def diffstrom_i(A, rho_c, rho_d, k_ci, k_di, xi, yi, Ki):
    """
    Massentransferstrom für Komponente i
    :param A: Stoffaustauschfläche [m^2]
    :param rho_c: Dichte der kontinuierlichen Phase [kg/m^3]
    :param rho_d: Dichte der dispersen Phase [kg/m^3]
    :param k_ci: Massentransferkoeff konti Phase Stoff i [m/s]
    :param k_di: Massentransferkoeff disperse Phase Stoff i [m/s]
    :param xi: Massenanteil Stoff i in konti Phase
    :param yi: Massenanteil Stoff i in disp Phase
    :param Ki: massenbezogener Verteilungskoeff Stoff i
    :return: ji: Massentransferstrom für Stoff i [kg/s]
    """
    #beta = 1/(1/(rho_d*k_di) + Ki/(rho_c*k_ci))
    beta = 1 / (1 / (rho_c * k_ci) + Ki / (rho_d * k_di))
    ji = A * beta * (Ki * xi - yi)
    return ji


def reynolds(rho, eta, v, d):
    """
    Bestimmt Reynoldszahl
    :param rho: Dichte [kg/m^3]
    :param eta: Viskosität [Pas]
    :param v: Geschwindigkeit [m/s]
    :param d: charakteristische Länge [m]
    :return: Reynoldszahl [-]
    """
    return rho*v*d/eta


def schmidt(rho, eta, diff):
    """
    Bestimme Schmidtzahl
    :param rho: Dichte [kg/m^3]
    :param eta: Viskosität [Pas]
    :param diff: Diffusionskoeff [m^2/s]
    :return: Schmidtzahl [-]
    """
    return eta/(rho*diff)


def hets_kbs(data, h, m):
    """
    Nth nach Kremser-Brown-Souders-Gleichung
    :param data: array mit [y_ethanolout,y_co2out, x_ethanolout, x_wasserout, y_ethanolin,y_co2in, x_ethanolin, x_wasserin, q_c, q_d, sf, holdup]
    :param h: Kolonnenhöhe [m]
    :param m: durschnittlicher Ke-Wert
    :return: HETS-Wert
    """
    # Beladung bezogen auf die CO2-Masse
    x_f = data[6]
    x_r = data[2]
    y_a = data[4]
    y_w = data[1]
    lam = data[10] * m
    zähler = (x_f-y_a/m)/(x_r-y_a/m) * (1-1/lam) + 1/lam
    nth = np.log(zähler) / np.log(lam)
    if nth < 1:
        nth = 1
    hets = h/nth
    return hets


def hets_kbs_bel(data, h, m1, m2):
    """
    Nth nach Kremser-Brown-Souders-Gleichung mit Beladungen (bezogen auf Wasser oder CO2 möglich)
    :param data: array mit [y_ethanolout,y_co2out, x_ethanolout, x_wasserout, y_ethanolin,y_co2in, x_ethanolin, x_wasserin, q_c, q_d, sf, holdup]
    :param h: Kolonnenhöhe [m]
    :param m: durschnittlicher Ke-Wert
    :return: HETS-Wert
    """
    # Beladung bezogen auf die CO2-Masse
    x_f = data[6]/(data[7])
    x_r = data[2]/(data[3])
    y_a = data[4]/(data[5])
    y_w = data[0]/data[1]
    m = m1
    lam = data[10] * m
    zähler = (x_f-y_a/m)/(x_r-y_a/m) * (1-1/lam) + 1/lam
    nth = np.log(zähler) / np.log(lam)
    if nth < 1:
        nth = 1
    hets = h/nth
    return hets




def p_HETS(T, pstart, pend, pv):
    """
    Vorhersage der Punkte zur Erstellung einer Perfomancemap
    :param T: Temperatur in K
    :param p: Druck in MPa
    :param sf: Array mit S/F-Verhältnissen, Bsp.: [5,10,20]
    :return:
    """
    if __name__ == '__main__':
        # Zeitmessung starten
        start = time.time()
        # erstellt Zeitstempel
        tlocal = time.localtime()
        current_time = time.strftime("%y%m%d_%H_%M_%S", tlocal)

        datei = "HETS_T%dK_p%d-%dMPa" % (T, pstart, pend)
        fig_path = 'fig//HETS//'+datei+'_'+current_time
        os.mkdir(fig_path)
        #excel_path = fig_path + '//Data.xlsx'


        param = [0.698, 0.4, 0.5, 0.00375, 0.023]
        liste_in = []
        pol = Pool(5)
        for i in pv:
            for j in range(pstart, pend, 1):
                ipath = '//pv%0d_%02sMPa.xlsx' % (i, j)
                excelpath = fig_path+ipath
                c = (35, 10, 4.19, T, j, 0.038, 700, 0.86, 0.004914286, 0.1, i, 0.0, 0.0, 0.1, 0.0, param, 0.9, excelpath)
                liste_in.append(c)


        model = np.array(pol.starmap(main, liste_in))
        #print(model[:,8:12])
        pol.terminate()
        pmap_rohdaten = pd.DataFrame()
        pmap_rohdaten.loc[:, 'Belastung c'] = 3600*model[:,8]
        pmap_rohdaten.loc[:, 'Belastung d'] = 3600*model[:,9]
        pmap_rohdaten.loc[:, 'PV'] = model[:,12]
        pmap_rohdaten.loc[:, 'Holdup'] = model[:,11]
        pmap_rohdaten.loc[:, 'HETS'] = model[:,13]
        pmap_rohdaten.loc[:, 'Sauterdurchmesser'] = model[:, 14]
        pmap_rohdaten.loc[:, 'v_c'] = model[:, 15]
        pmap_rohdaten.loc[:, 'v_d'] = model[:, 16]
        pmap_rohdaten.loc[:, 'avg_Ke'] = model[:, 17]
        pmap_rohdaten.loc[:, 'avg_Kw'] = model[:, 18]
        pmap_rohdaten.loc[:, 'avg_KCo2'] = model[:, 19]
        pmap_rohdaten.loc[:, 'p'] = model[:, 20]
        pmap_rohdaten.loc[:, 'HETS-bel'] = model[:,21]
        pmap_rohdaten.loc[:, 'y_ethanol'] = model[:,0]
        pmap_rohdaten.loc[:, 'y_co2'] = model[:,1]

        eAnalyse = pd.DataFrame()
        eAnalyse.loc[:, 'PV'] = model[:,12]
        eAnalyse.loc[:, 'PV'] = model[:,20]
        eAnalyse.loc[:, 'y_EtOH'] = model[:,0]
        eAnalyse.loc[:, 'q_c'] = model[:,22]
        eAnalyse.loc[:, 'Q_elek CO2'] = model[:,24]
        eAnalyse.loc[:, 'Q_heiz CO2'] = model[:,25]
        eAnalyse.loc[:, 'Q_kühl CO2'] = model[:,26]
        eAnalyse.loc[:, 'Q_elek W'] = model[:,27]
        eAnalyse.loc[:, 'Q_heiz W'] = model[:,28]

        pmap_writer = pd.ExcelWriter(fig_path+'//HETSvsp.xlsx')
        pmap_rohdaten.to_excel(pmap_writer, sheet_name='alleDaten')
        eAnalyse.to_excel(pmap_writer, sheet_name='EnergieAnalyse')
        pmap_writer.save()
        pmap_writer.close()
        e = time.time()
        print("Zeit")
        print((e-start)/60)



p_HETS(313, 9, 14, [45])
#p_HETS(313, 12, [5, 10, 20, 35, 50, 75, 100])
#p_HETS(313, 13, [5, 10, 20, 35, 50, 75, 100])
#p_HETS(313, 4, [5, 10, 20, 35, 50, 75, 100])
#p_HETS(318, 10, 1[5, 10, 20, 35, 50, 75, 100])