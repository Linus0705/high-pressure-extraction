import numpy as np
import pandas as pd
import math
import joblib
from matplotlib import pyplot as plt
from Holdup import holdup
from sklearn.preprocessing import MinMaxScaler
from sklearn.compose import ColumnTransformer
from Velocity import velocity
from Axiale_Dispersion import use_MLA_axial_dispersion
from Sauterdurchmesser import sauterdiameter
from Distribution_Coeff import MLA_distributioncoeff_ethanol
from Flooding import MLA_flooding_point_packed_columns

pagewitdh = 6.3  #inches
factorfullpage = 0.9
factorhalfpage = 0.475
fullpage = (factorfullpage*pagewitdh,factorfullpage*pagewitdh)
halfpage = (factorhalfpage*pagewitdh,factorhalfpage*pagewitdh)
landscape_vertical = (factorfullpage*pagewitdh,2*factorfullpage*pagewitdh)
landscape_horizontal = (2*factorfullpage*pagewitdh,factorfullpage*pagewitdh)
params = {"text.usetex": True,
          "font.family":"serif",
          "font.size": 12,
          "axes.labelsize": 12,
          "xtick.labelsize": 10,
          "ytick.labelsize": 10,
          "xtick.top": True,
          "ytick.right": True,
          "xtick.minor.visible": True,
          "ytick.minor.visible":True,
          "xtick.direction": 'in',
          "ytick.direction":'in',
          "savefig.dpi": 1200,
          "legend.frameon": False
          }
plt.rcParams.update(params)


def regressionsplot(y, yp, name, xlabel, ylabel, unit, output_path, marker, facecolors):
    """
    Methode Um Regressionsplot zu erstellen und unter dem Output Path zu speichern
    :param y: Reale Target Werte als Numpy Array des shapes(anzahl daten, 1)
    :param yp: Predicted Target Werte als Numpy Array des shapes(anzahl daten, 1)
    :return:  Nichts
    """
    plt.figure(figsize=fullpage)
    plt.scatter(x=y, y=yp, marker=marker,facecolors=facecolors, edgecolors='k', label='Data') #color=(0.5, 0.5, 0.5)
    #plt.xlabel(xlabel + unit)
    #plt.ylabel(ylabel + unit)
    train_omega = 1.1*y.max()
    #train_omega=40
    train_points = np.linspace(0, train_omega, 10)
    # Diagonale
    plt.plot(train_points, train_points, 'k', label='y=x',linewidth=1)
    plt.plot(train_points, train_points*0.8, 'k',linewidth=1)
    plt.plot(0.8*train_points, train_points,'k',linewidth=1)
    plt.text(train_points[-3]*1.05, train_points[-3] * 0.8*0.99, '-20$\\%$')
    plt.text(train_points[-3] * 0.75*0.9, train_points[-3]*1.05, '+20$\\%$')
    plt.xlim([0, train_omega])
    plt.ylim([0, train_omega])
    #plt.savefig(output_path + '/Regressionsplot_'+name+'.pdf',format='pdf', bbox_inches='tight')  # speichert Bild
    #plt.savefig(output_path + '/Regressionsplot_' + name + '.svg', format='svg', bbox_inches='tight')  # speichert Bild
    #plt.savefig(output_path + '/Regressionsplot_' + name + '.png', format='png', bbox_inches='tight')  # speichert Bild
    plt.xlabel(xlabel + unit)
    plt.ylabel(ylabel + unit)
    plt.savefig(output_path + '/Parityplot_' + name + '.pdf', format='pdf', bbox_inches='tight')  # speichert Bild
    plt.savefig(output_path + '/Parityplot_' + name + '.svg', format='svg', bbox_inches='tight')  # speichert Bild
    plt.savefig(output_path + '/Parityplot_' + name + '.png', format='png', bbox_inches='tight')  # speichert Bild
    plt.close()


def regressionsplot_multiple(y, yp, indexlist, name, xlabel, ylabel, unit, output_path, marker, facecolors, label):
    """
    Methode Um Regressionsplot mit Einteilung in einzelne Quellen zu erstellen und unter dem Output Path zu speichern
    :param y: Reale Target Werte als Numpy Array des shapes(anzahl daten, 1)
    :param yp: Predicted Target Werte als Numpy Array des shapes(anzahl daten, 1)
    :return:  Nichts
    """
    plt.figure(figsize=fullpage)
    plt.scatter(x=y[0:indexlist[0]], y=yp[0:indexlist[0]], marker=marker[0], facecolors=facecolors, edgecolors='k', label=label[0])
    for i in range(1,len(indexlist)):

        plt.scatter(x=y[indexlist[i-1]:indexlist[i]], y=yp[indexlist[i-1]:indexlist[i]], marker=marker[i],facecolors=facecolors, edgecolors='k', label=label[i]) #color=(0.5, 0.5, 0.5)
    #plt.xlabel(symbol + ' real ' + unit)
    #plt.ylabel(symbol + ' vorhergesagt ' + unit)
    train_omega = 1.1*y.max()
    #train_omega=40
    train_points = np.linspace(0, train_omega, 10)
    # Diagonale
    plt.plot(train_points, train_points, 'k', linewidth=1)
    plt.plot(train_points, train_points*0.8, 'k',linewidth=1)
    plt.plot(0.8*train_points, train_points,'k',linewidth=1)
    plt.text(train_points[-3]*1.05, train_points[-3] * 0.8*0.99, '-20$\\%$')
    plt.text(train_points[-3] * 0.75*0.9, train_points[-3]*1.05, '+20$\\%$')
    plt.xlim([0, train_omega])
    plt.ylim([0, train_omega])
    #plt.legend(frameon=False)
    #plt.savefig(output_path + '/Regressionsplot_'+name+'.pdf',format='pdf', bbox_inches='tight')  # speichert Bild
    #plt.savefig(output_path + '/Regressionsplot_' + name + '.svg', format='svg', bbox_inches='tight')  # speichert Bild
    #plt.savefig(output_path + '/Regressionsplot_' + name + '.png', format='png', bbox_inches='tight')  # speichert Bild
    plt.xlabel(xlabel + unit)
    plt.ylabel(ylabel + unit)
    plt.savefig(output_path + '/Parityplot_' + name + 'multiple.pdf', format='pdf', bbox_inches='tight')  # speichert Bild
    plt.savefig(output_path + '/Parityplot_' + name + 'multiple.svg', format='svg', bbox_inches='tight')  # speichert Bild
    plt.savefig(output_path + '/Parityplot_' + name + 'multiple.png', format='png', bbox_inches='tight')  # speichert Bild
    plt.close()


def plotovermco2(df, y_name, x_name, indexlist, marker, label, title):
    """

    :param df: Dataframe mit Input-Daten
    :param y_name: Name der Spalte im Df mit y-Wert Bsp. 'v_c'
    :param x_name: Name der Spalte im Df mit x-Wert Bsp. 'm_co2'
    :param indexlist: Index an der neue Datenreihe beginnt
    :param marker: Liste mit Markern für jede Datenreihe
    :param label: Liste mit Label für jede Datenreihe
    :param title: Titel des Plots und name der Datei
    :return:
    """
    y = np.array(df[y_name]).reshape(-1, 1).astype(np.float32)
    x = np.array(df[x_name]).reshape(-1, 1).astype(np.float32)
    plt.figure(figsize=fullpage)
    #plt.title(title)
    l = max(indexlist)
    s = min(indexlist)
    xmin = np.min(x[s:l])
    xmax = np.max(x[s:l])
    ymin = np.min(y[s:l])
    ymax = np.max(y[s:l])
    plt.axis([xmin/1.5, 1.05*xmax, ymin/1.5, 1.5*ymax])
    # plt.plot(x[indexlist[0]:indexlist[1]], y[indexlist[0]:indexlist[1]], marker[0], label=label[0], fillstyle='none', linewidth=0.5)
    for i in range(1,len(indexlist)):

        plt.plot(x[indexlist[i-1]:indexlist[i]], y[indexlist[i-1]:indexlist[i]], marker[i-1], label=label[i-1], fillstyle='none', linewidth=1)
    #plt.legend()
    plt.xlabel('$m_{co2}$ [kg/h]')
    plt.ylabel('$v_{d}$ [m/s]')

    plt.savefig('fig//Parityplot//' + title + '.png', format='png', bbox_inches='tight')
    plt.savefig('fig//Parityplot//' + title + '.pdf', format='pdf', bbox_inches='tight')

    plt.close()


def plotoverbel(df, namelist, indexlist, marker, label, title, ylabel):
    """

    :param df: Dataframe mit Input-Daten
    :param namelist: Name der Spalten für Zielwert und Massenstrom co2 und Massenstrom w
    :param indexlist: Index an der neue Datenreihe beginnt Datenreihe [startindex-1:endindex]
    :param marker: Liste mit Markern für jede Datenreihe
    :param label: Liste mit Label für jede Datenreihe
    :param title: Titel des Plots und name der Datei
    :return:
    """
    y = np.array(df[namelist[0]]).reshape(-1, 1).astype(np.float32)
    q_c = np.array(df[namelist[1]]).reshape(-1, 1).astype(np.float32)
    q_d = np.array(df[namelist[2]]).reshape(-1, 1).astype(np.float32)
    d = np.array(df['D']).reshape(-1, 1).astype(np.float32)
    if d.max()>10:
        d = np.divide(d,1000)
    epsilon = np.array(df[namelist[3]]).reshape(-1, 1).astype(np.float32)
    rho_c = np.array(df[namelist[4]]).reshape(-1, 1).astype(np.float32)
    rho_d = np.array(df[namelist[5]]).reshape(-1, 1).astype(np.float32)
    a = math.pi * np.power(d, 2)/4 * epsilon
    x = np.divide(np.divide(q_c, rho_c) + np.divide(q_d, rho_d), a)/3600
    plt.figure(figsize=fullpage)
    #plt.title(title)
    l = max(indexlist)
    s = min(indexlist)
    #xmin = np.min(x[s:l])
    xmin = 0
    xmax = np.max(x[s:l])
    #ymin = np.min(y[s:l])
    ymin = 0
    ymax = np.max(y[s:l])
    plt.axis([xmin/1.5, 1.05*xmax, ymin/1.5, 1.5*ymax])
    # plt.plot(x[indexlist[0]:indexlist[1]], y[indexlist[0]:indexlist[1]], marker[0], label=label[0], fillstyle='none', linewidth=0.5)
    for i in range(1,len(indexlist)):

        plt.plot(x[indexlist[i-1]:indexlist[i]], y[indexlist[i-1]:indexlist[i]], marker[i-1], label=label[i-1], fillstyle='none', linewidth=1)
    plt.legend()
    plt.xlabel('Belastung [m/s]')
    plt.ylabel(ylabel)

    plt.savefig('fig//Parityplot//' + title + '.png', format='png', bbox_inches='tight')
    plt.savefig('fig//Parityplot//' + title + '.pdf', format='pdf', bbox_inches='tight')

    plt.close()


def plotoverbel_korr(df, pkl, namelist, indexlist, marker, label, title, ylabel):
    """
    Plotet Zielwert über Belastung und berechnet Datengetriebene Korrelation
    :param df: Dataframe mit Input-Daten
    :param pkl: Pfad der pkl-Datei
    :param namelist: Name der Spalten für Zielwert und Massenstrom co2 und Massenstrom w epsilon und Dichten
    :param indexlist: Index an der neue Datenreihe beginnt Datenreihe [startindex-1:endindex]
    :param marker: Liste mit Markern für jede Datenreihe
    :param label: Liste mit Label für jede Datenreihe
    :param title: Titel des Plots und name der Datei
    :return:
    """
    # Spalten aus DF übergeben
    y = np.array(df[namelist[0]]).reshape(-1, 1).astype(np.float32)
    q_c = np.array(df[namelist[1]]).reshape(-1, 1).astype(np.float32)
    q_d = np.array(df[namelist[2]]).reshape(-1, 1).astype(np.float32)
    d = np.array(df['D']).reshape(-1, 1).astype(np.float32)
    if d.max()>10:
        d = np.divide(d,1000)
    epsilon = np.array(df[namelist[3]]).reshape(-1, 1).astype(np.float32)
    rho_c = np.array(df[namelist[4]]).reshape(-1, 1).astype(np.float32)
    rho_d = np.array(df[namelist[5]]).reshape(-1, 1).astype(np.float32)
    a = math.pi * np.power(d, 2)/4 * epsilon
    x = np.divide(np.divide(q_c, rho_c) + np.divide(q_d, rho_d), a)/3600


    # Datengetriebene Korrelation laden und Df bereitstellen
    pipeline = joblib.load(pkl)
    df_to_analyse = pd.DataFrame(columns=df.columns, index=range(50))
    save_writer = pd.ExcelWriter(path='fig//Parityplot//' + title + '_korr.xlsx')
    df_save = pd.DataFrame()
    # Plotten
    plt.figure(figsize=fullpage)
    #plt.title(title)
    l = max(indexlist)
    s = min(indexlist)
    #xmin = np.min(x[s:l])
    xmin = 0
    xmax = np.max(x[s:l])
    #ymin = np.min(y[s:l])
    ymin = 0
    ymax = np.max(y[s:l])
    plt.axis([xmin/1.5, 1.05*xmax, ymin/1.5, 1.5*ymax])
    # plt.plot(x[indexlist[0]:indexlist[1]], y[indexlist[0]:indexlist[1]], marker[0], label=label[0], fillstyle='none', linewidth=0.5)

    for i in range(1,len(indexlist)):

        plt.plot(x[indexlist[i-1]:indexlist[i]], y[indexlist[i-1]:indexlist[i]], marker[i-1], label=label[i-1], fillstyle='none', linewidth=1)

        q_c_min = np.min(q_c[indexlist[i - 1]:indexlist[i]])
        q_c_max = np.max(q_c[indexlist[i - 1]:indexlist[i]])
        pv = (q_c[indexlist[i - 1]:indexlist[i]] / q_d[indexlist[i - 1]:indexlist[i]]).mean()
        new_m = np.linspace(start=q_c_min, stop=q_c_max, num=50)

        for col in df_to_analyse.columns:
            df_to_analyse.loc[:, col] = df.loc[indexlist[i - 1]:indexlist[i], col].mean()

        df_to_analyse.loc[:, namelist[1]] = new_m
        new_mw = np.divide(new_m, pv)
        df_to_analyse.loc[:, namelist[2]] = new_mw
        xp, y_egal = splitfeaturetarget(df_to_analyse, namelist[0])
        yp = pipeline.predict(xp).reshape(-1, 1)
        df_to_analyse.loc[:, namelist[0]] = yp
        x_korr = np.divide(np.divide(new_m, rho_c[indexlist[i-1]]) + np.divide(new_mw, rho_d[indexlist[i-1]]), a[indexlist[i-1]])/3600
        df_save.loc[:,'%sx' %i] = x_korr
        df_save.loc[:,'%sy' %i] = yp
        if marker[i-1] != 'none':
            if i==len(indexlist)-1:
                plt.plot(x_korr, yp, 'k:', linewidth=1, label='Datengetrieben')

            else:
                plt.plot(x_korr, yp, 'k:', linewidth=1)

    #plt.legend()
    plt.xlabel('$\\frac{\\dot{V}_c+\\dot{V}_d}{A_K}$ [m/s]')
    plt.ylabel(ylabel)

    plt.savefig('fig//Parityplot//' + title + '_korr.png', format='png', bbox_inches='tight')
    plt.savefig('fig//Parityplot//' + title + '_korr.pdf', format='pdf', bbox_inches='tight')
    plt.close()
    df_save.to_excel(save_writer)
    save_writer.save()
    save_writer.close()


def plotoverbel_korr_xmax(df, pkl, namelist, indexlist, marker, label, title, ylabel):
    """
    Plotet Zielwert über Belastung und berechnet Datengetriebene Korrelation
    :param df: Dataframe mit Input-Daten
    :param pkl: Pfad der pkl-Datei
    :param namelist: Name der Spalten für Zielwert und Massenstrom co2 und Massenstrom w epsilon und Dichten
    :param indexlist: Index an der neue Datenreihe beginnt Datenreihe [startindex-1:endindex]
    :param marker: Liste mit Markern für jede Datenreihe
    :param label: Liste mit Label für jede Datenreihe
    :param title: Titel des Plots und name der Datei
    :return:
    """
    # Spalten aus DF übergeben
    y = np.array(df[namelist[0]]).reshape(-1, 1).astype(np.float32)
    q_c = np.array(df[namelist[1]]).reshape(-1, 1).astype(np.float32)
    q_d = np.array(df[namelist[2]]).reshape(-1, 1).astype(np.float32)
    d = np.array(df['D']).reshape(-1, 1).astype(np.float32)
    if d.max()>10:
        d = np.divide(d,1000)
    epsilon = np.array(df[namelist[3]]).reshape(-1, 1).astype(np.float32)
    rho_c = np.array(df[namelist[4]]).reshape(-1, 1).astype(np.float32)
    rho_d = np.array(df[namelist[5]]).reshape(-1, 1).astype(np.float32)
    a = math.pi * np.power(d, 2)/4 * epsilon
    x = np.divide(np.divide(q_c, rho_c) + np.divide(q_d, rho_d), a)/3600
    #x = np.divide(np.divide(q_c, 1) + np.divide(q_d, 1), a)/3600


    # Datengetriebene Korrelation laden und Df bereitstellen
    pipeline = joblib.load(pkl)
    df_to_analyse = pd.DataFrame(columns=df.columns, index=range(50))
    save_writer = pd.ExcelWriter(path='fig//Parityplot//' + title + '_korr.xlsx')
    df_save = pd.DataFrame()
    # Plotten
    plt.figure(figsize=fullpage)
    #plt.title(title)
    l = max(indexlist)
    s = min(indexlist)
    #xmin = np.min(x[s:l])
    xmin = 0
    xmax = 0.03
    #ymin = np.min(y[s:l])
    ymin = 0
    ymax = 6
    plt.axis([xmin/1.5, 1.05*xmax, ymin/1.5, 1.5*ymax])
    # plt.plot(x[indexlist[0]:indexlist[1]], y[indexlist[0]:indexlist[1]], marker[0], label=label[0], fillstyle='none', linewidth=0.5)

    for i in range(1,len(indexlist)):

        plt.plot(x[indexlist[i-1]:indexlist[i]], y[indexlist[i-1]:indexlist[i]], marker[i-1], label=label[i-1], fillstyle='none', linewidth=1)

        q_c_min = np.min(q_c[indexlist[i - 1]:indexlist[i]])
        q_c_max = np.max(q_c[indexlist[i - 1]:indexlist[i]])
        pv = (q_c[indexlist[i - 1]:indexlist[i]] / q_d[indexlist[i - 1]:indexlist[i]]).mean()
        new_m = np.linspace(start=q_c_min, stop=q_c_max, num=50)

        for col in df_to_analyse.columns:
            df_to_analyse.loc[:, col] = df.loc[indexlist[i - 1]:indexlist[i], col].mean()

        df_to_analyse.loc[:, namelist[1]] = new_m
        new_mw = np.divide(new_m, pv)
        df_to_analyse.loc[:, namelist[2]] = new_mw
        xp, y_egal = splitfeaturetarget(df_to_analyse, namelist[0])
        yp = pipeline.predict(xp).reshape(-1, 1)
        df_to_analyse.loc[:, namelist[0]] = yp
        x_korr = np.divide(np.divide(new_m, rho_c[indexlist[i-1]]) + np.divide(new_mw, rho_d[indexlist[i-1]]), a[indexlist[i-1]])/3600
        #x_korr = np.divide(np.divide(new_m, 1) + np.divide(new_mw, 1), a[indexlist[i-1]])/3600

        df_save.loc[:,'%sx' %i] = x_korr
        df_save.loc[:,'%sy' %i] = yp
        if marker[i-1] != 'none':
            if i==len(indexlist)-1:
                plt.plot(x_korr, yp, 'k:', linewidth=1, label='Datengetrieben')
            else:
                plt.plot(x_korr, yp, 'k:', linewidth=1)

    #plt.legend()
    plt.xlabel('Belastung [m/s]')
    plt.ylabel(ylabel)

    plt.savefig('fig//Parityplot//' + title + '_korr.png', format='png', bbox_inches='tight')
    plt.savefig('fig//Parityplot//' + title + '_korr.pdf', format='pdf', bbox_inches='tight')
    plt.close()
    df_save.to_excel(save_writer)
    save_writer.save()
    save_writer.close()


def plotoverfrred_korr_xmax(df, pkl, namelist, indexlist, marker, label, title, ylabel):
    """
    Plotet Zielwert über Belastung und berechnet Datengetriebene Korrelation
    :param df: Dataframe mit Input-Daten
    :param pkl: Pfad der pkl-Datei
    :param namelist: Name der Spalten für Zielwert und Massenstrom co2 und Massenstrom w epsilon und Dichten
    :param indexlist: Index an der neue Datenreihe beginnt Datenreihe [startindex-1:endindex]
    :param marker: Liste mit Markern für jede Datenreihe
    :param label: Liste mit Label für jede Datenreihe
    :param title: Titel des Plots und name der Datei
    :return:
    """
    # Spalten aus DF übergeben
    y = np.array(df[namelist[0]]).reshape(-1, 1).astype(np.float32)
    x = np.array(df[namelist[1]]).reshape(-1, 1).astype(np.float32)

    # Datengetriebene Korrelation laden und Df bereitstellen
    pipeline = joblib.load(pkl)
    df_to_analyse = pd.DataFrame(columns=df.columns, index=range(50))
    save_writer = pd.ExcelWriter(path='fig//Parityplot//' + title + '_korr.xlsx')
    df_save = pd.DataFrame()
    # Plotten
    plt.figure(figsize=fullpage)
    #plt.title(title)
    l = max(indexlist)
    s = min(indexlist)
    #xmin = np.min(x[s:l])
    xmin = 1e-10
    xmax = 1e-5
    #ymin = np.min(y[s:l])
    ymin = 0.001
    ymax = 1
    plt.axis([xmin/1.5, 1.05*xmax, ymin/1.5, 1.5*ymax])
    # plt.plot(x[indexlist[0]:indexlist[1]], y[indexlist[0]:indexlist[1]], marker[0], label=label[0], fillstyle='none', linewidth=0.5)

    for i in range(1,len(indexlist)):

        plt.plot(x[indexlist[i-1]:indexlist[i]], y[indexlist[i-1]:indexlist[i]], marker[i-1], label=label[i-1], fillstyle='none', linewidth=1)

        q_c_min = np.min(x[indexlist[i - 1]:indexlist[i]])
        q_c_max = np.max(x[indexlist[i - 1]:indexlist[i]])
        new_m = np.linspace(start=q_c_min, stop=q_c_max, num=50)

        for col in df_to_analyse.columns:
            df_to_analyse.loc[:, col] = df.loc[indexlist[i - 1]:indexlist[i], col].mean()

        df_to_analyse.loc[:, namelist[1]] = new_m
        xp, y_egal = splitfeaturetarget(df_to_analyse, namelist[0])
        yp = pipeline.predict(xp).reshape(-1, 1)
        df_to_analyse.loc[:, namelist[0]] = yp
        x_korr = new_m
        df_save.loc[:,'%sx' %i] = x_korr
        df_save.loc[:,'%sy' %i] = yp
        if marker[i-1] != 'none':
            if i==len(indexlist)-1:
                plt.plot(x_korr, yp, 'k:', linewidth=1, label='Datengetrieben')
            else:
                plt.plot(x_korr, yp, 'k:', linewidth=1)

    #plt.legend()
    plt.xlabel('Fr2Red [-]')
    plt.xscale('log')
    plt.ylabel(ylabel)
    plt.yscale('log')

    plt.savefig('fig//Parityplot//' + title + '_korr.png', format='png', bbox_inches='tight')
    plt.savefig('fig//Parityplot//' + title + '_korr.pdf', format='pdf', bbox_inches='tight')
    plt.close()
    df_save.to_excel(save_writer)
    save_writer.save()
    save_writer.close()



def plotaxDoverbel(df, namelist, indexlist, marker, label, title, ylabel):
    """

    :param df: Dataframe mit Input-Daten
    :param namelist: Name der Spalten für Zielwert und Massenstrom co2 und Massenstrom w
    :param indexlist: Index an der neue Datenreihe beginnt
    :param marker: Liste mit Markern für jede Datenreihe
    :param label: Liste mit Label für jede Datenreihe
    :param title: Titel des Plots und name der Datei
    :return:
    """
    y = np.array(df[namelist[0]]).reshape(-1, 1).astype(np.float32)
    q_c = np.array(df[namelist[1]]).reshape(-1, 1).astype(np.float32)
    q_d = np.array(df[namelist[2]]).reshape(-1, 1).astype(np.float32)
    d = 0.038

    epsilon = np.array(df[namelist[3]]).reshape(-1, 1).astype(np.float32)
    rho_c = np.array(df[namelist[4]]).reshape(-1, 1).astype(np.float32)
    rho_d = 995
    a = math.pi * d**2/4 * epsilon
    x = np.divide(np.divide(q_c, rho_c) + np.divide(q_d, rho_d), a)
    plt.figure(figsize=fullpage)
    #plt.title(title)
    l = max(indexlist)
    s = min(indexlist)
    #xmin = np.min(x[s:l])
    xmin = 0
    xmax = np.max(x[s:l])
    #ymin = np.min(y[s:l])
    ymin = 0
    ymax = np.max(y[s:l])
    plt.axis([xmin/1.5, 1.05*xmax, ymin/1.5, 1.5*ymax])
    # plt.plot(x[indexlist[0]:indexlist[1]], y[indexlist[0]:indexlist[1]], marker[0], label=label[0], fillstyle='none', linewidth=0.5)
    for i in range(1,len(indexlist)):

        plt.plot(x[indexlist[i-1]:indexlist[i]], y[indexlist[i-1]:indexlist[i]], marker[i-1], label=label[i-1], fillstyle='none', linewidth=1)
    plt.legend()
    plt.xlabel('Belastung [m/s]')
    plt.ylabel(ylabel)

    plt.savefig('fig//Parityplot//' + title + '.png', format='png', bbox_inches='tight')
    plt.savefig('fig//Parityplot//' + title + '.pdf', format='pdf', bbox_inches='tight')

    plt.close()

def splitfeaturetarget(df, targetname):
    """
    Methode um Feature und Target-Dataframe zu erstellen
    :param df: Dataframe mit Features und Target
    :return: x, y Dataframes
    """
    y = np.array(df[targetname]).reshape(-1, 1).astype(np.float32)
    x = df.drop(columns=[targetname])
    return x, y


def makepred(df, k1, k2):

    pipeline_holdup = joblib.load('Holdup/pkl-Dateien/MLA_holdup.pkl')

    m_w_min = df['q_d'].min()
    m_w_max = df['q_d'].max()

    new_m_w = np.linspace(start=m_w_min, stop=m_w_max, num=50)
    df_to_analyse = pd.DataFrame(columns=df.columns, index=range(50))

    for col in df_to_analyse.columns:
        df_to_analyse.loc[:, col] = df.loc[:, col].mean()

    df_to_analyse.loc[:, 'q_d'] = new_m_w
    yk, frre = holdup.korr_stockfleth(df_to_analyse, k1, k2)
    x, y_egal = splitfeaturetarget(df_to_analyse, 'Holdup')
    yp = pipeline_holdup.predict(x).reshape(-1, 1)
    return yp, yk, frre


def makepred_v_c(df):
    import joblib

    pipeline_v_c = joblib.load('Velocity/pkl-Dateien/MLA_v_c.pkl')

    min = df['m_co2'].min()
    max = df['m_co2'].max()

    new_m = np.linspace(start=min, stop=max, num=50)
    df_to_analyse = pd.DataFrame(columns=df.columns, index=range(50))

    for col in df_to_analyse.columns:
        df_to_analyse.loc[:, col] = df.loc[:, col].mean()

    df_to_analyse.loc[:, 'm_co2'] = new_m
    x, y_egal = splitfeaturetarget(df_to_analyse, 'v_c')
    yp = pipeline_v_c.predict(x).reshape(-1, 1)
    df_to_analyse.loc[:, 'v_c'] = yp
    return yp, new_m, df_to_analyse


def alpha_p(limlow, limhigh, steps):
    import joblib

    pipe_kuek = joblib.load('Distribution_Coeff/pkl-Dateien/MLA_distributioncoeff_Kuek.pkl')
    pipe_kw = joblib.load('Distribution_Coeff/pkl-Dateien/MLA_distributioncoeff_Kw.pkl')

    min = limlow
    max = limhigh
    df = pd.DataFrame({'T': [313.15], 'p': [8], '$x_{\\textrm{co2}}$': [0.909], '$x_{\\textrm{w}}$': [0.0728]})
    new_column = np.linspace(start=min, stop=max, num=steps)
    df_to_analyse = pd.DataFrame(columns=df.columns, index=range(steps))

    for col in df_to_analyse.columns:
        df_to_analyse.loc[:, col] = df.loc[:, col].mean()

    df_to_analyse.loc[:, 'p'] = new_column

    yp = pipe_kuek.predict(df_to_analyse).reshape(-1, 1)
    ypw = pipe_kw.predict(df_to_analyse).reshape(-1, 1)
    alpha = yp/ypw
    df_to_analyse.loc[:, 'alpha'] = alpha
    plt.figure(figsize=fullpage)
    plt.plot(new_column, alpha, 'k:')
    plt.show()

def validityrange_p(df, joblibpath, target, feature, limlow, limhigh, steps):
    import joblib

    pipeline = joblib.load(joblibpath)
    pipe_density_c = joblib.load('Density_Viscosity/pkl-Dateien/MLA_rho_c.pkl')
    pipe_density_d = joblib.load('Density_Viscosity/pkl-Dateien/MLA_rho_d.pkl')
    pipe_visc_c = joblib.load('Density_Viscosity/pkl-Dateien/MLA_eta_c.pkl')
    pipe_visc_d = joblib.load('Density_Viscosity/pkl-Dateien/MLA_eta_d.pkl')


    min = limlow
    max = limhigh

    new_column = np.linspace(start=min, stop=max, num=steps)
    df_to_analyse = pd.DataFrame(columns=df.columns, index=range(steps))

    for col in df_to_analyse.columns:
        df_to_analyse.loc[:, col] = df.loc[:, col].mean()

    df_to_analyse.loc[:, feature] = new_column
    Tp = df_to_analyse[['T','p']]
    dummy = Tp['p']/10
    Tp.loc[:,'p'] = dummy
    rho_c = pipe_density_c.predict(Tp)
    rho_d = pipe_density_d.predict(Tp)
    eta_c = pipe_visc_c.predict(Tp)
    eta_d = pipe_visc_d.predict(Tp)
    df_to_analyse.loc[:, 'rho_c'] = rho_c
    df_to_analyse.loc[:, 'eta_c'] = 1e6*eta_c
    if 'PV' in df_to_analyse.columns:
        df_to_analyse.loc[:, 'PV'] = df_to_analyse.loc[:, 'm_co2']/df_to_analyse.loc[:, 'm_w']
        df_to_analyse.loc[:, 'rho_d'] = rho_d
        df_to_analyse.loc[:, 'eta_d'] = eta_d
        df_to_analyse.loc[:, 'eta_c'] = eta_c

    x, y_egal = splitfeaturetarget(df_to_analyse, target)

    yp = pipeline.predict(x).reshape(-1, 1)
    df_to_analyse.loc[:, target] = yp
    return yp, new_column, df_to_analyse


def validityrange_mco2(df, joblibpath, target, feature, limlow, limhigh, pv, steps):
    import joblib

    pipeline = joblib.load(joblibpath)
    pipe_density_c = joblib.load('Density_Viscosity/pkl-Dateien/MLA_rho_c.pkl')
    pipe_density_d = joblib.load('Density_Viscosity/pkl-Dateien/MLA_rho_d.pkl')
    pipe_visc_c = joblib.load('Density_Viscosity/pkl-Dateien/MLA_eta_c.pkl')
    pipe_visc_d = joblib.load('Density_Viscosity/pkl-Dateien/MLA_eta_d.pkl')


    min = limlow
    max = limhigh

    new_column = np.linspace(start=min, stop=max, num=steps)
    df_to_analyse = pd.DataFrame(columns=df.columns, index=range(steps))

    for col in df_to_analyse.columns:
        df_to_analyse.loc[:, col] = df.loc[:, col].mean()

    df_to_analyse.loc[:, feature] = new_column
    Tp = df_to_analyse[['T','p']]
    dummy = Tp['p']/10
    Tp.loc[:,'p'] = dummy
    rho_c = pipe_density_c.predict(Tp)
    rho_d = pipe_density_d.predict(Tp)
    eta_c = pipe_visc_c.predict(Tp)
    eta_d = pipe_visc_d.predict(Tp)
    df_to_analyse.loc[:, 'rho_c'] = rho_c
    df_to_analyse.loc[:, 'eta_c'] = 1e6*eta_c
    df_to_analyse.loc[:, 'm_w'] = new_column/pv
    if 'PV' in df_to_analyse.columns:
        df_to_analyse.loc[:, 'PV'] = pv
        df_to_analyse.loc[:, 'rho_d'] = rho_d
        df_to_analyse.loc[:, 'eta_d'] = eta_d

    x, y_egal = splitfeaturetarget(df_to_analyse, target)

    yp = pipeline.predict(x).reshape(-1, 1)
    df_to_analyse.loc[:, target] = yp
    return yp, new_column, df_to_analyse



def validityrange_hl(df, target, feature, limlow, limhigh, steps, T,p):
    import joblib
    write_hl = pd.ExcelWriter(path='fig//Parityplot//Holdup_mw_korr.xlsx')
    pipeline = joblib.load('Holdup/pkl-Dateien/MLA_holdup.pkl')
    pipe_density_c = joblib.load('Density_Viscosity/pkl-Dateien/MLA_rho_c.pkl')
    pipe_density_d = joblib.load('Density_Viscosity/pkl-Dateien/MLA_rho_d.pkl')
    pipe_visc_c = joblib.load('Density_Viscosity/pkl-Dateien/MLA_eta_c.pkl')
    pipe_visc_d = joblib.load('Density_Viscosity/pkl-Dateien/MLA_eta_d.pkl')


    min = limlow
    max = limhigh

    new_column = np.linspace(start=min, stop=max, num=steps)
    df_to_analyse = pd.DataFrame(columns=df.columns, index=range(steps))
    df_to_analyse2 = pd.DataFrame(columns=df.columns, index=range(steps))

    for col in df_to_analyse.columns:
        df_to_analyse.loc[:, col] = df.loc[:, col].mean()
        df_to_analyse2.loc[:, col] = df.loc[:, col].mean()

    df_to_analyse.loc[:, feature] = new_column
    df_to_analyse2.loc[:, feature] = new_column

    Tp = pd.DataFrame(columns=['T','p'], index=range(steps))
    Tp.loc[:, ['T','p']] = T,p/10
    #dummy = Tp['p']/10
    #Tp.loc[:,'p'] = dummy
    rho_c = pipe_density_c.predict(Tp)
    rho_d = pipe_density_d.predict(Tp)
    eta_c = pipe_visc_c.predict(Tp)
    eta_d = pipe_visc_d.predict(Tp)
    df_to_analyse2.loc[:, '$\\rho_{\\textrm{c}}$'] = rho_c
    df_to_analyse2.loc[:, '$\\eta_{\\textrm{c}}$'] = eta_c
    df_to_analyse2.loc[:, '$\\rho_{\\textrm{d}}$'] = rho_d
    df_to_analyse2.loc[:, '$\\eta_{\\textrm{d}}$'] = eta_d


    x, y_egal = splitfeaturetarget(df_to_analyse, target)
    x2, y_egal2 = splitfeaturetarget(df_to_analyse2, target)
    yp = pipeline.predict(x).reshape(-1, 1)
    yp2 = pipeline.predict(x2).reshape(-1, 1)
    df_to_analyse.loc[:, target] = yp
    df_to_analyse2.loc[:, target] = yp2
    df_to_analyse.to_excel(write_hl, sheet_name='100')
    df_to_analyse2.to_excel(write_hl, sheet_name='110')
    write_hl.save()
    write_hl.close()
    plt.figure()
    plt.plot(new_column,yp,'k--')
    plt.plot(new_column,yp2,'r--')
    plt.plot([1.3,2.1,3.0,3.8,4.7,5.5],[0.0075,0.00989,0.013,0.0145,0.017,0.0227],'ko')
    plt.show()

    return yp, new_column, df_to_analyse


def parity_pcpsaft_data():
    def load_pcpsaft_data():
        """
        Methode um alle Excelsheets im Inputordner einzuladen welche in der Methode benannt werden
        :return: ein Dataframe mit allen Daten
        """

        import pandas as pd

        def loaddatafromexcel(filename, columns_used, column_names, categorical_or_boolean_columns_names,
                              rows_header):
            """
            Läd Daten aus einzelner Excel Datei
            :param filename: String Name der Datei
            :param columns_used: array zB. [1,2,4] Spalten in Excel mit einzulesenden Daten, beginne mit 0 zu zählen
            :param column_names: array zB. ['v_g','massentransport'] Spalten-Namen der einzulesenden Daten
            :param categorical_or_boolean_columns_names: array zB ['massentransport']
            :param rows_header: int zB 3 oberste Zeilen in Excel mit Überschriften, Variablenname und Einheiten, beginne mit 1 zu zählenrows_header = 3  # oberste Zeilen in Excel mit Überschriften, Variablenname und Einheiten, beginne mit 1 zu zählen
            :return:
            """
            columns_used = np.array(columns_used)
            # Pfadname der Exeldatei realtiv vom Speicherort des Skriptes Backslash macht evtl unter Linux Probleme und muss durch Slash ersetzt werden
            pathname = 'Distribution_Coeff/Input/' + filename + '.xlsx'
            # einlesen der Daten in ein Pandas Dataframe
            df = pd.read_excel(pathname, usecols=columns_used, names=column_names, header=None, skiprows=rows_header, sheet_name='Massenbezogen')
            # Filtern aller Zeilen mit fehlenden Daten, bei vielen fehlenden Daten kann man über Stragetien zum Füllen der Daten nachdenken
            df = df.dropna()
            df[categorical_or_boolean_columns_names] = df[categorical_or_boolean_columns_names].astype(
                np.int64)  # Kategorische Daten bei uns sind ganzzahlig und werden als integer gespeichert, eine Unterscheidung im Skript findet auch genau darüber statt
            return df

        # Läd drei verschiedene Dataframes ein
        # Hochdruck
        df_kuek = pd.concat([
            loaddatafromexcel(filename="GGW_calculated2", columns_used=[62,63],
                              column_names=["$K_{\\textrm{uek real}}$", "$K_{\\textrm{uek pcp}}$"],
                              categorical_or_boolean_columns_names=[], rows_header=3),

        ])
        df_kco2 = pd.concat([
            loaddatafromexcel(filename="GGW_calculated2", columns_used=[72,73],
                              column_names=["$K_{\\textrm{co2 real}}$", "$K_{\\textrm{co2 pcp}}$"],
                              categorical_or_boolean_columns_names=[], rows_header=3),

        ])
        df_kw = pd.concat([
            loaddatafromexcel(filename="GGW_calculated2", columns_used=[68,69],
                              column_names=["$K_{\\textrm{w real}}$", "$K_{\\textrm{w pcp}}$"],
                              categorical_or_boolean_columns_names=[], rows_header=3),

        ])

        # resettet Index, um doppelt vorkommende Indices zu vermeiden
        df_kuek = df_kuek.reset_index(drop=True)
        df_kco2 = df_kco2.reset_index(drop=True)
        df_kw = df_kw.reset_index(drop=True)
        print('Alle Daten eingelesen')
        return df_kuek, df_kco2, df_kw

    df_uek, df_co2, df_w = load_pcpsaft_data()
    yr_uek = np.array(df_uek["$K_{\\textrm{uek real}}$"]).reshape(-1, 1).astype(np.float32)
    ypcp_uek = np.array(df_uek["$K_{\\textrm{uek pcp}}$"]).reshape(-1, 1).astype(np.float32)

    yr_w = np.array(df_w["$K_{\\textrm{w real}}$"]).reshape(-1, 1).astype(np.float32)
    ypcp_w = np.array(df_w["$K_{\\textrm{w pcp}}$"]).reshape(-1, 1).astype(np.float32)

    yr_co2 = np.array(df_co2["$K_{\\textrm{co2 real}}$"]).reshape(-1, 1).astype(np.float32)
    ypcp_co2 = np.array(df_co2["$K_{\\textrm{co2 pcp}}$"]).reshape(-1, 1).astype(np.float32)

    return yr_uek, ypcp_uek, yr_w, ypcp_w, yr_co2, ypcp_co2

def make_alpha_plots():

    def loadKdata():
        """
        Methode um alle Excelsheets im Inputordner einzuladen welche in der Methode benannt werden
        :return: ein Dataframe mit allen Daten
        """

        import pandas as pd

        def loadKdatafromexcel(filename, columns_used, column_names, categorical_or_boolean_columns_names,
                              rows_header):
            """
            Läd Daten aus einzelner Excel Datei
            :param filename: String Name der Datei
            :param columns_used: array zB. [1,2,4] Spalten in Excel mit einzulesenden Daten, beginne mit 0 zu zählen
            :param column_names: array zB. ['v_g','massentransport'] Spalten-Namen der einzulesenden Daten
            :param categorical_or_boolean_columns_names: array zB ['massentransport']
            :param rows_header: int zB 3 oberste Zeilen in Excel mit Überschriften, Variablenname und Einheiten, beginne mit 1 zu zählenrows_header = 3  # oberste Zeilen in Excel mit Überschriften, Variablenname und Einheiten, beginne mit 1 zu zählen
            :return:
            """
            columns_used = np.array(columns_used)
            # Pfadname der Exeldatei realtiv vom Speicherort des Skriptes Backslash macht evtl unter Linux Probleme und muss durch Slash ersetzt werden
            pathname = 'Distribution_Coeff/Input/' + filename + '.xlsx'
            # einlesen der Daten in ein Pandas Dataframe
            df = pd.read_excel(pathname, usecols=columns_used, names=column_names, header=None, skiprows=rows_header)
            # Filtern aller Zeilen mit fehlenden Daten, bei vielen fehlenden Daten kann man über Stragetien zum Füllen der Daten nachdenken
            df = df.dropna()
            df[categorical_or_boolean_columns_names] = df[categorical_or_boolean_columns_names].astype(
                np.int64)  # Kategorische Daten bei uns sind ganzzahlig und werden als integer gespeichert, eine Unterscheidung im Skript findet auch genau darüber statt
            return df

        # Läd drei verschiedene Dataframes ein
        # Hochdruck
        df_alpha = pd.concat([
            loadKdatafromexcel(filename="Ethanol-K_masse", columns_used=[1, 3, 11, 12, 13, 20],
                              column_names=["$K_{\\textrm{uek}}$", "$K_{\\textrm{w}}$", "$x_{\\textrm{Ethanol}}$", "T", "p", "alpha"
                                        ],
                              categorical_or_boolean_columns_names=[], rows_header=3),

        ])


        # resettet Index, um doppelt vorkommende Indices zu vermeiden
        df_alpha = df_alpha.reset_index(drop=True)
        print('Alle Daten eingelesen')
        return df_alpha

    # Lädt Daten ein
    df_alpha = loadKdata()
    df_rest, alpha = splitfeaturetarget(df_alpha, 'alpha')
    df_rest2, x_ethanol = splitfeaturetarget(df_rest, '$x_{\\textrm{Ethanol}}$')
    df_rest3, K_ethanol = splitfeaturetarget(df_rest2, '$K_{\\textrm{uek}}$')

    def plot_alpha_p(y, x, indexlist, marker, label, title):
        plt.figure(figsize=fullpage)
        #plt.title(title)
        l = max(indexlist)
        s = min(indexlist)
        xmin = np.min(x[s:l])
        xmax = np.max(x[s:l])
        ymin = np.min(y[s:l])
        ymax = np.max(y[s:l])
        plt.axis([xmin / 1.5, 1.05 * xmax, ymin / 1.5, 1.5 * ymax])
        # plt.plot(x[indexlist[0]:indexlist[1]], y[indexlist[0]:indexlist[1]], marker[0], label=label[0], fillstyle='none', linewidth=0.5)
        for i in range(1, len(indexlist)):
            plt.plot(x[indexlist[i - 1]:indexlist[i]], y[indexlist[i - 1]:indexlist[i]], marker[i - 1],
                     label=label[i - 1], fillstyle='none', linewidth=1)
        plt.legend()
        plt.xlabel('$x_{\\textrm{Ethanol}}$')
        plt.ylabel('Separationsfaktor $\\alpha_{Ethanol/Wasser}$')

        plt.savefig('fig//Parityplot//' + title + '.png', format='png', bbox_inches='tight')
        plt.savefig('fig//Parityplot//' + title + '.pdf', format='pdf', bbox_inches='tight')

        plt.close()

    def plot_alpha_T(y, x, indexlist, marker, label, title):
        plt.figure(figsize=fullpage)
        #plt.title(title)
        l = max(indexlist)
        s = min(indexlist)
        xmin = np.min(x[s:l])
        xmax = np.max(x[s:l])
        ymin = np.min(y[s:l])
        ymax = np.max(y[s:l])
        plt.axis([xmin / 1.5, 1.05 * xmax, ymin / 1.1, 1.5 * ymax])
        # plt.plot(x[indexlist[0]:indexlist[1]], y[indexlist[0]:indexlist[1]], marker[0], label=label[0], fillstyle='none', linewidth=0.5)
        for i in range(1, len(indexlist)):
            plt.plot(x[indexlist[i - 1]:indexlist[i]], y[indexlist[i - 1]:indexlist[i]], marker[i - 1],
                     label=label[i - 1], fillstyle='none', linewidth=1)
        plt.legend()
        plt.xlabel('$x_{\\textrm{Ethanol}}$')
        plt.ylabel('Separationsfaktor $\\alpha_{Ethanol/Wasser}$')

        plt.savefig('fig//Parityplot//' + title + '.png', format='png', bbox_inches='tight')
        plt.savefig('fig//Parityplot//' + title + '.pdf', format='pdf', bbox_inches='tight')

        plt.close()

    def plot_kethanol_p(y, x, indexlist, marker, label, title):
        plt.figure(figsize=fullpage)
        #plt.title(title)
        l = max(indexlist)
        s = min(indexlist)
        xmin = np.min(x[s:l])
        xmax = np.max(x[s:l])
        ymin = np.min(y[s:l])
        ymax = np.max(y[s:l])
        plt.axis([xmin / 1.5, 1.05 * xmax, ymin / 1.5, 1.5 * ymax])
        # plt.plot(x[indexlist[0]:indexlist[1]], y[indexlist[0]:indexlist[1]], marker[0], label=label[0], fillstyle='none', linewidth=0.5)
        for i in range(1, len(indexlist)):
            plt.plot(x[indexlist[i - 1]:indexlist[i]], y[indexlist[i - 1]:indexlist[i]], marker[i - 1],
                     label=label[i - 1], fillstyle='none', linewidth=1)
        plt.legend()
        plt.xlabel('$x_{\\textrm{Ethanol}}$')
        plt.ylabel('Verteilungsfaktor $K_{\\textrm{Ethanol}}$')

        plt.savefig('fig//Parityplot//' + title + '.png', format='png', bbox_inches='tight')
        plt.savefig('fig//Parityplot//' + title + '.pdf', format='pdf', bbox_inches='tight')

        plt.close()

    def plot_kethanol_T(y, x, indexlist, marker, label, title):
        plt.figure(figsize=fullpage)
        #plt.title(title)
        l = max(indexlist)
        s = min(indexlist)
        xmin = np.min(x[s:l])
        xmax = np.max(x[s:l])
        ymin = np.min(y[s:l])
        ymax = np.max(y[s:l])
        plt.axis([xmin / 1.5, 1.05 * xmax, ymin / 1.1, 1.5 * ymax])
        # plt.plot(x[indexlist[0]:indexlist[1]], y[indexlist[0]:indexlist[1]], marker[0], label=label[0], fillstyle='none', linewidth=0.5)
        for i in range(1, len(indexlist)):
            plt.plot(x[indexlist[i - 1]:indexlist[i]], y[indexlist[i - 1]:indexlist[i]], marker[i - 1],
                     label=label[i - 1], fillstyle='none', linewidth=1)
        plt.legend()
        plt.xlabel('$x_{\\textrm{Ethanol}}$')
        plt.ylabel('Verteilungsfaktor $K_{\\textrm{Ethanol}}$')

        plt.savefig('fig//Parityplot//' + title + '.png', format='png', bbox_inches='tight')
        plt.savefig('fig//Parityplot//' + title + '.pdf', format='pdf', bbox_inches='tight')

        plt.close()


    plot_alpha_p(alpha, x_ethanol, [0,4,8,12,28,32,48,52,68,72], ['ks', 'none','ko', 'none', 'kv', 'none', 'k^', 'none', 'kp'], ['80 bar', '', '100bar', '', '150 bar', '', '200bar', '', '250bar'], 'Einfluss p auf Separationsfaktor bei (313K)')
    plot_alpha_p(alpha, x_ethanol, [3312,3316,3320,3324,3340,3344,3360,3364,3380,3384], ['ks', 'none','ko', 'none', 'kv', 'none', 'k^', 'none', 'kp'], ['80 bar', '', '100bar', '', '150 bar', '', '200bar', '', '250bar'], 'Einfluss p auf Separationsfaktor bei (313K) -2')
    plot_alpha_p(alpha, x_ethanol, [8,12,28,32,48,52,68,72], ['ks', 'none','ko', 'none', 'kv', 'none', 'k^'], ['100bar', '', '150 bar', '', '200bar', '', '250bar'], 'Einfluss p auf Separationsfaktor bei (313K) -3')
    plot_alpha_p(alpha, x_ethanol, [3320,3324,3340,3344,3360,3364,3380,3384], ['ks', 'none','ko', 'none', 'kv', 'none', 'k^', ], ['100bar', '', '150 bar', '', '200bar', '', '250bar'], 'Einfluss p auf Separationsfaktor bei (313K) -4')

    plot_alpha_T(alpha, x_ethanol, [8,12,380,384,744,748], ['ks', 'none','ko', 'none', 'kv'], ['313 K', '', '333 K', '', '353 K'], 'Einfluss T auf Separationsfaktor bei 100 bar')
    plot_alpha_T(alpha, x_ethanol, [3320,3324,3692,3696,4056,4060], ['ks', 'none','ko', 'none', 'kv'], ['313 K', '', '333 K', '', '353 K'], 'Einfluss T auf Separationsfaktor bei 100 bar -2')

    plot_kethanol_p(K_ethanol, x_ethanol, [0,4,8,12,28,32,48,52,68,72], ['ks', 'none','ko', 'none', 'kv', 'none', 'k^', 'none', 'kp'], ['80 bar', '', '100bar', '', '150 bar', '', '200bar', '', '250bar'], 'Einfluss p auf Verteilungskoeff K_ethanol bei (313K)')
    plot_kethanol_p(K_ethanol, x_ethanol, [3312,3316,3320,3324,3340,3344,3360,3364,3380,3384], ['ks', 'none','ko', 'none', 'kv', 'none', 'k^', 'none', 'kp'], ['80 bar', '', '100bar', '', '150 bar', '', '200bar', '', '250bar'], 'Einfluss p auf Verteilungskoeff K_ethanol bei (313K) -2')
    plot_kethanol_p(K_ethanol, x_ethanol, [8,12,28,32,48,52,68,72], ['ks', 'none','ko', 'none', 'kv', 'none', 'k^'], ['100bar', '', '150 bar', '', '200bar', '', '250bar'], 'Einfluss p auf Verteilungskoeff K_ethanol bei (313K) -3')
    plot_kethanol_p(K_ethanol, x_ethanol, [3320,3324,3340,3344,3360,3364,3380,3384], ['ks', 'none','ko', 'none', 'kv', 'none', 'k^', ], ['100bar', '', '150 bar', '', '200bar', '', '250bar'], 'Einfluss p auf Verteilungskoeff K_ethanol bei (313K) -4')

    plot_kethanol_T(K_ethanol, x_ethanol, [8,12,380,384,744,748], ['ks', 'none','ko', 'none', 'kv'], ['313 K', '', '333 K', '', '353 K'], 'Einfluss T auf Verteilungskoeff K_ethanol bei 100 bar')
    plot_kethanol_T(K_ethanol, x_ethanol, [3320,3324,3692,3696,4056,4060], ['ks', 'none','ko', 'none', 'kv'], ['313 K', '', '333 K', '', '353 K'], 'Einfluss T auf Verteilungskoeff K_ethanol bei 100 bar -2')


# initialisiert Excel-Schreiber
options = {}
options['strings_to_formulas'] = False
options['strings_to_urls'] = False
excel_path = 'fig//Parityplot//parityplot.xlsx'
writer = pd.ExcelWriter(excel_path, options=options)
stat_writer = pd.ExcelWriter('fig//Parityplot//datenbanken.xlsx', options=options)
# Daten einlesen
df_holdup = holdup.loadalldata()
df_v_c, df_v_d = velocity.loadalldata()
df_d32 = sauterdiameter.loadalldata()
df_axDoH = use_MLA_axial_dispersion.loadalldata()
df_flood = MLA_flooding_point_packed_columns.loadalldata()

#### Vorhersage + in Excel abspeichern ###
y_holdup, yp_holdup = holdup.parityplotdata(writer, df_holdup)
y_v_c, y_v_d, yp_v_c, yp_v_d = velocity.parityplotdata(writer, df_v_c, df_v_d)
# y_axD, yp_axD, yb_axD = use_MLA_axial_dispersion.parityplotdata(writer)
y_axD_oH, yp_axD_oH, yb_axD_oH = use_MLA_axial_dispersion.parityplotdata_ohneHeydrich(writer)
y_d32, yp_d32 , yk_d32 = sauterdiameter.parityplotdata(writer, df_d32)
y_kuek, yp_kuek, y_kw, yp_kw, y_kco2, yp_kco2, df_kuek, df_kw, df_kco2 = MLA_distributioncoeff_ethanol.parityplotdata(writer)


# statistische Analyze der Datenbanken
stat_holdup = df_holdup.describe()
stat_vc = df_v_c.describe()
stat_vd = df_v_d.describe()
stat_d32 = df_d32.describe()
stat_axD = df_axDoH.describe()
stat_kuek = df_kuek.describe()
stat_kw = df_kw.describe()
stat_kco2 = df_kco2.describe()
stat_flood = df_flood.describe()

#abspeichern
stat_holdup.to_excel(stat_writer, sheet_name='Holdup')
stat_vc.to_excel(stat_writer, sheet_name='kontiGeschwindigkeit')
stat_vd.to_excel(stat_writer, sheet_name='dispersGeschwindigkeit')
stat_d32.to_excel(stat_writer, sheet_name='Sauterdurchmesser')
stat_axD.to_excel(stat_writer, sheet_name='axiale Dispersion')
stat_kuek.to_excel(stat_writer, sheet_name='KEthanol')
stat_kw.to_excel(stat_writer, sheet_name='KWasser')
stat_kco2.to_excel(stat_writer, sheet_name='KCO2')
stat_flood.to_excel(stat_writer, sheet_name='Fluten')

stat_writer.save()
stat_writer.close()


#yr_uek, ypcp_uek, yr_w, ypcp_w, yr_co2, ypcp_co2 = parity_pcpsaft_data()
#regressionsplot(y_holdup, yp_holdup, 'Hold-up', '$Hold-up_{real}$', '$Hold-up_{datengetrieben}$', '[-]', 'fig//Parityplot', 'o', 'k')


def make_regressionsolot_multiple():
    # Plots erstellen
    #regressionsplot_multiple(y_holdup, yp_holdup, [44,53,62,70,107], 'Hold-up', 'Holdup real', 'Holdup datengetrieben', '[-]', 'fig//Parityplot', ['s', 'v', 'o', '^', 'o'], 'none', ['CFD', 'eigene Daten', 'Stockfleth', 'Heydrich', ''])
    #regressionsplot_multiple(y_v_c, yp_v_c, [29,43,69], 'Geschwindigkeit konti', '$u_{c}$ real', '$u_{c}$ datengetrieben', '[m/s]', 'fig//Parityplot',['s', 'v', 'o'], 'none', ['eigene Daten', 'Heydrich', 'Marckmann'])
    #regressionsplot_multiple(y_v_d, yp_v_d, [18,34,37], 'Geschwindigkeit dispers', '$u_{d}$ real', '$u_{d}$ datengetrieben', '[m/s]', 'fig//Parityplot',['s', 'v', 'o'], 'none', ['eigene Daten', 'Heydrich', 'Marckmann'])
    #regressionsplot_multiple(y_d32, yp_d32, [44,55,63], 'Tropfendurchmesser', '$d_{32}$ real', '$d_{32}$ datengetrieben', '[mm]', 'fig//Parityplot', ['s', 'v', 's'], 'none', ['CFD', 'Heydrich', ''])
    #regressionsplot_multiple(yr_uek, ypcp_uek, [0,124], 'Verteilungskoeffizient Ethanol (massenbezogen)', '$K_{E_{real}}$', '$K_{E_{PCP-SAFT}}$', '', 'fig//Parityplot', ['s', 's'], 'none', ['', ''])
    #regressionsplot_multiple(yr_uek, 2*ypcp_uek, [0,124], 'Verteilungskoeffizient Ethanol (massenbezogen) korrigiert', '$K_{E_{real}}$', '$K_{E_{PCP-SAFT}}$', '', 'fig//Parityplot', ['s', 's'], 'none', ['', ''])
    #regressionsplot_multiple(yr_w, ypcp_w, [0,124], 'Verteilungskoeffizient Wasser (massenbezogen)', '$K_{w_{real}}$', '$K_{w_{PCP-SAFT}}$', '', 'fig//Parityplot', ['s', 's'], 'none', ['', ''])
    #regressionsplot_multiple(yr_co2, ypcp_co2, [0,124], 'Verteilungskoeffizient Co2 (massenbezogen)', '$K_{CO2_{real}}$', '$K_{CO2_{PCP-SAFT}}$', '', 'fig//Parityplot', ['s', 's'], 'none', ['', ''])

    #regressionsplot_multiple(y_kuek, yp_kuek, [157,470], 'Verteilungskoeffizient Ethanol vs DG', '$K_{E_{real}}$', '$K_{E_{Datengetrieben}}$', '', 'fig//Parityplot', ['s', 's'], 'none', ['', ''])
    #regressionsplot_multiple(y_kw, yp_kw, [157,470], 'Verteilungskoeffizient Wasser vs DG', '$K_{w_{real}}$', '$K_{w_{Datengetrieben}}$', '', 'fig//Parityplot', ['s', 's'], 'none', ['', ''])
    #regressionsplot_multiple(y_kco2, yp_kco2, [157,470], 'Verteilungskoeffizient Co2 vs DG', '$K_{CO2_{real}}$', '$K_{CO2_{Datengetrieben}}$', '', 'fig//Parityplot', ['s', 's'], 'none', ['', ''])

    i_axD = [27, 31]
    marker_axD = ['s', 'v']
    label_axD = ['eigene Daten', 'Marckmann']

    #regressionsplot_multiple(y_axD_oH, yb_axD_oH, i_axD, 'axiale Dispersion Korrelation nach Brockkötter ohne Heydrich', 'Dax real', 'Dax Korrelation Brockkoetter', '[$cm^{2}$/s]', 'fig//Parityplot', marker_axD, 'none', label_axD)
    #regressionsplot_multiple(y_axD_oH, yp_axD_oH, i_axD, 'axiale Dispersion ohne Heydrich', 'Dax real', 'Dax Greybox-Modellierung', '[$cm^{2}$/s]', 'fig//Parityplot', marker_axD, 'none', label_axD)


#regressionsplot(y_d32, yk_d32, 'Tropfendurchmesser', '$d32_{real}$', '$d32_{Korrelation Seibert}$', '[mm]', 'fig//Parityplot', 'o', 'k')
#regressionsplot(y_v_c, yp_v_c, 'Geschwindigkeit konti', '$v_{c real}$', '$v_{c-datengetrieben}$', '[m/s]', 'fig//Parityplot', 'o', 'k')
#regressionsplot(y_v_d, yp_v_d, 'Geschwindigkeit dispers', '$v_{d real}$', '$v_{d-datengetrieben}$', '[m/s]', 'fig//Parityplot', 'o', 'k')
#regressionsplot(y_axD, yp_axD, 'axiale Dispersion', '$D_{ax real}$', '$Dax_{datadriven}$', '[$cm^{2}$/s]', 'fig//Parityplot', 'o', 'k')
#regressionsplot(y_axD_oH, yp_axD_oH, 'axiale Dispersion ohne Heydrich', '$Dax_{real}$', '$Dax_{datadriven}$', '[$cm^{2}$/s]', 'fig//Parityplot', 'o', 'k')


#plotovermco2(df_v_c, 'v_c', 'm_co2', [2,5,8,11,13,16,18,21,22,26,27,32,33,34,43], ['s', 'o', 'v', '^', '<', '>', 'p', '*', 'D', '8', 'h', 'H', 'd', 'P', 'X'], ['s', 'o', 'v', '^', '<', '>', 'p', '*', 'D', '8', 'h', 'H', 'd', 'P', 'X'])
# DISPERS
#plotovermco2(df_v_d, 'v_d', 'm_co2', [0, 3, 6, 9, 12, 14, 17, 19], ['ks--', 'ko--', 'kv--', 'k^--', 'k<--', 'k>--', 'kp--'], ['$m_{w}=0.5 kg/h$   80 bar', '$m_{w}=1.0 kg/h$   80 bar', '$m_{w}=0.5 kg/h$   100 bar', '$m_{w}=1.0 kg/h$   100 bar', '$m_{w}=1.5 kg/h$   100 bar', '$m_{w}=0.5 kg/h$   120 bar', '$m_{w}=1.0 kg/h$   120 bar'], 'vd SulzerCY 313K')
#plotovermco2(df_v_d, 'v_d', 'm_co2', [19, 22, 23, 27, 28, 33, 34, 35], ['ks--', 'ko--', 'kv--', 'k^--', 'k<--', 'k>--', 'kp--'], ['$m_{w}=2.5 kg/h$', '$m_{w}=3.75 kg/h$', '$m_{w}=5.0 kg/h$', '$m_{w}=7.5 kg/h$', '$m_{w}=10 kg/h$', '$m_{w}=15 kg/h$', '$m_{w}=20 kg/h$'], 'vd SulzerBX 313K')
#plotovermco2(df_v_d, 'v_d', 'm_co2', [35, 38], ['ks--'], ['$m_{co2}=21 kg/h$ 300 bar'], 'vd Mellapak 333K')

# KONTI
# single phase mit Drahtwendel
#plotovermco2(df_v_c, 'v_c', 'm_co2', [7, 13, 21, 24, 30, 34, 44, 79, 101, 127], ['ks--', 'none', 'ko--', 'none', 'kv--', 'none', 'k^--', 'none', 'kp--'], ['Sulzer CY', '', 'Sulzer CY', '', 'Sulzer BX', '', 'Drahtwendelpackung', '', 'Mellapak 500X'])
# sp ohne Drahtwendel
#plotovermco2(df_v_c, 'v_c', 'm_co2', [7, 13, 21, 24, 30, 34, 44, 70], ['ks--', 'none', 'ko--', 'none', 'kv--', 'none', 'k^--'], ['Sulzer CY 313K 100bar', '', 'Sulzer CY 333K 100bar', '', 'Sulzer BX 313K 100bar', '', 'Mellapak 500X 333K 300bar'], 'vc singlephase')

# multiphase mit Drahtwendelpackung
#plotovermco2(df_v_c, 'v_c', 'm_co2', [0,2,4,7,13,20,21,24,26,28,30,34,36,37,40,41,43,44,79,94,101], ['ko', 'ks--', 'kv--', 'none', 'k^--', 'k<', 'none', 'k>--', 'kp--', 'kx--', 'none', 'ko-.', 'ks-.', 'kv-.', 'k^-.', 'k<-.', 'k>-.', 'none', 'ko:', 'ks:'], ['$m_{co2}=1.0 kg/h$', '$m_{co2}=0.5 kg/h$', '$m_{co2}=1.5 kg/h$', '', '$m_{co2}=1.0 kg/h$', '$m_{co2}=2.0 kg/h$', '', '$m_{co2}=0.5 kg/h$', '$m_{co2}=1.5 kg/h$', '$m_{co2}=1.0 kg/h$', '', '$m_{co2}=2.5 kg/h$', '$m_{co2}=3.75 kg/h$', '$m_{co2}=5.0 kg/h$', '$m_{co2}=7.5 kg/h$', '$m_{co2}=10 kg/h$', '$m_{co2}=20 kg/h$', '', '$m_{co2}=1.38 kg/h$', '$m_{co2}=1.86 kg/h$'])
#plotovermco2(df_v_c, 'v_c', 'm_co2', [0,2,4,7,13,20,21,24,26,28,30], ['ko', 'ks--', 'kv--', 'none', 'k^--', 'k<', 'none', 'k>--', 'kp--', 'kx--'], ['$m_{co2}=1.0 kg/h$', '$m_{co2}=0.5 kg/h$', '$m_{co2}=1.5 kg/h$', '', '$m_{co2}=1.0 kg/h$', '$m_{co2}=2.0 kg/h$', '', '$m_{co2}=0.5 kg/h$', '$m_{co2}=1.5 kg/h$', '$m_{co2}=1.0 kg/h$'])
#plotovermco2(df_v_c, 'v_c', 'm_co2', [34,36,37,40,41,43,44,79,94,101], ['ko-.', 'ks-.', 'kv-.', 'k^-.', 'k<-.', 'k>-.', 'none', 'ko:', 'ks:'], ['$m_{co2}=2.5 kg/h$', '$m_{co2}=3.75 kg/h$', '$m_{co2}=5.0 kg/h$', '$m_{co2}=7.5 kg/h$', '$m_{co2}=10 kg/h$', '$m_{co2}=20 kg/h$', '', '$m_{co2}=1.38 kg/h$', '$m_{co2}=1.86 kg/h$'])
# mp ohne Drahtwendelpackung
#plotovermco2(df_v_c, 'v_c', 'm_co2', [0,2,4,7,13,20,21,24,26,28,30,34,36,37,40,41,43,44], ['ko', 'ks--', 'kv--', 'none', 'k^--', 'k<', 'none', 'k>--', 'kp--', 'kx--', 'none', 'ko-.', 'ks-.', 'kv-.', 'k^-.', 'k<-.', 'k>-.'], ['$m_{co2}=1.0 kg/h$ 80bar', '$m_{co2}=0.5 kg/h$ 90bar', '$m_{co2}=1.5 kg/h$ 90bar', '', '$m_{co2}=1.0 kg/h$ 100bar' , '$m_{co2}=2.0 kg/h$ 100bar', '', '$m_{co2}=0.5 kg/h$ 110bar', '$m_{co2}=1.5 kg/h$  110bar', '$m_{co2}=1.0 kg/h 120bar$', '', '$m_{co2}=2.5 kg/h$ 100bar', '$m_{co2}=3.75 kg/h$ 100bar', '$m_{co2}=5.0 kg/h$ 100bar', '$m_{co2}=7.5 kg/h$ 100bar', '$m_{co2}=10 kg/h$ 100bar', '$m_{co2}=20 kg/h$ 100bar'], 'vc multiphase 313K')
#plotovermco2(df_v_c, 'v_c', 'm_co2', [0,2,4,7,13,20,21,24,26,28,30], ['ko', 'ks--', 'kv--', 'none', 'k^--', 'k<', 'none', 'k>--', 'kp--', 'kx--'], ['$m_{co2}=1.0 kg/h$ 80bar', '$m_{co2}=0.5 kg/h$ 90bar', '$m_{co2}=1.5 kg/h$ 90bar', '', '$m_{co2}=1.0 kg/h$ 100bar', '$m_{co2}=2.0 kg/h$ 100bar', '', '$m_{co2}=0.5 kg/h$ 110bar', '$m_{co2}=1.5 kg/h$ 110bar', '$m_{co2}=1.0 kg/h$ 120bar'], 'vc SulzerCY 313K')
#plotovermco2(df_v_c, 'v_c', 'm_co2', [34,36,37,40,41,43,44], ['ko-.', 'ks-.', 'kv-.', 'k^-.', 'k<-.', 'k>-.'], ['$m_{w}=2.5 kg/h$', '$m_{w}=3.75 kg/h$', '$m_{w}=5.0 kg/h$', '$m_{w}=7.5 kg/h$', '$m_{w}=10 kg/h$', '$m_{w}=20 kg/h$'], 'vc SulzerBX 313K 100bar')

# mit DWP plotovermco2(df_v_c, 'v_c', 'm_co2', [35, 38, 41, 44], ['ks--', 'ko--', 'kv--'], ['$m_{co2}=18 kg/h$', '$m_{co2}=33 kg/h$', '$m_{co2}=21 kg/h$'])

# Sauterdurchmesser
#plotovermco2(df_d32, 'Sauterdurchmesser', 'q_d', [0,11,17,24,30,45,55], ['none', 'ko', 'ks', 'kv', 'none', 'kx'], ['', '$5 m_{co2}/m_{w}$ CFD', '$10 m_{co2}/m_{w}$ CFD', '$20 m_{co2}/m_{w}$ CFD','', 'Heydrich'], 'Sauterdurchmesser gegen mw')
#plotovermco2(df_d32, 'Sauterdurchmesser', 'q_c', [0,45,55], ['ko', 'ks'],['CFD', 'Heydrich'], 'Sauterdurchmesser gegen mco2')

#### Belastungsplots ######
def make_plotverbel():
    plotoverbel(df_d32, ['Sauterdurchmesser', 'q_c', 'q_d', '$\epsilon$', '$\\rho_{\\textrm{c}}$', '$\\rho_{\\textrm{d}}$'], [0,10,16,23,30,44,55], ['none', 'ko', 'ks', 'kv', 'none', 'kx'], ['', '$5 m_{co2}/m_{w}$ CFD', '$10 m_{co2}/m_{w}$ CFD', '$20 m_{co2}/m_{w}$ CFD','', 'Heydrich'], 'Sauterdurchmesser(mm) gegen Belastung', 'Sauterdurchmesser [mm]')
    plotoverbel(df_d32, ['Sauterdurchmesser', 'q_c', 'q_d', '$\epsilon$', '$\\rho_{\\textrm{c}}$', '$\\rho_{\\textrm{d}}$'], [0,11,17,24,30], ['none', 'ko', 'ks', 'kv'], ['', '$5 m_{co2}/m_{w}$ CFD', '$10 m_{co2}/m_{w}$ CFD', '$20 m_{co2}/m_{w}$ CFD'], 'Sauterdurchmesser(mm) gegen Belastung ohne Heydrich', 'Sauterdurchmesser[mm]')
    plotoverbel(df_holdup, ['Holdup', 'q_c', 'q_d', '$\epsilon$', '$\\rho_{\\textrm{c}}$', '$\\rho_{\\textrm{d}}$'], [0,10,16,23,30], ['none', 'ko', 'ks', 'kv'], ['', '$5 m_{co2}/m_{w}$ CFD', '$10 m_{co2}/m_{w}$ CFD', '$20 m_{co2}/m_{w}$ CFD'], 'Holdup gegen Belastung ohne Heydrich', 'Holdup')
    plotoverbel(df_holdup, ['Holdup', 'q_c', 'q_d', '$\epsilon$', '$\\rho_{\\textrm{c}}$', '$\\rho_{\\textrm{d}}$'], [0,10,16,23,30,62,66,68,70], ['none', 'ko', 'ks', 'kv', 'none', 'kx', 'k<', 'k>'], ['', '$5 m_{co2}/m_{w}$ CFD', '$10 m_{co2}/m_{w}$ CFD', '$20 m_{co2}/m_{w}$ CFD','', '$5 m_{co2}/m_{w}$ Heydrich', '$10 m_{co2}/m_{w}$ Heydrich', '$20 m_{co2}/m_{w}$ Heydrich'], 'Holdup gegen Belastung', 'Holdup')
    plotoverbel(df_holdup, ['Holdup', 'q_c', 'q_d', '$\epsilon$', '$\\rho_{\\textrm{c}}$', '$\\rho_{\\textrm{d}}$'], [62,66,68,70], ['ko:', 'ks:', 'kv:'], ['$10 m_{co2}/m_{w}$ Heydrich', '$5 m_{co2}/m_{w}$ Heydrich', '$20 m_{co2}/m_{w}$ Heydrich'], 'Holdup gegen Belastung nur Heydrich', 'Holdup')

    plotoverbel(df_v_d, ['v_d', 'm_co2', 'm_w', 'epsilon', 'rho_c', 'rho_d'], [0,1,3,5,9,11,13,15,17,23,27,28,32,35], ['none','ks', 'ko', 'none', 'kv', 'k^', 'none', 'k<', 'none', 'k>', 'none', 'kp', 'kx'], ['', '$10 m_{co2}/m_{w}$ SulzerCy 80 bar', '$14 m_{co2}/m_{w}$ SulzerCy 80 bar', '', '$10 m_{co2}/m_{w}$ SulzerCy 100 bar', '$14 m_{co2}/m_{w}$ SulzerCy 100 bar', '', '$10 m_{co2}/m_{w}$ SulzerCy 120 bar', '', '$5 m_{co2}/m_{w}$ SulzerBX 100 bar', '', '$10 m_{co2}/m_{w}$ SulzerBX 100 bar', '$20 m_{co2}/m_{w}$ SulzerBX 100 bar'], 'vd gegen Belastung', '$v_{d}$ [m/s]')
    plotoverbel(df_v_d, ['v_d', 'm_co2', 'm_w', 'epsilon', 'rho_c', 'rho_d'], [0,1,3,5,9,11,13,15,17], ['none','ks:', 'ko:', 'none', 'kv:', 'kx:', 'none', 'kp:'], ['', '$10 m_{co2}/m_{w}$ 80 bar', '$14 m_{co2}/m_{w}$ 80 bar', '', '$10 m_{co2}/m_{w}$ 100 bar', '$14 m_{co2}/m_{w}$ 100 bar', '', '$10 m_{co2}/m_{w}$ 120 bar'], 'vd gegen Belastung SulzerCY', '$v_{d}$ [m/s]')
    plotoverbel(df_v_d, ['v_d', 'm_co2', 'm_w', 'epsilon', 'rho_c', 'rho_d'], [0,23,27,28,32,35], ['none','ks:','none', 'ko:', 'kv:'], ['', '$5 m_{co2}/m_{w}$', '', '$10 m_{co2}/m_{w}$', '$20 m_{co2}/m_{w}$'], 'vd gegen Belastung SulzerBX 100bar', '$v_{d}$ [m/s]')
    plotoverbel(df_v_d, ['v_d', 'm_co2', 'm_w', 'epsilon', 'rho_c', 'rho_d'], [0,1,3,9,11,15,17], ['none','ks:', 'none', 'ko:', 'none', 'kv:'], ['', '80 bar', '', '100 bar', '', '120 bar'], 'vd gegen Belastung SulzerCY PV10', '$v_{d}$ [m/s]')


def make_plotverbel_korr():

    #plotoverbel_korr(df_holdup, 'Holdup/pkl-Dateien/MLA_holdup.pkl', ['Holdup', 'q_c', 'q_d', '$\epsilon$', '$\\rho_{\\textrm{c}}$', '$\\rho_{\\textrm{d}}$'], [62,66,68,70], ['ko', 'ks', 'kv'], ['$10 m_{co2}/m_{w}$ Heydrich', '$5 m_{co2}/m_{w}$ Heydrich', '$20 m_{co2}/m_{w}$ Heydrich'], 'Holdup gegen Belastung nur Heydrich', 'Holdup')
    plotoverbel_korr(df_holdup, 'Holdup/pkl-Dateien/MLA_holdup.pkl', ['Holdup', 'q_c', 'q_d', '$\epsilon$', '$\\rho_{\\textrm{c}}$', '$\\rho_{\\textrm{d}}$'], [0,10,16,23,30], ['none', 'ko', 'ks', 'kv'], ['', '$5 m_{co2}/m_{w}$ CFD', '$10 m_{co2}/m_{w}$ CFD', '$20 m_{co2}/m_{w}$ CFD'], 'Holdup gegen Belastung ohne Heydrich', 'Holdup')
    #plotoverbel_korr(df_holdup, 'Holdup/pkl-Dateien/MLA_holdup.pkl', ['Holdup', 'q_c', 'q_d', '$\epsilon$', '$\\rho_{\\textrm{c}}$', '$\\rho_{\\textrm{d}}$'], [0,10,16,23,30,62,66,68,70], ['none', 'ko', 'ks', 'kv', 'none', 'kx', 'k<', 'k>'], ['', '$5 m_{co2}/m_{w}$ CFD', '$10 m_{co2}/m_{w}$ CFD', '$20 m_{co2}/m_{w}$ CFD','', '$5 m_{co2}/m_{w}$ Heydrich', '$10 m_{co2}/m_{w}$ Heydrich', '$20 m_{co2}/m_{w}$ Heydrich'], 'Holdup gegen Belastung', 'Holdup')
    #plotoverbel_korr_xmax(df_holdup, 'Holdup/pkl-Dateien/MLA_holdup.pkl', ['Holdup', 'q_c', 'q_d', '$\epsilon$', '$\\rho_{\\textrm{c}}$', '$\\rho_{\\textrm{d}}$'], [0,10,16,70,75,83,88], ['none', 'ko', 'none', 'none', 'none', 'kv'], ['', '100bar', '', '120bar', '', '150bar'], 'HoldupvsBelastungvsp', 'Holdup')

    #plotoverbel_korr_xmax(df_holdup, 'Holdup/pkl-Dateien/MLA_holdup.pkl', ['Holdup', 'q_c', 'q_d', '$\epsilon$', '$\\rho_{\\textrm{c}}$', '$\\rho_{\\textrm{d}}$'], [0,10,16,70,74], ['none', 'ko', 'none', 'ks'], ['', '$5 m_{co2}/m_{w}$ 100bar', '', '$5 m_{co2}/m_{w}$ 120bar'], 'Holdup gegen Belastung Druck', 'Holdup')
    #plotoverbel_korr(df_d32, 'Sauterdurchmesser/pkl-Dateien/MLA_d32.pkl', ['Sauterdurchmesser', 'q_c', 'q_d', '$\epsilon$', '$\\rho_{\\textrm{c}}$', '$\\rho_{\\textrm{d}}$'], [0,10,16,23,30,44,55], ['none', 'ko', 'ks', 'kv', 'none', 'kx'], ['', '$5 m_{co2}/m_{w}$ CFD', '$10 m_{co2}/m_{w}$ CFD', '$20 m_{co2}/m_{w}$ CFD','', 'Heydrich'], 'Sauterdurchmesser(mm) gegen Belastung', 'Sauterdurchmesser [mm]')
    #plotoverbel_korr(df_d32, 'Sauterdurchmesser/pkl-Dateien/MLA_d32.pkl', ['Sauterdurchmesser', 'q_c', 'q_d', '$\epsilon$', '$\\rho_{\\textrm{c}}$', '$\\rho_{\\textrm{d}}$'], [0,11,16,23,30], ['none', 'ko', 'ks', 'kv'], ['', '$5 m_{co2}/m_{w}$ CFD', '$10 m_{co2}/m_{w}$ CFD', '$20 m_{co2}/m_{w}$ CFD'], 'Sauterdurchmesser(mm) gegen Belastung ohne Heydrich', 'Sauterdurchmesser[mm]')
    #plotoverbel_korr(df_d32, 'Sauterdurchmesser/pkl-Dateien/MLA_d32.pkl', ['Sauterdurchmesser', 'q_c', 'q_d', '$\epsilon$', '$\\rho_{\\textrm{c}}$', '$\\rho_{\\textrm{d}}$'], [0,10,16,55,59], ['none', 'ko', 'none', 'ks'], ['', '$5 m_{co2}/m_{w}$ 100bar', '', '$5 m_{co2}/m_{w}$ 120bar'], 'Sauterdurchmesser gegen Belastung Druck', 'Sauterdurchmesser [mm]')
    #plotoverbel_korr(df_d32, 'Sauterdurchmesser/pkl-Dateien/MLA_d32.pkl', ['Sauterdurchmesser', 'q_c', 'q_d', '$\epsilon$', '$\\rho_{\\textrm{c}}$', '$\\rho_{\\textrm{d}}$'], [0,10,16,55,60,68,73], ['none', 'ko', 'none', 'ks', 'none', 'k^'], ['', '$5 m_{co2}/m_{w}$ 100bar', '', '$5 m_{co2}/m_{w}$ 120bar','', '$5 m_{co2}/m_{w}$ 150bar'], 'SauterdurchmesservsBelastungvsp', 'Sauterdurchmesser [mm]')
    #plotoverbel_korr_xmax(df_d32, 'Sauterdurchmesser/pkl-Dateien/MLA_d32.pkl', ['Sauterdurchmesser', 'q_c', 'q_d', '$\epsilon$', '$\\rho_{\\textrm{c}}$', '$\\rho_{\\textrm{d}}$'], [0,23,30,60,65,68,73], ['none', 'ko', 'none', 'ks', 'none', 'none'], ['', '$5 m_{co2}/m_{w}$ 100bar', '', '$5 m_{co2}/m_{w}$ 120bar','', '$5 m_{co2}/m_{w}$ 150bar'], 'd32vsBelastungvsp', 'Sauterdurchmesser [mm]')

    #plotoverbel_korr(df_v_d, 'Velocity/pkl-Dateien/MLA_v_d.pkl', ['v_d', 'm_co2', 'm_w', 'epsilon', 'rho_c', 'rho_d'], [0,1,3,5,9,11,13,15,17,23,27,28,32,35], ['none','ks', 'ko', 'none', 'kv', 'k^', 'none', 'k<', 'none', 'k>', 'none', 'kp', 'kx'], ['', '$10 m_{co2}/m_{w}$ SulzerCy 80 bar', '$14 m_{co2}/m_{w}$ SulzerCy 80 bar', '', '$10 m_{co2}/m_{w}$ SulzerCy 100 bar', '$14 m_{co2}/m_{w}$ SulzerCy 100 bar', '', '$10 m_{co2}/m_{w}$ SulzerCy 120 bar', '', '$5 m_{co2}/m_{w}$ SulzerBX 100 bar', '', '$10 m_{co2}/m_{w}$ SulzerBX 100 bar', '$20 m_{co2}/m_{w}$ SulzerBX 100 bar'], 'vd gegen Belastung', '$v_{d}$ [m/s]')
    #plotoverbel_korr(df_v_d, 'Velocity/pkl-Dateien/MLA_v_d.pkl', ['v_d', 'm_co2', 'm_w', 'epsilon', 'rho_c', 'rho_d'], [0,1,3,5,9,11,13,15,17], ['none','ks', 'ko', 'none', 'kv', 'kx', 'none', 'kp'], ['', '$10 m_{co2}/m_{w}$ 80 bar', '$14 m_{co2}/m_{w}$ 80 bar', '', '$10 m_{co2}/m_{w}$ 100 bar', '$14 m_{co2}/m_{w}$ 100 bar', '', '$10 m_{co2}/m_{w}$ 120 bar'], 'vd gegen Belastung SulzerCY', '$v_{d}$ [m/s]')
    #plotoverbel_korr(df_v_d, 'Velocity/pkl-Dateien/MLA_v_d.pkl', ['v_d', 'm_co2', 'm_w', 'epsilon', 'rho_c', 'rho_d'], [0,23,27,28,32,35], ['none','ks','none', 'ko', 'kv'], ['', '$5 m_{co2}/m_{w}$', '', '$10 m_{co2}/m_{w}$', '$20 m_{co2}/m_{w}$'], 'vd gegen Belastung SulzerBX 100bar', '$u_{d}$ [m/s]')
    #plotoverbel_korr(df_v_d, 'Velocity/pkl-Dateien/MLA_v_d.pkl', ['v_d', 'm_co2', 'm_w', 'epsilon', 'rho_c', 'rho_d'], [0,1,3,9,11,15,17], ['none','ks', 'none', 'ko', 'none', 'kv'], ['', '80 bar', '', '100 bar', '', '120 bar'], 'vd gegen Belastung SulzerCY PV10', '$v_{d}$ [m/s]')
    #plotaxDoverbel(df_axDoH, ['Dax_Exp', 'm-Co2', 'm-H20', 'epsilon', 'Density'], [0,2,11,13,18,20], ['ks','none', 'ko', 'none', 'kv'], ['80bar', '', '100bar', '', '120bar'], 'Dax gegen Belastung', '$D_{ax} [cm^{2}/s]$')


#plotoverfrred_korr_xmax(df_holdup, 'Holdup/pkl-Dateien/MLA_holdup.pkl', ['Holdup', 'FrRed'], [0,103], ['ko'], [''], 'HoldupvsFrRed', 'Holdup')


# HOLDUP DIAGRAMME
def make_holdupplot(df_holdup):
    df_hl_cfd = df_holdup.loc[0:43,:]
    df_hl_heydrich = df_holdup.loc[62:69,:]
    df_hl_stock = df_holdup.loc[53:61,:]
    df_hl_eD = df_holdup.loc[44:52,:]
    #df_hl_marck = df_holdup.loc[152:171,:]

    df_hl_stock2 = pd.read_excel('Holdup/Input/Holdup.xlsx', sheet_name='mit fr2re', usecols=[3,22], names=['Holdup', 'Fr2re'], header=None, skiprows=3)
    df_hl_stock2 = df_hl_stock2.dropna()
    yp_stock2 = np.array(df_hl_stock2['Holdup']).reshape(-1, 1).astype(np.float32)
    fr2re_stock2 = np.array(df_hl_stock2['Fr2re']).reshape(-1, 1).astype(np.float32)

    yp_cfd, yk_cfd, fr2re_cfd = makepred(df_hl_cfd, 1.1, 0.25)
    fig = plt.figure(figsize=fullpage)
    plt.yscale('log')
    plt.xscale('log')
    plt.scatter(holdup.fr2_re(df_hl_cfd), yp_holdup[0:44], marker='o', label='CFD Daten')
    plt.plot(fr2re_cfd, yp_cfd, color='k', linestyle='-', label='Datengetrieben')
    plt.plot(fr2re_cfd, yk_cfd, color='k', linestyle=':', label='Korrelation Stockfleth')
    plt.xlabel('$Fr^{*2}/Re$')
    plt.ylabel('Hold-up')
    #plt.title('Sulzer CY')
    plt.legend()
    plt.savefig('fig//Parityplot//Holdup_SulzerCY.png', format='png', bbox_inches='tight')
    plt.savefig('fig//Parityplot//Holdup_SulzerCY.pdf', format='pdf', bbox_inches='tight')

    plt.close(fig)

    yp_eD, yk_eD, fr2re_eD = makepred(df_hl_eD, 1.1, 0.25)
    fig = plt.figure(figsize=fullpage)
    plt.yscale('log')
    plt.xscale('log')
    plt.scatter(holdup.fr2_re(df_hl_eD), yp_holdup[44:53], marker='o', label='eigene Daten')
    plt.plot(fr2re_eD, yp_eD, color='k', linestyle='-', label='Datengetrieben')
    plt.plot(fr2re_eD, yk_eD, color='k', linestyle=':', label='Korrelation Stockfleth')
    plt.xlabel('$Fr^{*2}/Re$')
    plt.ylabel('Hold-up')
    #plt.title('Sulzer CY')
    plt.legend()
    plt.savefig('fig//Parityplot//Holdup_SulzerCY_eD.png', format='png', bbox_inches='tight')
    plt.close(fig)

    yp_stock, yk_stock, fr2re_stock = makepred(df_hl_stock, 1.6, 0.23)
    fig = plt.figure(figsize=fullpage)
    plt.yscale('log')
    plt.xscale('log')
    plt.scatter(holdup.fr2_re(df_hl_stock), yp_holdup[53:62], marker='o', label='Stockfleth')
    # plt.scatter(fr2re_stock2[167:186], yp_stock2[167:186], marker='o', label='Stockfleth')
    plt.plot(fr2re_stock, yp_stock, color='k', linestyle='-', label='Datengetrieben')
    plt.plot(fr2re_stock, yk_stock, color='k', linestyle=':', label='Korrelation Stockfleth')
    plt.xlabel('$Fr^{*2}/Re$')
    plt.ylabel('Hold-up')
    #plt.title('Sulzer EX')
    plt.legend()
    plt.savefig('fig//Parityplot//Holdup_SulzerEX.png', format='png', bbox_inches='tight')
    plt.close(fig)

    yp_hey, yk_hey, fr2re_hey = makepred(df_hl_heydrich, 1.6, 0.23)
    fig = plt.figure(figsize=fullpage)
    plt.yscale('log')
    plt.xscale('log')
    plt.scatter(holdup.fr2_re(df_hl_heydrich), yp_holdup[62:70], marker='o', label='Heydrich')
    plt.plot(fr2re_hey, yp_hey, color='k', linestyle='-', label='Datengetrieben')
    plt.plot(fr2re_hey, yk_hey, color='k', linestyle=':', label='Korrelation Stockfleth')
    plt.xlabel('$Fr^{*2}/Re$')
    plt.ylabel('Hold-up')
    #plt.title('Sulzer BX64')
    plt.legend()
    plt.savefig('fig//Parityplot//Holdup_SulzerBX64.png', format='png', bbox_inches='tight')
    plt.close(fig)

    fig = plt.figure(figsize=fullpage)
    plt.yscale('log')
    plt.xscale('log')
    #plt.scatter(holdup.fr2_re(df_hl_heydrich), yp_holdup[56:64], marker='o', label='Heydrich')
    #plt.plot(fr2re_hey, yp_hey, color='k', linestyle='-', label='Datengetrieben')
    #plt.plot(fr2re_hey, yk_hey, color='k', linestyle=':', label='Korrelation Stockfleth')
    plt.scatter(holdup.fr2_re(df_hl_stock), yp_holdup[53:62], marker='s', label='Stockfleth')
    plt.plot(fr2re_stock, yp_stock, color='k', linestyle='-', label='')
    plt.plot(fr2re_stock, yk_stock, color='k', linestyle=':', label='')
    plt.scatter(holdup.fr2_re(df_hl_eD), yp_holdup[44:53], marker='v', label='eigene Daten')
    plt.plot(fr2re_eD, yp_eD, color='k', linestyle='-', label='')
    plt.plot(fr2re_eD, yk_eD, color='k', linestyle=':', label='')
    plt.scatter(holdup.fr2_re(df_hl_cfd), yp_holdup[0:44], marker='^', label='CFD Daten')
    plt.plot(fr2re_cfd, yp_cfd, color='k', linestyle='-', label='')
    plt.plot(fr2re_cfd, yk_cfd, color='k', linestyle=':', label='')
    plt.xlabel('$Fr^{*2}/Re$')
    plt.ylabel('Hold-up')
    #plt.title('Alle')
    plt.legend()
    plt.savefig('fig//Parityplot//Holdup_alles.png', format='png', bbox_inches='tight')
    plt.close(fig)


def make_v_c_single_plot(df):
    # aufteilen in einzelne Datensätze nach Packung, m_w, etc.
    df_v_c1 = df.loc[7:12, :]
    df_v_c2 = df.loc[21:23, :]
    df_v_c3 = df.loc[30:33, :]
    df_v_c4 = df.loc[44:69, :]


    # Datengetriebene Interpolation zwischen min und max Wert
    yp1, xp1, data1 = makepred_v_c(df_v_c1)
    yp2, xp2, data2 = makepred_v_c(df_v_c2)
    yp3, xp3, data3 = makepred_v_c(df_v_c3)
    yp4, xp4, data4 = makepred_v_c(df_v_c4)

    #abspeichern in Exel
    path = 'fig//Parityplot//dataplot_vc_single.xlsx'
    writer_vc = pd.ExcelWriter(path, options=options)
    data1.to_excel(writer_vc, sheet_name='Sulzer CY 100bar 313K')
    data2.to_excel(writer_vc, sheet_name='Sulzer CY 100 bar 333K')
    data3.to_excel(writer_vc, sheet_name='Sulzer BX64 100 bar 313K')
    data4.to_excel(writer_vc, sheet_name='Mellapak 500X 300bar 333K')

    writer_vc.save()
    writer_vc.close()

    # Plot
    fig = plt.figure(figsize=fullpage)
    plt.scatter(df_v_c['m_co2'][7:14], y_v_c[7:14], marker='o', label='Sulzer CY 100bar 313K', edgecolors='k', facecolors='none')
    plt.scatter(df_v_c['m_co2'][21:24], y_v_c[21:24], marker='s', label='Sulzer CY 100 bar 333K', edgecolors='k', facecolors='none')
    plt.scatter(df_v_c['m_co2'][30:34], y_v_c[30:34], marker='v', label='Sulzer BX64 100 bar 313K', edgecolors='k', facecolors='none')
    plt.scatter(df_v_c['m_co2'][44:70], y_v_c[44:70], marker='^', label='Mellapak 500X 300bar 333K', edgecolors='k', facecolors='none')

    plt.plot(xp1, yp1, color='k', linestyle=':', label='Datengetrieben')
    plt.plot(xp2, yp2, color='k', linestyle=':')
    plt.plot(xp3, yp3, color='k', linestyle=':')
    plt.plot(xp4, yp4, color='k', linestyle=':')

    plt.ylim(bottom=0)
    plt.xlabel('$m_{co2}$ [kg/h]')
    plt.ylabel('$v_{c}$ [m/s]')
    #plt.title('Geschwindigkeit konti single phase')
    plt.legend()
    plt.savefig('fig//Parityplot//v_c_singlephase.png', format='png', bbox_inches='tight')
    plt.savefig('fig//Parityplot//v_c_singlephase.pdf', format='pdf', bbox_inches='tight')

    plt.close(fig)


def make_v_cplot(df):
 # aufteilen in einzelne Datensätze nach Packung, m_w, etc.
    df_v_c1 = df.loc[2:3, :]
    df_v_c8 = df.loc[4:6, :]
    df_v_c2 = df.loc[13:19, :]
    df_v_c3 = df.loc[24:25, :]
    df_v_c4 = df.loc[26:27, :]
    df_v_c5 = df.loc[34:35, :]
    df_v_c6 = df.loc[37:38, :] # eigentlich bis 39
    df_v_c7 = df.loc[41:42, :]


    # Datengetriebene Interpolation zwischen min und max Wert
    yp1, xp1, data1 = makepred_v_c(df_v_c1)
    yp2, xp2, data2 = makepred_v_c(df_v_c2)
    yp3, xp3, data3 = makepred_v_c(df_v_c3)
    yp4, xp4, data4 = makepred_v_c(df_v_c4)
    yp5, xp5, data5 = makepred_v_c(df_v_c5)
    yp6, xp6, data6 = makepred_v_c(df_v_c6)
    yp7, xp7, data7 = makepred_v_c(df_v_c7)
    yp8, xp8, data8 = makepred_v_c(df_v_c8)

    #abspeichern in Exel
    path = 'fig//Parityplot//dataplot_vc.xlsx'
    writer_vc = pd.ExcelWriter(path, options=options)
    data1.to_excel(writer_vc, sheet_name='Sulzer CY 90bar 0.5kgh')
    data2.to_excel(writer_vc, sheet_name='Sulzer CY 100 bar 1.0kgh')
    data3.to_excel(writer_vc, sheet_name='Sulzer CY 110 bar 0.5kgh')
    data4.to_excel(writer_vc, sheet_name='Sulzer CY 110 bar 1.5kgh')
    data5.to_excel(writer_vc, sheet_name='Sulzer BX 100 bar 2.5kgh')
    data6.to_excel(writer_vc, sheet_name='Sulzer BX 100 bar 5kgh')
    data7.to_excel(writer_vc, sheet_name='Sulzer BX 100 bar 10kgh')
    data8.to_excel(writer_vc, sheet_name='Sulzer CY 90 bar 1.5kgh')
    writer_vc.save()
    writer_vc.close()

    # Plot
    fig = plt.figure(figsize=fullpage)
    plt.scatter(df_v_c['m_co2'][2:4], y_v_c[2:4], marker='o', label='SCY 90 bar 0.5kg/h', edgecolors='k', facecolors='none')
    plt.scatter(df_v_c['m_co2'][13:20], y_v_c[13:20], marker='s', label='SCY 100 bar 1.0kg/h', edgecolors='k', facecolors='none')
    plt.scatter(df_v_c['m_co2'][24:26], y_v_c[24:26], marker='v', label='SCY 110 bar 0.5kg/h', edgecolors='k', facecolors='none')
    plt.scatter(df_v_c['m_co2'][26:28], y_v_c[26:28], marker='^', label='SCY 110 bar 1.5kg/h', edgecolors='k', facecolors='none')
    plt.scatter(df_v_c['m_co2'][34:36], y_v_c[34:36], marker='x', label='SBX 100 bar 2.5kg/h', facecolors='k')
    plt.scatter(df_v_c['m_co2'][37:39], y_v_c[37:39], marker='p', label='SBX 100 bar 5kg/h', edgecolors='k', facecolors='none')

    plt.plot(xp1, yp1, color='k', linestyle='--', label='Datengetrieben')
    plt.plot(xp2, yp2, color='k', linestyle='--')
    plt.plot(xp3, yp3, color='k', linestyle='--')
    plt.plot(xp4, yp4, color='k', linestyle='--')
    plt.plot(xp5, yp5, color='k', linestyle='--')
    plt.plot(xp6, yp6, color='k', linestyle='--')

    plt.xlim(left=0)
    plt.xlabel('$m_{co2}$ [kg/h]')
    plt.ylabel('$v_{c}$ [m/s]')
    #plt.title('Geschwindigkeit konti 313K multiphase')
    #plt.legend()
    plt.savefig('fig//Parityplot//v_c_sulzercy_mp.png', format='png', bbox_inches='tight')
    plt.savefig('fig//Parityplot//v_c_sulzercy_mp.pdf', format='pdf', bbox_inches='tight')

    plt.close(fig)

    fig = plt.figure(figsize=fullpage)
    plt.scatter(df_v_c['m_co2'][2:4], y_v_c[2:4], marker='o', label='90 bar $m_{w}$ = 0.5kg/h', edgecolors='k', facecolors='none')
    plt.scatter(df_v_c['m_co2'][24:26], y_v_c[24:26], marker='v', label='110 bar $m_{w}$ = 0.5kg/h', edgecolors='k', facecolors='none')


    plt.plot(xp1, yp1, color='k', linestyle='--', label='Datengetrieben')
    plt.plot(xp3, yp3, color='k', linestyle='--')

    plt.xlim(left=0, right=20)
    plt.xlabel('$m_{co2}$ [kg/h]')
    plt.ylabel('$v_{c}$ [m/s]')
    # plt.title('Geschwindigkeit konti 313K multiphase')
    #plt.legend()
    plt.savefig('fig//Parityplot//v_c_sulzercy_mp_05.png', format='png', bbox_inches='tight')
    plt.savefig('fig//Parityplot//v_c_sulzercy_mp_05.pdf', format='pdf', bbox_inches='tight')

    plt.close(fig)




#make_v_cplot(df_v_c)
#make_v_c_single_plot(df_v_c)
#make_holdupplot(df_holdup)
#make_regressionsolot_multiple()
make_plotverbel_korr()
#make_alpha_plots()
#alpha_p(8,18,110)

def check_validityrange():
    def check_validityrange_vc():
        # Druck
        y1, x1, d1 = validityrange_p(df_v_c[0:1], 'Velocity/pkl-Dateien/MLA_v_c.pkl', 'v_c', 'p', 80, 300, 23)
        # Phasenverhältnis
        y2, x2, d2 = validityrange_p(df_v_c[13:14], 'Velocity/pkl-Dateien/MLA_v_c.pkl', 'v_c', 'm_co2', 5, 100, 50)
        # Phasenverhältnis variiert m_w
        y3, x3, d3 = validityrange_p(df_v_c[0:1], 'Velocity/pkl-Dateien/MLA_v_c.pkl', 'v_c', 'm_w', 0.12, 2.4, 50)

        y4, x4, d4 = validityrange_mco2(df_v_c[0:1], 'Velocity/pkl-Dateien/MLA_v_c.pkl', 'v_c', 'm_co2', 30, 65.5, 5.0, 50)
        y5, x5, d5 = validityrange_mco2(df_v_c[0:1], 'Velocity/pkl-Dateien/MLA_v_c.pkl', 'v_c', 'm_co2', 31, 76.5, 20.0, 50)
        y6, x6, d6 = validityrange_mco2(df_v_c[0:1], 'Velocity/pkl-Dateien/MLA_v_c.pkl', 'v_c', 'm_co2', 31, 80, 100.0, 50)
        plotvalidityrange(x1, y1, 'p [bar]', '$v_{c}$ [m/s]', '', 'vr_vc_p', [80,80,100,100,100,100,120,120], [0.008889, 0.008759, 0.0045199, 0.0039736, 0.0036946, 0.004114, 0.0036762, 0.0026302])
        plotvalidityrange(x2, y2, 'PV', '$v_{c}$ [m/s]', '', 'vr_vc_pv_mco2', [6,10,12,12,12,12,18], [0.002288, 0.00355, 0.0045199, 0.0039736, 0.0036946, 0.004114, 0.00605])
        plotvalidityrange(x3, y3, 'PV', '$v_{c}$ [m/s]', '', 'vr_vc_pv_mw', [],[])
        plotvalidityrange(x4, y4, '$m_{CO2}$ [kg/h]', '$v_{c}$ [m/s]', 'PV=5', 'vr_vc_mco2_5', [],[])
        plotvalidityrange(x5, y5, '$m_{CO2}$ [kg/h]', '$v_{c}$ [m/s]', 'PV=20', 'vr_vc_mco2_20', [],[])
        plotvalidityrange(x6, y6, '$m_{CO2}$ [kg/h]', '$v_{c}$ [m/s]', 'PV=100', 'vr_vc_mco2_100', [],[])
        plotvalidityrange(x1, d1['rho_c'], 'p [bar]', '$\\rho_{c}$ [$kg/m^{3}$]', '', 'vr_rhoc_p', [],[])
        plotvalidityrange(x1, d1['eta_c'], 'p [bar]', '[$\\nu Pas$]', '', 'vr_eta_p', [],[])


    def check_validityrange_vd():
        y1, x1, d1 = validityrange_p(df_v_d[1:2], 'Velocity/pkl-Dateien/MLA_v_d.pkl', 'v_d', 'p', 80, 300, 23)
        # Phasenverhältnis
        y2, x2, d2 = validityrange_p(df_v_d[10:11], 'Velocity/pkl-Dateien/MLA_v_d.pkl', 'v_d', 'm_co2', 5, 100, 50)
        # Phasenverhältnis variiert m_w
        y3, x3, d3 = validityrange_p(df_v_d[10:11], 'Velocity/pkl-Dateien/MLA_v_d.pkl', 'v_d', 'm_w', 0.12, 2.4, 50)
        plotvalidityrange(x1, y1, 'p [bar]', '$v_{d}$ [m/s]', '', 'vr_vd_p', [80, 100, 120], [0.007575, 0.006644, 0.005926])
        plotvalidityrange(x2, y2, 'PV', '$v_{d}$ [m/s]', '', 'vr_vd_pv_mco2',[6, 10, 14], [0.009302, 0.009367, 0.007042])
        plotvalidityrange(x3, y3, 'PV', '$v_{d}$ [m/s]', '', 'vr_vd_pv_mw', [],[])


    def plotvalidityrange(x, y, xname, yname, label, datei, x_exp, y_exp):
        fig = plt.figure(figsize=fullpage)
        plt.plot(x, y, color='k', linestyle=':', label=label)
        plt.plot(x_exp, y_exp, 'ko')
        #plt.plot([80,100,120], [0.008889, 0.004114, 0.0031], 'ko')
        plt.xlabel(xname)
        plt.ylabel(yname)
        plt.legend()
        plt.savefig('fig//Validity//' + datei +'.png', format='png', bbox_inches='tight')
        plt.close(fig)

    #check_validityrange_vc()
    #check_validityrange_vd()

def loadflooding():
    """
        Methode um alle Excelsheets im Inputordner einzuladen welche in der Methode benannt werden
        :return: ein Dataframe mit allen Daten
        """
    import numpy as np
    import pandas as pd
    def loaddatafromexcel(filename, columns_used, column_names, categorical_or_boolean_columns_names,
                          rows_header):
        """
        Läd Daten aus einzelner Excel Datei
        :param filename: String Name der Datei
        :param columns_used: array zB. [1,2,4] Spalten in Excel mit einzulesenden Daten, beginne mit 0 zu zählen
        :param column_names: array zB. ['v_g','massentransport'] Spalten-Namen der einzulesenden Daten
        :param categorical_or_boolean_columns_names: array zB ['massentransport']
        :param rows_header: int zB 3 oberste Zeilen in Excel mit Überschriften, Variablenname und Einheiten, beginne mit 1 zu zählenrows_header = 3  # oberste Zeilen in Excel mit Überschriften, Variablenname und Einheiten, beginne mit 1 zu zählen
        :return:
        """
        columns_used = np.array(columns_used)
        # Pfadname der Exeldatei realtiv vom Speicherort des Skriptes Backslash macht evtl unter Linux Probleme und muss durch Slash ersetzt werden
        pathname = '20200327-Datengetriebenes_Modell_Flutpunkte/Input/' + filename + '.xlsx'
        # einlesen der Daten in ein Pandas Dataframe
        df = pd.read_excel(pathname, usecols=columns_used, names=column_names, header=None, skiprows=rows_header, sheet_name='Lorenz')
        # Filtern aller Zeilen mit fehlenden Daten, bei vielen fehlenden Daten kann man über Stragetien zum Füllen der Daten nachdenken
        df = df.dropna()
        df[categorical_or_boolean_columns_names] = df[categorical_or_boolean_columns_names].astype(
            np.int64)  # Kategorische Daten bei uns sind ganzzahlig und werden als integer gespeichert, eine Unterscheidung im Skript findet auch genau darüber statt
        return df

    # Läd drei verschiedene Dataframes ein
    # Hochdruck
    df_flooding = pd.concat([
            loaddatafromexcel(filename='Database', columns_used=[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17],
                              column_names=['$(V_{\\textrm{c}}+V_{\\textrm{d}})_{\\textrm{f}}$', '$f$', '$A$', "$\\rho_{\\textrm{c}}$",
                         "$\\rho_{\\textrm{d}}$", "$\\eta_{\\textrm{c}}$", "$\\eta_{\\textrm{d}}$", "$\\sigma$", '$d$',
                         "$\\epsilon$", '$S$', '$D$', "Tr", "Cp",
                         "Ct", '$PV$', '$fA$'],
                              categorical_or_boolean_columns_names=["Tr", "Cp", "Ct"], rows_header=3),
        ])

    # resettet Index, um doppelt vorkommende Indices zu vermeiden
    df_flooding = df_flooding.reset_index(drop=True)
    print('Alle Daten eingelesen')

    return df_flooding

def plotfloodingoverpi_korr(df_in, indexlist, marker, label, title, ylabel, path):
    """
    Plotet Zielwert über Belastung und berechnet Datengetriebene Korrelation
    :param df: Dataframe mit Input-Daten
    :param pkl: Pfad der pkl-Datei
    :param namelist: Name der Spalten für Zielwert und Massenstrom co2 und Massenstrom w epsilon und Dichten
    :param indexlist: Index an der neue Datenreihe beginnt Datenreihe [startindex-1:endindex]
    :param marker: Liste mit Markern für jede Datenreihe
    :param label: Liste mit Label für jede Datenreihe
    :param title: Titel des Plots und name der Datei
    :return:
    """
    #df = pd.get_dummies(df_in, columns=df_in.columns[
    #    df_in.dtypes.apply(lambda c: np.issubdtype(c, np.int64))], drop_first=False, dtype=np.int64, prefix_sep='-')


    df = df_in
        # Spalten aus DF übergeben
    y = np.array(df['$(V_{\\textrm{c}}+V_{\\textrm{d}})_{\\textrm{f}}$']).reshape(-1, 1).astype(np.float32)*1000
    f = np.array(df['$f$']).reshape(-1, 1).astype(np.float32)
    x = np.array(df['$fA$']).reshape(-1,1).astype(np.float32)



    # Datengetriebene Korrelation laden und Df bereitstellen
    pipeline = joblib.load('20200327-Datengetriebenes_Modell_Flutpunkte/MLA_flooding_point_sieve_tray_extraction.pkl')
    df_to_analyse = pd.DataFrame(columns=df.columns, index=range(50))

    # Plotten
    plt.figure(figsize=fullpage)

    #plt.title(title)

    # plt.plot(x[indexlist[0]:indexlist[1]], y[indexlist[0]:indexlist[1]], marker[0], label=label[0], fillstyle='none', linewidth=0.5)

    for i in range(1,len(indexlist)):

        plt.plot(1000*x[indexlist[i-1]:indexlist[i]], y[indexlist[i-1]:indexlist[i]], marker[i-1], label=label[i-1], fillstyle='full', linewidth=1)

        f_min = np.min(f[indexlist[i - 1]:indexlist[i]])
        f_max = np.max(f[indexlist[i - 1]:indexlist[i]])

        new_f = np.linspace(start=f_min, stop=f_max, num=50)
        columns_cat = df.columns[df.dtypes.apply(lambda c: np.issubdtype(c, np.int64))]

        for col in df_to_analyse.columns:
            df_to_analyse.loc[:, col] = df.loc[indexlist[i - 1]:indexlist[i], col].mean()

        df_to_analyse.loc[:, '$f$'] = new_f
        df_to_analyse.loc[:, '$fA$'] = new_f*df_to_analyse['$A$']
        df_to_analyse[columns_cat] = df_to_analyse[columns_cat].astype(np.int64)

        xp, y_egal = splitfeaturetarget(df_to_analyse, '$(V_{\\textrm{c}}+V_{\\textrm{d}})_{\\textrm{f}}$')

        """scaler = MinMaxScaler()
        scaler_hp = ColumnTransformer(
            [('continuous', scaler, xp.columns[xp.dtypes.apply(lambda c: np.issubdtype(c, np.float64))].tolist())],
            remainder='passthrough', sparse_threshold=0)
        xp_scaled = pd.DataFrame(data=scaler_hp.fit_transform(xp), columns=xp.columns, index=xp.index)"""
        #xp_scaled = scaler_hp.transform(xp)
        yp = pipeline.predict(xp)*1000
        df_to_analyse.loc[:, '$(V_{\\textrm{c}}+V_{\\textrm{d}})_{\\textrm{f}}$'] = yp
        x_korr = np.array(df_to_analyse['$fA$']).reshape(-1, 1).astype(np.float32)*1000
        if marker[i-1] == 'kv':
            plt.plot(x_korr, yp, 'k:', linewidth=1)

        elif marker[i-1] == 'k^':
            plt.plot(x_korr, yp, 'k-', linewidth=1)

        else:
            plt.plot(x_korr, yp, 'k--', linewidth=1)




    plt.xlim(left=0, right=22)
    plt.ylim(bottom=0, top=17.5)
    plt.xlabel('Pulsation intensity  [mm/s]')
    plt.ylabel(ylabel)
    plt.xticks(np.arange(0,22,2.5))
    plt.savefig('fig//Parityplot//' + title + '_korr.png', format='png', bbox_inches='tight')
    plt.savefig('fig//Parityplot//' + title + '_korr.pdf', format='pdf', bbox_inches='tight')

    plt.close()


#check_validityrange()
#df_flooding = loadflooding()



#plotfloodingoverpi_korr(df_flooding, [33,38,43] , ['k^', 'ks'], 'waterbutanol', 'waterButanol', 'Overall throughput at flooding [mm/s]', 'fig//Parityplot//dataplot_waterbutanol.xlsx')
#plotfloodingoverpi_korr(df_flooding, [0,8,16] , ['kv', 'k^'], 'watertoluene', 'watertoluene', 'Overall throughput at flooding [mm/s]', 'fig//Parityplot//dataplot_watertoluene.xlsx')
#plotfloodingoverpi_korr(df_flooding, [16,23,27] , ['kv', 'k^'], 'waterbutylacetate', 'waterbutylacetate', 'Overall throughput at flooding [mm/s]', 'fig//Parityplot//dataplot_waterutylacetate.xlsx')


def paritityflood(df, path):
    import joblib
    writer_flood = pd.ExcelWriter(path, options=options)

    pipeline = joblib.load('20200327-Datengetriebenes_Modell_Flutpunkte/MLA_flooding_point_sieve_tray_extraction.pkl')
    xp, y_egal = splitfeaturetarget(df, '$(V_{\\textrm{c}}+V_{\\textrm{d}})_{\\textrm{f}}$')
    yp = pipeline.predict(xp)
    df.loc[:,'$(V_{\\textrm{c}}+V_{\\textrm{d}})_{\\textrm{f}}$predicted'] = yp
    df.to_excel(excel_writer=writer_flood)
    plt.figure()
    plt.plot(1000*xp['$fA$'], 1000*yp, 'kx')
    plt.plot(1000*xp['$fA$'], 1000*y_egal, 'ko')
    plt.xlim(0,22.5)
    plt.ylim(0,20)
    plt.show()
    #writer_flood.save()
    writer_flood.close()

#paritityflood(df_flooding[33:43], 'fig//Parityplot//dataplot_waterbutanol.xlsx')
#paritityflood(df_flooding[0:16], 'fig//Parityplot//dataplot_watertoluene.xlsx')
#paritityflood(df_flooding[16:27], 'fig//Parityplot//dataplot_waterbutylacetate.xlsx')

#y1, x1, d1 = validityrange_hl(df_holdup[11:17],'Holdup', 'q_d', 1, 6, 50, 313,110)

writer.save()
writer.close()


