import numpy as np
import pandas as pd
import math
import joblib
from matplotlib import pyplot as plt
from sklearn.preprocessing import MinMaxScaler
from sklearn.compose import ColumnTransformer
from sklearn.model_selection import train_test_split

pagewitdh = 6.3  #inches
factorfullpage = 0.9
factorhalfpage = 0.475
fullpage = (factorfullpage*pagewitdh,factorfullpage*pagewitdh)
halfpage = (factorhalfpage*pagewitdh,factorhalfpage*pagewitdh)
landscape_vertical = (factorfullpage*pagewitdh,2*factorfullpage*pagewitdh)
landscape_horizontal = (2*factorfullpage*pagewitdh,factorfullpage*pagewitdh)
params = {"text.usetex": True,
          "font.family":"serif",
          "font.size": 12,
          "axes.labelsize": 12,
          "xtick.labelsize": 10,
          "ytick.labelsize": 10,
          "xtick.top": True,
          "ytick.right": True,
          "xtick.minor.visible": True,
          "ytick.minor.visible":True,
          "xtick.direction": 'in',
          "ytick.direction":'in',
          "savefig.dpi": 1200,
          "legend.frameon": False
          }
plt.rcParams.update(params)

def loadalldata():
    """
    Methode um alle Excelsheets im Inputordner einzuladen welche in der Methode benannt werden
    :return: ein Dataframe mit allen Daten
    """
    import numpy as np
    import pandas as pd
    def loaddatafromexcel(process,source,columns_used,column_names,categorical_or_boolean_columns_names,rows_header):
       """
       Läd Daten aus einzelner Excel Datei
       :param process: string zB 'Extraktion','Destillation' oder 'Hochdruck
       :param source: string zB 'Thornton'
       :param columns_used: array zB. [1,2,4] Spalten in Excel mit einzulesenden Daten, beginne mit 0 zu zählen
       :param column_names: array zB. ['v_g','massentransport'] Spalten-Namen der einzulesenden Daten
       :param categorical_or_boolean_columns_names: array zB ['massentransport']
       :param rows_header: int zB 3 oberste Zeilen in Excel mit Überschriften, Variablenname und Einheiten, beginne mit 1 zu zählenrows_header = 3  # oberste Zeilen in Excel mit Überschriften, Variablenname und Einheiten, beginne mit 1 zu zählen
       :return:
       """
       columns_used= np.array(columns_used)
       # Pfadname der Exeldatei realtiv vom Speicherort des Skriptes Bachslash macht evtl unter Linux Probleme und muss durch Slash ersetzt werden
       pathname = 'pp-Flooding/Input/Flutpunkte_gepackt_' + process +'_' + source + '.xlsx'
       # einlesen der Daten in ein Pandas Dataframe
       df = pd.read_excel(pathname, usecols=columns_used, names=column_names, header=None, skiprows=rows_header)
       # Filtern aller Zeilen mit fehlenden Daten, bei vielen fehlenden Daten kann man über Stragetien zum Füllen der Daten nachdenken
       df = df.dropna()
       df[categorical_or_boolean_columns_names] = df[categorical_or_boolean_columns_names].astype(np.int64)  # Kategorische Daten bei uns sind ganzzahlig und werden als integer gespeichert, eine Unterscheidung im Skript findet auch genau darüber statt
       return df


    def loaddatafromdatabase(columns_used,column_names,categorical_or_boolean_columns_names,rows_header):
       """
       Läd Daten aus einzelner Excel Datei
       :param process: string zB 'Extraktion','Destillation' oder 'Hochdruck
       :param source: string zB 'Thornton'
       :param columns_used: array zB. [1,2,4] Spalten in Excel mit einzulesenden Daten, beginne mit 0 zu zählen
       :param column_names: array zB. ['v_g','massentransport'] Spalten-Namen der einzulesenden Daten
       :param categorical_or_boolean_columns_names: array zB ['massentransport']
       :param rows_header: int zB 3 oberste Zeilen in Excel mit Überschriften, Variablenname und Einheiten, beginne mit 1 zu zählenrows_header = 3  # oberste Zeilen in Excel mit Überschriften, Variablenname und Einheiten, beginne mit 1 zu zählen
       :return:
       """
       columns_used= np.array(columns_used)
       # Pfadname der Exeldatei realtiv vom Speicherort des Skriptes Bachslash macht evtl unter Linux Probleme und muss durch Slash ersetzt werden
       pathname = 'pp-Flooding/Input/Database.xlsx'
       # einlesen der Daten in ein Pandas Dataframe
       df = pd.read_excel(pathname, usecols=columns_used, names=column_names, header=None, skiprows=rows_header)
       # Filtern aller Zeilen mit fehlenden Daten, bei vielen fehlenden Daten kann man über Stragetien zum Füllen der Daten nachdenken
       df = df.dropna()
       df[categorical_or_boolean_columns_names] = df[categorical_or_boolean_columns_names].astype(np.int64)  # Kategorische Daten bei uns sind ganzzahlig und werden als integer gespeichert, eine Unterscheidung im Skript findet auch genau darüber statt
       return df
    # Läd drei verschiedene Dataframes ein

    # Destillation
    df_des = pd.concat([
        loaddatafromexcel(source='Liu', process='Destillation', columns_used=[3, 4, 5, 6, 7, 8, 10, 12, 13, 14, 15, 16],
                          column_names=["$(V_{\\textrm{c}}+V_{\\textrm{d}})_{\\textrm{f}}$", '$PV$', "$\\rho_{\\textrm{c}}$", "$\\rho_{\\textrm{d}}$", "$\\eta_{\\textrm{c}}$", "$\\eta_{\\textrm{d}}$", '$D$',
                                        "$a_{\\textrm{s}}$", "$\\epsilon_{\\textrm{v}}$", "Tr", "Mat", "Pt"],
                          categorical_or_boolean_columns_names=["Tr", "Mat", "Pt"], rows_header=3),
        loaddatafromexcel(source='Brunazzi', process='Destillation', columns_used=[3, 4, 5, 6, 7, 8, 10, 12, 13, 14, 15, 16],
                          column_names=["$(V_{\\textrm{c}}+V_{\\textrm{d}})_{\\textrm{f}}$", '$PV$', "$\\rho_{\\textrm{c}}$", "$\\rho_{\\textrm{d}}$", "$\\eta_{\\textrm{c}}$", "$\\eta_{\\textrm{d}}$", '$D$',
                                        "$a_{\\textrm{s}}$", "$\\epsilon_{\\textrm{v}}$", "Tr", "Mat", "Pt"],
                          categorical_or_boolean_columns_names=["Tr", "Mat", "Pt"], rows_header=3),
        loaddatafromexcel(source='Mackowiak', process='Destillation', columns_used=[3, 4, 5, 6, 7, 8, 10, 12, 13, 14, 15, 16],
                          column_names=["$(V_{\\textrm{c}}+V_{\\textrm{d}})_{\\textrm{f}}$", '$PV$', "$\\rho_{\\textrm{c}}$", "$\\rho_{\\textrm{d}}$", "$\\eta_{\\textrm{c}}$", "$\\eta_{\\textrm{d}}$", '$D$',
                                        "$a_{\\textrm{s}}$", "$\\epsilon_{\\textrm{v}}$", "Tr", "Mat", "Pt"],
                          categorical_or_boolean_columns_names=["Tr", "Mat", "Pt"], rows_header=3),
        loaddatafromexcel(source='Sherwood_und_Drinkwater', process='Destillation', columns_used=[3, 4, 5, 6, 7, 8, 10, 12, 13, 14, 15, 16],
                          column_names=["$(V_{\\textrm{c}}+V_{\\textrm{d}})_{\\textrm{f}}$", '$PV$', "$\\rho_{\\textrm{c}}$", "$\\rho_{\\textrm{d}}$", "$\\eta_{\\textrm{c}}$", "$\\eta_{\\textrm{d}}$", '$D$',
                                        "$a_{\\textrm{s}}$", "$\\epsilon_{\\textrm{v}}$", "Tr", "Mat", "Pt"],
                          categorical_or_boolean_columns_names=["Tr", "Mat", "Pt"], rows_header=3)
                    ])

    #Extraktion
    df_ex = pd.concat([
        loaddatafromexcel(source='Seibert', process='Extraktion', columns_used=[3, 4, 5, 6, 7, 8, 9,10, 12, 13, 14, 15, 16],
                          column_names=["$(V_{\\textrm{c}}+V_{\\textrm{d}})_{\\textrm{f}}$", '$PV$', "$\\rho_{\\textrm{c}}$", "$\\rho_{\\textrm{d}}$", "$\\eta_{\\textrm{c}}$", "$\\eta_{\\textrm{d}}$",'sigma', '$D$',
                                        "$a_{\\textrm{s}}$", "$\\epsilon_{\\textrm{v}}$", "Tr", "Mat", "Pt"],
                          categorical_or_boolean_columns_names=["Tr", "Mat", "Pt"], rows_header=3),
        loaddatafromexcel(source='Thornton', process='Extraktion', columns_used=[3, 4, 5, 6, 7, 8, 9,10, 12, 13, 14, 15, 16],
                          column_names=["$(V_{\\textrm{c}}+V_{\\textrm{d}})_{\\textrm{f}}$", '$PV$', "$\\rho_{\\textrm{c}}$", "$\\rho_{\\textrm{d}}$", "$\\eta_{\\textrm{c}}$", "$\\eta_{\\textrm{d}}$",'sigma', '$D$',
                                        "$a_{\\textrm{s}}$", "$\\epsilon_{\\textrm{v}}$", "Tr", "Mat", "Pt"],
                          categorical_or_boolean_columns_names=["Tr", "Mat", "Pt"], rows_header=3),
        loaddatafromexcel(source='Mackowiak', process='Extraktion', columns_used=[3, 4, 5, 6, 7, 8, 9,10, 12, 13, 14, 15, 16],
                          column_names=["$(V_{\\textrm{c}}+V_{\\textrm{d}})_{\\textrm{f}}$", '$PV$', "$\\rho_{\\textrm{c}}$", "$\\rho_{\\textrm{d}}$", "$\\eta_{\\textrm{c}}$", "$\\eta_{\\textrm{d}}$",'sigma', '$D$',
                                        "$a_{\\textrm{s}}$", "$\\epsilon_{\\textrm{v}}$", "Tr", "Mat", "Pt"],
                          categorical_or_boolean_columns_names=["Tr", "Mat", "Pt"], rows_header=3)
                    ])

    #Hochdruck
    df_hp = pd.concat([
        loaddatafromexcel(source='Meyer', process='Hochdruck',
                          columns_used=[3, 4, 7, 8, 9, 10, 12, 14, 15, 16, 17, 18],
                          column_names=["$(V_{\\textrm{c}}+V_{\\textrm{d}})_{\\textrm{f}}$", '$PV$', "$\\rho_{\\textrm{c}}$", "$\\rho_{\\textrm{d}}$", "$\\eta_{\\textrm{c}}$", "$\\eta_{\\textrm{d}}$", '$D$',
                                        "$a_{\\textrm{s}}$", "$\\epsilon_{\\textrm{v}}$", "Tr", "Mat", "Pt"],
                          categorical_or_boolean_columns_names=["Tr", "Mat", "Pt"], rows_header=3),
        loaddatafromexcel(source='Stockfleth', process='Hochdruck',
                          columns_used=[3, 4, 7, 8, 9, 10, 12, 14, 15, 16, 17, 18],
                          column_names=["$(V_{\\textrm{c}}+V_{\\textrm{d}})_{\\textrm{f}}$", '$PV$', "$\\rho_{\\textrm{c}}$", "$\\rho_{\\textrm{d}}$", "$\\eta_{\\textrm{c}}$", "$\\eta_{\\textrm{d}}$", '$D$',
                                        "$a_{\\textrm{s}}$", "$\\epsilon_{\\textrm{v}}$", "Tr", "Mat", "Pt"],
                          categorical_or_boolean_columns_names=["Tr", "Mat", "Pt"], rows_header=3)
    ])
    df_database = loaddatafromdatabase(columns_used=[1, 4, 5, 6,7,8, 10,12,13,14,15,16],
                          column_names=["$(V_{\\textrm{c}}+V_{\\textrm{d}})_{\\textrm{f}}$", "$\\rho_{\\textrm{c}}$", "$\\rho_{\\textrm{d}}$", "$\\eta_{\\textrm{c}}$", "$\\eta_{\\textrm{d}}$",
                                        "sigma", "$\\epsilon_{\\textrm{v}}$", '$D$',"Tr", "Mat", "Pt", '$PV$'],
                          categorical_or_boolean_columns_names=["Tr", "Mat", "Pt"], rows_header=3)
    # resettet Index, um doppelt vorkommende Indices zu vermeiden
    df_des = df_des.reset_index(drop=True)
    df_ex = df_ex.reset_index(drop=True)
    df_hp = df_hp.reset_index(drop=True)
    print('Alle Daten eingelesen')
    return df_des, df_ex, df_hp, df_database


def splitfeaturetarget(df, targetname):
    """
    Methode um Feature und Target-Dataframe zu erstellen
    :param df: Dataframe mit Features und Target
    :return: x, y Dataframes
    """
    y = np.array(df[targetname]).reshape(-1, 1).astype(np.float32)
    x = df.drop(columns=[targetname])
    return x, y


def regressionsplot_multiple(y, yp, indexlist, name, xlabel, ylabel, unit, output_path, marker, facecolors, label):
    """
    Methode Um Regressionsplot mit Einteilung in einzelne Quellen zu erstellen und unter dem Output Path zu speichern
    :param y: Reale Target Werte als Numpy Array des shapes(anzahl daten, 1)
    :param yp: Predicted Target Werte als Numpy Array des shapes(anzahl daten, 1)
    :return:  Nichts
    """
    plt.figure(figsize=fullpage)
    plt.scatter(x=y[0:indexlist[0]], y=yp[0:indexlist[0]], marker=marker[0], facecolors=facecolors, edgecolors='k', label=label[0])
    for i in range(1,len(indexlist)):

        plt.scatter(x=y[indexlist[i-1]:indexlist[i]], y=yp[indexlist[i-1]:indexlist[i]], marker=marker[i],facecolors=facecolors, edgecolors='k', label=label[i]) #color=(0.5, 0.5, 0.5)
    #plt.xlabel(symbol + ' real ' + unit)
    #plt.ylabel(symbol + ' vorhergesagt ' + unit)
    train_omega = 1.1*y.max()
    #train_omega=40
    train_points = np.linspace(0, train_omega, 10)
    # Diagonale
    plt.plot(train_points, train_points, 'k', linewidth=1)
    plt.plot(train_points, train_points*0.8, 'k',linewidth=1)
    plt.plot(0.8*train_points, train_points,'k',linewidth=1)
    plt.text(train_points[-3]*1.05, train_points[-3] * 0.8*0.99, '-20$\\%$')
    plt.text(train_points[-3] * 0.75*0.9, train_points[-3]*1.05, '+20$\\%$')
    plt.xlim([0, train_omega])
    plt.ylim([0, train_omega])
    #plt.legend(frameon=False)
    #plt.savefig(output_path + '/Regressionsplot_'+name+'.pdf',format='pdf', bbox_inches='tight')  # speichert Bild
    #plt.savefig(output_path + '/Regressionsplot_' + name + '.svg', format='svg', bbox_inches='tight')  # speichert Bild
    #plt.savefig(output_path + '/Regressionsplot_' + name + '.png', format='png', bbox_inches='tight')  # speichert Bild
    plt.xlabel(xlabel + unit)
    plt.ylabel(ylabel + unit)
    plt.savefig(output_path + '/Parityplot_' + name + 'multiple.pdf', format='pdf', bbox_inches='tight')  # speichert Bild
    plt.savefig(output_path + '/Parityplot_' + name + 'multiple.svg', format='svg', bbox_inches='tight')  # speichert Bild
    plt.savefig(output_path + '/Parityplot_' + name + 'multiple.png', format='png', bbox_inches='tight')  # speichert Bild
    plt.close()


def regressionsplot(y, yp):
    """
    Methode Um Regressionsplot zu erstellen und unter dem Output Path zu speichern
    :param y: Reale Target Werte als Numpy Array des shapes(anzahl daten, 1)
    :param yp: Predicted Target Werte als Numpy Array des shapes(anzahl daten, 1)
    :return:  Nichts
    """
    plt.figure(figsize=fullpage)
    plt.scatter(x=y, y=yp, marker='D',facecolors='k', edgecolors='k', label='Data') #color=(0.5, 0.5, 0.5)

    train_omega = 1.1*yp.max()
    train_omega=44
    train_points = np.linspace(0, train_omega, 10)
    # Diagonale
    plt.plot(train_points, train_points, 'k', label='y=x',linewidth=1)
    plt.plot(train_points, train_points*0.8, 'k',linewidth=1)
    plt.plot(0.8*train_points, train_points,'k',linewidth=1)
    plt.text(train_points[-3]*1.05, train_points[-3] * 0.8*0.99, '-20$\\%$')
    plt.text(train_points[-3] * 0.75*0.9, train_points[-3]*1.05, '+20$\\%$')
    plt.xlim(left=0, right=train_omega)
    plt.ylim(bottom=0, top=train_omega)
    plt.xlabel('Flutbelastung experimentell [mm/s]')
    plt.ylabel('Flutbelastung berechnet [mm/s]')
    plt.savefig('fig/Parityplot_Berger.pdf', format='pdf', bbox_inches='tight')  # speichert Bild
    plt.savefig('fig/Parityplot_Berger.svg', format='svg', bbox_inches='tight')  # speichert Bild
    plt.savefig('fig/Parityplot_Berger.png', format='png', bbox_inches='tight')  # speichert Bild
    plt.close()


df_des, df_ex, df_hp, database = loadalldata()
df_train, df_test = train_test_split(df_ex, test_size=0.1, random_state=42)
database_train, database_test = train_test_split(database, test_size=0.1, random_state=42)
x, y = splitfeaturetarget(df_test, '$(V_{\\textrm{c}}+V_{\\textrm{d}})_{\\textrm{f}}$')
x2,y2 = splitfeaturetarget(database_test, '$(V_{\\textrm{c}}+V_{\\textrm{d}})_{\\textrm{f}}$')
sigma = np.array(x['sigma']).reshape(-1, 1).astype(np.float32)
pv = np.array(x['$PV$']).reshape(-1, 1).astype(np.float32)
epsilon = np.array(x['$\\epsilon_{\\textrm{v}}$']).reshape(-1, 1).astype(np.float32)
sigma2 = np.array(x2['sigma']).reshape(-1, 1).astype(np.float32)
pv2 = np.array(x2['$PV$']).reshape(-1, 1).astype(np.float32)
epsilon2 = np.array(x2['$\\epsilon_{\\textrm{v}}$']).reshape(-1, 1).astype(np.float32)

yp = (0.00681+0.7047*sigma-15.222*np.power(sigma,2))*(1-4.55*epsilon+3.247*np.power(epsilon,2))+0.1778*np.log(np.divide(1,pv))+0.0437*np.power(np.log(np.divide(1,pv)),2)
yp2 = (0.00681+0.7047*sigma2-15.222*np.power(sigma2,2))*(1-4.55*epsilon2+3.247*np.power(epsilon2,2))+0.1778*np.log(np.divide(1,pv2))+0.0437*np.power(np.log(np.divide(1,pv2)),2)

#regressionsplot(1000*y,1000*yp)
regressionsplot(1000*y2,1000*yp2)