import pandas as pd
import numpy as np
# from matplotlib import pyplot as plt
# plt.ioff()
import time
import os

from sklearn.base import BaseEstimator
from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV
from sklearn.compose import ColumnTransformer
from sklearn.model_selection import KFold
from sklearn.preprocessing import StandardScaler,MinMaxScaler, OneHotEncoder
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import Ridge, Lasso, ElasticNet
from sklearn.svm import SVR
from sklearn.neighbors import KNeighborsRegressor
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.neural_network import MLPRegressor
from sklearn.gaussian_process.kernels import (RBF, Matern, RationalQuadratic,
                                              ExpSineSquared, DotProduct,
                                              ConstantKernel)


from sklearn.metrics import r2_score, mean_squared_error
from sklearn.metrics import make_scorer
# from matplotlib import pyplot as plt
import joblib
print('Module eingeladen')


#Funktion zum Trainieren des Models
def train_axial_dispersion_model():

    def splitfeaturetarget(df,targetname):
        """
        Methode um Feature und Target-Dataframe zu erstellen
        :param df: Dataframe mit Features und Target
        :return: x, y Dataframes
        """
        import numpy as np
        y = np.array(df[targetname]).reshape(-1,1).astype(np.float32)
        x = df.drop(columns=[targetname])
        return x, y
    # Erstelle Platzhalter Estimator zum Ersetzen in der GridSeach
    class DummyEstimator(BaseEstimator):
            def fit(self): pass

            def score(self): pass
    def average_absolute_relative_error(y_true, y_pred):
       # Berechnet durschschnittlichen absoluten relativen Fehler in Prozent
        import numpy as np
        y_true, y_pred = np.array(y_true), np.array(y_pred)
        return np.mean(np.abs((y_true - y_pred) / y_true)) * 100

    def root_mean_squared_error(y_true, y_pred):#Berechnet Wurzel aus MSE
        from sklearn.metrics import mean_squared_error
        import numpy as np
        return np.sqrt(mean_squared_error(y_true,y_pred))


    # Parameter
    target = 'Dax_Exp'
    targetunit = 'cm^2/s'
    numeric_features = ['p', 'T', 'm-Co2', 'm-H20', 'Density', 'Viscosity', 'Re', 'Sc', 'a_s', 'd_h', 'epsilon']
    categorical_features = []
    n_jobs = 48
    seed = None  # Nummer des Random Seed falls Reproduzierbarkeit erwünscht ist, falls nicht gleich None setzen
    n_folds_hyperparameter = 5  # Anzahl der Folds für die Kreuzvalidierung zur Hyperparametersuche; die Vaidierungs-Menge beträgt 1/n_folds der Trainings+Validierungsmenge
    n_folds_test_train = 10
    scaler = MinMaxScaler()
    print('Parameter erstellt')

    # Beginnt Programm

    # startet Zeitmessung
    start = time.time()

    # erstellt Zeitstempel
    t = time.localtime()
    current_time = time.strftime("%y%m%d__%H_%M_%S", t)

    print(current_time)

    # erstellt Output-Unterordner
    output_path = 'Output//' + str(current_time)
    os.mkdir(output_path)

    # initialisiert Excel-Schreiber
    options = {}
    options['strings_to_formulas'] = False
    options['strings_to_urls'] = False
    excel_path = output_path + '/Output_' + str(current_time) + '.xlsx'
    writer = pd.ExcelWriter(excel_path, options=options)

    # Lädt Daten ein
    columns_used = np.array([0, 1, 2, 3, 4, 7, 8, 9, 10, 11, 12, 13, 14])
    column_names = ['Quelle', 'p', 'T', 'm-Co2', 'm-H20', 'Density', 'Viscosity', 'Re', 'Sc', 'a_s', 'd_h', 'epsilon',
                    'Dax_Exp']
    rows_header = 2
    pathname = 'Input/Kopie von Auswertung_ax_Disp_jobr02-Sc-molaresVolumen' + '.xlsx'

    # einlesen der Daten in ein Pandas Dataframe
    df = pd.read_excel(pathname,sheet_name='Paper Tabelle', usecols=columns_used, names=column_names, header=None, skiprows=rows_header)

    df = df.drop(columns=['Quelle'],axis=1)

    # Feature-Target-Split
    X, y = splitfeaturetarget(df,target)

    # Initialisierung Transformationsschritt
    categorical_transformer = OneHotEncoder(drop='first', categories='auto')
    numeric_transformer = scaler

    mla = DummyEstimator()

    preprocessor = ColumnTransformer(transformers=[
        ('num', numeric_transformer, numeric_features),
        ('cat', categorical_transformer, categorical_features)])

    pipeline = Pipeline([
        ('preprocessor', preprocessor),
        ('MLA', mla)
    ])

    # Initialisierung der Kreuzvalidierung
    cv_gridsearch = KFold(n_splits=n_folds_hyperparameter, shuffle=True, random_state=seed)

    # Definiert Scoring Funktionen für Grid Search
    scoring = {'neg_mean_squared_error': 'neg_mean_squared_error',
               'r2': 'r2',
               'rmse': make_scorer(score_func=root_mean_squared_error, greater_is_better=False),
               'aare': make_scorer(score_func=average_absolute_relative_error, greater_is_better=False)}

    # Definition der Parametergrids
    gausskernels = [1.0 * RBF(length_scale=1.0, length_scale_bounds=(1e-1, 10.0)),
                    1.0 * RationalQuadratic(length_scale=1.0, alpha=0.1),
                    ConstantKernel(0.1, (0.01, 10.0))
                    * (DotProduct(sigma_0=1.0, sigma_0_bounds=(0.1, 10.0)) ** 2),
                    1.0 * Matern(length_scale=1.0, length_scale_bounds=(1e-1, 10.0),
                                 nu=1.5)]
    kernel = ['linear', 'poly', 'rbf', 'sigmoid']
    degree = [2, 3, 4]
    gamma = ['scale', 'auto']
    coef0 = [0.0, 0.01, 0.1],
    tol = 10.0 ** -np.arange(0, 9)
    C = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    epsilon = 10.0 ** -np.arange(1, 14)
    alpha = 10.0 ** -np.arange(3, 12)


    search_space = [{'MLA': [LinearRegression()]},

                    {'MLA': [Lasso()],
                     'MLA__alpha': np.arange(0.001, 0.051, 0.0001)},

                    {'MLA': [KNeighborsRegressor()],
                     'MLA__n_neighbors': np.arange(2, 7, 1),
                     'MLA__weights': ['distance', 'uniform'],
                     'MLA__algorithm': ['auto'],
                     'MLA__p': [1, 2]},

                        {'MLA': [DecisionTreeRegressor(random_state=seed)],
                         'MLA__criterion': ['mse'],
                         'MLA__splitter': ['best'],
                         'MLA__min_samples_split': [4, 3, 2],  # 5 empfohlen dann niedriger
                         'MLA__max_depth': np.arange(3, 21, 1)  # 3 und höher
                         },

                        {'MLA': [SVR()],
                         'MLA__kernel': ['rbf'],
                         'MLA__gamma': gamma,
                         'MLA__tol': tol,
                         'MLA__epsilon': epsilon,
                         'MLA__C': C,
                         'MLA__cache_size': [1000]},

                        {'MLA': [GaussianProcessRegressor(random_state=seed)],
                         'MLA__kernel': gausskernels,
                         'MLA__alpha': alpha,
                         'MLA__n_restarts_optimizer': [1,2,3,4,5,6],
                         'MLA__normalize_y': [False,True]},

                        {'MLA': [RandomForestRegressor(random_state=seed)],
                         'MLA__n_estimators': [1000],
                         'MLA__max_features': [None],
                         'MLA__criterion': ['mse'],
                         'MLA__bootstrap': [True],
                         'MLA__max_depth': [None],
                         'MLA__min_samples_split': [2]
                         },

                        {'MLA': [MLPRegressor(random_state=seed)],
                         'MLA__hidden_layer_sizes': [(5, 5),(10, 5), (12, 6), (15, 7), (20, 10), (25, 12)],
                         'MLA__activation': ['relu', 'tanh'],
                         'MLA__solver': ['lbfgs'],
                         'MLA__alpha': alpha,
                         'MLA__tol': tol,
                         'MLA__max_iter': [10,100,1000],
                         }]

    # Dazu Trainiert Modell auf allen Daten
    # Initialisiert und started Grid Search
    GridSCV = GridSearchCV(estimator=pipeline, param_grid=search_space, cv=cv_gridsearch, scoring=scoring,
                           refit='neg_mean_squared_error', n_jobs=n_jobs, return_train_score=True, verbose=1).fit(
        X, y.ravel())

    # schreibt die Ergebnisse der Grid Search in ein Dataframe und sortiert nach Score
    GridSCV_results = pd.DataFrame(GridSCV.cv_results_)
    GridSCV_results.sort_values(by='rank_test_neg_mean_squared_error', ascending=True, axis=0, inplace=True)

    # speichert Dataframe in Exceldatei
    GridSCV_results.to_excel(writer, sheet_name='Gridsearch alle Daten')
    best_pipe = GridSCV.best_estimator_

    joblib.dump(best_pipe, 'MLA_axial_dispersion.pkl')

    writer.save()
    writer.close()


# Funktion zum Abfragen
def make_axial_dispersion_prediction(p,T,mCO2,mH2O,Density,Viscosity,Re,Sc,a_s,d_h,epsilon):
    """

    :param p: Druck in bar
    :param T: Temperatur in K
    :param mCO2: Massenstrom Co2 in kg/h
    :param mH2O:Massenstrom H20 in kg/h
    :param Density: Dichte in kg/m^3
    :param Viscosity: dyn viskosität in muPas
    :param Re: Reynoldzahl
    :param Sc: Schmidtzahl
    :param a_s: spezifische packungsoberfläche in m^2/m^3
    :param d_h: hydraulischer Packungsdurchmesser in m
    :param epsilon: Leerenanteil packung
    :return:
    """

    import joblib

    # läd modell
    pipeline = joblib.load('MLA_axial_dispersion.pkl')

    # erstellt abfragepunkt
    X = pd.DataFrame(columns=['p', 'T', 'm-Co2', 'm-H20', 'Density', 'Viscosity', 'Re', 'Sc', 'a_s', 'd_h', 'epsilon'
                ])

    X.loc[0, 'p'] = float(p)
    X.loc[0, 'T'] = float(T)
    X.loc[0, 'm-Co2'] = float(mCO2)
    X.loc[0, 'm-H20'] = float(mH2O)
    X.loc[0, 'Density'] = float(Density)
    X.loc[0, 'Viscosity'] = float(Viscosity)
    X.loc[0, 'Re'] = float(Re)
    X.loc[0, 'Sc'] = float(Sc)
    X.loc[0, 'a_s'] = float(a_s)
    X.loc[0, 'd_h'] = float(d_h)
    X.loc[0, 'epsilon'] = float(epsilon)


    yp = pipeline.predict(X)

    return float(yp)

#Bsp-Training
train_axial_dispersion_model()
# Bsp-Abfrage
# print(make_axial_dispersion_prediction(p=80,T=313.15,mCO2=12,mH2O=1,Density=277.9,Viscosity=22.345,Re=106.54003,Sc=0.64901,a_s=700,d_h=0.00034,epsilon=0.86))
