import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
plt.ioff()
import warnings
import time
import os

from sklearn.model_selection import train_test_split
from sklearn.base import BaseEstimator
from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV
from copy import deepcopy
from sklearn.compose import ColumnTransformer
from sklearn.model_selection import KFold
from sklearn.preprocessing import StandardScaler,MinMaxScaler, OneHotEncoder
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import Ridge, Lasso, ElasticNet
from sklearn.svm import SVR
from sklearn.neighbors import KNeighborsRegressor
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.neural_network import MLPRegressor
from sklearn.gaussian_process.kernels import (RBF, Matern, RationalQuadratic,
                                              ExpSineSquared, DotProduct,
                                              ConstantKernel)


from sklearn.metrics import r2_score, mean_squared_error
from sklearn.metrics import make_scorer
from matplotlib import pyplot as plt
import joblib

pagewitdh = 6.3  #inches
factorfullpage = 0.9
factorhalfpage = 0.475
fullpage = (factorfullpage*pagewitdh,factorfullpage*pagewitdh)
halfpage = (factorhalfpage*pagewitdh,factorhalfpage*pagewitdh)
landscape_vertical = (factorfullpage*pagewitdh,2*factorfullpage*pagewitdh)
landscape_horizontal = (2*factorfullpage*pagewitdh,factorfullpage*pagewitdh)
params = {"text.usetex": False,
          "font.family":"serif",
          "font.size": 12,
          "axes.labelsize": 12,
          "xtick.labelsize": 10,
          "ytick.labelsize": 10,
          "xtick.top": True,
          "ytick.right": True,
          "xtick.minor.visible": True,
          "ytick.minor.visible":True,
          "xtick.direction": 'in',
          "ytick.direction":'in',
          "savefig.dpi": 1200
          }
plt.rcParams.update(params)


print('Module eingeladen')

def splitfeaturetarget(df,targetname):
    """
    Methode um Feature und Target-Dataframe zu erstellen
    :param df: Dataframe mit Features und Target
    :return: x, y Dataframes
    """
    import numpy as np
    y = np.array(df[targetname]).reshape(-1,1).astype(np.float32)
    x = df.drop(columns=[targetname])
    return x, y
# Erstelle Platzhalter Estimator zum Ersetzen in der GridSeach
class DummyEstimator(BaseEstimator):
        def fit(self): pass

        def score(self): pass
def average_absolute_relative_error(y_true, y_pred):
   # Berechnet durschschnittlichen absoluten relativen Fehler in Prozent
	import numpy as np
	y_true, y_pred = np.array(y_true), np.array(y_pred)
	return np.mean(np.abs((y_true - y_pred) / y_true)) * 100

def root_mean_squared_error(y_true, y_pred):#Berechnet Wurzel aus MSE
	from sklearn.metrics import mean_squared_error
	import numpy as np
	return np.sqrt(mean_squared_error(y_true,y_pred))
def regressionsplot(y, yp, name, output_path,marker,facecolors):
    import numpy as np
    """
    Methode Um Regressionsplot zu erstellen und unter dem Output Path zu speichern
    :param y: Reale Target Werte als Numpy Array des shapes(anzahl daten, 1)
    :param yp: Predicted Target Werte als Numpy Array des shapes(anzahl daten, 1)
    :return:  Nichts
    """
    #y = y*1000 #m/s auf mm/s
    #yp = yp*1000
    plt.figure(figsize=fullpage)
    plt.scatter(x=y, y=yp, marker=marker,facecolors=facecolors, edgecolors='k', label='Data') #color=(0.5, 0.5, 0.5)
    plt.xlabel('Axiale Dispersion, experimentell ['+ targetunit +']')
    plt.ylabel('Axiale Dispersion, vorhergesagt ['+ targetunit +']')
    train_omega = max(y.max(), yp.max())
    #train_omega=40
    train_points = np.linspace(0, train_omega, 10)
    plt.plot(train_points, train_points, 'k', label='y=x',linewidth=1)
    plt.plot(train_points, train_points*0.8, 'k',linewidth=1)
    plt.plot(0.8*train_points, train_points,'k',linewidth=1)
    plt.text(train_points[-3]*1.05, train_points[-3] * 0.8*0.99, '-20$\\%$')
    plt.text(train_points[-3] * 0.75*0.9, train_points[-3]*1.05, '+20$\\%$')
    plt.xlim([0, train_omega])
    plt.ylim([0, train_omega])
    plt.savefig(output_path + '/Regressionsplot_'+name+'.pdf',format='pdf', bbox_inches='tight')  # speichert Bild
    plt.savefig(output_path + '/Regressionsplot_' + name + '.png', format='png', bbox_inches='tight')  # speichert Bild
    plt.xlabel('Axial Dispersion, experimental ['+ targetunit +']')
    plt.ylabel('Axial Dispersion, predicted ['+ targetunit +']')
    plt.savefig(output_path + '/Parityplot_' + name + '.pdf', format='pdf', bbox_inches='tight')  # speichert Bild
    plt.savefig(output_path + '/Parityplot_' + name + '.png', format='png', bbox_inches='tight')  # speichert Bild
    plt.close()

def makepolynominalfeatures(df, degree):
    """
    Methode um polynominale Features zu erstellen
    :param df: Dataframe welches simple Features enthält
    :param includetarget: ob target in Ausgangsdataframe sein soll
    :param includecategorical: ob kathegorische Variablen in Ausgangsdataframe sein sollen
    :param includesimplefeatures: ob einache Features in Ausgangsdataframe sein sollen
    :return: Dataframe mit polynominalen Features
    """
    import numpy as np
    import pandas as pd
    from sklearn.preprocessing import PolynomialFeatures  # zum erstellen von Features aus Produkten von bestehenden Features
    poly = PolynomialFeatures(degree=degree, include_bias=False)  # erstellt Features als Produkt bis zum Grad degree
    df_poly = poly.fit_transform(df.drop(target, axis=1))  


    polynames = poly.get_feature_names(df.drop(target, axis=1).columns)
    polynames = [w.replace('^', '**') for w in polynames]
    df_poly = pd.DataFrame(df_poly, columns=polynames)  #Wandelt Array in DataFrame


    df_poly[target] = df[target]  # Fügt Target hinzu

 
    return df_poly

# Parameter
target = 'Dax_Exp'
targetunit = 'cm^2/s'
numeric_features = ['p', 'T', 'm-Co2', 'm-H20', 'Density', 'Viscosity', 'Re', 'Sc', 'a_s', 'd_h', 'epsilon']
categorical_features = []
n_jobs = 96
seed = None # Nummer des Random Seed falls Reproduzierbarkeit erwünscht ist, falls nicht gleich None setzen
n_folds_hyperparameter = 5  # Anzahl der Folds für die Kreuzvalidierung zur Hyperparametersuche; die Vaidierungs-Menge beträgt 1/n_folds der Trainings+Validierungsmenge
n_folds_test_train = 10
scaler = MinMaxScaler()
print('Parameter erstellt')

# Beginnt Programm

# startet Zeitmessung
start = time.time()

# erstellt Zeitstempel
t = time.localtime()
current_time = time.strftime("%y%m%d__%H_%M_%S", t)

print(current_time)

# erstellt Output-Unterordner
output_path = 'Output//' + str(current_time)
os.mkdir(output_path)

# initialisiert Excel-Schreiber
options = {}
options['strings_to_formulas'] = False
options['strings_to_urls'] = False
excel_path = output_path + '/Output_' + str(current_time) + '.xlsx'
writer = pd.ExcelWriter(excel_path, options=options)

# Lädt Daten ein
columns_used = np.array([0, 1, 2, 3, 4, 7, 8, 9, 10, 11, 12, 13, 14])
column_names = ['Quelle', 'p', 'T', 'm-Co2', 'm-H20', 'Density', 'Viscosity', 'Re', 'Sc', 'a_s', 'd_h', 'epsilon',
                'Dax_Exp']
rows_header = 2
pathname = 'Input/Kopie von Auswertung_ax_Disp_jobr02-Sc-molaresVolumen' + '.xlsx'

# einlesen der Daten in ein Pandas Dataframe
df = pd.read_excel(pathname,sheet_name='Paper Tabelle', usecols=columns_used, names=column_names, header=None, skiprows=rows_header)

df = df.drop(columns=['Quelle'],axis=1)

#Polynominal
#df = makepolynominalfeatures(df, 2)

# Feature-Target-Split
X, y = splitfeaturetarget(df,target)

# Test-Train-CV
cv_test_train = KFold(n_splits=n_folds_test_train, shuffle=True, random_state=seed)  # Für Hyperparametersuche

# Leere Listen zum Sammeln der Trainings und Testsplits
testsplits = []
trainsplits = []
result = pd.DataFrame(index=df.index, columns=['Dax_Fit','Dax_%', 'Used Estimator'])
splitnumber = 1

# in jedem der Splits
for train_index, test_index in cv_test_train.split(X=X, y=y):
    split_train_data = df.iloc[train_index]
    split_test_data = df.iloc[test_index]
    # speichert Mengen in Exceldatei
    split_train_data.to_excel(writer, sheet_name='Train-Set, Splitno '+str(splitnumber))
    split_test_data.to_excel(writer, sheet_name='Test-Set, Splitno '+str(splitnumber))
    trainsplits.append(split_train_data)
    testsplits.append(split_test_data)
    X_train, y_train = splitfeaturetarget(df= split_train_data, targetname=target)
    X_test, y_test = splitfeaturetarget(df=split_test_data, targetname=target)

    #Initialisierung Transformationsschritt
    categorical_transformer = OneHotEncoder(drop='first', categories='auto')
    numeric_transformer = scaler

    mla = DummyEstimator()

    preprocessor = ColumnTransformer(transformers=[
        ('num', numeric_transformer, numeric_features),
        ('cat', categorical_transformer, categorical_features)])

    pipeline = Pipeline([
        ('preprocessor', preprocessor),
        ('MLA', mla)
    ])

    # Initialisierung der Kreuzvalidierung
    cv_gridsearch = KFold(n_splits=n_folds_hyperparameter, shuffle=True, random_state=seed)

    # Definiert Scoring Funktionen für Grid Search
    scoring = {'neg_mean_squared_error': 'neg_mean_squared_error',
                'r2': 'r2',
                'rmse': make_scorer(score_func=root_mean_squared_error, greater_is_better=False),
                'aare': make_scorer(score_func=average_absolute_relative_error, greater_is_better=False)}


    #Definition der Parametergrids
    gausskernels = [1.0 * RBF(length_scale=1.0, length_scale_bounds=(1e-1, 10.0)),
                        1.0 * RationalQuadratic(length_scale=1.0, alpha=0.1),
                        ConstantKernel(0.1, (0.01, 10.0))
                        * (DotProduct(sigma_0=1.0, sigma_0_bounds=(0.1, 10.0)) ** 2),
                        1.0 * Matern(length_scale=1.0, length_scale_bounds=(1e-1, 10.0),
                                     nu=1.5)]
    kernel = ['linear', 'poly', 'rbf', 'sigmoid']
    degree = [2, 3, 4]
    gamma = ['scale', 'auto']
    coef0 = [0.0, 0.01, 0.1]
    tol = [0.1,0.005,0.01,0.015,0.001,0.0001,0.0005,0.0015]
    C = [12,13,14,15,16,17,18,19]
    #epsilon = 10.0 ** -np.arange(1,14)
    epsilon = [0.1,0.2,0.9,0.5,1.0]
    alpha = 10.0 ** -np.arange(-2, 12)
    ssearch_space = [{'MLA': [LinearRegression()]},

                      {'MLA': [Lasso()],
                      'MLA__alpha': np.arange(0.001, 0.051, 0.0001)}#,'MLA__tol': np.arange(0.001,0.011,0.001)},

                     ]

    search_space = [{'MLA': [LinearRegression()]},

                    {'MLA': [Lasso()],
                     'MLA__alpha': np.arange(0.001, 0.051, 0.0001)},

                    {'MLA': [KNeighborsRegressor()],
                     'MLA__n_neighbors': np.arange(2, 7, 1),
                     'MLA__weights': ['distance', 'uniform'],
                     'MLA__algorithm': ['auto'],
                     'MLA__p': [1, 2]},

                        {'MLA': [DecisionTreeRegressor(random_state=seed)],
                         'MLA__criterion': ['mse'],
                         'MLA__splitter': ['best'],
                         'MLA__min_samples_split': [4, 3, 2],  # 5 empfohlen dann niedriger
                         'MLA__max_depth': np.arange(3, 21, 1)  # 3 und höher
                         },

                        {'MLA': [SVR()],
                         'MLA__kernel': ['rbf'],
                         'MLA__gamma': gamma,
                         'MLA__tol': tol,
                         'MLA__epsilon': epsilon,
                         'MLA__C': C,
                         'MLA__cache_size': [1000]},

                        {'MLA': [GaussianProcessRegressor(random_state=seed)],
                         'MLA__kernel': gausskernels,
                         'MLA__alpha': alpha,
                         'MLA__n_restarts_optimizer': [1,2,3,4,5,6],
                         'MLA__normalize_y': [False,True]},

                        {'MLA': [RandomForestRegressor(random_state=seed)],
                         'MLA__n_estimators': [1000],
                         'MLA__max_features': [None],
                         'MLA__criterion': ['mse'],
                         'MLA__bootstrap': [True],
                         'MLA__max_depth': [None],
                         'MLA__min_samples_split': [2]
                         },

                        {'MLA': [MLPRegressor(random_state=seed)],
                         'MLA__hidden_layer_sizes': [(5, 5),(10, 5), (12, 6), (15, 7), (20, 10), (25, 12)],
                         'MLA__activation': ['relu', 'tanh'],
                         'MLA__solver': ['lbfgs'],
                         'MLA__alpha': alpha,
                         'MLA__tol': tol,
                         'MLA__max_iter': [10,100,1000],
                         }]


    # Initialisiert und started Grid Search
    GridSCV = GridSearchCV(estimator=pipeline, param_grid=search_space, cv=cv_gridsearch, scoring=scoring,
                                   refit='neg_mean_squared_error', n_jobs=n_jobs, return_train_score=True, verbose=1).fit(
               X_train, y_train.ravel())

    # schreibt die Ergebnisse der Grid Search in ein Dataframe und sortiert nach Score
    GridSCV_results = pd.DataFrame(GridSCV.cv_results_)
    GridSCV_results.sort_values(by='rank_test_neg_mean_squared_error', ascending=True, axis=0, inplace=True)

    # speichert Dataframe in Exceldatei
    GridSCV_results.to_excel(writer, sheet_name='Gridsearch Splitnumber '+ str(splitnumber))
    best_pipe = GridSCV.best_estimator_

    # benutzt besten Algorithmus zur Vorhersage auf Testset
    result.loc[X_test.index,'Dax_Fit'] = best_pipe.predict(X_test)
    result.loc[X_test.index, 'Used Estimator'] = GridSCV.best_estimator_[1]
    splitnumber = splitnumber + 1


# Berechnet Scores
df_scores = pd.DataFrame(index=['mse', 'r2', 'AARE', 'RMSE'])
df_scores.loc['mse', 'Whole_Data'] = mean_squared_error(y, result['Dax_Fit'])
df_scores.loc['r2', 'Whole_Data'] = r2_score(y, result['Dax_Fit'])
df_scores.loc['AARE',  'Whole_Data'] = average_absolute_relative_error(y, result['Dax_Fit'])
df_scores.loc['RMSE', 'Whole_Data'] = root_mean_squared_error(y, result['Dax_Fit'])

result['Dax_%'] = (y.ravel() - result['Dax_Fit']) /y.ravel() *100
result['Dax_Exp'] = df['Dax_Exp']

# Erstellt Regressionsplot
regressionsplot(y=result['Dax_Exp'], yp=result['Dax_Fit'], name='Axiale_Dispersion',output_path=output_path,marker='D',facecolors='None')

# Erstellt Musterplots
def Musterplot(df_Musterplot,name):
    plt.figure(figsize=fullpage)
    plt.scatter(x=df_Musterplot['m-CO2'], y=df_Musterplot['Dax_Exp'], marker='D', facecolors='None', edgecolors='k',
                label='Data')
    plt.scatter(x=df_Musterplot['m-CO2'], y=df_Musterplot['Dax_Fit'], marker='x', facecolors='k', edgecolors='k',
                label='Data')
    plt.ylabel('Axiale Dispersion [' + targetunit + ']')
    plt.xlabel('Massenstrom CO2 [kg/h]')

    plt.savefig(output_path + '/Musterdiagramm_' + name + '.pdf', format='pdf', bbox_inches='tight')  # speichert Bild
    plt.savefig(output_path + '/Musterdiagramm_' + name + '.png', format='png', bbox_inches='tight')  # speichert Bild
    plt.ylabel('Axial dispersion [' + targetunit + ']')
    plt.xlabel('Massflow CO2 [kg/h]')
    plt.savefig(output_path + '/Musterdiagramm_en_' + name + '.pdf', format='pdf',
                bbox_inches='tight')  # speichert Bild
    plt.savefig(output_path + '/Musterdiagramm_en_' + name + '.png', format='png',
                bbox_inches='tight')  # speichert Bild
    plt.close()

# Sulzer CY
name_SulzerCY = 'SulzerCY'
df_SulzerCY = df.iloc[[7,23,24,25]]
Musterplot_SulzerCY = pd.DataFrame()
Musterplot_SulzerCY['m-CO2'] = df_SulzerCY['m-Co2']
Musterplot_SulzerCY['Dax_Exp'] = df_SulzerCY['Dax_Exp']
Musterplot_SulzerCY['Dax_Fit'] = result['Dax_Fit']
Musterplot(Musterplot_SulzerCY, name_SulzerCY)
Musterplot_SulzerCY.to_excel(writer, sheet_name='Musterplot_SulzerCY')
df_scores.loc['mse', 'SulzerCY'] = mean_squared_error(Musterplot_SulzerCY['Dax_Exp'], Musterplot_SulzerCY['Dax_Fit'])
df_scores.loc['r2', 'SulzerCY'] = r2_score(Musterplot_SulzerCY['Dax_Exp'], Musterplot_SulzerCY['Dax_Fit'])
df_scores.loc['AARE',  'SulzerCY'] = average_absolute_relative_error(Musterplot_SulzerCY['Dax_Exp'], Musterplot_SulzerCY['Dax_Fit'])
df_scores.loc['RMSE', 'SulzerCY'] = root_mean_squared_error(Musterplot_SulzerCY['Dax_Exp'], Musterplot_SulzerCY['Dax_Fit'])



# Sulzer BX
#name_SulzerBX = 'SulzerBX'
#df_SulzerBX = df.iloc[[42,43,44,45]]
#Musterplot_SulzerBX = pd.DataFrame()
#Musterplot_SulzerBX['m-CO2'] = df_SulzerBX['m-Co2']
#Musterplot_SulzerBX['Dax_Exp'] = df_SulzerBX['Dax_Exp']
#Musterplot_SulzerBX['Dax_Fit'] = result['Dax_Fit']
#Musterplot(Musterplot_SulzerBX, name_SulzerBX)
#Musterplot_SulzerBX.to_excel(writer, sheet_name='Musterplot_SulzerBX')
#df_scores.loc['mse', 'SulzerBX'] = mean_squared_error(Musterplot_SulzerBX['Dax_Exp'], Musterplot_SulzerBX['Dax_Fit'])
#df_scores.loc['r2', 'SulzerBX'] = r2_score(Musterplot_SulzerBX['Dax_Exp'], Musterplot_SulzerBX['Dax_Fit'])
#df_scores.loc['AARE',  'SulzerBX'] = average_absolute_relative_error(Musterplot_SulzerBX['Dax_Exp'], Musterplot_SulzerBX['Dax_Fit'])
#df_scores.loc['RMSE', 'SulzerBX'] = root_mean_squared_error(Musterplot_SulzerBX['Dax_Exp'], Musterplot_SulzerBX['Dax_Fit'])

# Sulzer Mellapack
name_SulzerMellapack = 'SulzerMellapack'
df_SulzerMellapack = df.iloc[[28,29,30,31]]
Musterplot_SulzerMellapack = pd.DataFrame()
Musterplot_SulzerMellapack['m-CO2'] = df_SulzerMellapack['m-Co2']
Musterplot_SulzerMellapack['Dax_Exp'] = df_SulzerMellapack['Dax_Exp']
Musterplot_SulzerMellapack['Dax_Fit'] = result['Dax_Fit']
Musterplot(Musterplot_SulzerMellapack, name_SulzerMellapack)
Musterplot_SulzerMellapack.to_excel(writer, sheet_name='Musterplot_SulzerMellapack')
df_scores.loc['mse', 'SulzerMellapack'] = mean_squared_error(Musterplot_SulzerMellapack['Dax_Exp'], Musterplot_SulzerMellapack['Dax_Fit'])
df_scores.loc['r2', 'SulzerMellapack'] = r2_score(Musterplot_SulzerMellapack['Dax_Exp'], Musterplot_SulzerMellapack['Dax_Fit'])
df_scores.loc['AARE',  'SulzerMellapack'] = average_absolute_relative_error(Musterplot_SulzerMellapack['Dax_Exp'], Musterplot_SulzerMellapack['Dax_Fit'])
df_scores.loc['RMSE', 'SulzerMellapack'] = root_mean_squared_error(Musterplot_SulzerMellapack['Dax_Exp'], Musterplot_SulzerMellapack['Dax_Fit'])

#Simulationsparameter
df_parameter = pd.DataFrame(columns=['parameter','value'])
df_parameter.loc['Random Seed'] = str(seed)
df_parameter.loc['Anzahl Folds in Test-Train-Split'] = str(n_folds_test_train)
df_parameter.loc['Anzahl Folds in Kreuzvalidierung zur Hyperparamteroptimierung'] = str(n_folds_hyperparameter)
df_parameter.loc['Scaler'] = str(scaler)
df_parameter.to_excel(writer, sheet_name='Parameters')

# Parametergrid
pd.DataFrame(search_space).to_excel(writer, sheet_name='Parametergrid')

# Speichert Ergebnisse in Exceldatei & Pkl
result.to_excel(writer, sheet_name='Prädiktionen')
result.to_pickle(".\Result.pkl")
# Scores
df_scores.to_excel(writer, sheet_name='Scores')

# Schreibt Ergebnisse in Excel-Datei
writer.save()
writer.close()

# Stoppen und Ausgeben der Zeit
end = time.time()
runtime = (end - start)/60
print("Gesamtzeit: \n%.1f Minuten " %runtime )




"""
mCO2_min = df_points['m-Co2'].min()
mCO2_max = df_points['m-Co2'].max()
new_mCo2 = np.linspace(start=mCO2_min, stop=mCO2_max, num=50)

Re_min = df_points['Re'].min()
Re_max = df_points['Re'].max()
new_Re = np.linspace(start=Re_min, stop=Re_max, num=50)

df_to_analyse = pd.DataFrame(columns=df_points.columns, index=range(50))

for col in df_to_analyse.columns:
    df_to_analyse.loc[:, col] = df_points.loc[:, col].mean()

df_to_analyse.loc[:, 'm-Co2'] = new_mCo2
df_to_analyse.loc[:, 'Re'] = new_Re

X_to_analyse, y_falsch = splitfeaturetarget(df=df_to_analyse, targetname=target)
yp = best_pipe.predict(X_to_analyse)

plt.figure(figsize=fullpage)
plt.scatter(x=df_points['m-Co2'], y=df_points['Dax_Exp'], marker='D',facecolors='None', edgecolors='k', label_axD='Data') #color=(0.5, 0.5, 0.5)
plt.xlabel('Axiale Dispersion ['+ targetunit +']')
plt.ylabel('Massenstrom CO2 [kg/h]')
plt.plot(X_to_analyse['m-Co2'],yp)

plt.savefig(output_path + '/Musterdiagramm_'+name+'.pdf',format='pdf', bbox_inches='tight')  # speichert Bild
plt.savefig(output_path + '/Musterdiagramm_' + name + '.png', format='png', bbox_inches='tight')  # speichert Bild
plt.xlabel('Axial dispersion ['+ targetunit +']')
plt.ylabel('Massflow CO2 [kg/h]')
plt.savefig(output_path + '/Musterdiagramm_en_' + name + '.pdf', format='pdf', bbox_inches='tight')  # speichert Bild
plt.savefig(output_path + '/Musterdiagramm_en_' + name + '.png', format='png', bbox_inches='tight')  # speichert Bild
plt.close()
"""
