#!/usr/local_rwth/bin/zsh
 
### #SBATCH directives need to be in the first part of the jobscript

#SBATCH -A, --account=thes0689
#SBATCH --cpus-per-task=48
#SBATCH --mem-per-cpu=500M
#SBATCH -t, --time=0-12:00:00
#SBATCH -J Ax_Dis_1
#SBATCH --output=output.%J.txt
#SBATCH --mail-type=all
#SBATCH -vvv
### your code goes here, the second part of the jobscript

cd /home/mc446597/PycharmProjects/Axiale_Dispersion
/home/mc446597/PycharmProjects/kopieKDDpacked/venv/bin/python3.6 /home/mc446597/PycharmProjects/Axiale_Dispersion/axiale_dispersion.py

%#SBATCH -A, --account=thes0689
