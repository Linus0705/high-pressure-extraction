from Density_Viscosity import MLA_viscosity_co2_water, MLA_density_co2_water
from Distribution_Coeff import MLA_distributioncoeff_ethanol
from Sauterdurchmesser import sauterdiameter
from Velocity import velocity
from Submodelle import masstransfercoeff, Diffusion
from Holdup import holdup
from Axiale_Dispersion import use_MLA_axial_dispersion
import numpy as np
import pandas as pd
import math
import time
import os
from scipy.integrate import solve_ivp
import matplotlib.pyplot as plt
print('Module eingeladen')


def main(n, dt, h, T, p, D, a_s, epsilon, d_h, cutoff, q_c, q_d, y_e_0, y_w_0, x_e_0, x_co2_0, fit):
    """
    Hauptmethode
    :param n: Anzahl Stufen
    :param dt: Größe des Zeitschrittes [s]
    :param h: Kolonnenhöhe [m]
    :param T: Temperatur [K]
    :param p: Druck [bar]
    :param D: Kolonnendurchmesser [m]
    :param a_s: spezifische Oberfläche pro Packung [1/m]
    :param epsilon: Leervolumenanteil Kolonne [-]
    :param d_h: hydraulischer Durchmesser
    :param cutoff: Cut-off [-]
    :param q_c: Massenstrom konti Phase [kg/h]
    :param q_d: Massenstrom disperse Phase [kg/h]
    :param x_e_0: Massenanteil Ethanol im Feedstrom [g/g]
    :param x_co2_0: Massenanteil CO2 im Feedstrom [g/g]
    :param y_e_0: Massenanteil Ethanol im Lösemittel [g/g]
    :param y_w_0: Massenanteil Wasser im Lösemittel [g/g]
    :param fit: array mit empirischen Parametern für Massentransferkoeffizienten
    :return:
    """
    # Zeitmessung starten
    start = time.time()
    # erstellt Zeitstempel
    tlocal = time.localtime()
    current_time = time.strftime("%y%m%d__%H_%M_%S", tlocal)
    fig_path = 'fig//' + str(current_time) + 'Heydrich_mitDG'
    os.mkdir(fig_path)

    # initialisiert Excel-Schreiber
    options = {}
    options['strings_to_formulas'] = False
    options['strings_to_urls'] = False
    excel_path = fig_path + '//Data' + str(current_time) + '.xlsx'
    writer = pd.ExcelWriter(excel_path, options=options)

    pbar = p*10
    # Geometrische Größen
    z = h/n
    d_p = 6*(1-epsilon)/a_s
    S = math.pi * D**2 / 4
    V_K = S*h*epsilon

    # Stoffdaten
    df_rho_c, df_rho_d = MLA_density_co2_water.loadalldata()
    df_eta_c, df_eta_d = MLA_viscosity_co2_water.loadalldata()

    M_co2 = 44.01  # [g/mol]
    M_w = 18.01528  # [g/mol]
    M_e = 46.07  # [g/mol]
    rho_c, rho_d = MLA_density_co2_water.NIST_get_density(df_rho_c, df_rho_d, T, p)
    eta_c, eta_d = MLA_viscosity_co2_water.NIST_get_viscosity(df_eta_c, df_eta_d, T, p)

    #Diffusion
    phi_w = 2.6
    V_e, V_w, V_co2 = Diffusion.getVeb()
    D_co2 = Diffusion.SassiatMourier(T, M_co2, V_co2, eta_c)
    D_w_co2 = Diffusion.SassiatMourier(T, M_w, V_w, eta_c)
    D_e_co2 = Diffusion.SassiatMourier(T, M_e, V_e, eta_c)
    D_e_w = Diffusion.WilkeChang(T, M_w, V_e, eta_d, phi_w)
    D_co2_w = Diffusion.WilkeChang(T, M_w, V_co2, eta_d, phi_w)
    D_w = Diffusion.WilkeChang(T, M_w, V_w, eta_d, phi_w)
    #nach Heydrich
    #D_e_co2 = 2.33e-8
    #D_e_w = 1.75e-9


    kwinkel = 130
    sigma = 30
    # Beachte sigma in mN/m und D in mm umwandeln
    # Sauterdurchmesser, Stoffaustauschfläche


    # nach Heydrich
    d32 = 4.02 * ((q_c + q_d) / 3600 / S) ** (-0.34) * x_e_0 ** (-0.51) / 1000

    nTropfen = hl * (S * epsilon) / (d32 ** 3 / 6 * math.pi)
    A = nTropfen * d32 ** 2 * math.pi

    masse_d = V_K * hl * rho_d
    masse_c = V_K * (1 - hl) * rho_c
    # Geschwindigkeiten Leerrohr
    pv = q_c / q_d
    v_d_leer = q_d / 3600 / rho_d / S
    v_c_leer = q_c / 3600 / rho_c / S

    # angepasster Kumar
    c1 = 0.9912
    c2 = 0.86 ** -0.837
    c3 = ((rho_d - rho_c) / rho_c) ** -0.9999
    c4 = ((1 / 700) * ((((rho_c ** 2) * 9.81) / (eta_c ** 2)) ** (1 / 3))) ** -0.99999
    c5 = (eta_d / eta_c) ** 0.6254
    c6 = ((v_d_leer * ((rho_c / (9.81 * eta_c)) ** (1 / 3)))) ** 0.40441
    c7 = math.exp(0.6696 * v_c_leer * ((rho_c / (9.81 * eta_c)) ** (1 / 3)))
    c8 = ((eta_c / (((rho_c * 70 * 10 ** -3) / (700)) ** 0.5)) ** -0.4091)
    hl = c1 * c2 * c3 * c4 * c5 * c6 * c7 * c8 / 100

    #stat = 4.773359091  # CY
    #stat = 8.592192302   # Mellapak
    stat = 6.031801026  # gesamt
    v_d = q_d / (rho_d * S * 3600 * epsilon * hl * stat)
    v_c = q_c / (rho_c * S * 3600 * epsilon * (1 - hl * stat))
    v_s = (v_c + v_d)*1.414213562

    # Reynolds und Schmidtzahl
    re_sc = reynolds(rho_c, eta_c, v_c, d_p)
    #re_d = reynolds(rho_d, eta_d, v_d, 1/a_s)
    sc_c = schmidt(rho_c, eta_c, D_co2)
    #sc_d = schmidt(rho_d, eta_d, D_w)
    # axiale Dispersion in [m^2/s]
    pe = use_MLA_axial_dispersion.make_axial_dispersion_prediction(re_sc, sc_c)
    dax = v_c*h/pe

    #Massentransferkoeffizienten bestimmen
    fit_c = fit[0:4]
    fit_d = fit[4:7]
    k_c_co2 = masstransfercoeff.mtcoeff_c_seibert(D_co2, d32, v_s, rho_c, eta_c, hl, fit_c)
    k_d_co2 = masstransfercoeff.mtcoeff_d_seibert(D_co2_w, v_s, rho_d, eta_c, eta_d, fit_d)
    k_c_w = masstransfercoeff.mtcoeff_c_seibert(D_w_co2, d32, v_s, rho_c, eta_c, hl, fit_c)
    k_d_w = masstransfercoeff.mtcoeff_d_seibert(D_w, v_s, rho_d, eta_c, eta_d, fit_d)
    k_c_e = masstransfercoeff.mtcoeff_c_seibert(D_e_co2, d32, v_s, rho_c, eta_c, hl, fit_c)
    k_d_e = masstransfercoeff.mtcoeff_d_seibert(D_e_w, v_s, rho_d, eta_c, eta_d, fit_d)

    def rhs_fast(t, u, k):
        """
        DGL-System (K- und J-Berechnung ausgelagert für schnellere Berechnung)
        """
        # Vektor anlegen
        rhs_fast = np.zeros(4 * (n + 1))
        je = k[0]
        jco2 = k[1]
        jw = k[2]
        # y_ethanol 0 - n
        # y_wasser n+1 - 2n+1
        # x_ethanol 2n+2 - 3n+2
        # x_co2 3n+3 - 4n+3
        # rhs = dyi/dt
        # u[i] = y an Ort i

        # Kompartments zwischen Boden und Kopf
        for i in range(1, n):  # 1....n-1
            i1 = i + n + 1
            i2 = i + 2 * (n + 1)
            i3 = i + 3 * (n + 1)
            # Massentransfer bestimmen

            rhs_fast[i] = (v_c / z) * (u[i - 1] - u[i]) + (dax / z ** 2) * (u[i + 1] - 2 * u[i] + u[i - 1]) \
                          + je[i] / (h * S * epsilon * (1 - hl) * rho_c)

            rhs_fast[i1] = (v_c / z) * (u[i1 - 1] - u[i1]) + (dax / z ** 2) * (u[i1 + 1] - 2 * u[i1] + u[i1 - 1]) \
                           + jw[i] / (h * S * epsilon * (1 - hl) * rho_c)
            rhs_fast[i2] = (v_d / z) * (u[i2 + 1] - u[i2]) \
                           - je[i] / (h * S * epsilon * hl * rho_d)

            rhs_fast[i3] = (v_d / z) * (u[i3 + 1] - u[i3]) \
                           - jco2[i] / (h * S * epsilon * hl * rho_d)

        # RANDBEDINGUNGEN
        # Eingang (konti)
        rhs_fast[0] = (dax / z * rhs_fast[1]) / (v_c + dax / z)
        rhs_fast[n + 1] = (dax / z * rhs_fast[n + 2]) / (v_c + dax / z)
        # Ausgang (dispers)
        rhs_fast[2 * (n + 1)] = rhs_fast[2 * (n + 1) + 1]  # dx0dt = dx1dt
        rhs_fast[3 * (n + 1)] = rhs_fast[3 * (n + 1) + 1]
        # Ausgang (konti)
        rhs_fast[n] = rhs_fast[n - 1]  # dyN/dt=dyN-1/dt
        rhs_fast[2 * n + 1] = rhs_fast[2 * n]
        # Eingang (dispers)
        rhs_fast[3 * n + 2] = 0  # dxN/dt=0 da xN=xein (konst)
        rhs_fast[4 * n + 3] = 0
        return rhs_fast


    def rhs_fast_k_konstant(t, u, k):
        """
        DGL-System (K- und J-Berechnung ausgelagert für schnellere Berechnung)
        """
        # Vektor anlegen
        rhs_fast = np.zeros(4 * (n + 1))
        je = np.zeros(n)
        jw = np.zeros(n)
        jco2 = np.zeros(n)
        Ke = k[0]
        Kco2 = k[1]
        Kw = k[2]
        # y_ethanol 0 - n
        # y_wasser n+1 - 2n+1
        # x_ethanol 2n+2 - 3n+2
        # x_co2 3n+3 - 4n+3
        # rhs = dyi/dt
        # u[i] = y an Ort i

        # Kompartments zwischen Boden und Kopf
        for i in range(1, n):  # 1....n-1
            i1 = i + n + 1
            i2 = i + 2 * (n + 1)
            i3 = i + 3 * (n + 1)
            # Massentransfer bestimmen
            je[i] = diffstrom_i(A, rho_c, rho_d, k_c_e, k_d_e, u[i2], u[i], Ke)
            jw[i] = diffstrom_i(A, rho_c, rho_d, k_c_w, k_d_w, 1 - u[i2] - u[i3], u[i1], Kw)
            jco2[i] = diffstrom_i(A, rho_c, rho_d, k_c_co2, k_d_co2, u[i3], 1 - u[i] - u[i1], Kco2)

            rhs_fast[i] = (v_c / z) * (u[i - 1] - u[i]) + (dax / z ** 2) * (u[i + 1] - 2 * u[i] + u[i - 1]) + je[i] / (
                    h * S * epsilon * (1 - hl) * rho_c)

            rhs_fast[i1] = (v_c / z) * (u[i1 - 1] - u[i1]) + (dax / z ** 2) * (u[i1 + 1] - 2 * u[i1] + u[i1 - 1]) + jw[i] / (
                    h * S * epsilon * (1 - hl) * rho_c)
            rhs_fast[i2] = (v_d / z) * (u[i2 + 1] - u[i2]) - je[i] / (h * S * epsilon * hl * rho_d)

            rhs_fast[i3] = (v_d / z) * (u[i3 + 1] - u[i3]) - jco2[i] / (h * S * epsilon * hl * rho_d)

        # RANDBEDINGUNGEN
        # Eingang (konti)
        rhs_fast[0] = (dax / z * rhs_fast[1]) / (v_c + dax / z)
        rhs_fast[n + 1] = (dax / z * rhs_fast[n + 2]) / (v_c + dax / z)
        # Ausgang (dispers)
        rhs_fast[2 * (n + 1)] = rhs_fast[2 * (n + 1) + 1]  # dx0dt = dx1dt
        rhs_fast[3 * (n + 1)] = rhs_fast[3 * (n + 1) + 1]
        # Ausgang (konti)
        rhs_fast[n] = rhs_fast[n - 1]  # dyN/dt=dyN-1/dt
        rhs_fast[2 * n + 1] = rhs_fast[2 * n]
        # Eingang (dispers)
        rhs_fast[3 * n + 2] = 0  # dxN/dt=0 da xN=xein (konst)
        rhs_fast[4 * n + 3] = 0
        return rhs_fast


    def j_zwischen(u):
        """
        ausgelagerte Berechnung von Ji
        :param u: Vektor mit Massenanteilen für alle Kompartments
        :return: j_ethanol, j_co2, j_wasser
        """
        je = np.zeros(n+1)
        jw = np.zeros(n+1)
        jco2 = np.zeros(n+1)
        z_ethanol = np.zeros(n+1)
        z_wasser = np.zeros(n+1)
        z_co2 = np.ones(n+1)
        z_ethanol[1:n-1+1] = (masse_d * u[2*n+3:3*n+1+1] + masse_c * u[1:n-1+1]) / (masse_c + masse_d)
        z_wasser[1:n-1+1] = (masse_d * (1 - u[2*n+3:3*n+1+1] - u[3*n+4:4*n+2+1]) + masse_c * u[n+2:2*n+1]) / (masse_c + masse_d)
        #z_ethanol[1:n - 1 + 1] = (q_d * u[2 * n + 3:3 * n + 1 + 1] + q_c * u[1:n - 1 + 1]) / (q_c + q_d)
        #z_wasser[1:n - 1 + 1] = (q_d * (1 - u[2 * n + 3:3 * n + 1 + 1] - u[3 * n + 4:4 * n + 2 + 1]) + q_c * u[n + 2:2 * n + 1]) / (q_c + q_d)
        z_co2 = z_co2 - z_ethanol - z_wasser


        for i in range(1, n+1):  # 1....n-1
            i1 = i + n + 1
            i2 = i + 2 * (n + 1)
            i3 = i + 3 * (n + 1)
            # Verteilungskoeff bestimmen
            # globale Zusammensetzung :todo Methode schreiben

            Ke, Kco2, Kw = MLA_distributioncoeff_ethanol.make_distributioncoeff_predictions_hp(T=T, p=p, x_co2=z_co2[i], x_w=z_wasser[i], x_ethanol=z_ethanol[i])

            je[i] = diffstrom_i(A, rho_c, rho_d, k_c_e, k_d_e, u[i2], u[i], Ke)
            jw[i] = diffstrom_i(A, rho_c, rho_d, k_c_w, k_d_w, 1 - u[i2] - u[i3], u[i1], Kw)
            jco2[i] = diffstrom_i(A, rho_c, rho_d, k_c_co2, k_d_co2, u[i3], 1 - u[i] - u[i1], Kco2)
        return je, jco2, jw

    def solve():
        """
        Löst das DGL-System für Kompartments n und Zeitschritte dt und plottet Konzentrationsverläufe
        :return: array mit Endkonzentrationen [y_ethanol, y_co2, x_ethanol, x_wasser]
        """
        print("Starte Solver")
        daten = pd.DataFrame()
        # u = xi(z),yi(z)
        u = np.zeros(4*(n+1))
        # u0 Startwerte für Solver
        u0 = np.zeros(4*(n+1))
        u0[0] = y_e_0
        u0[n+1] = y_w_0
        u0[3*n+2] = x_e_0
        u0[4*n+3] = x_co2_0
        t=0
        y_e_exit = np.zeros((3000,7))
        success = True
        # Änderung < tol = stationärer Zustand
        tol = 5e-3/dt/n
        deltay = 1  # dummy
        i = 0
        while success and deltay > tol:  # Solver erfolgreich und instationär
            i = i+1
            sol = solve_ivp(rhs_fast, [t, t+dt], u0, method='BDF', t_eval=[t+dt], args=[j_zwischen(u0)])
            success = sol.success
            deltay = max(sol.y[:,0]-u0)  # größte Änderung zum vorherigen Zeitschritt
            t = i * dt
            # Lösungen werden Startwerte für nächsten Zeitschritt

            u0 = sol.y[:, 0]
            y_e_exit[i, 0] = u0[n]  # y ethanol
            y_e_exit[i, 1] = t  # Zeit
            y_e_exit[i, 2] = u0[2*n+1]  # y wasser
            y_e_exit[i, 3] = u0[2*n+2]  # x ethanol
            y_e_exit[i, 4] = u0[3*n+3]  # x co2
            y_e_exit[i, 5] = 1-y_e_exit[i, 0]-y_e_exit[i, 2]  # y co2
            y_e_exit[i, 6] = 1-y_e_exit[i, 3]-y_e_exit[i, 4]  # x wasser
            y_ethanol = u0[0:n+1]
            y_wasser = u0[n + 1:2 * n + 1+1]
            x_ethanol = u0[2 * (n + 1):3 * n + 2+1]
            x_co2 = u0[3 * (n + 1):4 * n + 3+1]
            x_w = np.ones(n+1) - x_ethanol - x_co2
            y_co2 = np.ones(n+1) - y_ethanol - y_wasser

            # plotten
            plt.figure()  # leere Figure
            plt.axis([0.0, n, 0, 0.01])
            plt.xlabel('n')
            plt.ylabel('Massenanteil')
            plt.title('t=' + str(t))
            plt.plot(y_ethanol, 'bo--', label='y_Ethanol')
            plt.plot(y_wasser, 'ro--', label='y_Wasser')
            daten['y_Ethanol'] = y_ethanol
            daten['y_wasser'] = y_wasser
            daten['y_CO2'] = y_co2
            plt.legend()
            plt.savefig(fig_path + '//y_t=%04d.png' % t)
            plt.close()
            plt.figure()  # leere Figur
            plt.axis([0.0, n, 0, x_e_0])
            plt.xlabel('n')
            plt.ylabel('Massenanteil')
            plt.title('t=' + str(t))
            plt.plot(x_ethanol, 'bo--', label='x_Ethanol')
            plt.plot(x_co2, 'ko--', label='x_CO2')
            daten['x_Ethanol'] = x_ethanol
            daten['x_CO2'] = x_co2
            daten['x_Wasser'] = x_w
            daten.to_excel(excel_writer=writer, sheet_name='Zeit = ' + str(t))
            plt.legend()
            plt.savefig(fig_path + '//x_t=%04d.png' % t)
            plt.close()

            plt.figure()  # leere Figur
            plt.axis([0.0, n, 0, 1])
            plt.xlabel('n')
            plt.ylabel('Massenanteil')
            plt.title('t=' + str(t))
            plt.plot(y_co2, 'ko--', label='y_CO2')
            plt.plot(x_w, 'ro--', label='x_Wasser')
            plt.legend()
            plt.savefig(fig_path + '//Rest_t=%04d.png' % t)
            plt.close()
            data = [y_ethanol[n-1+1], y_co2[n-1+1], x_ethanol[0], x_w[0], y_ethanol[0], y_co2[0], x_ethanol[n-1+1], x_w[n-1+1], pv]

        z_co2_final = np.ones(n+1)
        z_e_final = (masse_c * y_ethanol + masse_d * x_ethanol) / (masse_c + masse_d)
        z_w_final = (masse_c * y_wasser + masse_d * x_w) / (masse_c + masse_d)
        #z_e_final = (q_c * y_ethanol + q_d * x_ethanol) / (q_c + q_d)
        #z_w_final = (q_c * y_wasser + q_d * x_w) / (q_c + q_d)
        z_co2_final = z_co2_final - z_e_final - z_w_final
        Ke = np.ones(n)
        Kw = np.ones(n)
        Kco2 = np.ones(n)
        for l in range(1, n):  # 1....n-1
            # Verteilungskoeff bestimmen

            Ke[l], Kco2[l], Kw[l] = MLA_distributioncoeff_ethanol.make_distributioncoeff_predictions_hp(T=T, p=p, x_co2=
            z_co2_final[l], x_w=z_w_final[l], x_ethanol=z_e_final[l])
        average_Ke = Ke[1:n - 1+1].mean()
        average_Kco2 = Kco2[1:n - 1+1].mean()
        average_Kw = Kw[1:n - 1+1].mean()
        print("HETS")
        print(hets_kbs(data, h, average_Ke))
        print(hets_kbs_bel(data, h, average_Ke, average_Kco2, x_e_0))

        plt.figure()  # leere Figur
        plt.axis([0.0, n, 0, 0.2])
        plt.xlabel('n')
        plt.ylabel('K_Ethanol')
        plt.title('t=' + str(t))
        plt.plot(Ke[1:n], 'k--', label='K_Ethanol')
        plt.legend()
        plt.savefig(fig_path + '//Ke_über_n.png')
        plt.close()

        plt.figure()  # leere Figur
        plt.axis([0.0, n, 0, 0.02])
        plt.xlabel('n')
        plt.ylabel('K_Wasser')
        plt.title('t=' + str(t))
        plt.plot(Kw[1:n], 'k--', label='K_Wasser')
        plt.legend()
        plt.savefig(fig_path + '//Kw_über_n.png')
        plt.close()

        plt.figure()  # leere Figur
        plt.axis([0.0, n, 0, 15])
        plt.xlabel('n')
        plt.ylabel('K_CO2')
        plt.title('t=' + str(t))
        plt.plot(Kco2[1:n], 'k--', label='K_CO2')
        plt.legend()
        plt.savefig(fig_path + '//Kco2_über_n.png')
        plt.close()


        plt.figure()  # leere Figur
        plt.axis([0.0, n, 0, 1])
        plt.xlabel('n')
        plt.ylabel('Massenanteil')
        plt.title('t=' + str(t))
        plt.plot(y_co2, 'k--', label='y_CO2')
        plt.plot(x_w, 'r:', label='x_Wasser')
        plt.plot(x_ethanol, 'b:', label='x_Ethanol')
        plt.plot(x_co2, 'k:', label='x_CO2')
        plt.plot(y_ethanol, 'b--', label='y_Ethanol')
        plt.plot(y_wasser, 'r--', label='y_Wasser')
        plt.legend()
        plt.savefig(fig_path + '//Alle_über_n.png')
        plt.close()


        # Plot Ausganskonzentration über Zeit
        plt.figure()
        plt.xlabel('t')
        plt.ylabel('Massenanteil am Ausgang')
        plt.plot(y_e_exit[0:i, 1], y_e_exit[0:i, 0], 'b--', label='y_Ethanol')
        plt.plot(y_e_exit[0:i, 1], y_e_exit[0:i, 2], 'r--', label='y_Wasser')
        plt.plot(y_e_exit[0:i, 1], y_e_exit[0:i, 3], 'b:', label='x_Ethanol')
        plt.plot(y_e_exit[0:i, 1], y_e_exit[0:i, 4], 'k:', label='x_CO2')
        #plt.plot(y_e_exit[0:i, 1], y_e_exit[0:i, 5], 'k--', label='y_CO2')
        #plt.plot(y_e_exit[0:i, 1], y_e_exit[0:i, 6], 'r:', label='x_Wasser')

        plt.legend()
        plt.savefig(fig_path + '//Exit.png')
        plt.close()

        # Plot Ausganskonzentration über Zeit Vergleich mit Raphael
        plt.figure()
        plt.xlabel('t')
        plt.ylabel('Massenanteil CO2-frei')
        plt.plot(y_e_exit[0:i, 1], y_e_exit[0:i, 0]/(y_e_exit[0:i, 2]), 'b--', label='y_Ethanol]')
        plt.hlines(0.86, 0, y_e_exit[i, 1])
        plt.plot(y_e_exit[0:i, 1], y_e_exit[0:i, 3]/(1-y_e_exit[0:i, 6]-y_e_exit[0:i, 3]), 'b:', label='x_Ethanol')
        plt.hlines(0.05, 0, y_e_exit[i, 1])

        plt.legend()
        plt.savefig(fig_path + '//Exit_vgl_raphael.png')
        plt.close()

        # Plot Ausganskonzentration über Zeit Vergleich mit Heydrich
        """plt.figure()
        plt.xlabel('t')
        plt.ylabel('Ethanol Beladung am Ausgang')
        plt.plot(y_e_exit[0:i, 1], y_e_exit[0:i, 0]/(y_e_exit[0:i, 5]), 'b--', label='y_Ethanol [kgEthanol/kgCO2]')
        plt.hlines(0.21, 0, y_e_exit[i, 1])
        plt.plot(y_e_exit[0:i, 1], y_e_exit[0:i, 3]/(y_e_exit[0:i, 6]), 'b:', label='x_Ethanol [kgEthanol/kgWasser]')
        plt.hlines(0.062, 0, y_e_exit[i, 1])

        plt.legend()
        plt.savefig(fig_path + '//Exit_vgl_heydrich.png')
        plt.close()"""

        return data

    def solve_k_konstant():
        """
        Löst das DGL-System für Kompartments n und Zeitschritte dt und plottet Konzentrationsverläufe
        :return: array mit Endkonzentrationen [y_ethanol, y_co2, x_ethanol, x_wasser]
        """
        print("Starte Solver")
        daten = pd.DataFrame()
        # u = xi(z),yi(z)
        u = np.zeros(4*(n+1))
        # u0 Startwerte für Solver
        u0 = np.zeros(4*(n+1))
        u0[0] = y_e_0
        u0[n+1] = y_w_0
        u0[3*n+2] = x_e_0
        u0[4*n+3] = x_co2_0
        t=0
        y_e_exit = np.zeros((3000,7))
        success = True
        # Änderung < tol = stationärer Zustand
        tol = 5e-3/dt/n
        deltay = 1  # dummy
        i = 0
        z_ethanol = np.zeros(n)
        z_wasser = np.zeros(n)
        z_co2 = np.ones(n)
        z_ethanol[1:n - 1] = (masse_d * u[2 * n + 3:3 * n + 1] + masse_c * u[1:n - 1]) / (masse_c + masse_d)
        z_wasser[1:n - 1] = (masse_d * (1 - u[2 * n + 3:3 * n + 1] - u[3 * n + 4:4 * n + 2]) + masse_c * u[
                                                                                                         n + 2:2 * n]) / (
                                    masse_c + masse_d)
        z_co2 = z_co2 - z_ethanol - z_wasser
        Ke, Kco2, Kw = MLA_distributioncoeff_ethanol.make_distributioncoeff_predictions_hp(T, p, z_co2[1], z_wasser[1], z_ethanol[1])
        while success and deltay > tol:  # Solver erfolgreich und instationär
            i = i+1
            sol = solve_ivp(rhs_fast_k_konstant, [t, t+dt], u0, method='BDF', t_eval=[t+dt], args=[[Ke, Kco2, Kw]])
            success = sol.success
            deltay = max(sol.y[:,0]-u0)  # größte Änderung zum vorherigen Zeitschritt
            t = i * dt
            # Lösungen werden Startwerte für nächsten Zeitschritt

            u0 = sol.y[:, 0]
            y_e_exit[i, 0] = u0[n]  # y ethanol
            y_e_exit[i, 1] = t  # Zeit
            y_e_exit[i, 2] = u0[2*n+1]  # y wasser
            y_e_exit[i, 3] = u0[2*n+2]  # x ethanol
            y_e_exit[i, 4] = u0[3*n+3]  # x co2
            y_e_exit[i, 5] = 1-y_e_exit[i, 0]-y_e_exit[i, 2]  # y co2
            y_e_exit[i, 6] = 1-y_e_exit[i, 3]-y_e_exit[i, 4]  # x wasser
            y_ethanol = u0[0:n+1]
            y_wasser = u0[n + 1:2 * n + 1+1]
            x_ethanol = u0[2 * (n + 1):3 * n + 2+1]
            x_co2 = u0[3 * (n + 1):4 * n + 3+1]
            x_w = np.ones(n+1) - x_ethanol - x_co2
            y_co2 = np.ones(n+1) - y_ethanol - y_wasser

            # plotten
            plt.figure()  # leere Figure
            plt.axis([0.0, n, 0, 0.01])
            plt.xlabel('n')
            plt.ylabel('Massenanteil')
            plt.title('t=' + str(t))
            plt.plot(y_ethanol, 'bo--', label='y_Ethanol')
            plt.plot(y_wasser, 'ro--', label='y_Wasser')
            daten['y_Ethanol'] = y_ethanol
            daten['y_wasser'] = y_wasser
            daten['y_CO2'] = y_co2
            plt.legend()
            plt.savefig(fig_path + '//y_t=%04d.png' % t)
            plt.close()
            plt.figure()  # leere Figur
            plt.axis([0.0, n, 0, x_e_0])
            plt.xlabel('n')
            plt.ylabel('Massenanteil')
            plt.title('t=' + str(t))
            plt.plot(x_ethanol, 'bo--', label='x_Ethanol')
            plt.plot(x_co2, 'ko--', label='x_CO2')
            daten['x_Ethanol'] = x_ethanol
            daten['x_CO2'] = x_co2
            daten['x_Wasser'] = x_w
            daten.to_excel(excel_writer=writer, sheet_name='Zeit = ' + str(t))
            plt.legend()
            plt.savefig(fig_path + '//x_t=%04d.png' % t)
            plt.close()

            plt.figure()  # leere Figur
            plt.axis([0.0, n, 0, 1])
            plt.xlabel('n')
            plt.ylabel('Massenanteil')
            plt.title('t=' + str(t))
            plt.plot(y_co2, 'ko--', label='y_CO2')
            plt.plot(x_w, 'ro--', label='x_Wasser')
            plt.legend()
            plt.savefig(fig_path + '//Rest_t=%04d.png' % t)
            plt.close()
            data = [y_ethanol[n-1+1], y_co2[n-1+1], x_ethanol[0], x_w[0], y_ethanol[0], y_co2[0], x_ethanol[n-1+1], x_w[n-1+1], pv]


        #HETS-Wert bestimmen
        z_co2_final = np.ones(n+1)
        z_e_final = (masse_c * y_ethanol + masse_d * x_ethanol) / (masse_c + masse_d)
        z_w_final = (masse_c * y_wasser + masse_d * x_w) / (masse_c + masse_d)
        z_co2_final = z_co2_final - z_e_final - z_w_final
        Ke = np.ones(n)
        Kw = np.ones(n)
        Kco2 = np.ones(n)
        for l in range(1, n):  # 1....n-1
            # Verteilungskoeff bestimmen

            Ke[l], Kco2[l], Kw[l] = MLA_distributioncoeff_ethanol.make_distributioncoeff_predictions_hp(T=T, p=p, x_co2=
            z_co2_final[l], x_w=z_w_final[l])
        average_Ke = Ke[1:n - 1+1].mean()
        average_Kco2 = Kco2[1:n - 1+1].mean()
        average_Kw = Kw[1:n - 1+1].mean()
        print("HETS")
        print(hets_kbs(data, h, average_Ke))
        print(hets_kbs_bel(data, h, average_Ke, average_Kco2, x_e_0))

        plt.figure()  # leere Figur
        plt.axis([0.0, n, 0, 0.2])
        plt.xlabel('n')
        plt.ylabel('K_Ethanol')
        plt.title('t=' + str(t))
        plt.plot(Ke[1:n], 'k--', label='K_Ethanol')
        plt.legend()
        plt.savefig(fig_path + '//Ke_über_n.png')
        plt.close()

        plt.figure()  # leere Figur
        plt.axis([0.0, n, 0, 0.04])
        plt.xlabel('n')
        plt.ylabel('K_Wasser')
        plt.title('t=' + str(t))
        plt.plot(Kw[1:n], 'k--', label='K_Wasser')
        plt.legend()
        plt.savefig(fig_path + '//Kw_über_n.png')
        plt.close()

        plt.figure()  # leere Figur
        plt.axis([0.0, n, 0, 15])
        plt.xlabel('n')
        plt.ylabel('K_CO2')
        plt.title('t=' + str(t))
        plt.plot(Kco2[1:n], 'k--', label='K_CO2')
        plt.legend()
        plt.savefig(fig_path + '//Kco2_über_n.png')
        plt.close()


        plt.figure()  # leere Figur
        plt.axis([0.0, n, 0, 1])
        plt.xlabel('n')
        plt.ylabel('Massenanteil')
        plt.title('t=' + str(t))
        plt.plot(y_co2, 'k--', label='y_CO2')
        plt.plot(x_w, 'r:', label='x_Wasser')
        plt.plot(x_ethanol, 'b:', label='x_Ethanol')
        plt.plot(x_co2, 'k:', label='x_CO2')
        plt.plot(y_ethanol, 'b--', label='y_Ethanol')
        plt.plot(y_wasser, 'r--', label='y_Wasser')
        plt.legend()
        plt.savefig(fig_path + '//Alle_über_n.png')
        plt.close()


        # Plot Ausganskonzentration über Zeit
        plt.figure()
        plt.xlabel('t')
        plt.ylabel('Massenanteil am Ausgang')
        plt.plot(y_e_exit[0:i, 1], y_e_exit[0:i, 0], 'b--', label='y_Ethanol')
        plt.plot(y_e_exit[0:i, 1], y_e_exit[0:i, 2], 'r--', label='y_Wasser')
        plt.plot(y_e_exit[0:i, 1], y_e_exit[0:i, 3], 'b:', label='x_Ethanol')
        plt.plot(y_e_exit[0:i, 1], y_e_exit[0:i, 4], 'k:', label='x_CO2')
        #plt.plot(y_e_exit[0:i, 1], y_e_exit[0:i, 5], 'k--', label='y_CO2')
        #plt.plot(y_e_exit[0:i, 1], y_e_exit[0:i, 6], 'r:', label='x_Wasser')

        plt.legend()
        plt.savefig(fig_path + '//Exit.png')
        plt.close()

        # Plot Ausganskonzentration über Zeit Vergleich mit Raphael
        plt.figure()
        plt.xlabel('t')
        plt.ylabel('Massenanteil CO2-frei')
        plt.plot(y_e_exit[0:i, 1], y_e_exit[0:i, 0]/(y_e_exit[0:i, 2]), 'b--', label='y_Ethanol]')
        plt.hlines(0.86, 0, y_e_exit[i, 1])
        plt.plot(y_e_exit[0:i, 1], y_e_exit[0:i, 3]/(1-y_e_exit[0:i, 6]-y_e_exit[0:i, 3]), 'b:', label='x_Ethanol')
        plt.hlines(0.05, 0, y_e_exit[i, 1])

        plt.legend()
        plt.savefig(fig_path + '//Exit_vgl_raphael.png')
        plt.close()

        # Plot Ausganskonzentration über Zeit Vergleich mit Heydrich
        """plt.figure()
        plt.xlabel('t')
        plt.ylabel('Ethanol Beladung am Ausgang')
        plt.plot(y_e_exit[0:i, 1], y_e_exit[0:i, 0]/(y_e_exit[0:i, 5]), 'b--', label='y_Ethanol [kgEthanol/kgCO2]')
        plt.hlines(0.21, 0, y_e_exit[i, 1])
        plt.plot(y_e_exit[0:i, 1], y_e_exit[0:i, 3]/(y_e_exit[0:i, 6]), 'b:', label='x_Ethanol [kgEthanol/kgWasser]')
        plt.hlines(0.062, 0, y_e_exit[i, 1])

        plt.legend()
        plt.savefig(fig_path + '//Exit_vgl_heydrich.png')
        plt.close()"""

        return data


    model = solve()

    # Excel speichern
    writer.save()
    writer.close()
    # Stoppen und Ausgeben der Zeit
    end = time.time()
    runtime = (end - start) / 60
    print("Gesamtzeit: \n%.1f Minuten " % runtime)
    return model


def diffstrom_i(A, rho_c, rho_d, k_ci, k_di, xi, yi, Ki):
    """
    Massentransferstrom für Komponente i
    :param A: Stoffaustauschfläche [m^2]
    :param rho_c: Dichte der kontinuierlichen Phase [kg/m^3]
    :param rho_d: Dichte der dispersen Phase [kg/m^3]
    :param k_ci: Massentransferkoeff konti Phase Stoff i [m/s]
    :param k_di: Massentransferkoeff disperse Phase Stoff i [m/s]
    :param xi: Massenanteil Stoff i in konti Phase
    :param yi: Massenanteil Stoff i in disp Phase
    :param Ki: massenbezogener Verteilungskoeff Stoff i
    :return: ji: Massentransferstrom für Stoff i [kg/s]
    """
    #beta = 1/(1/(rho_d*k_di) + Ki/(rho_c*k_ci))
    beta = 1 / (1 / (rho_c * k_ci) + Ki / (rho_d * k_di))
    ji = A * beta * (Ki * xi - yi)
    return ji


def diffstromco2(A, rho_c, rho_d, k_ci, k_di, xi, yi, Ki):
    """
    Massentransferstrom für Komponente co2
    :param A: Stoffaustauschfläche [m^2]
    :param rho_c: Dichte der kontinuierlichen Phase [kg/m^3]
    :param rho_d: Dichte der dispersen Phase [kg/m^3]
    :param k_ci: Massentransferkoeff konti Phase Stoff i [m/s]
    :param k_di: Massentransferkoeff disperse Phase Stoff i [m/s]
    :param xi: Massenanteil Stoff i in konti Phase
    :param yi: Massenanteil Stoff i in disp Phase
    :param Ki: massenbezogener Verteilungskoeff Stoff i
    :return: jco2: Massentransferstrom für Stoff co2 [kg/s]
    """
    beta = 1 / (1 / (rho_c * k_ci * Ki) + 1 / (rho_d * k_di))
    jco2 = A * beta * (yi/Ki - xi)
    return jco2


def reynolds(rho, eta, v, d):
    """
    Bestimmt Reynoldszahl
    :param rho: Dichte [kg/m^3]
    :param eta: Viskosität [Pas]
    :param v: Geschwindigkeit [m/s]
    :param d: charakteristische Länge [m]
    :return: Reynoldszahl [-]
    """
    return rho*v*d/eta


def schmidt(rho, eta, diff):
    """
    Bestimme Schmidtzahl
    :param rho: Dichte [kg/m^3]
    :param eta: Viskosität [Pas]
    :param diff: Diffusionskoeff [m^2/s]
    :return: Schmidtzahl [-]
    """
    return eta/(rho*diff)


def hets_kbs(data, h, m):
    """
    Nth nach Kremser-Brown-Souders-Gleichung
    :param data: array mit [y_ethanolout,y_co2out, x_ethanolout, x_wasserout, y_ethanolin,y_co2in, x_ethanolin, x_wasserin, q_c, q_d, sf, holdup]
    :param h: Kolonnenhöhe [m]
    :param m: durschnittlicher Ke-Wert
    :return: HETS-Wert
    """
    x_f = data[6]
    x_r = data[2]
    y_a = data[4]
    y_w = data[1]
    lam = data[8] * m
    if lam <1.01 and lam > 0.99:
        nth = (x_f-x_r)/(x_r-y_a/m)
    else:
        zähler = (x_f-y_a/m)/(x_r-y_a/m) * (1-1/lam) + 1/lam
        nth = np.log(zähler) / np.log(lam)
    if nth < 1:
        nth = 1
    hets = h/nth
    return hets


def hets_kbs_bel(data, h, m1, m2, x_e0):
    """
    Nth nach Kremser-Brown-Souders-Gleichung mit Beladungen (bezogen auf Wasser oder CO2 möglich)
    :param data: array mit [y_ethanolout,y_co2out, x_ethanolout, x_wasserout, y_ethanolin,y_co2in, x_ethanolin, x_wasserin, sf, holdup]
    :param h: Kolonnenhöhe [m]
    :param m: durschnittlicher Ke-Wert
    :return: HETS-Wert
    """
    # Beladung bezogen auf die CO2-Masse
    x_f = data[6]/(data[7])
    x_r = data[2]/(data[3])
    y_a = data[4]/data[5]
    y_w = data[0]/data[1]
    m = m1
    lam = data[8] * m / (1-x_e0)
    zähler = (x_f-y_a/m)/(x_r-y_a/m) * (1-1/lam) + 1/lam
    nth = np.log(zähler) / np.log(lam)
    if nth < 1:
        nth = 1
    hets = h/nth
    return hets


def scorefit():
    """
    Vergleicht Modell mit experimentellen Daten "beladung_data"
    :return: Differenz Modell experimentelle Daten als array [konti, dispers]
    """

    fit = [0.01476669, 1.90732289, 0.78412048, 0.0035, 0.023] #Mella
        #[0.01476669, 1.90732289, 0.78412048, 0.0035, 0.023] #Mella
    #[0.13854462, 0.96936199, 0.62412655, 0.0035, 0.023]  # CY
    model = np.array(main(n=35, dt=10, h=4.19, T=313, p=10, D=0.038, a_s=700, epsilon=0.86, d_h=0.004914286, cutoff=0.1, q_c=16, q_d=3,
         y_e_0=0.0, y_w_0=0.0, x_e_0=0.1, x_co2_0=0.0, fit=fit))

    beladung_model = np.array([model[0]/(1-model[1]), model[2]/(model[3]+model[2])])
    beladung_data = np.array([0.86, 0.05])
    print(beladung_model)
    return (beladung_model-beladung_data)

print(scorefit())